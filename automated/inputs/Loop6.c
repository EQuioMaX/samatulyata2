#include <stdio.h>

void main ()
{
    int a, b, c, i, j;
    
    scanf ("%d", &a);
    c = 0;
    
    for (i = 0; i < a; i++)
    {
        c += 3;
        if (i == 10)
            c++;
        else
            c--;
        c += 2;
    }
    printf ("%d", c);
}
