#include <stdio.h>

void main ()
{
    int n, guess, i, prdct, sqrt, sqrt_prev;
    
    // printf ("Number = ");
    scanf ("%d", &n);
    
    // printf ("Guess = ");
    scanf ("%d", &guess);
    
    sqrt_prev = 0;
    sqrt = 1;
    
    while (sqrt_prev != sqrt)
    {
        guess = guess - ((guess * guess) - n) / (2 * guess);
        
        sqrt_prev = sqrt;
        sqrt      = guess;
    }
    // printf ("square root = %d\n", sqrt);
    printf ("%d", sqrt);
}