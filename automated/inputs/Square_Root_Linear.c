#include <stdio.h>

void main ()
{
    int n, i, s, p;
    scanf ("%d", &n);
    s = 0;
    for (i = 0; i < n; i++)
    {
        p = i*i;
        if (p > n)
            break;
        else
            s = i;
    }
    
    if (n == 1)
        s = 1;
    
    printf ("%d", s);
}