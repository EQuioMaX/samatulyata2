#include <stdio.h>

void main()
{
    int num, max, min, i;

    // printf ("Enter four numbers: ");
    scanf ("%d", &num);
    max = min = num;

    for (i = 0; i < 3; i++)
    { 
        scanf ("%d", &num);
        if (max < num)
            max = num;
        else if (min > num)
            min = num;
    }

    // printf ("The smallest and largest of given four numbers are %d and %d respectively.\n", min,  max);
    printf ("%d", min);
    printf ("%d", max);
    
    // return 0;
}
