#include <stdio.h>

void main ()
{
    int i, j, k, n, s;
    scanf ("%d", &n);
    s = 0;
    
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            for (k = 0; k < n; k++)
            {
                s += k;
            }
        }
    }
    printf ("%d", s);
}