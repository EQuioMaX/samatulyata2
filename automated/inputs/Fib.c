#include <stdio.h>

void main ()
{
    int n, i, f1, f2, fd;
    scanf ("%d", &n);

    f1 = 1;
    f2 = 1;
    
    if (n == 0)
    {
        n = 1;
        printf ("%d", n);
    }
    
    else if (n == 1)
        printf ("%d", n);
    
    else
    {
        for (i = 2; i <= n; i++)
        {
            fd = f1 + f2;
            f1 = f2;
            f2 = fd;
        }
        printf ("%d", fd);
    }
}