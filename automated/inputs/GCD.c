#include <stdio.h>


void main ()
{
    int a, b;
    scanf ("%d", &a);
    scanf ("%d", &b);
    
    if (a < 1 || b < 1)
    {
        a = -1;
        printf ("%d", a);
    }
    else
    {
        while (a != b)
        {
            if (a > b)
                a = a - b;
            else
                b = b - a;
        }
        printf ("%d", a);
    }
}