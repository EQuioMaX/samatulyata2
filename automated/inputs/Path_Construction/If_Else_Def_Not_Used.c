#include <stdio.h>

void main ()
{
    /* Simple if-else example: a is defined but not used. 
     * Path Constructor:
     *    WORKING, only if extra output places are not constructed. 
     *    NOT WORKING, otherwise. */ 
    int a, b, c, d;
    scanf ("%d", &c);
    a = 0;
    b = 0;
    if (c > 0)
        a = c + 10;
    else
        a = c - 10;
    
    d = a + b;
    
    printf ("%d", d);
}
