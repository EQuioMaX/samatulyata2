#include <stdio.h>

void main ()
{
    /* Simple if-else example: No transition defines a variable which 
     *   is not used eventually. 
     * Path Construtor: WORKING. */ 
    int a, b, c, d;
    scanf ("%d", &c);
    a = b = 0;
    if (c > 0)
        a = c + 10;
    else
        b = c - 10;
    
    d = a + b;
    
    printf ("%d", d);
}
