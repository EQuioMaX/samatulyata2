#include <stdio.h>

void main ()
{
    int n, i, p;
    scanf ("%d", &n);
    p = 1; // Number is prime.
    for (i = 2; i < n; i++)
    {
        if (n % i == 0)
        {
            p = 0;
            break;
        }
    }
    if (n < 2)
        p = 0;

    printf ("%d", p);
}