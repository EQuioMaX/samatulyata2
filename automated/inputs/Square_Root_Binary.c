#include <stdio.h>

void main ()
{
    int n, l, h, i, p, s;
    scanf ("%d", &n);
    
    i = 2;
    while (i < n)
    {
        i = 2 * i;
    }

    l = 2;
    s = 1;
    h = i;
    while (l < h)
    {
        // printf ("l = %d, h = %d\n", l, h);
        i = l + (h - l) / 2;
        p = i * i;
        if (p == n)
        {
            s = i;
            break;
        }
        else if (p < n)
        {
            s = i;
            l = i+1;
        }
        else
        {
            // p > n.
            h = i;
        }
    }
    
    if (n < 2)
        s = 0;
    
    printf ("%d", s);
}