#include <stdio.h>

void main ()
{
    int n, i, j, p;
    scanf ("%d", &n);
    for (i = 2; i <= n; i++)
    {
        // For each integer i in the range [2, n].
        if (n % i == 0)
        {
            // i divides n. 
            p = 1; // Initialize i to be a prime number.
            for (j = 2; j < i; j++)
            {
                // For each integer in the range [2, i-1]
                if (i % j == 0)
                {
                    // j divides i.
                    p = 0; // i is not a prime number
                    break;
                }
            }
            if (p == 1)
            {
                // i is a prime number -- print it.
                printf ("%d", i);
            }
        }
    }
}