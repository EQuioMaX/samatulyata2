#include <stdio.h>

//Goes on adding the sum of digits until a single digit result is found
//Eg: 12345 --> 1+2+3+4+5 = 15 --> 1+5 = 6
void main()
{
    int n, sum;

    // printf("\nInput number:");
    scanf("%d",&n);

    while(n > 9)
    {
        sum = 0;

        while(n > 0)
        {
            sum += n%10;
            n = n/10;
        }

        n = sum;
    }

    printf("\n%d\n",n);
    
    
}
