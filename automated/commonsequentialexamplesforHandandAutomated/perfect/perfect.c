#include <stdio.h>

//6 is a perfect number. 
//6 = 1 + 2 + 3 (equal to sum of its divisors)
// Prints 1 if n is perfect; 0 otherwise.
void main()
{
  int sum = 1, i = 2, n, out;
  
  // printf("\nInput Number:");
  scanf("%d",&n);
  
  while( i < n )
  {
      if( n % i == 0 )
        sum = sum + i;
        
      i = i + 1;
  }

  if( sum == n )
  {
    out = 1;
    printf("\n%d is a perfect number\n",out);
  }
  else 
  {
    out = 0;
    printf("\n%d is not a perfect number\n",out);
  }

}
