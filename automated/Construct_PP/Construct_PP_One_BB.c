#include "Types.h"




/** Input:
 *  pp: The overall PRES+ net. The PRES+ subnet for the basic block bI
 *      is stored in it.
 *  pp_obb: (subnets) Pointer to an array s.t. bI element contains indices of the 
 *          places and transitions in pp.places and pp.transitions array
 *          corresponding to the subnet of the basic block bI.
 *  bI: Index of the basic block whose PRES+ subnet shall be constructed.
 *  bba: Pointer to the array of basic blocks.
 *  st: Pointer to the Symbol Table.
 *  Output:
 *     Constructs the PRES+ subnet for the basic block bI.
 *     The PRES+ subnet is stored in the overall PRES+ pointed by pp.
 *     The indices of the places and the transitions constructed for 
 *   the subnet are stored in the pp_obb array at the bI-th index.
 *  
 *  ALGORITHM:
 *    Invoke a function to construct the PRES+ subnet for each statement.
 *    The function not just constructs the subnet for the given statement 
 *  but also attaches the subnet corresponding to the statement to the 
 *  subnet of the containing basic block.
 * */
void construct_PP_One_BB (Pres_Plus *pp, PP_One_BB *pp_obb, 
                            BB_Array *bba, int bI, Symbol_Table *st)
{
    /******************* FUNCTION DECLARATIONS *********************/
    void construct_PP_One_Stm (Pres_Plus *pp, PP_One_BB *pp_obb, 
                        BB_Array *bba, int bI, int sI, Symbol_Table *st);

    // Initialize the PRES+ for a BB to default values.
    void initialize_PP_One_BB (PP_One_BB *pp_obb, int bI);

    // Prints PRES+ coressponding to specified BB to file PRINT_PP_BB.
    void print_PP_BB_Wise (Pres_Plus *pp, PP_One_BB *pp_obb, int bI, 
                            Symbol_Table *st, char *output_file);
    /***************************************************************/
    initialize_PP_One_BB (pp_obb, bI);
    
    int sI; // Index of a statement.
    for (sI = 0; sI < bba->BB[bI].total_Stmnts; sI++)
    {
        construct_PP_One_Stm (pp, pp_obb, bba, bI, sI, st);
    }
    
    #ifdef PRINT_PP_BB
        print_PP_BB_Wise (pp, pp_obb, bI, st, PRINT_PP_BB);
    #endif
}



/** Input:
 *      pp_obb: pointer array in which i-th element contains information
 *           from PRES+ corresponding to i-th basic block.
 *      bI:  index of the basic block for which the PRES+ shall be 
 *           constructed.
 *  Output:
 *      Return nothing.
 *  Description:
 *      Initialize the PRES+ for bI to default values.
 * */
void initialize_PP_One_BB (PP_One_BB *pp_obb, int bI)
{
    pp_obb[bI].c_in  = 0;
    pp_obb[bI].c_out = 0;
    pp_obb[bI].c_PL  = 0;
    pp_obb[bI].c_TR  = 0;
    
    pp_obb[bI].max_trans = -1;
    
    
    int i;
    // Initialize Latest defining transition for each var to -1.
    for (i = 0; i < MAX_VARIABLES; i++)
        pp_obb[bI].ldt[i] = -1;
    
}





