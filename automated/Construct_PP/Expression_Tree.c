#include "Types.h"


/*Observation:
 *  if (a) is changed to if (a != 0) by the -fdump-tree-cfg.
 * */
 
 
 
 
/** Input:
 *      bba: pointer to the basic blocks array.
 *      bI:  index of the current basic block.
 *      sI:  index of a statement.
 *  Output:
 *      returns the index of the first operand on RHS.
 *      Terminates the program with error message if not fails.
 * */
int get_First_Operand (BB_Array *bba, int bI, int sI)
{
    int vI;
    Expr *e = bba->BB[bI].stmnt[sI].expr;
    
    if (e == NULL)
    {
        printf ("get_First_Operand ():\n");
        printf ("Error in expression tree of statement %d of BB %d.\n",
                                                            sI+1, bI+2);
        printf ("Expression tree is NULL.\nFatal, Exiting.\n\n");
        exit (1);
    }
    
    switch (e->eType)
    {
        case e_BINARY:
        case e_UNARY:
            e = e->left;
            if (e == NULL)
            {
                printf ("get_First_Operand ():\n");
                printf ("Error in expression tree of statement %d of BB %d.\n",
                                                                    sI+1, bI+2);
                printf ("Expression tree is has NULL left child.\n");
                printf ("Fatal Error, Exiting.\n\n");
                exit (1);
            }
            break;
        case e_VAR:
        case e_LITERAL:
            break; // Do nothing.
        default:
            printf ("get_First_Operand ():\n");
            printf ("Error in expression tree of statement %d of BB %d.\n",
                                                                sI+1, bI+2);
            printf ("Invalid type of expression tree.\n");
            printf ("Fatal Error, Exiting.\n\n");
            exit (1);
    }
    
    /*Expr *left = bba->BB[bI].stmnt[sI].expr->left;
    if (left == NULL)
    {
        printf ("get_Left_Operand.\n");
        printf ("Error in expression tree of statement %d of BB %d.\n",
                                                            sI, bI);
        printf ("Left pointer is NULL.\nFatal, Exiting.\n");
        exit (1);
    }
    
    switch (left->eType)
    {
        case e_LITERAL:
            return -1;
        case e_VAR:
            break; // Continue for code after switch.
        default:
            printf ("get_Left_Operand.\n");
            printf ("BB = %d, Statement = %d.\n", bI, sI);
            printf ("The left operand node is nither VAR nor LITERAL.\n");
            printf ("Exception, Continuing by assuming LITERAL.\n");
            return -1;
    }
    
    rI = left->var_Index; 
    
    if (vI < 0 || vI >= MAX_VARIABLES)
    {
        printf ("get_First_Operand ():\n");
        printf ("Error in expression tree of statement %d of BB %d.\n",
                                                            sI, bI);
        printf ("Variable is out of domain.\nFatal, Exiting.\n\n");
        exit (1);
    }
    * */
    
    if (e->eType == e_VAR)
    {
            return e->val_var;
    }
    return -1; 

    // vI = e->val_var; 
    
    // return vI;
}


/** Inputs:
 *      root: An expression from which we want to get the first 
 *            operand.
 *  Outputs:
 *      Index in the symbol table: if the operand is a variable.
 *      -1: If there is no operand.
 *  Functionality:
 *    If root->left is not NULL then,
 *       returns the index of the variable stored in that node if node
 *         type is VAR.
 *       -1, otherwise.
 * */
int get_First_Operand_Expr (Expr *root)
{
    Expr *e = root;
    if (e == NULL)
    {
        return -1; // Returning without printing the error.

        /*/ Unreachable code for backup.
        printf ("get_First_Operand_Expr ():\n");
        printf ("Invoked for NULL expression tree.\n");
        printf ("Exception, Continuing by returning -1.\n\n");
        return -1; */
    }
    
    switch (root->eType)
    {
        case e_BINARY:
        case e_UNARY:
            e = e->left;

            if (e == NULL)
            {
                return -1; // Returning instead of printing error.
                
                /*/ Unreachable code for backup.
                printf ("get_First_Operand_Expr ():\n");
                printf ("Left child of a Binary/Unary expression tree is NULL.\n");
                printf ("Fatal Error, Exiting.\n\n");
                exit (1);*/
            }
            break;
        case e_VAR:
            break; // Root itself is the first operand.
            
        case e_LITERAL:
            return -1; // Because first operand is not variable.
            // break; // Do nothing.
        default:
            printf ("get_First_Operand ():\n");
            printf ("Error in expression tree of statement.\n");
            printf ("Invalid type of expression tree.\n");
            printf ("Fatal Error, Exiting.\n\n");
            exit (1);
    }

    
    if (e->eType == e_VAR)
    {
            return e->val_var;
    }
    return -1; 
}


/** Input:
 *      bba: pointer to the basic blocks array.
 *      bI:  index of the current basic block.
 *      sI:  index of a statement.
 *  Output:
 *      returns the index of the second operand on RHS.
 *      Terminates the program with error message if fails.
 * */
int get_Second_Operand (BB_Array *bba, int bI, int sI)
{

    int vI;
    Expr *e = bba->BB[bI].stmnt[sI].expr;
    
    if (e == NULL)
    {
        printf ("get_Second_Operand ():\n");
        printf ("Error in expression tree of statement %d of BB %d.\n",
                                                            sI+1, bI+2);
        printf ("Expression tree is NULL.\nFatal, Exiting.\n\n");
        exit (1);
    }
    
    switch (e->eType)
    {
        case e_BINARY:
            e = e->right;
            if (e == NULL)
            {
                printf ("get_Second_Operand ():\n");
                printf ("Error in expression tree of statement %d of BB %d.\n",
                                                                    sI+1, bI+2);
                printf ("Expression tree is has NULL right child.\n");
                printf ("Fatal Error, Exiting.\n\n");
                exit (1);
            }
            break;
        case e_UNARY:
        case e_VAR:
        case e_LITERAL:
            printf ("get_Second_Operand ():\n");
            printf ("Statement %d of BB %d.\n", sI+1, bI+2);
            printf ("Second operand requested for a statement which doesn't ");
            printf ("contain a binary operation.\n");
            printf ("Exception, Continuing by returning -1.\n\n");
            return -1;
            break; // Do nothing.
        default:
            printf ("get_Second_Operand ():\n");
            printf ("Error in expression tree of statement %d of BB %d.\n",
                                                                sI+1, bI+2);
            printf ("Invalid type of expression tree.\n");
            printf ("Fatal Error, Exiting.\n\n");
            exit (1);
    }
    
    if (e->eType == e_VAR)
    {
            return e->val_var;
    }
    return -1; 

    // vI = e->val_var; 
    
    // return vI;

    /* Old Code.
    int i;
    Expr *right = bba->BB[bI].stmnt[sI].expr->right;
    if (right == NULL)
    {
        printf ("get_Second_Operand.\n");
        printf ("Error in expression tree of statement %d of BB %d.\n",
                                                            sI, bI);
        printf ("Right pointer is NULL.\nFatal, Exiting.\n");
        exit (1);
    }
    
    switch (right->eType)
    {
        case e_LITERAL:
            return -1;
        case e_VAR:
            break; // Continue for code after switch.
        default:
            printf ("get_Second_Operand.\n");
            printf ("BB = %d, Statement = %d.\n", bI, sI);
            printf ("The right operand node is nither VAR nor LITERAL.\n");
            printf ("Exception, Continuing by assuming LITERAL.\n");
            return -1;
    }

    i = right->var_Index;
    if (i < 0 || i >= MAX_VARIABLES)
    {
        printf ("get_Second_Operand.\n");
        printf ("Error in expression tree of statement %d of BB %d.\n",
                                                            sI, bI);
        printf ("Variable is invalid.\nFatal, Exiting.\n");
        exit (1);
    }
    return i; */
}


/** Inputs:
 *      root: An expression from which we want to get the second 
 *            operand.
 *  Outputs:
 *      Index in the symbol table: if the operand is a variable.
 *      -1: If there is no operand.
 *  Functionality:
 *    If root->right is not NULL then,
 *       returns the index of the variable stored in that node if node
 *         type is VAR.
 *       -1, otherwise.
 * */
int get_Second_Operand_Expr (Expr *root)
{
    Expr *e = root;
    if (e == NULL)
    {
        return -1; // Returning instead of printing error.

        /*/ Unreachable code for backup.
        printf ("get_Second_Operand_Expr ():\n");
        printf ("Invoked for NULL expression tree.\n");
        printf ("Exception, Continuing by returning -1.\n\n");
        return -1; */
    }
    
    switch (root->eType)
    {
        case e_BINARY:
            e = e->right;
            if (e == NULL)
            {
                return -1; // Returning instead of printing error.

                /* Unreachable code for backup.
                printf ("get_Second_Operand_Expr ():\n");
                printf ("Right child of a Binary expression tree is NULL.\n");
                printf ("Fatal Error, Exiting.\n\n");
                exit (1); */
            }
            break;
        case e_UNARY:
        case e_VAR:
        case e_LITERAL:
            return -1; // Returning instead of printing error.

            /*/ Unreachable code for backup.
            printf ("get_Second_Operand ():\n");
            printf ("Second operand requested for a statement which doesn't ");
            printf ("contain a binary operation.\n");
            printf ("Exception, Continuing by returning -1.\n\n");
            return -1;
            break; // Do nothing.*/
        default:
            printf ("get_Second_Operand_Expr ():\n");
            printf ("Error in expression tree of statement.\n");
            printf ("Invalid type of expression tree.\n");
            printf ("Fatal Error, Exiting.\n\n");
            exit (1);
    }

    if (e->eType == e_VAR)
    {
            return e->val_var;
    }
    return -1;
}




/* Input: 
 *    src: pointer to the root node of an expression tree.
 * Returns:
 *    A copy of the expression tree, only operator is negated.
 * 
 * Observation:
 *  if (a) is changed to if (a != 0) by the -fdump-tree-cfg.
 * */
Expr *negate_Expression (Expr *e)
{
    /******************* FUNCTION DECLARATIONS *********************/
    // Returns pointer to a duplicate of the Expr tree pointed by src.
    Expr *duplicate_Expression_Tree (Expr *src);
    
    // Negates the operator op.
    Oprtr negate_Operator (Oprtr op);
    /***************************************************************/
    if (e == NULL)
    {
        printf ("negate_Expression ():\n");
        printf ("Input expression is NULL. Fatal error, exiting.\n");
        exit (1);
    }
    
    Oprtr op;
    Expr *new_Expr;
    switch (e->eType)
    {
        case e_BINARY:
        case e_UNARY:
            op = negate_Operator (e->oprtr);
            new_Expr = duplicate_Expression_Tree (e);
            new_Expr->oprtr = op;
            break;
        default:
            printf ("negate_Expression ():\n");
            printf ("The function is invoked for an expression which doesn't");
            printf (" contain any operator. \n");
            printf ("Fatal error, exiting.\n\n");
            exit (1);
    }
    return new_Expr;
}


/* Input: 
 *    src: pointer to the root node of an expression tree.
 * Returns:
 *      Allocates the memory for an expression tree identical to the
 *  input, and copies the contents.
 *      Returns the pointer to root of the new duplicate expression 
 *  tree.
 * */
Expr *duplicate_Expression_Tree (Expr *src)
{
    if (src == NULL)
    {
        printf ("duplicate_Expression_Tree ():\n");
        printf ("Input expression tree is NULL. Fatal error, exiting.\n\n");
        exit (1);
    }

    Expr *dest = malloc (sizeof (Expr));
    if (dest == NULL)
    {
        printf ("duplicate_Expression_Tree ():\n");
        perror ("While allocating memory for a node: ");
        printf ("Fatal error, exiting.\n\n");
        exit (1);
    }
    
    Expr *t = memcpy (dest, src, sizeof (Expr));
    if (t == NULL)
    {
        printf ("duplicate_Expression_Tree ():\n");
        perror ("While copying a node: ");
        printf ("Fatal error, exiting.\n\n");
        exit (1);
    }
    
    if (src->left != NULL)
        dest->left = duplicate_Expression_Tree (src->left);
    
    if (src->right != NULL)
        dest->right = duplicate_Expression_Tree (src->right);
    
    return dest;
}


/* Input:
 *     op: an operator to be negated.
 * Returns:
 *      A negation of the operator op.
 * */
Oprtr negate_Operator (Oprtr op)
{
    switch (op)
    {
        case op_PLUS:          // binary "+"
        case op_BINARY_MINUS:      // binary "-"
        case op_MULTIPLY:           // "*"
        case op_DIVIDE:             // "/"
        case op_MODULUS:            // %
            return op_ERROR;
    
        case op_GRT_THN_OR_EQUAL:    // >=
            return op_LESS_THN;
        case op_GRT_THN:             // >
            return op_LESS_THN_OR_EQUAL;
        case op_LESS_THN_OR_EQUAL:   // <=
            return op_GRT_THN;
        case op_LESS_THN:            // <
            return op_GRT_THN_OR_EQUAL;
        case op_DOUBLE_EQUAL:       // ==
            return op_NOT_EQUAL;
        case op_NOT_EQUAL:          // !=
            return op_DOUBLE_EQUAL;
    
        case op_SINGLE_EQUAL:       // =
            return op_ERROR;

        case op_BITWISE_OR:         // |
            return op_BITWISE_AND;
        case op_BITWISE_AND:        // &
            return op_BITWISE_OR;

        case op_NOT:                // ~
            return op_UNKNOWN_OPERATOR;
        case op_LEFT_SHIFT:         // <<
        case op_RIGHT_SHIFT:        // >>
            return op_ERROR;
    
        case op_LOGICAL_OR:         // ||
            return op_LOGICAL_AND;
        case op_LOGICAL_AND:         // &&
            return op_LOGICAL_OR;
        
        case op_IR_RELEVANT:
        case op_UNKNOWN_OPERATOR:
            return op_ERROR;
        
        default:
            printf ("negate_Operator: ()");
            printf ("Operand is of not recognized, error.\n");
            printf ("Fatal Error. Exiting.\n\n");
            exit (1);
    }
}



/********* CONVERSION TO STRING *************************************/

/** Input:
 *      A variable of type enum:
 *  Output:
 *      The string for this operator.
 * */
char *get_Operator_As_String (Oprtr op)
{
    switch (op)
    {
        case op_PLUS:          // binary "+"
            return "+";
        case op_BINARY_MINUS:      // binary "-"
            return "-";
        case op_MULTIPLY:           // "*"
            return "*";
        case op_DIVIDE:             // "/"
            return "/";
        case op_MODULUS:            // %
            return "%";
    
        case op_GRT_THN_OR_EQUAL:    // >=
            return ">=";
        case op_GRT_THN:             // >
            return ">";
        case op_LESS_THN_OR_EQUAL:   // <=
            return "<=";
        case op_LESS_THN:            // <
            return "<";
        case op_SINGLE_EQUAL:       // =
            return "=";
        case op_DOUBLE_EQUAL:       // ==
            return "==";
        case op_NOT_EQUAL:          // !=
            return "!=";
    
        case op_BITWISE_OR:         // |
            return "|";
        case op_BITWISE_AND:        // &
            return "&";
        case op_NOT:                // ~
            return "~";
        case op_LEFT_SHIFT:         // <<
            return "<<";
        case op_RIGHT_SHIFT:        // >>
            return ">>";
    
        case op_LOGICAL_OR:         // ||
            return "||";
        case op_LOGICAL_AND:         // &&
            return "&&";
        
        case op_IR_RELEVANT:
            return "";
        
        default:
            printf ("get_Operator_As_String: ");
            printf ("Operand if of no recognized type, error.\n");
            return "ERROR";
    }
    return "ERROR";
}


/** Input:
 *      index: index of the variable, whose string value is required in
 *             symbol table.
 *      st:    pointer to the symbol table.
 *  Output:
 *      returns the name of the variable at index, "index" in the
 *      symbol table.
 * */
char *get_Var_As_String (int index, Symbol_Table *st)
{
    return st->symbol[index].vName;
}


/** Input:
 *      root: pointer to the root of an expression tree.
 *      st:   pointer to the symbol table.
 *  Output:
 *      returns the expression represented by the input expression 
 *      tree as a string. 
 *    This function assumes that the expression tree is of form:
 *       root
 *       /  \
 *     Left  Right(may or may not exist)
 * 
 **** Thus this function will return WRONG results if ****************
 *     1. Left child to root doesn't exit, 
 *  OR
 *     2. The Left child to root also has a left child.
 *  */
char *get_Expr_As_String (Expr *root, Symbol_Table *st)
{
    /******************* FUNCTION DECLARATIONS *********************/
    // Returns string for the variable at index "index" in symbol table.
    char *get_Var_As_String (int index, Symbol_Table *st);

    // Returns the operator op as a string.
    char *get_Operator_As_String (Oprtr op);
    /***************************************************************/
    if (root == NULL)
        return "";
        
    // Memory for the string.
    static char str[MAX_EXPR_SIZE > 64 ? MAX_EXPR_SIZE : 64]; 
    static Bool flag = True;
    
    /*/ The left child.
    Expr *left  = root->left;
    if (left != NULL)
    {
        str[0] = '\0';
        get_Expr_As_String (left, st); // str is returned.
    } */
    char *oprtr, *var, val[10];
    
    switch (root->eType)
    {
        case e_BINARY: // Root node can have either of these two types.
            str[0] = '\0';
            flag = False;

            get_Expr_As_String (root->left, st);
            
            oprtr = get_Operator_As_String (root->oprtr);
            if ((strlen (oprtr) + strlen (str) +2) >= MAX_EXPR_SIZE)
            {
                sprintf (str, "Expression Too Long.");
            }
            else
            {
                strcat (str, " ");
                strcat (str, oprtr);
                strcat (str, " ");
            }

            get_Expr_As_String (root->right, st);
            
            flag = True;
            break;
        case e_UNARY:
            str[0] = '\0';
            flag = False;

            oprtr = get_Operator_As_String (root->oprtr);
            if ((strlen (oprtr) + strlen (str) +2) >= MAX_EXPR_SIZE)
            {
                sprintf (str, "Expression Too Long.");
            }
            else
            {
                strcat (str, " ");
                strcat (str, oprtr);
                strcat (str, " ");
            }

            get_Expr_As_String (root->left, st);

            flag = True;
            break;
        case e_LITERAL:
            if (flag)
                str[0] = '\0';

            sprintf (val, "%d", root->val_var); // root->value is int.
            strcat  (str, val);
            break;
        case e_VAR:
            if (flag)
                str[0] = '\0';

            var = NULL;
            var = get_Var_As_String (root->val_var, st);
            if ((strlen (var) + strlen (str) + 1) >= MAX_EXPR_SIZE)
            {
                sprintf (str, "Expression Too Long.");
            }
            else
            {
                strcat (str, var);
            }
            /*if (root->negation == True)
            {
                var = get_Var_As_String (root->var_Index, st);

                if ((strlen (var) + strlen (str) + 1) >= MAX_EXPR_SIZE)
                {
                    sprintf (str, "Expression Too Long.");
                }
                else
                {
                    strcat (str, "-");
                    strcat (str, var);
                }
            }
            else ** Because the notion of negation has been removed. 
            {
                var = get_Var_As_String (root->var_Index, st);
                if ((strlen (var) + strlen (str) + 1) >= MAX_EXPR_SIZE)
                {
                    sprintf (str, "Expression Too Long.");
                }
                else
                {
                    strcat (str, var);
                }
            }*/
            break;
        default:
            printf ("get_Expr_As_String: () \n");
            printf ("An expression tree node doesn't have type information.\n");
            printf ("Exception, ignoring and continuing.\n\n");
    }

    /*Expr *right = root->right;
    if (right != NULL)
        get_Expr_As_String (right, st);*/
    return str;
}

