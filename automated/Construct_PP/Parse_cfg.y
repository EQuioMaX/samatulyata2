 
%{
#include "Types.h"


// Used by the parser.
extern  yy_buffer_state;
typedef struct yy_buffer_state *YY_BUFFER_STATE;


void   yyerror (char *s); // Declaration of the error function.
extern FILE *yyin;
FILE   *fw;  // Output on debugging.
    
/************ GLOBAL VARIABLES *************************************/
BB_Array      *g_BBA; // Storing pointer to the basic blocks array.
Symbol_Table  *g_ST;  // Storing pointer to the Symbol table.
int            g_cur_bb; /* Index of the basic block currently being read
                    * from the input file.
                    * Can be argued that not required but made some tasks 
                    * easier. */
extern int     g_line_count; // For printing errors.     
/************ FUNCTION DECLARARIONS ********************************/

/* Called on begining of a BB, To set the Type and successor of the
 * previous BB. */
extern void switch_To_Next_BB (BB_Array *bba, int bI);

/* Called on a variable declaration. Adds the variable to the 
 * symbol table.  */
void add_Var_To_Symbol_Table (char *name, Symbol_Table *st);

/* Sets the current statement type. */
void set_Statement_Type (BB_Array *bba, Stmnt_Type s);

/* Adds a new empty statement to the current basic block. */
void add_Statement (BB_Array *bba);

/* Adds various attributes of an assignment statement to the current
 * BB. */
void add_Assignment_Statement (BB_Array *bba, char *dVar, Expr *root,
                                Symbol_Table *st);

/* Sets the condition in current BB to expression tree of the 
 * condition (root). */
void add_Condition (BB_Array *bba, Expr *root);

/* Adds successor to the current bb. */
void add_Successor (BB_Array *bba, int succ);

/* Adds the variable to the current statement. Called for Scanf and 
 * Printf statements. */
void add_Var (BB_Array *bba, char *var, Symbol_Table *st);

/* Constructs node of the expression tree for a variable.*/
Expr *construct_Var_Node (char *var, Symbol_Table *st);

/* Constructs a node of expression tree for an int. */
Expr *construct_Int_Node (int val);

/* Constructs a node of expression tree for an operator.*/
Expr *construct_Oprtr_Node (char *oprtr);

/* Sets the current BB as the last BB. */
void set_Last_BB (BB_Array *bba);

/* Unlike name of the function, sets that current statement defines the variable var.*/
void set_As_Defining_Statement (BB_Array *bba, char *var, Symbol_Table *st);

%}


/************ TOKENS ***********************************************/
%token MAIN   
%token INT_DT INT_LIT /* INT_DT  --> Integer Data type, i.e. "int"
                       * INT_LIT --> An integer, e.g. 5.
                       */

%token VAR              // A variable
%token SCANF  PRINTF    // For the printf and the scanf statements.
%token IF     ELSE      // For the if and else statements.
%token GOTO             // For goto statement
%token OPRTR            // For the operator in an expression.


/* Begining of a basic block, BBS, Basic Block Statement.
 * BBS_ST = "<bb "
 * BBS_END1 ">:"
 * BBS_END2   ">;"
 * Example:   
 * <bb 2>:
 *      Statements;
 *      goto <bb 3>;
 */
%token BBS_ST    // "<bb "
%token BBS_END1  // ">:" 
%token BBS_END2  // ">[.]*;" modfied to assit labeled statements. 


// Opening and closing Braces, i.e. { & }
%token OP_BR  // "{" 
%token CL_BR  // "}"


// Opening and closing Parantheses, ( & )
%token OP_PR  //"("  
%token CL_PR  //")"
                         
%token EQUAL   // "="
%token COMMA   // ","       
%token NBSP    // "&"
%token SM_CLN  // ";"       
%token COLON   // ":"
%token RETURN  // For the return statement.

/****** Tokens End ************************************************/


%start begin
%union {
    char  *name;
    int   int_val;
    
    struct Expr *expr;
};

/******** TOKEN AND SYMBOL TYPES ***********************************/
%type  <name>     VAR     // A variable has type string.
%type  <name>     OPRTR   // An operator has type string.
%type  <int_val>  INT_LIT // An integer has type int.

%type  <expr>     expr    // Expressions has type expr, a pointer.
%type  <expr>     var_lit

// %type  <expr>     expr2
// %type  <expr>     expr3

%%
/*******************************************************************/


/******** GRAMMAR **************************************************/

begin:   MAIN    main  { 
                #ifdef DEBUG_PARSER
                    fprintf (fw, "begin:   MAIN    main.\n");
                #endif
            }  
         ;

main:    OP_BR    functn   CL_BR  { 
                #ifdef DEBUG_PARSER
                    fprintf (fw, "main:    OP_BR    functn   CL_BR.\n");
                #endif
                // set_Last_BB (g_BBA); // can be moved to return statement.
            }   
         ;

functn:    functn  BBS_ST  INT_LIT  BBS_END1  {
                #ifdef DEBUG_PARSER
                    fprintf (fw, "functn:    functn  BBS_ST  INT_LIT=|%d|    BBS_END1.\n", $3);
                #endif
                /* New basic block begins, and $3 contains the index of 
                 * this basic block as an integer.
                 *   If the true successor of bb [$3 - 1] == -1, 
                 *        set true successor of bb [$3 - 1] = $3.
                 *   Pointer to the basic block array is available 
                 * in g_bba.
                 * */
                switch_To_Next_BB (g_BBA, $3 - 2);
                 g_cur_bb++;
            }  
        |  functn VAR COLON {
                #ifdef DEBUG_PARSER
                    fprintf (fw, "functn:    functn  VAR  COLON BB = %d.\n", g_cur_bb+3);
                #endif
                switch_To_Next_BB (g_BBA, ++g_cur_bb);
                /* It also marks the beginning of a new basic block. 
                 *   The index of this basic block is 1 plus that of the 
                 *   previous BB.
                 */
            }
        |  functn  INT_DT  VAR  SM_CLN  { 
                #ifdef DEBUG_PARSER
                    fprintf (fw, "functn:    functn  INT_DT  VAR=|%s|    SM_CLN.\n", $3);
                #endif
                /* A variable declaration.
                 * Add this variable to the Symbol table.
                 * Pointer to the symbol table is available in g_st.
                 *
                 * To make sure that variable declarations occur only in
                 * beginning of a funtion, Add
                 * functn_Decls -->   functn_Decls <this rule>
                 *                  | functn ;
                 * */
                 add_Var_To_Symbol_Table ($3, g_ST);
            } 
        |  functn  bb  {
                /* Goto a basic block inside this function. */
                #ifdef DEBUG_PARSER
                    fprintf (fw, "functn:    functn     bb.\n");
                #endif
            } 
        |  /* Empty Rule */  {
                #ifdef DEBUG_PARSER
                    fprintf (fw, "functn:    /* Empty Rule */.\n");
                #endif
            } 
        ;

bb:    stmnt   { /* This Rule can be removed, by replacing
            * "functn bb" with "functn stmnt" in the above rule.
            * This rule is here just to SHOW, not impose, the hirearchy 
            * that a function contains a basic block and a basic block
            * contains statements. */
                #ifdef DEBUG_PARSER
                    fprintf (fw, "bb:     stmnt.\n");
                #endif
            } 
    ;

stmnt:   SCANF    COMMA    NBSP   VAR   CL_PR    SM_CLN   {
                #ifdef DEBUG_PARSER
                    fprintf (fw, "stmnt:   SCANF    var   CL_PR    SM_CLN.\n");
                #endif
                /* A Scanf statement. 
                 * Set statement type as SCANF STATEMENT.
                 * The read variable is stored in the statement.
                 * It is assumed that a scanf statement reads atmost one
                 * value.
                 * */
                add_Var (g_BBA, $4, g_ST);
                set_Statement_Type (g_BBA, s_SCANF);
                set_As_Defining_Statement (g_BBA, $4, g_ST);
                add_Statement (g_BBA);
                
            } 
      |  PRINTF   COMMA   VAR   CL_PR    SM_CLN   {
                #ifdef DEBUG_PARSER
                    fprintf (fw, "stmnt:   PRINTF   var   CL_PR    SM_CLN.\n");
                #endif
                /* A Printf statement. 
                 * Set statement type as PRINTF STATEMENT.
                 * It is assumed that a printf statement prints atmost
                 * one variable.
                 * */
                add_Var (g_BBA, $3, g_ST);
                set_Statement_Type (g_BBA, s_PRINTF);
                add_Statement (g_BBA);
                
            } 
      |  VAR      EQUAL      expr     SM_CLN   {
                #ifdef DEBUG_PARSER
                    fprintf (fw, "stmnt:   VAR=|%s|      EQUAL      expr     SM_CLN.\n", $1);
                #endif
                /* An assignment statement.
                 * Set statement type as ASSIGNMENT STATEMENT.
                 * Set the Defined Variable as $1. 
                 * Operands will be handled in the expr.
                 * $3 contains the expression tree corresponding to the
                 * RHS.
                 * */
                add_Assignment_Statement (g_BBA, $1, $3, g_ST);
                set_As_Defining_Statement (g_BBA, $1, g_ST);
                add_Statement (g_BBA);
            } 
      |  IF       OP_PR      expr     CL_PR    {
                #ifdef DEBUG_PARSER
                    fprintf (fw, "stmnt:   IF       OP_PR      expr     CL_PR.\n");
                #endif
                /* An if statement:
                 * The condition will be evaluated and stored in expr.
                 * expr returns the expression tree, in $3
                 * bba is available in g_bba, and cond in $3
                 */
                add_Condition (g_BBA, $3);
            } 
      |  ELSE   {
                #ifdef DEBUG_PARSER
                    fprintf (fw, "stmnt:   ELSE.\n");
                #endif
                /* Else statement:
                 * Do nothing. */
            } 
      |  GOTO     BBS_ST     INT_LIT  BBS_END2  {
                #ifdef DEBUG_PARSER
                    fprintf (fw, "stmnt:   GOTO     BBS_ST     INT_LIT=|%d|    BBS_END2.\n", $3);
                #endif
                /* A Jump:
                 * Set true successor = $3 if ture succ == -1.
                 * else set false successor = $3.
                 * Pointer to the basic block array is available in
                 * g_bba.
                 */
                add_Successor (g_BBA, $3 - 2);
            } 
      |  RETURN   SM_CLN      {
                #ifdef DEBUG_PARSER
                    fprintf (fw, "stmnt:   RETURN   SM_CLN.\n");
                #endif
                set_Statement_Type (g_BBA, s_RETURN);
                set_Last_BB (g_BBA);
            } 
      ;  

expr:       var_lit     OPRTR   var_lit {
                #ifdef DEBUG_PARSER
                    fprintf (fw, "expr: var_lit     OPRTR=|%s|     var_lit.\n", $2);
                #endif
                /* Construct Root node of expression tree, saying 
                 * that expression represents a BINARY expression. 
                 * Set $$ = The node (root).
                 * root -> right = $3.
                 * */
                Expr *node  = construct_Oprtr_Node ($2);
                node->eType = e_BINARY;
                node->left  = $1;
                node->right = $3;

                // It is a binary expression.
                set_Statement_Type (g_BBA, s_ASGNMT_BI);

                $$          = node;
            }

      |     var_lit {
                #ifdef DEBUG_PARSER
                    fprintf (fw, "expr: var_lit.\n");
                #endif
                
                if ($1->eType == e_VAR)
                {
                    // A statement with identity expression (var) on RHS.
                    set_Statement_Type (g_BBA, s_ASGNMT_ID);
                }
                else if ($1->eType == e_LITERAL)
                {
                    // A statement with unary expression on RHS.
                    set_Statement_Type (g_BBA, s_ASGNMT_UN);
                }
                else
                {
                    printf ("From grammar: A statmenet having exactly one var/literal on rhs");
                    printf (" classifies nither as an ID statement nor as an UNARY statement.\n");
                    printf ("Fatal Error: Exiting.\n\n");
                    exit (1);
                }
                $$ = $1; // Return the root itself.
            }
      
      |     OPRTR   var_lit {
                #ifdef DEBUG_PARSER
                    fprintf (fw, "expr: OPRTR=|%s|     var_lit.\n", $1);
                #endif
                Expr *node  = construct_Oprtr_Node ($1);
                node->eType = e_UNARY;
                node->left  = $2;
                node->right = NULL;

                // A statement with unary expression on RHS.
                set_Statement_Type (g_BBA, s_ASGNMT_UN);

                $$          = node;
            }
      
      ;
     
var_lit:    VAR     {
                #ifdef DEBUG_PARSER
                    fprintf (fw, "var_lit:    VAR=|%s|.\n", $1);
                #endif
                /* Construct a node of type Expr, containing infromation
                 * about this variable. 
                 * Set $$ = This newly constructed node.
                 * */
                Expr *node = construct_Var_Node ($1, g_ST);
                $$ = node;
            }

      |     INT_LIT {
                #ifdef DEBUG_PARSER
                    fprintf (fw, "var_lit:    INT_LIT=|%d|.\n", $1);
                #endif
                /* Construct a node of type Expr, containing infromation
                 * about this Literal. 
                 * Set $$ = This newly constructed node.
                 * */
                Expr *node = construct_Int_Node ($1);
                $$ = node;
            }
      
      ;

%%

/********************************************************************/

/** Input:
 *   inFile: Name of the input file to be parsed.
 *   bba:    pointer to the basic block array.
 *   st:     pointer to the symbol table.
 *  Output:
 *   returns nothing.
 *   This function parses the given file, and stores the
 *  information reterieved from this statement in the basic blocks 
 *  array and the symbol table.
 */
void invoke_Parser (char *inFile, BB_Array *bba, Symbol_Table *st)
{
    // open the input file.
    yyin = fopen (inFile, "r");
    if (yyin == NULL)
    {
        printf ("invoke_Parser:\n");
        perror (inFile);
        printf ("Fatal Error, Exiting.\n");
        exit (1);
    }
     
    // An output file.   
    #ifdef DEBUG_PARSER
       fw = fopen (DEBUG_PARSER, "w+");
       if (fw == NULL)
       {
           printf ("Parser: Couldn't open file for writing debug info\n");
           perror (DEBUG_PARSER);    
       }
    #endif

    /* The grammar will invoke functions which take bba and st as
     * arguments.
     * Thus set the g_BBA and g_ST as the pointers to these objects,
     * so that the grammar actions can use them to invoke functions.
     * */
    g_BBA = bba;
    g_ST  = st;
    g_cur_bb = -1;
    g_line_count = 0;
    
    // File opened successfully, invoke parser.
    yyparse ();
    fclose (yyin);
    
    #ifdef DEBUG_PARSER
         fclose (fw);
    #endif 
}


/** For printing the error message. */
void yyerror (char *e)
{
    printf ("\nLine = %d, Error: %s\nExiting..\n\n\n", g_line_count, e);
    exit (1);
}
   

/*
int test_Grammar (int argc, char *argv[])
{
    #if YYDEBUG == 1
        extern int yydebug;
        yydebug = 1;
    #endif

    if (argc == 2)
    {
        yyin = fopen (argv [1], "r");
        if (yyin == NULL)
            perror (argv [1]); 
    }
    
    fw = fopen ("Parsing_Out.txt", "w+");
    if (fw == NULL)
        perror ("output file");
            
    yyparse ();
    
    fclose (yyin);
    fclose (fw);
    return 0;    
}*/







