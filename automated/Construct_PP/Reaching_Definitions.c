#include "Types.h"

/* 1.  The dragon book assumes: A statement can define atmost one 
 *  variable, but that is not true for scanf statements. 
 *    Thus the appoach using Bit vectors for reaching definitions won't
 *  work. 
 *    Using Bit vectors provide really efficient UNION and INTERSECTION
 *  operations.
 * 
 * 3.  Implementation is:
 *   We have a kill set for each basic block.
 *   Hence the implementation is 
 *   kill_Gen_Array [bI] --> kill gen set of basic block bI, It has 
 *      to be an array.
 *   kill_Gen_Array [bI][vI] --> Latest definition of variable vI is 
 *      in gen, and the second latest definition is in kill.
 * */


/** Input:
 *      bba:  pointer to the basic blocks array.
 *      st:   pointer to the symbol table.
 *  Output:
 *      returns nothing.
 *      sets the Reaching Definitions at beginning (Din) and End (Dout)
 *    of each BB.
 * DESCRIPTION *******************************************************
 * 1. Compute GEN and KILL sets of each basic block.
 * 2. Invoke the Reaching Definitions Algorithm to compute the Din and
 *    Dout of each basic block.
 * */
void reaching_Definitions (BB_Array *bba, Symbol_Table *st)
{
    /******************* FUNCTION DECLARATIONS *********************/
    // Sets the gen and kill sets of the basic block bb.
    void get_Gen_Kill_One_BB (BB_Array *bba, Gen_Kill *gk_BB, int bI, 
                                Symbol_Table *st);

    // Runs the Reaching definitions Algorithm.
    void reaching_Definitions_Algo (BB_Array *bba, Gen_Kill *gk);
 
    // Prints the gen and kill sets of all BB.
    void print_Gen_Kill_All_BB (char *file, BB_Array *bba, Gen_Kill *gk);
    /***************************************************************/
    Gen_Kill gk [MAX_BBs]; /* A gen and kill set for each BB. */
    int i;

    #ifdef O_GEN_KILL_ALL_STMNT
        FILE *fp = fopen (O_GEN_KILL_ALL_STMNT, "w+");
        if (fp == NULL) {
            printf ("reaching_Definitions:\n");
            perror (O_GEN_KILL_ALL_STMNT);
        }
        else {
            fprintf (fp, "\nGEN and KILL of each statement.\n\n");
            fclose (fp);
        }
    #endif

    /*** GEN and KILL of all BBs ************************************/
    // For each Basic block.
    for (i = 0; i < bba->total_BBs; i++) {
        get_Gen_Kill_One_BB (bba, gk, i, st);
    }
    
    #ifdef O_GEN_KILL_ALL_BB
        print_Gen_Kill_All_BB (O_GEN_KILL_ALL_BB, bba, gk);
    #endif

    /*** Din and Dout of all BBs ************************************/
    reaching_Definitions_Algo (bba, gk); /* Computes the reaching 
        * definitions bit vectors at beginning and end of each BB. */
}


/** Input:
 *      bba: Pointer to the basic blocks array.
 *      gk:  Gen and Kill sets array, each element contains the 
 *           gen and kill set of the respecitve BB.
 *  Output:
 *      Returns nothing.
 *      Evaluates and sets the Reaching definitions, Din and Dout,
 *    of each BB.
 *      Din and Dout are two fields in each BB. 
 *      Din and Dout are Reaching Definitions at beginning and end of 
 *    each BB.
 *  ALGORITHM:
 *  1. For each basic block, say B:
 *  2.    B.Din = B.Dout = EMPTY;
 *  3. change = true
 *  4. while (change == true)
 *  5.    change = false.
 *  6.    For each basic blcok, say B:
 *  7.        B.Din  <-- Union of Douts of all parents (predecessors)
 *                       of B.
 *  8.        B.Dout <-- Gen[B] U (B.Din - Kill[B])
 *  9.    if any of the Douts changed
 *  10.       change = true 
 * */
void reaching_Definitions_Algo (BB_Array *bba, Gen_Kill *gk)
{
    /******************* FUNCTION DECLARATIONS *********************/
    // Stores u - v, in bit vector d.
    extern void bit_Vector_Substraction (Bit_Vector d, Bit_Vector u, 
                                                    Bit_Vector v);

    // Stores u U v in Bit vector d.
    extern void bit_Vector_Union (Bit_Vector d, Bit_Vector u, 
                                                    Bit_Vector v);
 
    // Initializes the bit vector to zeros.
    extern void initialize_Bit_Vector (Bit_Vector bv);
    
    // Retruns True if u and v are different, False otherwise.
    extern Bool are_Bit_Vectors_Different (Bit_Vector u, Bit_Vector v);
    
    // copies the contents of bit vector s to d.
    extern void copy_Bit_Vector (Bit_Vector d, Bit_Vector s);

    // Prints the Din and Douts of each BB to file specified in "file".
    void print_Din_Dout_All_BB_To_File (char *file, BB_Array *bba);
    /***************************************************************/
    int i;
    // Initialization.
    for (i = 0; i < bba->total_BBs; i++)
    {    
        initialize_Bit_Vector (bba->BB[i].Din);
        initialize_Bit_Vector (bba->BB[i].Dout);
    }
    
    #ifdef O_REACHING_DEFs
        FILE *fp = fopen (O_REACHING_DEFs, "w+");
        if (fp == NULL)
        {
            printf ("reaching_Definitions_Algo:\n");
            perror (O_REACHING_DEFs);
        }
        else
        {
            fprintf (fp, "\nThis file contains the output of the Reaching");
            fprintf (fp, " over various iterations of the algorithm.\n");
            fprintf (fp, "1. - is printed in place of 0 for clarity.\n");
            fprintf (fp, "2. $ is printed at the end of statements in a");
            fprintf (fp, " basic block.\n\n");
            fclose (fp);
        }
    #endif
    
    int b, p;           // for iteration.
    Bool change;        // for detecting change in some Dout.
    int  tP;            // for accessing all parents of a bb.

    Bit_Vector temp;     // to store temporary bit vectors.
    Bit_Vector old_Dout; // A copy of the Dout before modification.
    
    change = True;
    while (change == True)
    {
        change = False; //Last itr if "change" is not re-assigned True.
        // For each Basic Block.
        for (b = 0; b < bba->total_BBs; b++)
        {
            /*** Din *************************************************/
            initialize_Bit_Vector (temp);
            // For each predecessor (Parent).
            for (p = 0; p < bba->BB[b].parent_Count; p++)
            {
                tP = bba->BB[b].parent[p];
                bit_Vector_Union (temp, temp, bba->BB[tP].Dout);
            }
            copy_Bit_Vector (bba->BB[b].Din, temp);
            
            /*** Dout ************************************************/
            initialize_Bit_Vector (temp);
            
            // 1. temp <= Din[b] - KILL[b]
            bit_Vector_Substraction (temp, 
                                        bba->BB[b].Din, gk[b].kill);
            
            // 2. Dout[b] <= GEN [b] U temp.
            copy_Bit_Vector (old_Dout, bba->BB[b].Dout);
            bit_Vector_Union (bba->BB[b].Dout, gk[b].gen, temp);
            
            /*** change **********************************************/
            // change <= True if old_Dout and Dout[b] are different.
            if (change == False)
            {
                // Alter the value of change only if it is false.
                change = are_Bit_Vectors_Different (old_Dout, 
                                                    bba->BB[b].Dout);
            }
        }
        #ifdef O_REACHING_DEFs
            print_Din_Dout_All_BB_To_File (O_REACHING_DEFs, bba);
        #endif
    }
}


/** Input:
 *      bb: pointer to the basic block, to which we need to initialize
 *          gen and kill sets.
 *      gk_BB: A stuct containing two bit vectors, one for the kill and 
 *    other for the gen set of this BB.
 *      bI: index of the basic block, bb, in the basic blocks array.
 *      st: pointer to the symbol table.
 *  Output:
 *      Sets the kill and gen set of this basic block in the give 
 *    array kg..
 *
 * ALGORITHM ******************************************************
 * ***** Kill[i], Gen[i] is the kill and Gen sets of i-th statements.
 * ***** KILL_BB and GEN_BB are kill and Gen sets of BB.
 * 
 * KILL set of a basic block:
 * KILL_BB = Kill[n] U Kill[n-1] U ... U Kill[1].
 * 
 * HENCE, 
 *  1. KILL_BB is computed using a single loop.
 *  2. The contents of the kill array of statements can be modified,
 *     for storing sets necessary for computing GEN_BB, after   
 *     computation of KILL_BB.
 * 
 * GEN set of the basic block:
 * GEN_BB = Gen[n] 
 *          U (Gen[n-1] - Kill[n]) 
 *          U ... 
 *          U (Gen[i] - (Kill[i+1] U Kill[i+2] U ... U Kill[n]))
 *          U ... 
 *          U (Gen[1] - (Kill[2] U ... U Kill[n])).
 * 
 * The i-th step:
 *  1. Uses the value of (Kill[i+1] U ... U Kill[n]).
 *  2. The value will be computed and stored in kill[i+1].
 * 
 * Computing the value of Kill[i] <= Kill[i] U Kill[i+1] U ... U Kill[n]. 
 * Kill[n] <= Unaltered.
 * For n-1 down to 0:
 *    kill[i] <=  kill[i] U kill[i+1]  
 * endFor.
 * *******
 *
 * Therefore, the formula for calculating Gen_BB is now:
 * Gen_BB <= Gen [n] U (Gen[n-1] - Kill[n])  
 *                   U (Gen[n-2] - Kill[n-1]) 
 *                   U ... 
 *                   U (Gen[1] - Kill[2])
 * This value will be coumputed using a for loop.
 * ****************************************************************
 * */
void get_Gen_Kill_One_BB (BB_Array *bba, Gen_Kill *gk_BB, int bI, 
                            Symbol_Table *st)
{
    /******************* FUNCTION DECLARATIONS *********************/
    // Sets the gen and kill sets of the Statement st.
    void get_Gen_Kill_One_Statement (BB_Array *bba, Gen_Kill *gk, int bI, 
                                    int sI, Symbol_Table *st);

    // Stores u - v, in bit vector d.
    void bit_Vector_Substraction (Bit_Vector d, Bit_Vector u, 
                                    Bit_Vector v);

    // Stores u U v in Bit vector d.
    void bit_Vector_Union (Bit_Vector d, Bit_Vector u, Bit_Vector v);
 
    // Initializes the bit vector to zeros.
    void initialize_Bit_Vector (Bit_Vector bv);

    // copies the contents of bit vector s to d.
    extern void copy_Bit_Vector (Bit_Vector d, Bit_Vector s);

    // Print the Gen and Kill sets of each statement.
    void print_Gen_Kill_All_Statement (char *file, BB_Array *bba, int bI, 
                                        Gen_Kill *gk, Symbol_Table *st);
    /***************************************************************/
    // Initialize the bit vectors.
    initialize_Bit_Vector (gk_BB[bI].gen);
    initialize_Bit_Vector (gk_BB[bI].kill);
    
    if (bba->BB[bI].total_Stmnts == 0)
        return;
    
    Gen_Kill gk_ST[MAX_BB_STMNTS]; /* An element contains gen and kill
        * set, for each statement in the BB. */
    int i; // For iteration.

    /**** GEN and KILL set of each statement. ***********************/
    for (i = 0; i < bba->BB[bI].total_Stmnts; i++) {
        get_Gen_Kill_One_Statement (bba, gk_ST, bI, i, st);
    }    
    
    #ifdef O_GEN_KILL_ALL_STMNT
        print_Gen_Kill_All_Statement (O_GEN_KILL_ALL_STMNT, 
                                        bba, bI, gk_ST, st);
    #endif
    
    /**** KILL set of the BB. ***************************************/
    // Construct the kill for the basic block bI.
    initialize_Bit_Vector (gk_BB[bI].kill);
    for (i = 0; i < bba->BB[bI].total_Stmnts; i++) {
        bit_Vector_Union (gk_BB[bI].kill, gk_BB[bI].kill, gk_ST[i].kill);
    }
    
    /**** GEN set of BB. ********************************************/
    // Computing Kill[i] <= Kill[i+1] U ... U Kill[n].
    for (i = bba->BB[bI].total_Stmnts - 2; i >= 0; i--) {
        bit_Vector_Union (gk_ST[i].kill, 
                            gk_ST[i].kill, gk_ST[i + 1].kill);
    }
    
    // GEN_BB <= Gen[n].
    copy_Bit_Vector (gk_BB[bI].gen, 
                            gk_ST [bba->BB[bI].total_Stmnts - 1].gen);
    
    /* bv <= Gen[i] - Kill[i+1].
     * GEN_BB <= GEN_BB U bv. */
    Bit_Vector bv; // An intermediate bit vector.
    for (i = 0; i < bba->BB[bI].total_Stmnts - 1; i++)
    {
        bit_Vector_Substraction (bv, gk_ST[i].gen, gk_ST[i + 1].kill);
        
        bit_Vector_Union (gk_BB[bI].gen, gk_BB[bI].gen, bv);
    }
}


/** Input:
 *     bba: pointer to the basic block array.
 *      gk: pointer to the array containing gen and kill of statements.
 *      bI: Index of the basic block to which the statement st belongs.
 *      sI: Index of the statement, stmnt in the basic block, bI.
 *      st: pointer to the symbol table.
 *  Output:
 *      Sets the kill and gen set of this statement in the give 
 *    array kg..
 *  Description:
 *  1. Get the map of the bI, sI pair as an integer, say uI.
 *  2. In the gen set, set gen[uI] as 1, rest as 0.
 *  3. Set the kill set as copy of the def (definition) set in the 
 *     symbol table for the variable defined in this statement, except
 *     make kill[mapI] as 0.
 * */
void get_Gen_Kill_One_Statement (BB_Array *bba, Gen_Kill *gk, int bI, 
                                    int sI, Symbol_Table *st)
{
    /******************* FUNCTION DECLARATIONS *********************/
    // Pairs the indices u and v to one integer.
    int pair_Indices (int u, int v);
    
    // Sets the bit at index i, in vector to 1.
    void set_Bit (Bit_Vector vector, int i);
    
    // Sets the bit at index i, in vector to 0.
    void reset_Bit (Bit_Vector vector, int i);

    // Initializes the bit vector to zeros.
    void initialize_Bit_Vector (Bit_Vector bv);
    
    // copies the contents of bit vector s to d.
    extern void copy_Bit_Vector (Bit_Vector d, Bit_Vector s);
    /***************************************************************/
    // Initialize the bit vectors.
    initialize_Bit_Vector (gk[sI].gen);
    initialize_Bit_Vector (gk[sI].kill);

    int vI; // index of variable defined in the statement sI.
    int uI; // Unique index for bI and sI
    
    switch (bba->BB[bI].stmnt[sI].sType)
    {
        case s_ASGNMT_BI:
        case s_ASGNMT_ID:
        case s_ASGNMT_UN:
        case s_SCANF:
            // This statement definies a variable.
            uI = pair_Indices (bI, sI);
            set_Bit (gk[sI].gen, uI); // Sets the uI-th bit.
            
            // Kills all definitions of defined var, except this one.
            vI = bba->BB[bI].stmnt[sI].var; // Associated var.
            copy_Bit_Vector (gk[sI].kill, st->symbol[vI].def);
            reset_Bit (gk[sI].kill, uI); // Sets the uI-th bit.
            break;

        case s_PRINTF:
        case s_RETURN:
            // Do nothing.
            break;
            
        default:
            printf ("get_Gen_Kill_One_Statement");
            printf ("BB = %d, Statement = %d Type is invalid.\n", 
                                                        bI + 2, sI + 1);
            printf ("Fatal error. Exiting.\n");
            exit (1);
    }
}


/********* OUTPUT **************************************************/

/** Input:
 *      file: name of file to which output shall be printed as a string.
 *      bba: pointer to the Basic block array.
 *  Output:
 *      prints din and douts of all bbs to file specified in 
 *    O_GEN_KILL_ALL_BB.
 * */
void print_Din_Dout_All_BB_To_File (char *file, BB_Array *bba)
{
    /******************* FUNCTION DECLARATIONS *********************/
    // Returns the string represting bit vector bv.
    extern char *bit_Vector_To_String_BB (Bit_Vector bv, BB_Array *bba);
    /***************************************************************/
    static int iteration_Count = 1;
    
    FILE *fp = fopen (file, "a+");
    if (fp == NULL)
    {
        printf ("print_Din_Dout_All_BB_To_File:\n");
        perror (file);
        printf ("Returning without printing to the file.\n");
        return;
    }
    
    fprintf (fp, "\n----------------\n");
    fprintf (fp, "Iteration = %d \n", iteration_Count++);
    fprintf (fp, "----------------\n\n");
    
    int b;
    for (b = 0; b < bba->total_BBs; b++)
    {
        fprintf (fp, "BB = %d\n", b + 2);
        fprintf (fp, "Din: %s\n", bit_Vector_To_String_BB 
                                            (bba->BB[b].Din, bba));
        fprintf (fp, "Dout: %s\n\n", bit_Vector_To_String_BB
                                            (bba->BB[b].Dout, bba));
    }

    fprintf (fp, "\n*************************************************");
    fprintf (fp, "*************************************************\n");
    fprintf (fp, "\n*************************************************");
    fprintf (fp, "*************************************************\n");
    fprintf (fp, "\n\n");

    fclose (fp);
}


/** Input:
 *      file: name of file to which output shall be printed as a string.
 *      bba: pointer to the Basic block array.
 *      bI: Index of the current basic block.
 *      gk: pointer to the array where i-th element contains the gen 
 *    and kill sets of the i-th statement.
 *  Output:
 *      prints din and douts of all bbs to file specified in 
 *    O_GEN_KILL_ALL_STMNT.
 * */
void print_Gen_Kill_All_Statement (char *file, BB_Array *bba, int bI, 
                                        Gen_Kill *gk, Symbol_Table *st)
{
    /******************* FUNCTION DECLARATIONS *********************/
    // Returns the string represting bit vector bv.
    extern char *bit_Vector_To_String_BB (Bit_Vector bv, BB_Array *bba);
    /***************************************************************/
    FILE *fp = fopen (file, "a+");
    if (fp == NULL)
    {
        printf ("print_Gen_Kill_Each_Statement:\n");
        perror (file);
        printf ("Returning without printing to the file.\n");
        return;
    }

    fprintf (fp, "\n*************************************************");
    fprintf (fp, "*************************************************\n");
    fprintf (fp, "BB = %d\n", bI + 2);
    fprintf (fp, "*************************************************");
    fprintf (fp, "*************************************************\n");
    fprintf (fp, "\n\n");

    int i, vI;
    for (i = 0; i < bba->BB[bI].total_Stmnts; i++)
    {
        fprintf (fp, "Statment = %d\n", i + 1);
        fprintf (fp, "Gen: %s\n", bit_Vector_To_String_BB 
                                            (gk[i].gen, bba));
        fprintf (fp, "Kill: %s\n\n", bit_Vector_To_String_BB
                                            (gk[i].kill, bba));
    
        switch (bba->BB[bI].stmnt[i].sType)
        {
            case s_ASGNMT_BI:
            case s_ASGNMT_ID:
            case s_ASGNMT_UN:
            case s_SCANF:
                vI = bba->BB[bI].stmnt[i].var;
                fprintf (fp, "Var: %s\n\n", bit_Vector_To_String_BB
                                       (st->symbol[vI].def, bba));
                break;
            case s_PRINTF:
            case s_RETURN:
                fprintf (fp, "Var: It should be all 0s\n\n");
        }
        fprintf (fp, "*************************************************");
        fprintf (fp, "*************************************************\n\n");
    }
    fclose (fp);
}


/** Input:
 *      file: name of file to which output shall be printed as a string.
 *      bba: pointer to the Basic block array.
 *      gk: pointer to the array where i-th element contains the gen 
 *    and kill sets of the i-th basic block.
 *  Output:
 *      prints din and douts of all bbs to file specified in 
 *    O_GEN_KILL_ALL_BB.
 * */
void print_Gen_Kill_All_BB (char *file, BB_Array *bba, Gen_Kill *gk)
{
    /******************* FUNCTION DECLARATIONS *********************/
    // Returns the string represting bit vector bv.
    extern char *bit_Vector_To_String_BB (Bit_Vector bv, BB_Array *bba);
    /***************************************************************/
    static int iteration_Count = 1;
    
    FILE *fp = fopen (file, "w+");
    if (fp == NULL)
    {
        printf ("print_Gen_Kill_All_BB:\n");
        perror (file);
        printf ("Returning without printing to the file.\n");
        return;
    }
    
    fprintf (fp, "\nGEN and KILL sets of each basic block:\n\n");
    
    int b;
    for (b = 0; b < bba->total_BBs; b++)
    {
        fprintf (fp, "BB = %d\n", b + 2);
        fprintf (fp, "Gen: %s\n", bit_Vector_To_String_BB 
                                            (gk[b].gen, bba));
        fprintf (fp, "Kill: %s\n\n", bit_Vector_To_String_BB
                                            (gk[b].kill, bba));
    }

    fprintf (fp, "\n*************************************************");
    fprintf (fp, "*************************************************\n");
    fprintf (fp, "\n*************************************************");
    fprintf (fp, "*************************************************\n");
    fprintf (fp, "\n\n");

    fclose (fp);
}






