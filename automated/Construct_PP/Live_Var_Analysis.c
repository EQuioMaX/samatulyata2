#include "Types.h"

/*   This file contains implementation of the dragon book algorithm for
 * for live variable analysis.
 * 
 * Definition of a live variable:
 *      In live-variable analysis we wish to know for variable x and 
 *   point p whether the value of x at p could be used along some path 
 *   in the flow graph starting at p. If so, we say x is live at p; 
 *   otherwise, x is dead at p.
 * 
 *   The implementation will return two bit vectors for each basic block.
 *   The first bit vector, live_in, tells us if a variable is live at 
 *  beginning of a basic block or not. live_in[i] is 1, if variable with
 *  index i in the Symbol Table is live at beginning of the associated 
 *  basic block.
 *   Similar is live_out bit vector. It provides similar information at
 *  end of a basic block.
 * */

/* Functions:
 * 1. live_Var_Analysis ().
 * 2. live_Var_Analysis_Algo ().
 * 3. live_Var_One_BB ().
 * 4. live_Var_One_Statement ().
 * 5. print_Live_Vars_Per_Statement ().
 * */


/** Input:
 *      bba: pointer to the basic blocks array.
 *      st:  pointer to the symbol table.
 *  Output:
 *      returns nothing.
 *      Sets the variables live at beginning and end of each basic block.
 *  DESCRIPTION:
 * Two definitions first:
 * 1. def-B the set of variables defined (i.e. definitely assigned 
 *           values) in B prior to any use of that variable in B.
 * 2. use-B the set of variables whose values may be used in B prior to 
 *          any definition of the variable.
 * 
 * This function does:
 * 1. Invokes a routine to set use and def sets for each basic block.
 * 2. Invokes a routine which implements the live variable analysis 
 *    algorithm.
 * */
void live_Var_Analysis (BB_Array *bba, Symbol_Table *st)
{
    /******************* FUNCTION DECLARATIONS *********************/
    // Constructs the def and use sets for a basic block.
    void get_Def_Use_Sets_One_BB (BB_Array *bba, Def_Use *du_BB, int bI);

    // Implements the Live variable analysis algo.
    void live_Var_Analysis_Algo (BB_Array *bba, Def_Use *du);

    // Prints the Def and Use sets used for live variable analysis.
    void print_Def_Use_sets_live_var_analyasis (BB_Array *bba, 
                            Symbol_Table *st, Def_Use *du, char *file);
    /***************************************************************/

    int i; // for iteration.
    Def_Use du[MAX_BBs]; // Def and use sets of each BB.
    
    // For each BB get the def and use sets.
    for (i = 0; i < bba->total_BBs; i++)
        get_Def_Use_Sets_One_BB (bba, du, i);
    
    #ifdef OUT_DEF_USE_SETs
        print_Def_Use_sets_live_var_analyasis (bba, st, du, 
                                                OUT_DEF_USE_SETs);
    #endif
    
    // Invoke the Algorithm for calcualting live vars for each BB.
    live_Var_Analysis_Algo (bba, du);
}


/** Input:
 *      bba: pointer to the basic blocks array.
 *      du:  pointer to the array where i-th element contains the
 *           def and use sets of BB-i.
 *  Output:
 *      returns nothing.
 *      Sets the live variables at beginning and end of each BB.
 *  DESCRIPTION:
 *  1. For each basic block, say B:
 *  2.    B.live_in = B.live_out = EMPTY;
 *  3. change = true
 *  4. while (change == true)
 *  5.    change = false.
 *  6.    For each basic blcok, say B:
 *  7.        B.live_out <-- Union of live_in of all sucessors.
 *  8.        B.live_in <-- use[B] U (B.live_out - def[B]).
 *  9.    if any of the live_in changed
 *  10.       change = true 
 * */
void live_Var_Analysis_Algo (BB_Array *bba, Def_Use *du)
{
    /******************* FUNCTION DECLARATIONS *********************/
    // Stores u - v, in bit vector d.
    extern void bit_Vector_Substraction (Bit_Vector d, Bit_Vector u, 
                                                    Bit_Vector v);

    // Stores u U v in Bit vector d.
    extern void bit_Vector_Union (Bit_Vector d, Bit_Vector u, 
                                                    Bit_Vector v);
 
    // Initializes the bit vector to zeros.
    extern void initialize_Bit_Vector (Bit_Vector bv);
    
    // Retruns True if u and v are different, False otherwise.
    extern Bool are_Bit_Vectors_Different (Bit_Vector u, Bit_Vector v);
    
    // copies the contents of bit vector s to d.
    extern void copy_Bit_Vector (Bit_Vector d, Bit_Vector s);
    /***************************************************************/
    // Set live_in and live_out of each BB to empty.
    int b;
    for (b = 0; b < bba->total_BBs; b++)
    {
        initialize_Bit_Vector (bba->BB[b].live_in);
        initialize_Bit_Vector (bba->BB[b].live_out);
    }
    
    int s;
    Bit_Vector temp; // For intermediate bit vectors in computations.
    Bit_Vector old_live_in;
    Bool change = True; // For iteration in the following loop.

    while (change == True)
    {
        change = False;
        // For each basic block.
        for (b = 0; b < bba->total_BBs; b++)
        {
            /* live_out computation: ********************************/
            initialize_Bit_Vector (temp); // temp <-- EMPTY
            // A basic block can have 0, 1, or 2 children.
            switch (bba->BB[b].bb_Type)
            {
                case bb_LAST: // No child, do nothing.
                    break;
                case bb_NORM: // Only true sucessor:
                    s = bba->BB[b].trueNext;
                    bit_Vector_Union (bba->BB[b].live_out, 
                             bba->BB[b].live_out, bba->BB[s].live_in);
                    break;
                case bb_COND:  // Both sucessors exist:
                    s = bba->BB[b].trueNext;
                    bit_Vector_Union (bba->BB[b].live_out, 
                             bba->BB[b].live_out, bba->BB[s].live_in);

                    s = bba->BB[b].falseNext;
                    bit_Vector_Union (bba->BB[b].live_out, 
                             bba->BB[b].live_out, bba->BB[s].live_in);
                    break;
                default: // error.
                printf ("live_Var_Analysis_Algo");
                printf ("BB = %d, Type is invalid.\n", b + 2);
                printf ("Fatal error. Exiting.\n");
                exit (1);
            }
            
            /* live in computation ***********************************/
            copy_Bit_Vector (old_live_in, bba->BB[b].live_in);
            
            bit_Vector_Substraction (temp, bba->BB[b].live_out, 
                                        du[b].def);
            bit_Vector_Union (bba->BB[b].live_in, du[b].use, temp);

            /*** change **********************************************/
            // change <= True if old_Dout and Dout[b] are different.
            if (change == False)
            {
                // Alter the value of change only if it is false.
                change = are_Bit_Vectors_Different (old_live_in, 
                                                    bba->BB[b].live_in);
            }
        }
    }
}


/** Input:
 *      bba: pointer to the basic blocks array.
 *      du_BB:  pointer to the array where i-th element contains the
 *           def and use sets of BB-i.
 *      bI:  Index of the basic block currently being considered.
 *  Output:
 *      returns nothing.
 *      Sets the def and use set of the current BB.
 *  DESCRIPTION:
 * 1. Invokes a routine to set use and def sets for each statement.
 * 2. From the def and use sets of all the statements, constructs these
 *    for the current BB.
 *  FORMULA used to calculate def-use sets for a BB from those of its
 *  statements.
 *  Use_bb <-- Use_s1  U  (Use_s2 - Def_s1) U ... U 
 *                  (Use_sN - (Def_s1 U Def_s2 U ... U Def_sN-1))
 *  Def_bb <-- (Def_s1 - Use_s1)  U  (Def_s2 - (Use_s1 U Use_s2)) U ...
 *                  U (Def_sN - (Use_s1 U Use_s2 U ... U Use_sN))
 * 
 *  Implementation of the above formula is as below:
 *  We define one more array of Def_Use, say du_union.
 *  The array elements are:
 *  du_union[i] represents the def and use sets for statement i, as they
 *              should be used in above formula.
 *  
 *  du_union[i].use <-- (Use_s1 U Use_s2 U ... U Use_si)
 *  du_union[i].def <-- (Def_s1 U Def_s2 U ... U Def_si-1)
 * 
 *  So the above formula becomes:
 *  Use_bb <-- Use_s1 U (Use_s2 - du_union[2].def) U ... U
 *                  (Use_sN - du_union[N].def)
 *  Def_bb <-- (Def_s1 - du_union[1].use) U (Def_s2 - du_union[2].use) 
 *                  U ... U (Def_sN - du_union[N].use)
 * */
void get_Def_Use_Sets_One_BB (BB_Array *bba, Def_Use *du_BB, int bI)
{
    /******************* FUNCTION DECLARATIONS *********************/
    // Constructs the def and use sets of a statement.
    void get_Def_Use_Sets_One_Statement (BB_Array *bba, Def_Use *du_ST, 
                                        int bI, int sI);

    // Initializes the bit vector to zeros.
    void initialize_Bit_Vector (Bit_Vector bv);

    // Stores u - v, in bit vector d.
    extern void bit_Vector_Substraction (Bit_Vector d, Bit_Vector u, 
                                                    Bit_Vector v);

    // Stores u U v in Bit vector d.
    extern void bit_Vector_Union (Bit_Vector d, Bit_Vector u, 
                                                    Bit_Vector v);

    /* Constructs the def-use sets for the conditional expression 
     * associated to the conditional BB bI to the du_ST at the index
     * indx. */
    void get_Def_Use_Sets_Cond_Statement (BB_Array *bba, Def_Use *du_ST, 
                                       const int bI, const int indx);
    /***************************************************************/
    // Initialize the def and use sets for the current BB.
    initialize_Bit_Vector (du_BB[bI].def);
    initialize_Bit_Vector (du_BB[bI].use);
    
    if (bba->BB[bI].total_Stmnts <= 0 && bba->BB[bI].bb_Type != bb_COND)
        return;
  
    Def_Use du_ST[MAX_BB_STMNTS+1]; /* A def-use set for each statement.
        * +1 for the conditional statement if the BB is a cond bb. */

    int i; // for iteration.
    
    // For each statement construct the def and use sets.
    for (i = 0; i < bba->BB[bI].total_Stmnts; i++)
        get_Def_Use_Sets_One_Statement (bba, du_ST, bI, i);
    
    /* If current bb is a conditional BB, then add def-use set 
     *    corresponding to it to the du_ST.
     * */
    if (bba->BB[bI].bb_Type == bb_COND)
    {
        get_Def_Use_Sets_Cond_Statement (bba, du_ST, bI, i); /* Adds the
            * def-use bit vectors to the du_ST at the index i for the
            * condition associated to the BB. */
        i++;
    }
    
    const int inclusive_stmnts = i; /* Number of statements in the current
        *   BB bI including the condition associated to the BB as a 
        *   statement. 
        * Note that the condition is not added to the statements
        *   array in a BB.
        * Only the def-use sets corresponding to the condition are
        *   added to the du_ST array. 
        * */
        
    /* For current BB constructing the def and use sets **************/
    
    // From each statement construct the union def and use sets.
    Def_Use du_union[MAX_BB_STMNTS+1]; // +1 for the condition.
    
    initialize_Bit_Vector (du_union[0].def);
    initialize_Bit_Vector (du_union[0].use);
    
    /* The logic here is quiet complex but correct. *
     * */
    
    bit_Vector_Union (du_union[0].use, du_union[0].use, du_ST[0].use);
    
    // For each statement set the values in the du_union.
    for (i = 1; i < inclusive_stmnts; i++)
    {
        bit_Vector_Union (du_union[i].use, 
                            du_union[i-1].use, du_ST[i].use);
        
        bit_Vector_Union (du_union[i].def, 
                            du_union[i-1].def, du_ST[i-1].def);
    }
    
    Bit_Vector temp;
    // From above two bit vectors, construct bit vectors for BB.
    for (i = 0; i < inclusive_stmnts; i++)
    {
        bit_Vector_Substraction (temp, du_ST[i].use, du_union[i].def);
        bit_Vector_Union (du_BB[bI].use, du_BB[bI].use, temp);
        
        bit_Vector_Substraction (temp, du_ST[i].def, du_union[i].use);
        bit_Vector_Union (du_BB[bI].def, du_BB[bI].def, temp);
    }
}


/** Inputs:
 *      bba: pointer to the basic blocks array.
 *      du_ST:  pointer to the array where i-th element contains the
 *           def and use sets of Statement i.
 *      bI:  Index of the basic block currently being considered.
 *      indx: Index to the du_ST array where the def-use sets 
 *              corresponding to the conditional statement must be
 *              added.
 *  Output:
 *      returns nothing.
 *  DESCRIPTION:
 *      Adds the def-use set corresponding to the condition associated
 *         to the basic block bI to the du_ST[indx].
 *      Since a conditional expression doesn't define any variable the
 *         the def set will be empty. [all 0's in the bit vector.].
 *      The use variable will contain at-most two variables.
 * */
void get_Def_Use_Sets_Cond_Statement (BB_Array *bba, Def_Use *du_ST, 
                                       const int bI, const int indx)
{
    /******************* FUNCTION DECLARATIONS *********************/
    // Returns the first operand in the expression tree root.
    // -1 if no first operand.
    int get_First_Operand_Expr (Expr *root);
        
    // Returns the second operand in the expression tree root.
    // -1 if no second operand.
    int get_Second_Operand_Expr (Expr *root);

    // Sets the bit at index i, in vector to 1.
    void set_Bit (Bit_Vector vector, int i);

    // Initializes the bit vector to zeros.
    void initialize_Bit_Vector (Bit_Vector bv);
    /***************************************************************/
    // Initialize the bit vectors of the current statement.
    initialize_Bit_Vector (du_ST[indx].def);
    initialize_Bit_Vector (du_ST[indx].use);
    
    if (bba->BB[bI].cond == NULL)
    {
        printf ("get_Def_Use_Sets_Cond_Statement ()\n");
        printf ("For the basic block = %d, the function is invoked ", bI+2);
        printf ("assuming the BB is a CONDITIONAL BB, but it \n");
        printf ("doesn't have any condition associated to itself.\n");
        printf ("Fatal Error: Exiting.\n\n");
        exit (1);
    }

    const int o1 = get_First_Operand_Expr (bba->BB[bI].cond); /* first
        * operand in the condition; Index of a variable in the
        * Symbol Table. */
    
    const int o2 = get_Second_Operand_Expr (bba->BB[bI].cond); /* the
        * second operand. */
    
    
    /*** o1 can be a variable or a literal. ****/
    if (o1 != -1)
        set_Bit (du_ST[indx].use, o1); /* set that o1 is used in the
            * conditional expression. */
    
    if (o2 != -1)
    {
        // printf ("Second operand Cond expr var = %d.\n", o2);
        set_Bit (du_ST[indx].use, o2); /* set that o2 is used in the
            * conditional expression. */
    }
}


/** Input:
 *      bba: pointer to the basic blocks array.
 *      du_ST:  pointer to the array where i-th element contains the
 *           def and use sets of Statement i.
 *      bI:  Index of the basic block currently being considered.
 *      sI:  Index of the statement currently being considered.
 *  Output:
 *      returns nothing.
 *      Sets the def and use set of the current statement.
 *  DESCRIPTION:
 * 1. Initializes use and def sets to empty.
 * 2. Sets the definied variable to 1 in def and used to 1 in use sets.
 * */
void get_Def_Use_Sets_One_Statement (BB_Array *bba, Def_Use *du_ST, 
                                        int bI, int sI)
{
    /******************* FUNCTION DECLARATIONS *********************/
    // Returns index of the first operand, -1 if doesn't exist.
    int get_First_Operand (BB_Array *bba, int bI, int sI);
    
    // Returns the index of the second operand, -1 if doesn't exist.
    int get_Second_Operand (BB_Array *bba, int bI, int sI);

    // Sets the bit at index i, in vector to 1.
    void set_Bit (Bit_Vector vector, int i);

    // Initializes the bit vector to zeros.
    void initialize_Bit_Vector (Bit_Vector bv);
    /***************************************************************/
    // Initialize the bit vectors of the current statement.
    initialize_Bit_Vector (du_ST[sI].def);
    initialize_Bit_Vector (du_ST[sI].use);
    
    int vI;
    // Based on the statement type, set the def and use sets.
    switch (bba->BB[bI].stmnt[sI].sType)
    {
        case s_ASGNMT_BI: 
            // Set the associated variable as definied.
            vI = bba->BB[bI].stmnt[sI].var;
            set_Bit (du_ST[sI].def, vI);
            
            // If First Operand is variable, set it as used.
            vI = get_First_Operand (bba, bI, sI);
            if (vI != -1) // operand is a variable, set it as used.
                set_Bit (du_ST[sI].use, vI);
            
            // If Second Operand is variable, set it as used.
            vI = get_Second_Operand (bba, bI, sI);
            if (vI != -1) // operand is a variable, set it as used.
                set_Bit (du_ST[sI].use, vI);
            break;
        
        case s_ASGNMT_ID:
        case s_ASGNMT_UN:
            // Set the associated variable as definied.
            vI = bba->BB[bI].stmnt[sI].var;
            set_Bit (du_ST[sI].def, vI);
            
            // If variable on RHS, set it as used.
            vI = get_First_Operand (bba, bI, sI);
            if (vI != -1) // operand is a variable, set it as used.
                set_Bit (du_ST[sI].use, vI);
            
            break;

        case s_SCANF:   // Set the associated variable as definied.
            vI = bba->BB[bI].stmnt[sI].var;
            set_Bit (du_ST[sI].def, vI);
            break;
        case s_PRINTF: // Set the associated variable as used.
            vI = bba->BB[bI].stmnt[sI].var;
            set_Bit (du_ST[sI].use, vI);
            break; 
            
        case s_RETURN:
            break;  // Do nothing.
        default:
            printf ("get_Def_Use_Sets_One_Statement");
            printf ("BB = %d, Statement = %d Type is invalid.\n", 
                                                        bI + 2, sI + 1);
            printf ("Fatal error. Exiting.\n");
            exit (1);
    }
}



/**** OUTPUT *******************************************************/





























