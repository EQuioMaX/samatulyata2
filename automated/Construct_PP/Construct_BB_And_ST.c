#include "Types.h"

/** Input:
 *  inFile: name of the input file as a string.
 *  bba, st: pointers to empty instances of the Basic Block array
 *      and the Symbol table respectively.
 *  Output:
 *      Initializes the basic block array and the symbol table instances
 *    pointed by the pointers.
 *      Returns 0 on success, a +ve integer on error.
 *  Desription:
 *      Invokes parser for the given input file.
 *      Parser constructs the basic block array and the symbol table.
 *      After construction of the basic blocks array and the symbol 
 *    invokes routine for computing the Reaching definitions at 
 *    beginning and end of each BB.
 * */
int construct_BBArray_and_ST (char *inFile, BB_Array *bba, 
                                Symbol_Table *st)
{
    /******************* FUNCTION DECLARATIONS *********************/

    // Initialize bba and st to default values.
    void initialize_BBArray_and_ST (BB_Array *bba, Symbol_Table *st);

    /* This function invokes the praser on the file name represented
     * by inFile. The parser stores the information from file to
     * basic block array and the symbol table. */
    extern void invoke_Parser (char *line, BB_Array *bba, 
                                    Symbol_Table *st);

    // Prints the symbol table to the file specified in file.
    void print_ST_To_File (Symbol_Table *st, char *file);

    /* Responsible for Reaching Definition Algo. Sets Din and Dout of 
     * each BB. */
    extern void reaching_Definitions (BB_Array *bba, Symbol_Table *st);

    // Sets the parent list for each BB.
    extern void fill_Parent_Array_DFS (BB_Array *bba, int bI);
    
    // Performs the live variable analysis.
    void live_Var_Analysis (BB_Array *bba, Symbol_Table *st);
    
    // Adds a variable to the symbol table.
    void add_Var_To_Symbol_Table (char *name, Symbol_Table *st);
    
    // Plots the cfg for dot tool.
    void plot_BB_Array (BB_Array *bba, Symbol_Table *st, FILE *file);
    
    /* Prints the statement vs varaible defined array. */
    void print_Statement_Var_Def_Array (BB_Array *bba, Symbol_Table *st,
                                    char *file);
    /***************************************************************/
    
    // Initialize bba and st to default values.
    initialize_BBArray_and_ST (bba, st);
  
  #ifdef CHECK_EQV
    // Add a variable for dummy places to the symbol table.
    add_Var_To_Symbol_Table (DUM_VAR_STR, st);
  #endif // CHECK_EQV.
  
    invoke_Parser (inFile, bba, st);
    
    fill_Parent_Array_DFS (bba, 0); // 0 for first BB.
    
    reaching_Definitions (bba, st);
    
    live_Var_Analysis (bba, st);
    
    // Print Symbol Table to file
    #ifdef PRINT_SYMBOL_TABLE
        print_ST_To_File (st, PRINT_SYMBOL_TABLE);
    #endif
    
    #ifdef PLOT_CFG
        plot_BB_Array (bba, st, PLOT_CFG);
    #endif
    
    #ifdef STM_VAR_DEF_ARRAY
        print_Statement_Var_Def_Array (bba, st, STM_VAR_DEF_ARRAY);
    #endif
    
    return 0;
    
}


/** Input: 
 *    bba: pointer to a basic block array.
 *    st:  pointer to the symbol table.
 *  Output:
 *      None, This funtion initailzes the basic block array and 
 *  the symbol table to default values.
 * */
void initialize_BBArray_and_ST (BB_Array *bba, Symbol_Table *st)
{
    /******************* FUNCTION DECLARATIONS *********************/
    
    // Initialize the bit vector bv, to all zeros.
    extern void initialize_Bit_Vector (Bit_Vector bv);

    /***************************************************************/

    /* Initializing basic block array. ************************/
    bba->total_BBs  = 0;
    
    int i;
    // For each basic block, initalize it.
    for (i = 0; i < MAX_BBs; i++)
    {
        bba->BB[i].bb_Type      = bb_ERROR;
        bba->BB[i].parent_Count = 0;
        bba->BB[i].total_Stmnts = 0;
        
        bba->BB[i].cond  = NULL;
        
        bba->BB[i].trv_state     = NOT_VISITED;
        bba->BB[i].visited       = False;
        bba->BB[i].ppConstructed = False;
        
        initialize_Bit_Vector (bba->BB[i].Din);
        initialize_Bit_Vector (bba->BB[i].Dout);

        initialize_Bit_Vector (bba->BB[i].live_in);
        initialize_Bit_Vector (bba->BB[i].live_out);
        
        bba->BB[i].trueNext  = -1;
        bba->BB[i].falseNext = -1;
    }
    
    // Initialize the statement variable definitions array to -1;
    for (i = 0; i < MAX_STMNTS; i++)
        bba->def_Var[i] = -1;
    
    /* Intializing Symbol Table. *******************************/
    st->total_Symbols = 0;
    // For each symbol initialize definition set to Empty.
    for (i = 0; i < MAX_VARIABLES; i++)
        initialize_Bit_Vector (st->symbol[i].def);
};


/** Input:
 *      bba: pointer to the basic blocks array.
 *      bI:  Index of a basic block.
 *      prI: Index of a parent OR ancestor basic block.
 *  Output:
 *      True: if prI is an immediate parent of bI.
 *      False: if prI is an ancestor of bI.
 * */
Bool is_Immediate_Parent (BB_Array *bba, int bI, int prI)
{
    int i;
    // For each parent in the parent array of bI.
    for (i = 0; i < bba->BB[bI].parent_Count; i++)
    {
        if (bba->BB[bI].parent[i] == prI)
            return True;
    }
    return False;
}



/******** DFS TO FILL PARENT ARRAY *********************************/

/** Input:
 *      bba: pointer to the basic block array.
 *      bI:  index of the first basic block. In recurrsive call it is
 *          the index of the basic block currently being processed.
 *  Output:
 *      Sets the parent array in each basic block to all the parents of 
 *     a basic block for each BB.
 * */
void fill_Parent_Array_DFS (BB_Array *bba, int bI)
{
    /******************* FUNCTION DECLARATIONS *********************/

    // Adds "parent" to the parent list of bI.
    void add_To_Parent_List (BB_Array *bba, int bI, int parent);

    /***************************************************************/
    if (bba->BB [bI].visited == True)
    {
        return; // This BB has already been visited in DFS.
    }
    else
    {
        bba->BB [bI].visited = True; // Mark bI as visited and continue.
    }
    // Based on the type of the basic block.
    switch (bba->BB [bI].bb_Type)
    {
        case bb_NORM:
            if (bba->BB [bI].trueNext == -1 || 
                    bba->BB [bI].falseNext != -1)
            {
                printf ("fill_Parent_Array_DFS:\n");
                printf ("bb_NORM: BB %d has an invalid successor, true succ = %d, false succ %d.\n", 
                            bI+2, bba->BB [bI].trueNext, 
                            bba->BB [bI].falseNext);
                printf ("Fatal Error, Exiting.\n\n");
                exit (1);
            }
            else
            {
                /*  Add bI to the parent list of true successor.
                 *  Call Dfs for true successor. */
                add_To_Parent_List (bba, bba->BB [bI].trueNext, bI);
                fill_Parent_Array_DFS (bba, bba->BB [bI].trueNext);
            }
            break;
        case bb_COND:
            if (bba->BB [bI].trueNext == -1 || 
                    bba->BB [bI].falseNext == -1)
            {
                printf ("fill_Parent_Array_DFS:\n");
                printf ("bb_COND: BB %d has an invalid successor, true succ = %d, false succ %d.\n", 
                            bI+2, bba->BB [bI].trueNext, 
                            bba->BB [bI].falseNext);
                printf ("Fatal Error, Exiting.\n\n");
                exit (1);
            }
            else
            {
                /*  Add bI to the parent list of both successors.
                 *  Call Dfs for both successors. */
                add_To_Parent_List (bba, bba->BB [bI].trueNext, bI);
                fill_Parent_Array_DFS (bba, bba->BB [bI].trueNext);

                add_To_Parent_List (bba, bba->BB [bI].falseNext, bI);
                fill_Parent_Array_DFS (bba, bba->BB [bI].falseNext);
            }
            break;
        case bb_LAST:
            if (bba->BB [bI].trueNext != -1 && 
                    bba->BB [bI].falseNext != -1)
            {
                printf ("fill_Parent_Array_DFS:\n");
                printf ("bb_LAST: BB %d has an invalid successor, %d or %d.\n", 
                            bI+2, bba->BB [bI].trueNext, 
                            bba->BB [bI].falseNext);
                printf ("Fatal Error, Exiting.\n\n");
                exit (1);
            }
            else{
                // Since it has no successor, nothing shall be added to
                // parent list.
            }
            break;

        default:
            printf ("fill_Parent_Array_DFS:\n");
            printf ("default: BB %d has invalid type.\n", bI + 2);
            printf ("Fatal Error, Exiting.\n\n");
            exit (1);
    }
}


/** Input:
 *      bba: pointer to the basic blocks array.
 *      bI:  index of the basic block to which we shall add parent.
 *      parent: index of a parent of bI.
 *  Output:
 *      Adds "parent" to the parent list of bI.
 * */
void add_To_Parent_List (BB_Array *bba, int bI, int parent)
{
    int i = bba->BB [bI].parent_Count++;
    if (i >= MAX_BBs)
    {
        printf ("add_To_Parent_List:\n");
        printf ("%d BB parent count exceeded MAX_BBs\n", bI);
        printf ("Fatal Error, Exiting.\n");
        exit (1);
    }

    bba->BB [bI].parent [i] = parent;
}





















