
How to use:

    
    
-- Converting FSMD to C
    COMPILE:    make compile_FSMDtoC
    RUN:        make run_FSMDtoC    FSMD_FILE="<FSMD file>"  C_FILE="<target .c file>"
    
    COMPILE and RUN:   make  FSMDtoC  FSMD_FILE="<FSMD file>"  C_FILE="<target .c file>"



        
-- Convert C file to .cfg file:
    USE:    make cfg    CFG_FILE="<.cfg file>"   C_FILE="<.c file>"


************************************************************************
-- Constructing Kulwant's PRES+:
    COMPILE:    make compile_construct
    RUN:        make run_construct      CFG_FILE="<.cfg file>"
    

OUTPUTS:
    The outputs are produced in PP_Outputs directory.
        Pres_plus.txt contains the constructed PRES+.
        The png support is disabled because the PRES+ nets are too big for 
            current examples.
        The Output for Python script is produced in the Debug directory 
            only.




************************************************************************
-- Generating Soumyadip's PRES+ model from given CFG file
    COMPILE: make compile_convert
    RUN:     make run_convert        CFG_FILE="<.cfg file>"
                or you can change the CFG_FILE variable in the make file.
    

OUTPUTS:
    The Outputs are:
        Pres_Plus.txt contains Kulwant's PRES+.
        PP_Conversion_Result.txt contains Soumyadip's PRES+.



************************************************************************
-- Checking Equivalence of two .cfg files.
    COMPILE: compile_equivalence
    RUN:     run_equivalence    CFG_FILE_1="<.cfg file>"  CFG_FILE_2="<.cfg file>"

    The Outputs are:
        Pres_Plus_1.txt and Pres_Plus_2.txt contains the two PRES+ nets
            in Kulwant's format.
        Equivalence_Check_Results.txt contains the two PRES+ nets in
            Soumyadip's format and the result of the equivalence algorithm.



************************************************************************
NOT WORKING: ??
    There is a bug in the gcc -fdump-tree-cfg flag.
    Sometimes it produces a cfg file in which the meta data before
      main () is not commented.
    In case of Syntax error check it.
    
    



-----------------------------------------------------------------------
Code for PRES+ completion is complete in edit_18.

In this edit:
1.  Test results will be organized.

