%{
    // Include the file
    #include "Parse_cfg.tab.h"
    #include "Types.h"
    extern YYSTYPE  yylval;
    extern char    *yytext;
    int g_line_count;
    
    // Allocates memory for a copy of inStr, and copies inStr to that.
    char *copy_Of_YYtext (char *inStr);
%}

LB   ^[ \t]*
CH   [a-zA-Z]
DI   [0-9]

%%

{LB}main[ ]*\([.]*\) {
                        //printf ("-MAIN\n");
                        return MAIN; // the main() call.
                    }

{LB}int[ ]            {
                        // Token_Integer_Data Type.
                        return INT_DT;
                    }

{LB}scanf[ ]?\(\".*\"   {
                        return SCANF;
                    }
                    
{LB}printf[ ]?\(\".*\"  {
                        return PRINTF;
                    }

{LB}if[ ]           {
                        return IF; // If statement.
                    }

{LB}else            {
                        return ELSE; // If statement.
                    }

{LB}goto[ ]         {
                        return GOTO;
                    }

{LB}return          {
                        return RETURN;
                    }

^.*\{CLOBBER\};[\n] {
                        ;// Ignore it.
                    }

[+-]?[0-9]+  		{    
                        yylval.int_val = atoi (yytext);
                        //yylval.name    = copy_Of_YYtext (yytext); 
                        /* printf("one.l: literal |%s|\n", yytext); */
                        return INT_LIT;  
                    }

{CH}[a-zA-Z0-9_\.]* {    // [+-]? removed.
                        yylval.name = copy_Of_YYtext (yytext); 
                        // printf("one.l: variable |%s|\n", yytext); 
                        return VAR; 
                    }

"<bb "              {
                        //printf ("BBS_ST\n");
                        return BBS_ST;  // next integer is a BB Index.
                    }
                    
">:"                {
                        //yylval.name = yytext;
                        return BBS_END1; // token Greater than Colon.
                        // represent end of a Basic block call.
                    }
                    
>.*;                {
                        return BBS_END2;
                    }

^;;[^\n]*[\n]               {
                       // printf ("-comment\n");
                        //;  // A comment, ignore it.
                        g_line_count++;
                    }


"+"|"-"|"*"|"/"|"%"|"=="|">="|"<="|"<"|">"|"!="|"~"	 {    
                        yylval.name = copy_Of_YYtext (yytext); 
                        /* printf("one.l: operator |%s|\n", yytext); */
                        return OPRTR; 
                    }

":"                 {
                        return COLON; // It is ":".
                    }

^[{]                {
                        return OP_BR; // It is "{".
                    }

^[}]                {
                        return CL_BR; // It is "}".
                    }

"("                 {
                        return OP_PR; // Paranthesis Open
                    }

")"                 {
                        return CL_PR; // Paranthesis Close
                    }


";"                 {    
                        //yylval.name = yytext; 
                        /* printf("one.l: semi colon |%s|\n", yytext);*/
                        return SM_CLN;
                    }

"="                 {   
                        /* printf("one.l: equal |%s|\n", yytext); */
                        return EQUAL;
                    }

"&"                 {
                        return NBSP;
                    }

","                 {
                        return COMMA;
                    }
                    
[\n]                {   
                       // printf ("-newline\n");
                        g_line_count++; /* Skip these characters. */
                    }

.                   {    ; /* Skip.   */
                    }

%%


int yywrap (void)
{
    return 1;
}


/** Input:
 *    inStr: A character string.
 *  Output:
 *    copies the input string into a dynamically allocated memory, 
 *  and returns a pointer to it.
 */
char *copy_Of_YYtext (char *inStr)
{
    if (strlen (inStr) >= MAX_VAR_LEN)
    {
        printf ("%s is too long to store. \n", inStr);
        printf ("Increase value of MAX_VAR_LEN.\n");
        printf ("Fatal error: Exiting.\n");
        exit (1);
    }
    
    char *str = malloc (MAX_VAR_LEN); // Allocate memory.
    if (str == NULL)
    {
        printf ("While allocating memory for copying yytext:\n");
        perror (inStr);
        printf ("Fatal error: Exiting.\n");
        exit (1);
    }
    
    strcpy (str, inStr);
    return str;
}
