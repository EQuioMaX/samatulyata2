#include "Types.h"

/******** PAIR / UNPAIR INDICES ************************************/

/** Input:
 *      u, v: two integers which shall be paired to get one integer
 *  Output:
 *      An integer which is a bijective mapping of u and v.
 *      From this integer u and v can be obtained by inverse mapping.
 * */
int pair_Indices(int u, int v) { return u * MAX_BB_STMNTS + v; }

/** Input:
 *      u: An integer, whose constituting ingeter shall be returned.
 *  Output:
 *      Pair, a set of two ordered integers, such that they are a
 *    bijective mapping of u in Z^2 space.
 * */
void unpair_Indices(int u, int *bI, int *sI) {
  *bI = u / MAX_BB_STMNTS;
  *sI = u % MAX_BB_STMNTS;
}

/******** BIT VECTOR OPERATIONS ************************************/

/** Input:
 *      bv: pointer to a bit vector.
 *  Initializes the bit vector bv to zero.
 * */
void initialize_Bit_Vector(Bit_Vector bv) {
  int i;
  for (i = 0; i < BV_ELEMS; i++) {
    bv[i] = 0;
  }
}

/** Input:
 *      u, v: two bit vectors, whoose AND shall be returened.
 *      d, pointer to the bit vector to which result shall be stored.
 *  Output:
 *      A bit vector such that it is AND of u and v.
 * */
void bit_Vector_And(Bit_Vector d, Bit_Vector u, Bit_Vector v) {
  int i;
  // For each element in the bit vector.
  for (i = 0; i < BV_ELEMS; i++) {
    d[i] = u[i] & v[i];
  }
}

/** Input:
 *      u, v: two bit vectors, whoose OR shall be returened.
 *      d, pointer to the bit vector to which result shall be stored.
 *  Output:
 *      A bit vector such that it is OR of u and v.
 * */
void bit_Vector_Or(Bit_Vector d, Bit_Vector u, Bit_Vector v) {
  int i;
  // For each element in the bit vector, which is array of ints.
  for (i = 0; i < BV_ELEMS; i++) {
    d[i] = u[i] | v[i];
  }
}

/** Input:
 *      vector: A bit vector.
 *      i: index of the bit in this bit vector that shall be set to 1.
 *  Output:
 *      This function alters the i-th bit of the bit vector v.
 *      This bit is changed to 1 if it is 0, and remains 1, if it is 1.
 * */
void set_Bit(Bit_Vector vector, int i) {
  int block = i / BV_ELEM_BITS;
  int offset = i % BV_ELEM_BITS;

  int r_offset = (BV_ELEM_BITS - offset) -
                 1; /* LOGIC:
                     * [0, 1, 2, ..., i, ..., n] is my set (Bit Vector).
                     * TOTAL NUMBER OF ELEMENTS = n+1 (BV_ELEM_BITS)
                     *
                     * Indexing from back:
                     * Index of i, with reverse_Index (n) = 0.
                     * reverse_Index (n-1) = 1, i.e. (n+1) - (n-1) - 1
                     * ...
                     * reverse_Index (i) = (n+1) - i - 1. */

  unsigned int z = 1; // Set the right most bit to 1.
  z = z << r_offset;  // Shift the bit to r_offset index from reverse.

  vector[block] = vector[block] | z; // Set the i-th bit.
}

/** Input:
 *      vector: A bit vector.
 *      i: index of the bit in this bit vector that shall be set to 0.
 *  Output:
 *      This function alters the i-th bit of the bit vector v.
 *      This bit is changed to 0 if it is 1, and
 *    remains 0, if it is 0.
 * */
void reset_Bit(Bit_Vector vector, int i) {
  int block = i / BV_ELEM_BITS;
  int offset = i % BV_ELEM_BITS;

  int r_offset = BV_ELEM_BITS - offset - 1; // See set_Bit ().

  unsigned int z = 1; // Set the right most bit to 1.
  z = z << r_offset;
  z = ~z;

  vector[block] = vector[block] & z; // Reset the i-th bit.
}

/** Input:
 *      d: Bit vector to which result of u - v shall be stored.
 *      u, v: Bit vectors, operands.
 *  Output:
 *      Returns nothing.
 *      Stores the result of operation (u - v) in d.
 * */
void bit_Vector_Substraction(Bit_Vector d, Bit_Vector u, Bit_Vector v) {
  int i;
  // For Each element in the bit vector.
  // A - B = A INTERSECTION (NOT B)
  unsigned int z;
  for (i = 0; i < BV_ELEMS; i++) {
    z = ~v[i];
    d[i] = u[i] & z;
  }
}

/** Input:
 *      d: Bit vector to which union of u and v shall be stored.
 *      u, v: two bit vectors.
 *  Output:
 *      returns nothing,
 *      Stores the Union of bit vectors u and v in d.
 * */
void bit_Vector_Union(Bit_Vector d, Bit_Vector u, Bit_Vector v) {
  int i;
  // For each element in the bit vector.
  for (i = 0; i < BV_ELEMS; i++) {
    d[i] = u[i] | v[i];
  }
}

/** Input:
 *      d, s: Destination and source bit vectors respectively.
 *  Output:
 *      copies the contents of bit vetcor s to d.
 * */
void copy_Bit_Vector(Bit_Vector d, Bit_Vector s) {
  if (d == NULL) {
    printf("copy_Bit_Vector:\n");
    printf("Input is an Empty Bit Vector.\n");
    printf("Fatal Error, Exiting.\n");
    exit(1);
  }

  int i;
  for (i = 0; i < BV_ELEMS; i++)
    d[i] = s[i];
}

/** Input:
 *      u, v: Two bit vectors which shall be compared.
 *  Output:
 *      True, if the bit vectors u and v are different.
 *      False, otherwise (if u and v are identical).
 * */
Bool are_Bit_Vectors_Different(Bit_Vector u, Bit_Vector v) {
  int i;
  for (i = 0; i < BV_ELEMS; i++) {
    if (u[i] != v[i])
      return True;
  }
  return False;
}

/** Input:
 *      bv: A bit vector
 *      arr: An array of integers, to which integers of bv will be
 *           stored.
 *  Output:
 *      returns the number of elements put in the array arr.
 * */
int get_Set_Bit_Indices(Bit_Vector bv, int *arr) {
  Bit_Vector z;
  int i, j, k;
  /*** Initialize the z bit vector. *******************************/
  for (i = 0; i < BV_ELEMS; i++) {
    z[i] = (unsigned int)2147483648u; /* It is value of 2 ** 32.
                                       * Initailze each unsigned it in bit
                                       * vector to 100...0 */
  }

  for (i = 0, j = 0, k = 0; i < BV_ELEMS; i++) {
    for (; z[i] > 0; z[i] = z[i] >> 1) {
      if ((bv[i] & z[i]) == z[i])
        arr[j++] = k;
      else
        ; // Do nothing.
      ++k;
    }
  }
  return j;
}

/** Input:
 *      vector: A bit vector.
 *      i: index of the bit in this bit vector that shall be checked.
 *  Output:
 *      True, if the i-th bit in bv is set.
 *      False, otherwise.
 * */
Bool is_Bit_Set(Bit_Vector bv, int i) {
  int block = i / BV_ELEM_BITS;
  int offset = i % BV_ELEM_BITS;

  int r_offset = BV_ELEM_BITS - offset - 1; // See set_Bit ().

  unsigned int z = 1; // Set the right most bit to 1.
  z = z << r_offset;

  z = z & bv[block]; // Take and to see the effect of i-th bit.

  if (z != 0)
    return True;

  return False;
}

/********* PRINT FUNCTIONS ****************************************/

/** Input:
 *      bv: The bit vector which shall be printed as a string.
 *  Output:
 *      pointer to a string containing the bit vector as a string.
 *      The returned pointer points to a static string.
 *      Hence the char pointer points to the same string, but contents
 *      of the string are overwritten on each function call.
 * */
char *bit_Vector_To_String(Bit_Vector bv) {
  Bit_Vector z;
  static char
      str[MAX_STMNTS * 2]; /*
                            * The number of bits in multiplied by 2 because
                            *  There will be a space after every 4 bits, and
                            *  three spaces after every sizeof (int) bits */
  int i;

  /*** Initialize the z bit vector. *******************************/
  for (i = 0; i < BV_ELEMS; i++) {
    z[i] = (unsigned int)2147483648u; /* It is value of 2 ** 32.
                                       * This is done to initailze each bit
                                       * vector to 100...0 */
                                      // printf ("z = %u\n", z[i]);
  }

  str[0] = '\0'; /// Initialize first char to NULL.

  /*** Printing the bit vector as a string.
   * z [i] will have atmost one 1.
   * z [i] & b [i], will be
   *     1, if b [i] has 1 at coressponding position,
   *     0, otherwise.
   * */
  int k = 0, bI = 0;
  char bbstr[32];
  for (i = 0; i < BV_ELEMS; i++) {
    for (; z[i] > 0; k++, z[i] = z[i] >> 1 /* Right Shift, 1 bit.*/) {

      if (k % MAX_BB_STMNTS == 0) {
        if (bI % 4 == 0 && bI != 0)
          strcat(str, "\n\t");
        sprintf(bbstr, "\t[%2d]", bI + 2);
        bI++;
        strcat(str, bbstr);
        k = 0;
      }

      strcat(str, ((bv[i] & z[i]) == z[i]) ? "1" : "0");

      if (k % 4 == 0 && k != 0)
        strcat(str, " "); // Space after 4 bits
    }
    // strcat (str, "\n\t "); // Three spaces after every element.
  }
  // printf ("%s\n", str);
  return str;
}

/** Input:
 *      bv: The bit vector which shall be printed as a string.
 *  Output:
 *      pointer to a string containing the bit vector as a string.
 *      The returned pointer points to a static string.
 *      Hence the char pointer points to the same string, but contents
 *      of the string are overwritten on each function call.
 * */
char *bit_Vector_To_String_BB(Bit_Vector bv, BB_Array *bba) {
  Bit_Vector z;
  static char
      str[MAX_STMNTS * 2]; /*
                            * The number of bits in multiplied by 2 because
                            *  There will be a space after every 4 bits, and
                            *  three spaces after every sizeof (int) bits */
  int i;

  /*** Initialize the z bit vector. *******************************/
  for (i = 0; i < BV_ELEMS; i++) {
    z[i] = (unsigned int)2147483648u; /* It is value of 2 ** 32.
                                       * Initailze each unsigned it in bit
                                       * vector to 100...0 */
  }

  /*** Printing the bit vector as a string.
   * z [i] will have atmost one 1.
   * z [i] & b [i], will be
   *     1, if b [i] has 1 at coressponding position,
   *     0, otherwise.
   * */
  int k = 0, bI = 0, sI;
  char bbstr[32];
  str[0] = '\0'; // Initialize first char to NULL.

  for (i = 0, k = 0, bI = 0; i < BV_ELEMS; i++) {
    for (; z[i] > 0; z[i] = z[i] >> 1, sI++, k++) {
      if (k % MAX_BB_STMNTS == 0) {
        if (k != 0)
          bI++;

        if (bI % 4 == 0 && bI != 0)
          strcat(str, "\n\t");
        sprintf(bbstr, "\t[%2d]", bI + 2);
        strcat(str, bbstr);

        sI = 0;
      }

      if (sI % 4 == 0 && sI != 0)
        strcat(str, " "); // space after every 4 bits.

      if (sI == bba->BB[bI].total_Stmnts)
        strcat(str, "$");

      if (sI >= bba->BB[bI].total_Stmnts || bI >= MAX_BBs)
        strcat(str, ((bv[i] & z[i]) == z[i]) ? "1" : "-");
      else
        strcat(str, ((bv[i] & z[i]) == z[i]) ? "1" : "0");
    }
  }

  return str;
}

/** Input:
 *      bv: The bit vector which shall be printed as a string.
 *  Output:
 *      pointer to a string containing the bit vector as a string.
 *      The returned pointer points to a static string.
 *      Hence the char pointer points to the same string, but contents
 *      of the string are overwritten on each function call.
 * */
char *bit_Vector_To_String_ST(Bit_Vector bv) {
  Bit_Vector z;
  static char
      str[MAX_STMNTS * 2]; /*
                            * The number of bits in multiplied by 2 because
                            *  There will be a space after every 4 bits, and
                            *  three spaces after every sizeof (int) bits */
  int i;

  /*** Initialize the z bit vector. *******************************/
  for (i = 0; i < BV_ELEMS; i++) {
    z[i] = (unsigned int)2147483648u; /* It is value of 2 ** 32.
                                       * Initailze each unsigned it in bit
                                       * vector to 100...0 */
  }

  /*** Printing the bit vector as a string.
   * z [i] will have atmost one 1.
   * z [i] & b [i], will be
   *     1, if b [i] has 1 at coressponding position,
   *     0, otherwise.
   * */
  int k = 0, bI = 0, sI;
  char bbstr[32];
  str[0] = '\0'; // Initialize first char to NULL.

  for (i = 0, k = 0, bI = 0; i < BV_ELEMS; i++) {
    for (; z[i] > 0; z[i] = z[i] >> 1, sI++, k++) {
      if (k % MAX_BB_STMNTS == 0) {
        if (k != 0)
          bI++;

        if (bI % 4 == 0 && bI != 0)
          strcat(str, "\n\t\t\t");

        if (bI == 0)
          sprintf(bbstr, "[%2d]", bI + 2);
        else
          sprintf(bbstr, "\t[%2d]", bI + 2);

        strcat(str, bbstr);

        sI = 0;
      }

      if (sI % 4 == 0 && sI != 0)
        strcat(str, " "); // space after every 4 bits.

      strcat(str, ((bv[i] & z[i]) == z[i]) ? "1" : "-");
    }
  }
  return str;
}

void test_bit_vector() {
  printf("size of int = %lu.\n", sizeof(int));

  Bit_Vector u, v;

  u[0] = 18390;
  v[0] = 9823;

  printf("v = %s\n", bit_Vector_To_String(v));
  u[0] = u[0] | (~v[0]);
  printf("v = %s\n", bit_Vector_To_String(v));

  /*
  set_Bit (u, 3);
  set_Bit (u, 4);
  set_Bit (u, 5);
  set_Bit (u, 6);
  set_Bit (u, 6);

  reset_Bit (u, 3);
  reset_Bit (u, 4);
  reset_Bit (u, 5);
  reset_Bit (u, 6);
  reset_Bit (u, 7);

  /*
  u [2] = 30293;
  v [1] = 3932;

  Bit_Vector a;
  bit_Vector_And (a, u, v);
  char *s = bit_Vector_To_String (u);
  printf ("AND \nu = %s\n", s);

  s = bit_Vector_To_String (v);
  printf ("v = %s\n", s);

  s = bit_Vector_To_String (a);
  printf ("a = %s\n\n", s);

  Bit_Vector o;
  bit_Vector_Or (o, u, v);
  s = bit_Vector_To_String (u);
  printf ("OR \nu = %s\n", s);

  s = bit_Vector_To_String (v);
  printf ("v = %s\n", s);

  s = bit_Vector_To_String (o);
  printf ("o = %s\n\n", s);
  */
}
