expr:     VAR      expr2   {
                #ifdef DEBUG_PARSER
                    fprintf (fw, "expr:     VAR=|%s|      expr2.\n", $1);
                #endif
                /* Construct a node of type Expr, containing infromation
                 * about this variable. 
                 * Set: $2 -> left = This newly constructed node
                 * Set $$ = $2
                 * */
                Expr *node = construct_Var_Node ($1, g_ST);
                $2->left   = node;
                $$         = $2;
                if ($2->right == NULL)
                {
                    // It is a unary expression.
                    if (*($1) != '-')
                        // It is an Identity statement.
                        set_Statement_Type (g_BBA, s_ASGNMT_ID);
                    else
                        // unary expression.
                        set_Statement_Type (g_BBA, s_ASGNMT_UN);
                }
                else
                    // It is a binary expression.
                    set_Statement_Type (g_BBA, s_ASGNMT_BI);
            
            } 
      |   INT_LIT  expr2    {
                #ifdef DEBUG_PARSER
                    fprintf (fw, "expr:     INT_LIT=|%d|      expr2.\n", $1);
                #endif
                /* Construct a node of type Expr, containing infromation
                 * about this Literal. 
                 * Set: $2 -> left = This newly constructed node
                 * Set $$ = $2
                 * */
                Expr *node = construct_Int_Node ($1);
                $2->left   = node;
                $$         = $2;

                if ($2->right == NULL)
                    // It is a unary expression.
                    set_Statement_Type (g_BBA, s_ASGNMT_UN);
                else
                    // It is a binary expression.
                    set_Statement_Type (g_BBA, s_ASGNMT_BI);
            } 
      |     OPRTR   expr3  {
      
            }
      ;

expr2:    OPRTR    expr3    {
                #ifdef DEBUG_PARSER
                    fprintf (fw, "expr2:    OPRTR=|%s|     expr3.\n", $1);
                #endif
                /* Construct Root node of expression tree, saying 
                 * that expression represents a BINARY expression. 
                 * Set $$ = The node (root).
                 * root -> right = $2.
                 * */
                Expr *node  = construct_Oprtr_Node ($1);
                node->right = $2;
                $$          = node;
            } 
       |  /* Empty Rule */  {
                #ifdef DEBUG_PARSER
                    fprintf (fw, "expr2:    /* Empty Rule */.\n");
                #endif
                /* Construct Root node of expression tree, saying 
                 * that expression represents an UNARY expression. 
                 * Set $$ = The newly constructed node.
                 * */
                Expr *node = construct_Oprtr_Node (NULL);
                $$         = node;
            }
       ;

expr3:    VAR  {
                // $$ = $1;
                #ifdef DEBUG_PARSER
                    fprintf (fw, "expr3:    VAR=|%s|.\n", $1);
                #endif
                /* Construct a node of type Expr, containing infromation
                 * about this variable. 
                 * Set $$ = This newly constructed node.
                 * */
                Expr *node = construct_Var_Node ($1, g_ST);
                $$ = node;
            } 
       |  INT_LIT  {
                // $$ = $1;
                #ifdef DEBUG_PARSER
                    fprintf (fw, "expr3:    INT_LIT=|%d|.\n", $1);
                #endif
                /* Construct a node of type Expr, containing infromation
                 * about this Literal. 
                 * Set $$ = This newly constructed node.
                 * */
                Expr *node = construct_Int_Node ($1);
                $$ = node;
            } 
       ;

