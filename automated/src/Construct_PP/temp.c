

/** LDT_Type: enum for type of a latest definion, is it a place or
 *   a transition.
 * */
typedef enum {
  ld_ERROR = 0,
  ld_PLC, /// for place.
  ld_TR   /// for transition.
} LD_Type;

/** Inputs:
 *    pp, pp_obb: Usual meaning.
 *    predI: Index of the predecessor of bI whose maximum transition the
 *             places in the loop_synch_pl[] array of the subnet bI
 *             should be attached to.
 *    bI: Index of the subnet which is a loop header and is being attached
 *          to the subnet predI.
 *  Outputs:
 *      returns Nothing.
 *  Functionality:
 *     Adds each place in the loop_synch_pl[] array of the subnet bI
 *       to the post set of the maximum transition of the subnet predI.
 * */
void attach_mt_to_loop_synch_places(Pres_Plus *pp, PP_One_BB *pp_obb,
                                    const int predI, const int bI) {
  /******************* FUNCTION DECLARATIONS *********************/
  // Adds given place to the postset of given transition.
  void add_To_Postset_Of_Transition(Pres_Plus * pp, int tI, int pI);

  // Adds given transition to the preset of given place.
  void add_To_Preset_Of_Place(Pres_Plus * pp, int pI, int tI);
  /***************************************************************/

  int i;
  int pI;  // Holds index of a place.
  int mtI; // Index of the maximum transition of the predecessor.
  mtI = pp_obb[predI].max_trans;

  // For each place in the loop_synch_pl[] array of the subnet bI:
  for (i = 0; i < pp_obb[bI].c_loop_synch_pl; i++) {
    pI = pp_obb[bI].loop_synch_pl[i];

    // Add pI to the post set of mtI.
    add_To_Postset_Of_Transition(pp, mtI, pI);
    add_To_Preset_Of_Place(pp, pI, mtI);
  }
}

/** Inputs:
 *   pp, pp_obb: Usual arguments.
 *   bI: Index of the subnet under consideration.
 *   tarr: An array containing indices of transitions for which pre-places
 *         are to be constructed.
 *   len: Number of transitions in the array tarr.
 *  Output:
 *    returns nothing.
 *  Functionality:
 *  1. For each transition in the array tarr[] constructs a new pre-place.
 *  2. Adds the place to the loop_synch_pl[] array.
 * */
void const_synch_pre_places_for_trans(Pres_Plus *pp, PP_One_BB *pp_obb,
                                      const int bI, int *tarr, const int len) {
  /******************* FUNCTION DECLARATIONS *********************/
  // Adds a new place to the PRES+ of bI, returns its index.
  int new_Dummy_Place(Pres_Plus * pp, PP_One_BB * pp_obb, int bI);

  // Adds given place to the preset of given transition.
  void add_To_Preset_Of_Transition(Pres_Plus * pp, int tI, int pI);

  // Adds given transtion to the postset of given place.
  void add_To_Postset_Of_Place(Pres_Plus * pp, int pI, int tI);
  /***************************************************************/
  int i;
  int tI;   // Index of a transition.
  int ngtI; // index of the transition with negated guard to that of tI.
  int pI;   // Index of a place.
  int j;    // An temp int.

  // For each transition in the array tarr[]:
  for (i = 0; i < len; i++) {
    tI = tarr[i];
    pI = new_Dummy_Place(pp, pp_obb,
                         bI); /* Also adds the place to
                               * the input places array of the subnet. */

    // Attach pI to the transition arr[i] as a pre-place.
    add_To_Preset_Of_Transition(pp, tI, pI);
    add_To_Postset_Of_Place(pp, pI, tI);

    /* If tI is a guarded trans, add tI to the pre-set of the trans
     *   with negation of the guard also. */
    if (pp->transition[tI].itng != -1) {
      ngtI = pp->transition[tI].itng;
      add_To_Preset_Of_Transition(pp, ngtI, pI);
      add_To_Postset_Of_Place(pp, pI, ngtI);
    }

    // Add the place to the loop_synch_pl[] array.
    j = pp_obb[bI].c_loop_synch_pl++;
    if (j >= MAX_PLACES) {
      printf("const_synch_pre_places_for_trans ():\n");
      printf("Number of places in the loop_synch_pl[] array ");
      printf("exceeded MAX_PLACES.\n");
      printf("Fatal, Exiting...\n\n");
      exit(1);
    } else {
      pp_obb[bI].loop_synch_pl[j] = pI;
    }
  }
}

/**Inputs:
 *    pp, pp_obb: Usual meanings.
 *    bI: Index of the subnet for which synch places should be
 *          constructed.
 *        [bI must be a loop header.]
 * Outputs:
 *    returns nothing.
 * Functionality:
 *  Synch places control the execution of a transition.
 *  What is the value of a token in a synch place doesn't matter.
 *    Hence, they are dummy places.
 *  A place for each transition we want to synchronization.
 *
 *  A loop can have multiple parallel execution paths. To ensure that
 *    execution paths of different iteration don't overlap, this
 *    synchronization is used.
 *
 *  1. Stores the transitions in the post-set of input places of the
 *        subnet bI to an array.
 *  2. Remove the duplicates from the array.
 *  3. For each transition in the array, constructs a pre-place (DUMMY).
 *  4. Adds this DUMMY place to the loop_synch_places array of the
 *       subnet bI.
 * */
void create_loop_synch_places(Pres_Plus *pp, PP_One_BB *pp_obb, const int bI) {
  /******************* FUNCTION DECLARATIONS *********************/
  // Constructs synch pre places for the transitions specified in the
  // array arr.
  void const_synch_pre_places_for_trans(Pres_Plus * pp, PP_One_BB * pp_obb,
                                        const int bI, int *tarr, const int len);

  /* Removes the duplicates from the array arr. Max size of the arr
   * can be MAX_TRANSITIONS.
   * Also of the two transitions present in the array with negation
   *   each others guards, keeps only one and removes the other. */
  int remove_duplicates_and_guard_trans_pairs(Pres_Plus * pp, int *arr,
                                              int len);
  /***************************************************************/

  int i, j;
  int pI; // Holds index of a place.
  int tI; // Holds index of a transition.

  int arr[MAX_TRANSITIONS]; // To store the transitions in the post-set
                            // of the input places.
  int count = 0;

  // For each input place of the subnet bI.
  for (i = 0; i < pp_obb[bI].c_in; i++) {
    /* IMPORTANT:
     *  When this function is invoked, subnet bI has been attached
     *    to its successors.
     *  Hence, input places of the subnet can have more than one
     *    post-transitions.
     *  [But at-most 2] because a subnet can have at-most two
     *    successors.
     * */
    pI = pp_obb[bI].in[i]; // The current input place of bI.

    // For each transition in the post-set of this place.
    for (j = 0; j < pp->place[pI].c_Postset; j++) {
      tI = pp->place[pI].postset[j]; /* A transition in the post-
                                      * set of the place pI. */

      if (count == MAX_TRANSITIONS) {
        printf("create_loop_synch_places ():\n");
        printf("Increase the value of MAX_TRANSITIONS.\n");
        printf("Exiting the program.\n\n");
        exit(1);
      }

      arr[count++] = tI; // Add tI to the array arr.
    }
  }

  count = remove_duplicates_and_guard_trans_pairs(
      pp, arr,
      count); /*
               * Removes the duplicate transitions from the array arr[].
               *   AND
               * For transitions t and t' such that guard of a trans t is
               *    is negation the guard of trans t' in the array:
               *    Among t and t' keeps one and removes other from the array.
               * */

  const_synch_pre_places_for_trans(pp, pp_obb, bI, arr,
                                   count); /*
                                            * Constructs sycn pre-places for the
                                            * transitions in the array arr.
                                            */
}

/** Inputs:
 *    arr: An array containing integers.
 *         Max size of the array must be MAX_TRANSITIONS.
 *    len: Number of elements in the array.
 *  Outputs:
 *    returns the number of elements in the array arr after removing
 *      duplicates.
 *  Functionality:
 * Removes
 * the duplicate transitions from the array arr[].
 * AND
 * For transitions t and t' such that guard of a trans t is
 *    is negation the guard of trans t' in the array:
 *    Among t and t' keeps one and removes other from the array.
 * */
int remove_duplicates_and_guard_trans_pairs(Pres_Plus *pp, int *arr, int len) {
  if (len > MAX_TRANSITIONS) {
    printf("remove_duplicates ()\n");
    printf("The number of elements in the array is more than maximum");
    printf(" permissible limit i.e. MAX_TRANSITIONS.\n");
    printf("Fatal error. Exiting...\n\n");
    exit(1);
  }

  int i;    // For iteration.
  int tI;   // Index of a transition.
  int ngtI; // index of the transition with negated guard to that of tI.

  // A temp array initialized to zero.
  int temp[MAX_TRANSITIONS];
  for (i = 0; i < MAX_TRANSITIONS; i++)
    temp[i] = 0;

  // For each transition in the array arr.
  for (i = 0; i < len;) {
    tI = arr[i];
    if (temp[tI] == 1) {
      // Replace arr[len-1] with arr[i] and reduce len.
      arr[i] = arr[len - 1];
      len--;
    } else {
      // tI occurred for the first time in the array.
      temp[tI] = 1;
      i++;

      if (pp->transition[tI].itng != -1) {
        // tI is a guarded transition.
        ngtI = pp->transition[tI].itng;
        temp[ngtI] = 1; /* Mark this trans also as visited.*/
      }
      /* Else tI is an unguarded trans, do nothing. */
    }
  }
  return len;
}

/** Inputs: (May remove this function. .. Edit exception handling first
 *               used. )
 *    pp, pp_obb: Usual meaning.
 *    bI: Current subnet.
 *    tI: A transition for which a loop synch place shall be constructed.
 *  Outputs:
 *    returns nothing.
 *  Functionality:
 *  1. Traverses the loop_synch_pl[] array of the subnet bI to find if
 *       tI already exists as post-trans of any place.
 *     Returns immediately if exists.
 *  2. Constructs a DUMMY place.
 *  3. Adds the dummy place to the pre-set of the transition tI and
 *       vice-versa.
 *  4. Adds the dummy place to the loop_synch_pl[] array of the subnet bI.
 *  5. Adds the dummy place to he input places array of the subnet bI.
 * /
void create_loop_synch_pl_internal (Pres_Plus *pp, PP_One_BB *pp_obb,
                        const int bI, const int tI)
{
    int i;
    // int tI; // Index of a transition.
    int pI; // Index of a place.

    // For each place in the loop_sych_pl[] array.
    for (i = 0; i < pp_obb[bI].c_loop_synch_pl; i++)
    {
        pI = pp_obb[bI].loop_synch_pl[i];

        if (pp->place[pI].c_Postset != 1)
        {
            // Print exception.
            printf ("create_loop_synch_pl_internal ()\n");
            printf ("");
            continue;
        }

        pI2 = new_Dummy_Place (pp, pp_obb, bI); /* Also adds the pI2 to
            * the input places array of the subnet bI. /

        add_place_to_loop_synch_pl_array (pp_obb, bI, pI2); /* Adds the
            * place pI2 to the loop synch places array of the subnet bI.
            * /

        // Add pI2 to the pre-set of the transition tI and vice-versa.
        add_To_Preset_Of_Transition (pp, tI, pI2);
        add_To_Postset_Of_Place (pp, pI2, tI);
    }

}*/

/***** EXTRA FUNCTIONS ************************************************/

/** INPUT:
 *      bba: Pointer to the structure containing array of basic blocks.
 *      bs, be: BBs in the edge bs --> be.
 *  Returns
 *          True: if bs-->be is a back edge:
 *              i.e. if be is a loop header.
 *          False: otherwise.
 * */
Bool is_back_edge(BB_Array *bba, const int bs, const int be) {
  if ((bba->BB[be].bb_Type == bb_COND) && (bba->BB[be].trueNext < be))
    return True;

  return False;
}

/** Inputs:
 *    pp, pp_obb: Usual meaning.
 *    bI: Index of the subnet in which maximum transition is to be
 *        constructed.
 *  Outputs:
 *    returns nothing.
 *  Functionality:
 * 1. Constructs a new transition. It is the maximum transition.
 * 2. Assigns ID type to this transition, though it is debatable.
 * 3. Attaches the transition to all the transitions in the subnet bI
 *    which have an empty post-set via DUMMY places and as post-trans.
 * 4. In pp_obb, sets the max_trans attribute to this trans.
 * */
void const_maximum_trans_backup(Pres_Plus *pp, PP_One_BB *pp_obb,
                                const int bI) {
  /******************* FUNCTION DECLARATIONS *********************/
  // Adds a new place to the PRES+ of bI, returns its index.
  int new_Place(Pres_Plus * pp, PP_One_BB * pp_obb, int bI);

  // Adds given place to the postset of given transition.
  void add_To_Postset_Of_Transition(Pres_Plus * pp, int tI, int pI);

  // Adds given transition to the preset of given place.
  void add_To_Preset_Of_Place(Pres_Plus * pp, int pI, int tI);

  // Adds given place to the preset of given transition.
  void add_To_Preset_Of_Transition(Pres_Plus * pp, int tI, int pI);

  // Adds given transtion to the postset of given place.
  void add_To_Postset_Of_Place(Pres_Plus * pp, int pI, int tI);

  // Add a new transition to the PRES+ of bI, returns its index.
  int new_Transition(Pres_Plus * pp, PP_One_BB * pp_obb, int bI);
  /***************************************************************/
  int i;
  int pI; // Index of a place.
  int tI; // Index of a transition.

  int mtI; // Index of the maximum transition.
  mtI = new_Transition(pp, pp_obb, bI);

  pp->transition[mtI].t_Type = t_ID;

  pp_obb[bI].max_trans = mtI;

  // Attach tI to all the transitions of the subnet bI with empty post-set.
  for (i = 0; i < pp_obb[bI].c_TR; i++) {
    tI = pp_obb[bI].tr[i]; // The i-th transition of bI.

    if (pp->transition[tI].c_Postset == 0 && tI != mtI) {
      pI = new_Place(
          pp, pp_obb,
          bI); /* new_Dummy_Place () must not
                * be used as it also adds pI to the input places array of bI.*/

      pp->place[pI].p_Type = p_DUMMY;
#ifdef CHECK_EQV
      pp->place[pI].var = DUM_VAR_INDEX;
#endif // CHECK_EQV

      // Add pI to the post set of transition tI.
      add_To_Postset_Of_Transition(pp, tI, pI);
      add_To_Preset_Of_Place(pp, pI, tI);

      // Add pI to the pre-set of the transition mtI:
      add_To_Preset_Of_Transition(pp, mtI, pI);
      add_To_Postset_Of_Place(pp, pI, mtI);

      // printf ("BB= %d, attaching the mtI=%d to the trans %d.\n", bI+2, mtI,
      // tI);
    }
  }
}

/** INPUTS:
 *  pp: The overall PRES+ net consisting of unattached subnets of all
 *     the basic blocks in the control flow graph.
 *  pp_obb: (subnets) Pointer to an array s.t. an element contains
 *          indices of the places and transitions in pp.places and
 *          pp.transitions array corresponding to the subnet of the
 *          basic block at the same index in the basic blocks array.
 *  bba: Pointer to the array of basic blocks.
 *  st: Pointer to the Symbol Table.
 *  bI:  Index of the subnet in which defining transitions shall be
 *       constructed for each variable at the end of the subnet.
 *  Output:
 *      None
 *
 *  DESCRIPTION:
 *      This functions constructs a defining transitions for each
 *  variable v s.t. v is live at the end of the subnet bI and the subnet
 *  has no defining transition for v.
 * */
void ensure_def_trans_for_live_vars(Pres_Plus *pp, PP_One_BB *pp_obb,
                                    BB_Array *bba, const int bI,
                                    Symbol_Table *st) {
  /******************* FUNCTION DECLARATIONS *********************/

  /* Returns the number of entries made to arr.
   * arr will contain the indices corresponding to set bits in bv. */
  int get_Set_Bit_Indices(Bit_Vector bv, int *arr);

  // Adds a new transition to the PRES+.
  int new_Transition(Pres_Plus * pp, PP_One_BB * pp_obb, int bI);

  // Adds a new place to the PRES+.
  int new_Place(Pres_Plus * pp, PP_One_BB * pp_obb, int bI);

  // Adds place pI to input places array of subnet bI.
  void add_To_Input_Places_Array(Pres_Plus * pp, PP_One_BB * pp_obb, int bI,
                                 int pI);

  // Adds pI to the preset of the transition tI.
  void add_To_Preset_Of_Transition(Pres_Plus * pp, int tI, int pI);

  // Add transition tI to the post-set of the place pI.
  void add_To_Postset_Of_Place(Pres_Plus * pp, int pI, int tI);
  /***************************************************************/

  /* IMPORTANT PROPERTY:
   * Variables live at the end of a subnet is a superset of the
   *   variables live at the beginning of all its successors.
   * OR
   *  Var-Live-end[bI] = UNION Var-Live-beginning[s]
   *                          where s is a successor of bI.
   * */

  // Get all variables live at the end of bI.
  int arr[MAX_VARIABLES]; // Array to store live variables at end of bI.
  int len;                // size of the array arr.
  len = get_Set_Bit_Indices(bba->BB[bI].live_out, arr);

  int i; // For iteration.
  int v; // Stores index of a variable.
  int t; // Stores index of a transition.
  int p; // Stores index of a place.

  // For each variable live at the end of bI.
  for (i = 0; i < len; i++) {
    v = arr[i];
    if (pp_obb[bI].ldt[v] == -1) {
      /* No definition of v in bI and v is live at end of BB bI. */

      // 1. Construct a defining transition for the variable v.
      t = new_Transition(pp, pp_obb, bI);

      pp->transition[t].t_Type = t_ID; // Set the trans as an identity trans.

      pp_obb[bI].ldt[v] = t; // Set t as the ldt for v.

      // 2. Construct a pre-place for the transition.
      p = new_Place(pp, pp_obb, bI);

      pp->place[p].var = v;        // Assign a variable to the place.
      pp->place[p].p_Type = p_VAR; // Assign a type to the place.

      add_To_Input_Places_Array(
          pp, pp_obb, bI, p); /* Add p
                               * to the input places array of the subnet bI. */

      // 3. Attach the place and the transition.
      add_To_Preset_Of_Transition(pp, t, p);
      add_To_Postset_Of_Place(pp, p, t);
    }
  }
}
