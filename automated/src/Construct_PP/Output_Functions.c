#include "Types.h"

/***** PLOTTING FOR GRAPHVIZ ****************************************/

/** Input:
 *      pp:  pointer to PRES+.
 *      pp_obb: pointer to the array where i-th element contains indices
 *          of places and transitions corressponding to i-th BB.
 *      bba: pointer to the Basic Blocks array.
 *      st:  pointer to the Symbol Table.
 *  Output:
 *      Returns nothing.
 *    Plots the PRES+ as an image, in the file defined in PLOT_PP.
 * 
 * PLACES:
 *  INPUT/OUTPUT = SHADED
 *  BLACK = VAR
 *  BROWN = DUMMY
 *  BLUE  = SYNCHRONIZING
 *  BROWN = SCANF
 *  BROWN = PRINTF
 *  RED   = ERROR
 * 
 *  Light yellow = Input
 *  Light blue   = Output
 *
 * TRANSITIONS
 *  EXPR = BLACK
 *  SYNCH= BLUE
 *  ID   = BROWN
 *  OTHER= RED
 * */
void plot_Pres_Plus_With_BB_Indices (Pres_Plus *pp, PP_One_BB *pp_obb,
                        BB_Array *bba, Symbol_Table *st)
{
    /******************* FUNCTION DECLARATIONS *********************/
    // Returns string for the variable at index "index" in symbol table.
    char *get_Var_As_String (int index, Symbol_Table *st);

    // Returns the expression represented by expr tree "root" as string.
    char *get_Expr_As_String (Expr *root, Symbol_Table *st);
    
    // Draws the Edege BB wise.
    void plot_PP_One_BB (Pres_Plus *pp, PP_One_BB *pp_obb, int bI, 
                        Symbol_Table *st, FILE *fp);
    /***************************************************************/
    int i, j, b, p, t;
    FILE *fp = fopen (PLOT_PP_WITH_BB_INDX, "w+");
    if (fp == NULL)
    {
        printf ("plot_Pres_Plus_With_BB_Indices:\n");
        perror (PLOT_PP_WITH_BB_INDX);
        printf ("Returning without plotting.\n");
        return;
    }
    fprintf (fp, "digraph G {\n");
    
    
    int level = 0;
    
    /*fprintf (fp, "\nsubgraph g2{\n");
    fprintf (fp, "\nordering=out; rankdir=TB; ranksep=2; \n\n");
    for (i = 0; i < bba->total_BBs-1; i++, level++)
        fprintf (fp, "L%d -> ", level);

    fprintf (fp, "\"L%d\";", level);
    fprintf (fp, "\n}\n\n");*/
    
    fprintf (fp, "graph[\ncompound=true, ranksep=1] \n\n");
    
    fprintf (fp, "\"Input Place\" [style=\"bold, filled\", fillcolor=lightyellow]");
    
    fprintf (fp, "\"Output Place\" [style=\"bold, filled\", fillcolor=lightblue]");

    fprintf (fp, "/* PLACES */\n");
    for (j = 0; j < bba->total_BBs; j++)
    {
        b = j;
        
        // Start cluster
        fprintf (fp, "subgraph cluster%d {\n", b);
        
        fprintf (fp, "node [shape=ellipse, style=solid];\n");
        // For Each place, print a node:
        for (i = 0; i < pp_obb[b].c_PL; i++)
        {
            p = pp_obb[b].pl[i];
            
            switch (pp->place[p].p_Type)
            {
                case p_DUMMY:
                    fprintf (fp, "p%d [label=\"DUMMY, p-%d\", color=brown, style=bold",
                                p, p);
                    break;
                case p_VAR:
                    fprintf (fp, "p%d [label=\"%s, p-%d\", color=black", p,
                                get_Var_As_String (pp->place[p].var, st), p);
                    break;
                case p_SCANF:
                    fprintf (fp, "p%d [label=\"%s, p-%d\", color=forestgreen, style=bold",
                                p, get_Var_As_String (pp->place[p].var, st), p);
                    break;
                case p_PRINTF:
                    fprintf (fp, "p%d [label=\"%s, p-%d\", color=gold2, style=bold",
                                p, get_Var_As_String (pp->place[p].var, st), p);
                    break;
                default:
                    fprintf (fp, "p%d [label=\"ERROR, p-%d\", fillcolor=red", p, p);
            }
            if (pp->place[p].c_Preset == 0)
                fprintf (fp, ", style=\"bold, filled\", fillcolor=lightyellow];\n");
            else if (pp->place[p].c_Postset == 0)
                fprintf (fp, ", style=\"bold, filled\", fillcolor=lightblue];\n");
            else
                fprintf (fp, "];\n");
        }
    
    
        fprintf (fp, "\n/* TRANSITIONS */\n");
        // For each transition.
        fprintf (fp, "node [shape=box, style=solid];\n");
        for (i = 0; i < pp_obb[b].c_TR; i++)
        {
            t = pp_obb[b].tr[i];
            switch (pp->transition[t].t_Type)
            {
                case t_ID:
                    if (pp->transition[t].guard != NULL)
                        fprintf (fp, "t%d [label=\"ID, t-%d, [%s]\", color=brown];\n", t, t,
                                    get_Expr_As_String (pp->transition[t].guard, st));
                    else
                        fprintf (fp, "t%d [label=\"ID, t-%d\", color=brown];\n", t, t);
                    break;
                case t_EXPR:
                    fprintf (fp, "t%d [label=\"%s, t-%d\", color=black];\n", t,
                           get_Expr_As_String (pp->transition[t].expr, st), t);
                    break;
                case t_DUMMY:
                    fprintf (fp, "t%d [label=\"DUMMY, t-%d\", color=brown];\n", t, t);
                    break;
                default:
                    fprintf (fp, "t%d [label=\"ERROR, t-%d\", color=red];\n", t, t);
            }
        }
        // End cluster
        fprintf (fp, "label=BB%d;\n", b+2);
        fprintf (fp, "\n}\n\n");
    }

    // For each basic block:
    for (i = 0; i < bba->total_BBs; i++)
    {
        bba->BB[i].visited = False;
    }
    
    // For each BB, assign a rank to cluster:
    int nxt, n1, n2;
    level = 0;
    //fprintf (fp, "{rank=same; L%d; p%d;}\n", level, pp_obb[0].in[0]);
    bba->BB[0].visited = True;
    for (i = 0, level=0; i < bba->total_BBs; i++)
    {
        if (bba->BB[i].bb_Type == bb_LAST)
            continue;
            
        if (bba->BB[i].falseNext != -1)
        {
            nxt = bba->BB[i].falseNext;
            n1 = pp_obb[i].pl[0];
            n2 = pp_obb[nxt].pl[0];
            fprintf (fp, "p%d -> p%d [ltail=cluster%d, lhead=cluster%d, style=\"dashed, setlinewidth(2)\", minlen=4];\n", 
                        n1, n2, i, nxt);

           /* if (bba->BB[nxt].visited == False)
            {
                fprintf (fp, "{rank=same; L%d; p%d;}\n", level+1, pp_obb[nxt].in[0]);
                fprintf (fp, "// BB = %d, Ch = %d, Level = %d, Plc = %d.\n", i+2, nxt+2, level+1, n2);
                bba->BB[nxt].visited = True;
            }*/
        }

        if (bba->BB[i].trueNext != -1)
        {
            nxt = bba->BB[i].trueNext;
            n1 = pp_obb[i].pl[0];
            n2 = pp_obb[nxt].pl[0];
            fprintf (fp, "p%d -> p%d [ltail=cluster%d, lhead=cluster%d, style=\"dashed, setlinewidth(2)\", minlen=4];\n", 
                        n1, n2, i, nxt);
            
            /*if (bba->BB[nxt].visited == False)
            {
                fprintf (fp, "{rank=same; L%d; p%d;}\n", level+1, pp_obb[nxt].in[0]);
                fprintf (fp, "// BB = %d, Ch = %d, Level = %d, Plc = %d.\n", i+2, nxt+2, level+1, n2);
                bba->BB[nxt].visited = True;
                level++;
            }*/
        }
        
    }
    fprintf (fp, "\n");
    
    // Edges.
    //  For each place:
    for (i = 0; i < pp->pl_count; i++)
    {
        // For each element in its postset:
        for (j = 0; j < pp->place[i].c_Postset; j++)
        {
            fprintf (fp, "p%d -> t%d;\n", i, pp->place[i].postset[j]);
        }
    }
    
    
    // For each Transition:
    for (i = 0; i < pp->tr_count; i++)
    {
        // For each element in its postset:
        for (j = 0; j < pp->transition[i].c_Postset; j++)
        {
            fprintf (fp, "t%d -> p%d;\n", i, pp->transition[i].postset[j]);
        }
    }
    
    fprintf (fp, "\n}\n");
    fclose (fp);
}



/** Input:
 *      pp:  pointer to PRES+.
 *      pp_obb: pointer to the array where i-th element contains indices
 *          of places and transitions corressponding to i-th BB.
 *      bba: pointer to the Basic Blocks array.
 *      st:  pointer to the Symbol Table.
 *  Output:
 *      Returns nothing.
 *    Plots the PRES+ as an image, in the file defined in PLOT_PP.
 * 
 * The function is not used [MAR 30, 2016].
 * 
 * PLACES:
 *  INPUT/OUTPUT = SHADED
 *  BLACK = VAR
 *  BROWN = DUMMY
 *  BLUE  = SYNCHRONIZING
 *  BROWN = SCANF
 *  BROWN = PRINTF
 *  RED   = ERROR
 *
 * TRANSITIONS
 *  EXPR = BLACK
 *  SYNCH= BLUE
 *  ID   = BROWN
 *  OTHER= RED
 * */
void plot_Pres_Plus (Pres_Plus *pp, PP_One_BB *pp_obb, BB_Array *bba,
                        Symbol_Table *st)
{
    /******************* FUNCTION DECLARATIONS *********************/
    // Returns string for the variable at index "index" in symbol table.
    char *get_Var_As_String (int index, Symbol_Table *st);

    // Returns the expression represented by expr tree "root" as string.
    char *get_Expr_As_String (Expr *root, Symbol_Table *st);
    
    // Draws the Edege BB wise.
    void plot_PP_One_BB (Pres_Plus *pp, PP_One_BB *pp_obb, int bI, 
                        Symbol_Table *st, FILE *fp);
    /***************************************************************/
    int i, j;
    FILE *fp = fopen (PLOT_PP, "w+");
    if (fp == NULL)
    {
        printf ("plot_Pres_Plus:\n");
        perror (PLOT_PP);
        printf ("Returning without plotting.\n");
        return;
    }
    fprintf (fp, "digraph G {\n");
    
    // fprintf (fp, "concentrate=true;\n");
    
    fprintf (fp, "/* PLACES */\n");
    // For Each place, print a node:
    for (i = 0; i < pp->pl_count; i++)
    {
        switch (pp->place[i].p_Type)
        {
            case p_DUMMY:
                fprintf (fp, "P%d [label=DUMMY, color=brown, style=bold",
                            i);
                break;
            case p_VAR:
                fprintf (fp, "P%d [label=\"%s\", color=black", i,
                             get_Var_As_String (pp->place[i].var, st));
                break;
            case p_SCANF:
                fprintf (fp, "P%d [label=\"%s\", color=forestgreen, style=bold",
                            i, get_Var_As_String (pp->place[i].var, st));
                break;
            case p_PRINTF:
                fprintf (fp, "P%d [label=\"%s\", color=gold2, style=bold",
                            i, get_Var_As_String (pp->place[i].var, st));
                break;
            default:
                fprintf (fp, "P%d [label=ERROR, fillcolor=red", i);
        }
        if (pp->place[i].c_Preset == 0)
            fprintf (fp, ", style=\"bold, filled\", fillcolor=lightskyblue];\n");
        else if (pp->place[i].c_Postset == 0)
            fprintf (fp, ", style=\"bold, filled\", fillcolor=lightslateblue];\n");
        else
            fprintf (fp, "];\n");
    }
    
    fprintf (fp, "\n/* TRANSITIONS */\n");
    // For each transition.
    fprintf (fp, "node [shape=box, style=solid];\n");
    for (i = 0; i < pp->tr_count; i++)
    {
        switch (pp->transition[i].t_Type)
        {
            case t_ID:
                fprintf (fp, "t%d [label=ID, color=brown];\n", i);
                break;
            case t_EXPR:
                fprintf (fp, "t%d [label=\"%s\", color=black];\n", i,
                       get_Expr_As_String (pp->transition[i].expr, st));
                break;
            case t_DUMMY:
                fprintf (fp, "t%d [label=DUMMY, color=brown];\n", i);
                break;
            default:
                fprintf (fp, "t%d [label=ERROR, color=red];\n", i);
        }
    }
    
    // For each basic block:
    for (i = 0; i < bba->total_BBs; i++)
    {
        plot_PP_One_BB (pp, pp_obb, i, st, fp);
    }
    
    /* For each basic block:
    for (i = 0; i < bba->total_BBs; i++)
    {
        if (bba->BB[i].trueNext != -1)
            fprintf (fp, "BB%d -- BB%d;\n", i, bba->BB[i].trueNext);

        if (bba->BB[i].falseNext != -1)
            fprintf (fp, "BB%d -- BB%d;\n", i, bba->BB[i].falseNext);
    }*/
    
    
    
    /*/ Edges.
    // For each place:
    for (i = 0; i < pp->pl_count; i++)
    {
        // For each element in its postset:
        for (j = 0; j < pp->place[i].c_Postset; j++)
        {
            fprintf (fp, "P%d -- t%d;\n", i, pp->place[i].postset[j]);
        }
        /*
        // For each element in its preset: Comment this section:
        for (j = 0; j < pp->place[i].c_Preset; j++)
        {
            fprintf (fp, "P%d -> t%d;\n", i, pp->place[i].preset[j]);
        }/
    }*/
    
    
    /*/ For each Transition:
    for (i = 0; i < pp->tr_count; i++)
    {
        // For each element in its postset:
        for (j = 0; j < pp->transition[i].c_Postset; j++)
        {
            fprintf (fp, "t%d -- P%d;\n", i, pp->transition[i].postset[j]);
        }
        /*
        // For each element in its preset: Comment this section:
        for (j = 0; j < pp->transition[i].c_Preset; j++)
        {
            fprintf (fp, "t%d -> P%d;\n", i,  pp->transition[i].preset[j]);
        }/
    }*/
    
    fprintf (fp, "\n}\n");
    fclose (fp);
}



/** Input:
 *      pp:  Pointer to PRES+.
 *      pp_obb: pointer to the array where i-th element contains indices
 *          of places and transitions corressponding to i-th BB.
 *      bI:   Basic Block Index.
 *      st:  pointer to the Symbol Table.
 *      fp:  pointer to file to which PRES+ shall be ploted.
 *  Output:
 *      Retunrs nothing.
 *  DESCRITPION:
 *    Prints PRES+ for BB bI to file pointed by fp.
 * */
void plot_PP_One_BB (Pres_Plus *pp, PP_One_BB *pp_obb, int bI, 
                        Symbol_Table *st, FILE *fp)
{
    fprintf (fp, "subgraph cluster%d {\n", bI);
    
    fprintf (fp, "node [style=filled];\n");

    int i, j, p, t;
    
    /*
    fprintf (fp, "{rank=same; ");
    // For each transition belonging to this BB.
    for (i = 0; i < pp_obb[bI].c_TR; i++)
    {
        t = pp_obb[bI].tr[i];
        fprintf (fp, "t%d; ", t+1);
    }
    fprintf (fp, "}\n\n");*/

    // for each place belonging to this BB.
    for (i = 0; i < pp_obb[bI].c_PL; i++)
    {
        p = pp_obb[bI].pl[i];
        // For each element in its postset:
        for (j = 0; j < pp->place[p].c_Postset; j++)
        {
            fprintf (fp, "p%d -> t%d;\n", p, pp->place[p].postset[j]);
        }
    }
    
    // for each place belonging to this BB.
    for (i = 0; i < pp_obb[bI].c_PL; i++)
    {
        p = pp_obb[bI].pl[i];
        // For each element in its preset: Comment this section:
        for (j = 0; j < pp->place[i].c_Preset; j++)
        {
            fprintf (fp, "t%d -> p%d;\n", pp->place[i].preset[j], i);
        }
    }
    
    /* For each transition belonging to this BB.
    for (i = 0; i < pp_obb[bI].c_TR; i++)
    {
        t = pp_obb[bI].tr[i];
        // For each element in its postset:
        for (j = 0; j < pp->transition[t].c_Postset; j++)
        {
            fprintf (fp, "t%d -> p%d;\n", t, pp->transition[t].postset[j]);
        }
    }*/
    
    fprintf (fp, "label=BB%d;\n", bI+2);

    fprintf (fp, "\n}\n\n");
}



/** Input:
 *      bba, st: array of basic blocks and the symbol table respectively.
 *      file: File name in which the dot file shall be written.
 *  Output:
 *      returns nothing.
 *  Functionality:
 *      Prints the control flow graph in dot format. Ready to be
 *        converted to an image using the "dot" command.
 * */
void plot_BB_Array (BB_Array *bba, Symbol_Table *st, FILE *file)
{
    /******************* FUNCTION DECLARATIONS *********************/

    // Returns string for the variable at index "index" in symbol table.
    char *get_Var_As_String (int index, Symbol_Table *st);

    // Returns the expression represented by expr tree "root" as string.
    char *get_Expr_As_String (Expr *root, Symbol_Table *st);

    // Returns the number of enteries made to arr.
    // Arr will contain the indices coressponding to 1 in bv.
    int get_Set_Bit_Indices (Bit_Vector bv, int *arr);
    
    // Plots statements in a basic block.
    void plot_BB_Statements (BB_Array *bba, Symbol_Table *st, const int bI,
                                const FILE *fp);
    /***************************************************************/
    // Open output file
    FILE *fp= fopen (file, "w+");
    if (fp == NULL)
    {
        printf ("plot_BB_Array ():\n");
        perror (file);
        printf ("Exception, returning without printing.\n");
        return ;
    }
    int i, j, k;

    // For live variables at the beginning and at the end of BBs.
    int arr[MAX_STMNTS];
    int len;
    
    fprintf (fp, "digraph G {\n");
    fprintf (fp, "b [shape=circle, style=filled, fillcolor=White];\n");
    fprintf (fp, "e [shape=circle, style=filled, fillcolor=Black];\n");
    fprintf (fp, "node [shape=record]\n\n");

    //   bb2 [label="{<bgn> bb2 |{<lvb> a | b | c} | {<stm> s1 } | {<s2> s2} |{<end> d | e | f}  }"]
    
    // For each BB:
    for (i = 0; i < bba->total_BBs; i++)
    {
        // Print BB title.
        fprintf (fp, "bb%d [label=\"{<bgn> bb%d |{<lvb> ", i, i+2);
    
        // Print Live variables at the beginning.
        len = get_Set_Bit_Indices (bba->BB[i].live_in, arr);
        
        if (len > 0) {
            fprintf (fp, " %s", get_Var_As_String (arr[0], st));
        }
        for (j = 1; j < len; j++) {
            fprintf (fp, " | %s",  get_Var_As_String (arr[j], st));
        }
        
        // fprintf (fp, "} | {<stm> "); //Live vars end stmnts begin.
        fprintf (fp, "} "); //Live vars end; stmnts begin part removed.
        
        // Print all the statements.
        plot_BB_Statements (bba, st, i, fp);
    
        // fprintf (fp, "} |{<end> ");
        fprintf (fp, " | {<end> "); // Statements end part removed.
        // Print Live variables at the end.
        len = get_Set_Bit_Indices (bba->BB[i].live_out, arr);
        
        if (len > 0) {
            fprintf (fp, " %s", get_Var_As_String (arr[0], st));
        }
        for (j = 1; j < len; j++) {
            fprintf (fp, " | %s",  get_Var_As_String (arr[j], st));
        }

        fprintf (fp, "}}\"];\n");
    }
    
    fprintf (fp, "\n\n");
    if (bba->total_BBs > 0)
        fprintf (fp, "b -> bb0;\n");// Start to first BB.
    else
        fprintf (fp, "b -> e;\n");// Start to first BB.

    
        
    // Print the edges:
    for (i = 0; i < bba->total_BBs; i++)
    {
        switch (bba->BB[i].bb_Type)
        {
            case bb_NORM:
                fprintf (fp, "bb%d -> bb%d;\n", i, bba->BB[i].trueNext);
                break;
            case bb_COND:
                fprintf (fp, "bb%d -> bb%d;\n", i, bba->BB[i].trueNext);
                fprintf (fp, "bb%d -> bb%d;\n", i, bba->BB[i].falseNext);
                break;
            case bb_LAST:
                fprintf (fp, "bb%d -> e;\n", i);
                break;
            default:
                printf ("plot_BB_Array ():\n");
                printf ("BB=%d has invalid type.\n", i+2);
                printf ("Fatal Error. Exiting\n\n");
                exit (1);
        }
    }
    
    fprintf (fp, "\n\n}\n");
    
    fclose (fp);
}


/** Inputs:
 *      bba: Array of basic blocks.
 *      st: Symbol Table.
 *      bI: Index of the basic block whose statements shall be printed.
 *      fp: Pointer to the file to which statements shall be printed.
 *  Outputs:
 *      returns nothing.
 *  Functionality:
 *      Prints all the statements of the basic block bI in the format
 *        as per the dot language for gui of control flow graph.
 * */
void plot_BB_Statements (BB_Array *bba, Symbol_Table *st, const int bI,
                        const FILE *fp)
{
    /******************* FUNCTION DECLARATIONS *********************/
    // Returns string for the variable at index "index" in symbol table.
    char *get_Var_As_String (int index, Symbol_Table *st);

    // Returns the expression represented by expr tree "root" as string.
    char *get_Expr_As_String (Expr *root, Symbol_Table *st);
    /***************************************************************/

    int sI; // Statement.
    char *e; // Expressin associated to a statement.
    char *v; // A variable.
    
    // For each statement.
    for (sI = 0; sI < bba->BB[bI].total_Stmnts; sI++)
    {
        fprintf (fp, "\t | {"); // Begin statement
        
        switch (bba->BB[bI].stmnt[sI].sType)
        {
            case s_ASGNMT_BI:
            case s_ASGNMT_ID:
            case s_ASGNMT_UN:
                v = get_Var_As_String (bba->BB[bI].stmnt[sI].var, st);
                e = get_Expr_As_String (bba->BB[bI].stmnt[sI].expr, st);
                
                fprintf (fp, "%s = %s", v, e);
                break;
                
            case s_SCANF:
                v = get_Var_As_String (bba->BB[bI].stmnt[sI].var, st);
                fprintf (fp, "input (%s)", v);
                break;

            case s_PRINTF:
                v = get_Var_As_String (bba->BB[bI].stmnt[sI].var, st);
                fprintf (fp, "output (%s)", v);
                break;
            
            default:
                fprintf (fp, "ERROR");
        
        }
        fprintf (fp, "} "); // End statement.
    }
    if (bba->BB[bI].total_Stmnts == 0)
    {
        fprintf (fp, " | {} ");
    }
}

/******** OUTPUT BASIC BLOCKS ARRAY ***********************************/

/** Input:
 *      bba:  pointer to the basic block array.
 *      st:   pointer to the symbol table.
 *      file: name of the file to which the bb array should be printed
 *            as a string.
 *  Output:
 *      prints the basic block array to the file specified in file.
 * */
void print_BB_Array_To_File (BB_Array *bba, Symbol_Table *st, 
                                char *file)
{
    /******************* FUNCTION DECLARATIONS *********************/

    // Returns string for the variable at index "index" in symbol table.
    char *get_Var_As_String (int index, Symbol_Table *st);

    // Returns the expression represented by expr tree "root" as string.
    char *get_Expr_As_String (Expr *root, Symbol_Table *st);

    // Returns the number of enteries made to arr.
    // Arr will contain the indices coressponding to 1 in bv.
    int get_Set_Bit_Indices (Bit_Vector bv, int *arr);

    // Returns the string represting bit vector bv.
    extern char *bit_Vector_To_String_BB (Bit_Vector bv, BB_Array *bba);

    // revaluates u to bI and sI.
    void unpair_Indices (int u, int *bI, int *sI);

    // Prints the statements of the BB bI to fp.
    void print_statements (BB_Array *bba, const int bI,
                    Symbol_Table *st, FILE *fp);

    // Prints the type of the basic block bI.
    void print_BB_type (BB_Array *bba, const int bI,
                    Symbol_Table *st, FILE *fp);

    // Prints the successors of the basic block bI to fo.
    void print_BB_successors (BB_Array *bba, const int bI, FILE *fo);
    /***************************************************************/
    // open the output file.
    FILE *fo= fopen (file, "w+");
    if (fo == NULL)
    {
        printf ("print_BB_Array_To_File:\n");
        perror (file);
        printf ("Error, Exiting without printing Basic Block Array.\n");
        return ;
    }
    int i, j, k;
    
    fprintf (fo, "\nPrints the Basic blocks array, along with reaching");
    fprintf (fo, " definitions. \n");
    fprintf (fo, "\"$\" is printed at end of statements in a BB.\n");
    fprintf (fo, "After \"$\", \"-\" is printed for a \"0\", and \"1\" ");
    fprintf (fo, " is printed for \"1\".\n\n");
    
    
    // The Statement Definitions array.
    fprintf (fo, "\nStatement-Definition Array:\nFormat:\n");
    fprintf (fo, "[[BASIC BLOCK INDEX, as in .cfg file]]\n");
    fprintf (fo, "[Uniqe index of statement, Index of the statement ");
    fprintf (fo, "in BB] <defined variable>;\n\n\n");
    for (i = 0, j = 0; i < (MAX_BB_STMNTS * MAX_BBs); i++)
    {
        if (i % MAX_BB_STMNTS == 0)
        {
            fprintf (fo, "[[BB %d]]\n", j + 2);
            j++;
        }
           
        if (bba->def_Var[i] != -1)
            fprintf (fo, "[%3d, %2d] %s; \t", i, i % MAX_BB_STMNTS + 1,
                             get_Var_As_String (bba->def_Var[i], st));
        else
            fprintf (fo, "[%3d, %2d] -; \t", i, i % MAX_BB_STMNTS + 1);
            
        if (i % 5 == 4)
            fprintf (fo, "\n");
            
        if (i % MAX_BB_STMNTS == (MAX_BB_STMNTS - 1))
            fprintf (fo, "\n");
    }
    
    // File opened success fully. Print the basic block array.
    fprintf (fo, "\nTotal Number of Basic blocks = %d\n", 
                bba->total_BBs);
    
    // For the bits set in the Din or Dout of a BB.
    int arr[MAX_STMNTS];
    int len;
    int bI, sI;

    // For each Basic Block
    for (i = 0; i < bba->total_BBs; i++)
    {
        // Print the index of the basic block, along separator.
        fprintf (fo, "\n**********************************************\n");
        fprintf (fo, "**********************************************\n");
        fprintf (fo, "BB = %d\n", i+2);
        
        print_BB_type (bba, i, st, fo);

        fprintf (fo, "\nParent Count = %d\n", bba->BB[i].parent_Count);
        // Print the parents.
        for (j = 0; j < bba->BB[i].parent_Count; j++)
        {
            fprintf (fo, "%d, ", bba->BB[i].parent[j] + 2);
        }
        fprintf (fo, "\n\n");
        
        // Live variables at the beginning.
        fprintf (fo, "Live Variables At Beginning:\t");
        len = get_Set_Bit_Indices (bba->BB[i].live_in, arr);
        for (j = 0; j < len; j++)
        {
            fprintf (fo, "%5s; ",  get_Var_As_String (arr[j], st));
        }
        fprintf (fo, "\n\n");

        // Print the input bit vector.
        fprintf (fo, "\nDin:");
        fprintf (fo, "%s", bit_Vector_To_String_BB (bba->BB[i].Din,
                                                        bba));

        // Reaching definitions at the beginning of BB-i.
        len = get_Set_Bit_Indices (bba->BB[i].Din, arr);
        for (j = 0; j < len; j++)
        {
            if (j % 5 == 0)
                fprintf (fo, "\n\t\t");
            unpair_Indices (arr[j], &bI, &sI);
            fprintf (fo, "[%2d, %2d](%5s);\t", bI + 2, sI + 1,
                         get_Var_As_String (bba->def_Var[arr[j]], st));
        }
        fprintf (fo, "\t\t\t");
        fprintf (fo, "\n-----------------------------------------\n\n");
        
        print_statements (bba, i, st, fo);
        
        fprintf (fo, "\n-----------------------------------------\n");
        fprintf (fo, "Dout:");
        fprintf (fo, "%s", bit_Vector_To_String_BB (bba->BB[i].Dout,
                                                            bba));

        // Reaching Definitions at the end.
        len = get_Set_Bit_Indices (bba->BB[i].Dout, arr);
        for (j = 0; j < len; j++)
        {
            if (j % 5 == 0)
                fprintf (fo, "\n\t\t");
            unpair_Indices (arr[j], &bI, &sI);
            fprintf (fo, "[%2d, %2d](%5s);\t", bI + 2, sI + 1,
                         get_Var_As_String (bba->def_Var[arr[j]], st));
        }
        fprintf (fo, "\n\n");

        // Live variables at the end.
        fprintf (fo, "Live Variables At End:\t");
        len = get_Set_Bit_Indices (bba->BB[i].live_out, arr);
        for (j = 0; j < len; j++)
        {
            fprintf (fo, "%5s; ",  get_Var_As_String (arr[j], st));
        }
        fprintf (fo, "\n\n");

        print_BB_successors (bba, i, fo);
    }
    
    fclose (fo);
}


/** Input:
 *      st:   pointer to the symbol table.
 *      file: name of the file to which symbol table should be printed
 *            as a string.
 *  Output:
 *      prints the symbol table to the file specified in file.
 * */
void print_ST_To_File (Symbol_Table *st, char *file)
{
    /******************* FUNCTION DECLARATIONS *********************/

    // Returns the string represting bit vector bv.
    extern char *bit_Vector_To_String_ST (Bit_Vector bv);

    // Returns the number of enteries made to arr.
    // Arr will contain the indices coressponding to 1 in bv.
    int get_Set_Bit_Indices (Bit_Vector bv, int *arr);

    // revaluates u to bI and sI.
    void unpair_Indices (int u, int *bI, int *sI);

    /***************************************************************/
    // open the output file.
    FILE *fo= fopen (file, "w+");
    if (fo == NULL)
    {
        printf ("print_ST_To_File:\n");
        perror (file);
        printf ("Error, Exiting without printing Symbol Table.\n");
        return ;
    }
    
    // Output file opened successfully, print symbol table.
    int i, j;
    int bI, sI;
    char *str;
    int arr[MAX_BB_STMNTS * MAX_BBs];
    int len;
    
    fprintf (fo, "In Definied Statements: BB = BB Index in Program + 2 ");
    fprintf (fo, "and ST = Statement Index in Program + 1.\n");
    fprintf (fo, "This is done to match the basic block and statement ");
    fprintf (fo, "numbering in the input .cfg file. \n");
    
    fprintf (fo, "\n\nIndex  Name\t\t\n");
    // For each symbol in the symbol table.
    for (i = 0; i < st->total_Symbols; i++)
    {
        fprintf (fo, "%5d  %-7s\t", i, st->symbol [i].vName);
        fprintf (fo, "Bit Vector:\n\t\t\t\t");
        str = bit_Vector_To_String_ST (st->symbol [i].def);
        // printf ("%s\n", str);
        fprintf (fo, "%s\n", str); 
        
        fprintf (fo, "\n\t\t\t\tDefined in Statements: unified_Index , [BB, ST]");
        len = get_Set_Bit_Indices (st->symbol[i].def, arr);
        
        fprintf (fo, "\n\t\t\t\t");
        for (j = 0; j < len; j++)
        {
            unpair_Indices (arr[j], &bI, &sI);
            fprintf (fo, "%d, [%d, %d]; ", arr[j], bI+2, sI+1);
            if (j % 5 == 4)
                fprintf (fo, "\n\t\t\t\t");
        }
        fprintf (fo, "\n");
        
        fprintf (fo, "\n\t\t\t\t************************************************\n\n");
    }
    
    fclose (fo);
}



/** Prints the type of the basic block bI and associated condition if
 *    bI is a conditional basic block.
 * */
void print_BB_type (BB_Array *bba, const int bI,
                    Symbol_Table *st, FILE *fp)
{
    /******************* FUNCTION DECLARATIONS *********************/
    // Returns the expression represented by expr tree "root" as string.
    char *get_Expr_As_String (Expr *root, Symbol_Table *st);
    /***************************************************************/
    // Print the type of the basic block, and condition if necc.
    switch (bba->BB [bI].bb_Type)
    {
        case bb_NORM:
            fprintf (fp, "BB Type = NORMAL\n");
            break;
        case bb_COND:
            fprintf (fp, "BB Type = CONDITONAL\n");
            fprintf (fp, "Associated condition: %s\n", 
                    get_Expr_As_String (bba->BB [bI].cond, st));
            break;
        case bb_LAST:
            fprintf (fp, "BB Type = LAST \n");
            break;
        default:
            fprintf (fp, "BB Type is not known, an error. BB = %d\n", 
                        bI+2);
            fprintf (fp, "Exception.. Continuing.\n\n");
    }
}



/** Inputs:
 *    bba: basic blocks array.
 *    bI : index of the basic block whose successors are to be printed.
 *    fo:  Pointer to the file in which the output should be printed.
 *  Outputs:
 *    Returns nothing.
 *  Functionality:
 *    Prints the type of the basic block bI to the file fo.
 * */
void print_BB_successors (BB_Array *bba, const int bI, FILE *fo)
{
    // Print the true and the false successor.
    switch (bba->BB [bI].bb_Type)
    {
        case bb_NORM:
            fprintf (fo, "Successor = %d\n", bba->BB [bI].trueNext+2);
            break;
        case bb_COND:
            fprintf (fo, "True Successor = %d\nFalse Successor = %d\n", 
                            bba->BB [bI].trueNext+2, 
                            bba->BB [bI].falseNext+2);
            break;
        case bb_LAST:
            fprintf (fo, "*LAST BASIC BLOCK, NO SUCCESSOR.\n");
            break;
        default:
            // Error
            fprintf (fo, "ERROR: Invalid type.\n");
    }
}



/** Prints the statements of the basic block bI to the file fp.
 * */
void print_statements (BB_Array *bba, const int bI,
                    Symbol_Table *st, FILE *fp)
{
    /******************* FUNCTION DECLARATIONS *********************/
    // Returns string for the variable at index "index" in symbol table.
    char *get_Var_As_String (int index, Symbol_Table *st);

    // Returns the expression represented by expr tree "root" as string.
    char *get_Expr_As_String (Expr *root, Symbol_Table *st);
    /***************************************************************/
    int sI; // Index of a statement. 
    
    fprintf (fp, "Total number of statements = %d\n",
                bba->BB [bI].total_Stmnts);
    fprintf (fp, "S. No.  Statement\n");
    // For each statement in the basic block.
    for (sI = 0; sI < bba->BB [bI].total_Stmnts; sI++)
    {
        // Print based on statement type.
        switch (bba->BB [bI].stmnt [sI].sType)
        {
            case s_ASGNMT_BI:
            case s_ASGNMT_UN:
            case s_ASGNMT_ID:
                fprintf (fp, "%4d    %s = %s;\n", sI+1, 
                    get_Var_As_String (
                        bba->BB [bI].stmnt [sI].var, st),
                    get_Expr_As_String (
                        bba->BB [bI].stmnt [sI].expr, st));
                break;
            case s_SCANF:
                fprintf (fp, "%4d    scanf:   %s\n", sI+1,
                    get_Var_As_String (
                        bba->BB [bI].stmnt [sI].var, st));
                /* Print all the varialbes involved.
                for (k = 0; k < bba->BB [i].stmnt [j].vCount; k++)
                {
                    fprintf (fo, "%s, ", 
                        get_Var_As_String (
                         bba->BB [i].stmnt [j].iList [k], st));
                }
                fprintf (fo, "\n");*/
                break;
            case s_PRINTF:
                fprintf (fp, "%4d    printf:   %s\n", sI+1,
                    get_Var_As_String (
                        bba->BB [bI].stmnt [sI].var, st));
                /* Print all the varialbes involved.
                for (k = 0; k < bba->BB [i].stmnt [j].vCount; k++)
                {
                    fprintf (fo, "%s, ", 
                        get_Var_As_String (
                         bba->BB [i].stmnt [j].iList [k], st));
                }
                fprintf (fo, "\n");*/
                break;
            case s_RETURN:
                fprintf (fp, "%4d    return; \n", sI+1);
                break;
            default:
                fprintf (fp, "%4d has Invalid type.\n", sI+1);
        }
    }
}



/** Inputs:
 *      bba:  pointer to the basic block array.
 *      st:   pointer to the symbol table.
 *      fp: name of the file to which the bb array should be printed
 *            as a string.
 *  Output:
 *   returns nothing.
 * 
 *   Prints the basic blocks array to the file specified in fp.
 *   Prints the variables live at the beginning and at the end of each
 *     BB. Doesn't print the corresponding bit vectors.
 * */
void print_BB_Array_To_File_Without_Reaching_Defs (BB_Array *bba, 
                                         Symbol_Table *st, char *file)
{
    /******************* FUNCTION DECLARATIONS *********************/
    // Returns string for the variable at index "index" in symbol table.
    char *get_Var_As_String (int index, Symbol_Table *st);

    // Returns the number of enteries made to arr.
    // Arr will contain the indices coressponding to 1 in bv.
    int get_Set_Bit_Indices (Bit_Vector bv, int *arr);

    // Prints the statements of the BB bI to fp.
    void print_statements (BB_Array *bba, const int bI,
                    Symbol_Table *st, FILE *fp);

    // Prints the type of the basic block bI.
    void print_BB_type (BB_Array *bba, const int bI,
                    Symbol_Table *st, FILE *fp);
    
    // revaluates u to bI and sI.
    void unpair_Indices (int u, int *bI, int *sI);

    // Prints the successors of the basic block bI to fo.
    void print_BB_successors (BB_Array *bba, const int bI, FILE *fo);
    /***************************************************************/
    // open the output file.
    FILE *fo= fopen (file, "w+");
    if (fo == NULL)
    {
        printf ("print_BB_Array_To_File_Without_Reaching_Defs:\n");
        perror (file);
        printf ("Error, Exiting without printing Basic Block Array.\n");
        return ;
    }
    int i, j, k;
    
    fprintf (fo, "\nPrints the Basic blocks array along with the live variables\n");
    fprintf (fo, "and the reaching definitions.\n");
    fprintf (fo, "A reaching definition is represented as [i, j] where i is the\n");
    fprintf (fo, "index of the basic block to which the defition belongs and j is\n");
    fprintf (fo, "the index of the definition in the basic block i.\n\n");
    
    // Print the basic block array.
    fprintf (fo, "\nTotal Number of Basic blocks = %d\n\n", 
                bba->total_BBs);
    
    // For the bits set in the Din or Dout of a BB.
    int arr[MAX_STMNTS];
    int len;
    int bI, sI;

    // For each Basic Block
    for (i = 0; i < bba->total_BBs; i++)
    {
        // Print the index of the basic block, along separator.
        fprintf (fo, "\n**********************************************\n");
        fprintf (fo, "**********************************************\n");
        fprintf (fo, "BB = %d\n", i+2);
        fprintf (fo, "**********************************************\n");
        
        print_BB_type (bba, i, st, fo);

        fprintf (fo, "\nParent Count = %d; [", bba->BB[i].parent_Count);
        // Print the parents.
        for (j = 0; j < bba->BB[i].parent_Count; j++)
        {
            fprintf (fo, "%d, ", bba->BB[i].parent[j] + 2);
        }
        fprintf (fo, "]\n\n");
        
        // Live variables at the beginning.
        fprintf (fo, "Live Variables At Beginning:  {");
        len = get_Set_Bit_Indices (bba->BB[i].live_in, arr);
        for (j = 0; j < len; j++)
        {
            fprintf (fo, "%s; ",  get_Var_As_String (arr[j], st));
        }
        fprintf (fo, "}\n");
        
        
        // Reaching Definitions at the beginning.
        fprintf (fo, "Reaching Definitions At Beginning:  {");
        len = get_Set_Bit_Indices (bba->BB[i].Din, arr);
        for (j = 0; j < len; j++)
        {
            unpair_Indices (arr[j], &bI, &sI);
            fprintf (fo, "[%d, %d]; ",  bI + 2, sI + 1);
        }
        fprintf (fo, "}\n");
        
        

        // Print the statements of the basic block.
        fprintf (fo, "\n-----------------------------------------\n");
        print_statements (bba, i, st, fo);
        fprintf (fo, "\n-----------------------------------------\n");
        fprintf (fo, "\n");

        // Live variables at the end.
        fprintf (fo, "Live Variables At End: {");
        len = get_Set_Bit_Indices (bba->BB[i].live_out, arr);
        for (j = 0; j < len; j++)
        {
            fprintf (fo, "%s; ",  get_Var_As_String (arr[j], st));
        }
        fprintf (fo, "}\n");


        // Reaching Definitions at the end.
        fprintf (fo, "Reaching Definitions At End:  {");
        len = get_Set_Bit_Indices (bba->BB[i].Dout, arr);
        for (j = 0; j < len; j++)
        {
            unpair_Indices (arr[j], &bI, &sI);
            fprintf (fo, "[%d, %d]; ",  bI + 2, sI + 1);
        }
        fprintf (fo, "}\n\n");

        // Print the successors.
        print_BB_successors (bba, i, fo);
    }
    
    if (fclose (fo))
        perror ("print_BB_Array_To_File_Without_Reaching_Defs (): while closing");
}





/** Inputs:
 *    bba, st: basic block array and symbol table respectively.
 *    file: name of the file to which the output should be printed.
 *  Outputs:
 *    returns nothing.
 *  Functionality:
 *    Prints the Def and Use Sets of all the basic blocks to the file 
 *       specified in OUT_DEF_USE_SETs.
 * */
void print_Def_Use_sets_live_var_analyasis (BB_Array *bba, 
                            Symbol_Table *st, Def_Use *du, char *file)
{
    /******************* FUNCTION DECLARATIONS *********************/
    // Returns string for the variable at index "index" in symbol table.
    char *get_Var_As_String (int index, Symbol_Table *st);

    // Returns the number of enteries made to arr.
    // Arr will contain the indices coressponding to 1 in bv.
    int get_Set_Bit_Indices (Bit_Vector bv, int *arr);

    // Prints the statements of the BB bI to fp.
    void print_statements (BB_Array *bba, const int bI,
                    Symbol_Table *st, FILE *fp);

    // Prints the type of the basic block bI.
    void print_BB_type (BB_Array *bba, const int bI,
                    Symbol_Table *st, FILE *fp);
    
    // Prints the successors of the basic block bI to fo.
    void print_BB_successors (BB_Array *bba, const int bI, FILE *fo);
    /***************************************************************/
    
    // open the output file.
    FILE *fo= fopen (file, "w+");
    if (fo == NULL)
    {
        printf ("print_Def_Use_sets_live_var_analyasis:\n");
        perror (file);
        printf ("Error, Exiting without printing Basic Block Array.\n");
        return ;
    }
    int i, j, k;
    
    fprintf (fo, "\nPrints the Basic blocks array along with the DEF and");
    fprintf (fo, " USE sets of BBs used in Live Variable Analysis. \n\n\n");
    
    
    // Print the basic block array.
    fprintf (fo, "\nTotal Number of Basic blocks = %d\n\n", 
                bba->total_BBs);
    
    // For the bits set in the Din or Dout of a BB.
    int arr[MAX_STMNTS];
    int len;
    int bI, sI;

    // For each Basic Block
    for (i = 0; i < bba->total_BBs; i++)
    {
        // Print the index of the basic block, along separator.
        fprintf (fo, "\n**********************************************\n");
        fprintf (fo, "**********************************************\n");
        fprintf (fo, "BB = %d\n", i+2);
        fprintf (fo, "**********************************************\n");
        
        print_BB_type (bba, i, st, fo);

        fprintf (fo, "\nParent Count = %d; [", bba->BB[i].parent_Count);
        // Print the parents.
        for (j = 0; j < bba->BB[i].parent_Count; j++)
        {
            fprintf (fo, "%d, ", bba->BB[i].parent[j] + 2);
        }
        fprintf (fo, "]\n\n");
        
        // Print the statements of the basic block.
        fprintf (fo, "\n-----------------------------------------\n");
        print_statements (bba, i, st, fo);
        fprintf (fo, "\n-----------------------------------------\n");
        fprintf (fo, "\n");

        // Def set of the BB.
        fprintf (fo, "Defined prior to any use: [");
        len = get_Set_Bit_Indices (du[i].def, arr);
        for (j = 0; j < len; j++)
        {
            fprintf (fo, "%5s; ",  get_Var_As_String (arr[j], st));
        }
        fprintf (fo, "]\n\n");

        // Use set of the BB.
        fprintf (fo, "Used prior to any definition: [");
        len = get_Set_Bit_Indices (du[i].use, arr);
        for (j = 0; j < len; j++)
        {
            // printf ("BB= %d \n", i);
            fprintf (fo, "%5s; ",  get_Var_As_String (arr[j], st));
        }
        fprintf (fo, "]\n\n");

        print_BB_successors (bba, i, fo);
    }
    
    if (fclose (fo))
        perror ("print_Def_Use_sets_live_var_analyasis (): While closing the file: ");
}



/** Inputs:
 *      bba: An array of basic blocks
 *      st: The symbol table.
 *      file: Name of the file to which output shall be printed.
 *  Outputs:
 *     Returns nothing.
 *  Functionality: 
 *     Prints the contents of the array def_Var which maps for each
 *        defition/statement the variable defined by it.
 * */
void print_Statement_Var_Def_Array (BB_Array *bba, Symbol_Table *st,
                                    char *file)
{
    /******************* FUNCTION DECLARATIONS *********************/
    // Returns string for the variable at index "index" in symbol table.
    char *get_Var_As_String (int index, Symbol_Table *st);

    // revaluates u to bI and sI.
    void unpair_Indices (int u, int *bI, int *sI);
    /***************************************************************/
    FILE *fo= fopen (file, "w+");
    if (fo == NULL)
    {
        printf ("print_Statement_Var_Def_Array ():\n");
        perror (file);
        printf ("Error, Exiting without printing statement and defined variable array.\n");
        return ;
    }
    
    fprintf (fo, "\nThis file contains the variables defined by the statements.\n");
    fprintf (fo, "If a statement doesn't define any variable, then it is not printed.\n");
    fprintf (fo, "A statement is represted as a basic block and statement index in \n");
    fprintf (fo, "the basic block pair.\n\n");
    
    int i;
    int vI, bI, sI; // indices of a variable, basic block and statement resp.
    
    fprintf (fo, "Statement\t Variable defined\n");
    
    // For each definition in the def var array:
    for (i = 0; i < (MAX_STMNTS); i++)
    {
        vI = bba->def_Var[i]; // Variable defined by the statement i.
        
        if (vI != -1)
        {
            unpair_Indices (i, &bI, &sI);
            fprintf (fo, "[%d, %d]\t\t%s\n", bI, sI, 
                                            get_Var_As_String (vI, st));
        }
        // else
        //    fprintf (fo, "[%d, %d]\t\t-1\n", bI, sI);
            
    }
    
    fclose (fo);
}






/*** PRINTING PRES+ TO A TEXT FILE ***********************************/


/** Input:
 *      pp:  pointer to PRES+.
 *      pp_obb: pointer to the array where i-th element contains indices
 *          of places and transitions corressponding to i-th BB.
 *      bI:  index of the current basic block.
 *      st:  pointer to the Symbol Table.
 *  Output:
 *      Returns nothing.
 *    Prints the PRES+ corresponding to basic block bI, in the file
 *  defined in PRINT_PP_BB.
 * */
void print_PP_BB_Wise (Pres_Plus *pp, PP_One_BB *pp_obb, int bI, 
                        Symbol_Table *st, char *output_file)
{
    /******************* FUNCTION DECLARATIONS *********************/
    // Prints places and transitions of specified BB to specified file.
    void print_Places_And_Transitions (Pres_Plus *pp, PP_One_BB *pp_obb, 
                                    int bI, Symbol_Table *st, FILE *fp);
    /***************************************************************/
    
    
    FILE *fp = fopen (output_file, "a+");
    if (fp == NULL)
    {
        printf ("print_PP_BB_Wise:\n");
        perror (output_file);
        printf ("Returning without printing.\n\n");
        return;
    }

    fprintf (fp, "\n\n**********************************************\n");
    fprintf (fp, "**********************************************\n");
    fprintf (fp, "BB = %d\n", bI+2);
    
    fprintf (fp, "Number of Places belonging to this BB = %d.\n", 
                                                    pp_obb[bI].c_PL);
    fprintf (fp, "Number of Transitions belonging to this BB = %d.\n", 
                                                    pp_obb[bI].c_TR);
    
    // Print Input places:
    int i;
    fprintf (fp, "\nInput Places: count = %d\n", pp_obb[bI].c_in);
    for (i = 0; i < pp_obb[bI].c_in; i++)
    {
        fprintf (fp, "P%d,\t", pp_obb[bI].in[i]);
    }
    
    // Print output transitions.:
    fprintf (fp, "\n\nOutput Transtions: count = %d\n", pp_obb[bI].c_out);
    for (i = 0; i < pp_obb[bI].c_out; i++)
    {
        fprintf (fp, "t%d,\t", pp_obb[bI].out[i]);
    }
    fprintf (fp, "\n\n");
    
    print_Places_And_Transitions (pp, pp_obb, bI, st, fp);

    fclose (fp);
}



/** Input:
 *      pp:  pointer to PRES+.
 *      pp_obb: pointer to the array where i-th element contains indices
 *          of places and transitions corressponding to i-th BB.
 *      bI:  index of the current basic block.
 *      sI:  index of the statement for which to construct the PRES+.
 *      st:  pointer to the Symbol Table.
 *  Output:
 *      Returns nothing.
 *    Prints the PRES+ corresponding to basic block bI, in the file
 *  defined in PRINT_PP_ST.
 * */
void print_PP_Statement_Wise (Pres_Plus *pp, PP_One_BB *pp_obb, 
                                int bI, int sI, Symbol_Table *st)
{
    /******************* FUNCTION DECLARATIONS *********************/
    // Prints places and transitions of specified BB to specified file.
    void print_Places_And_Transitions (Pres_Plus *pp, PP_One_BB *pp_obb, 
                                    int bI, Symbol_Table *st, FILE *fp);
    /***************************************************************/

    FILE *fp = fopen (PRINT_PP_ST, "a+");
    if (fp == NULL)
    {
        printf ("print_PP_Statement_Wise:\n");
        perror (PRINT_PP_ST);
        printf ("Returning without printing.\n\n");
        return;
    }
    
    static int last_BB = -1;
    if (last_BB != bI)
    { // print separator.
        fprintf (fp, "\n\n**********************************************\n");
        fprintf (fp, "**********************************************\n");
        fprintf (fp, "BB = %d\n", bI+2);
        last_BB = bI;
    }
    
    fprintf (fp, "\n**********************************************\n\n");
    fprintf (fp, "Printing PRES+ Upto Statement = %d.\n\n", sI+1);
    
    print_Places_And_Transitions (pp, pp_obb, bI, st, fp);
    
    fclose (fp);
}


/** Input:
 *      pp:  pointer to PRES+.
 *      pp_obb: pointer to the array where i-th element contains indices
 *          of places and transitions corressponding to i-th BB.
 *      bI:  index of the current basic block.
 *      st:  pointer to the Symbol Table.
 *  Output:
 *      Returns nothing.
 *    Prints the PRES+ corresponding to basic block bI, in the file
 *  defined in fp.
 * */
void print_Places_And_Transitions (Pres_Plus *pp, PP_One_BB *pp_obb, 
                                    int bI, Symbol_Table *st, FILE *fp)
{
    /******************* FUNCTION DECLARATIONS *********************/
    // Returns string for the variable at index "index" in symbol table.
    char *get_Var_As_String (int index, Symbol_Table *st);

    // Returns the expression represented by expr tree "root" as string.
    char *get_Expr_As_String (Expr *root, Symbol_Table *st);
    /***************************************************************/
    // Print the PRES+.
    int i, j, p, t; 
    
    // Print places:
    fprintf (fp, "----------\n");
    fprintf (fp, "Places:\n");
    fprintf (fp, "----------");
    
    // For each place corressponding to this BB.
    for (i = 0; i < pp_obb[bI].c_PL; i++)
    {
        p = pp_obb[bI].pl[i];
        fprintf (fp, "\nPlace = %d\n", p);
        switch (pp->place[p].p_Type)
        {
            case p_VAR:
                fprintf (fp, "Place Type = VARIABLE\n");
                fprintf (fp, "Associated variable = %s\n", 
                              get_Var_As_String (pp->place[p].var, st));
                break;
            case p_DUMMY:
                fprintf (fp, "Place Type = DUMMY\n");
                break;
            case p_SCANF:
                fprintf (fp, "Place Type = SCANF\n");
                fprintf (fp, "Associated variable = %s\n", 
                              get_Var_As_String (pp->place[p].var, st));
                break;
            case p_PRINTF:
                fprintf (fp, "Place Type = PRINTF\n");
                fprintf (fp, "Associated variable = %s\n", 
                              get_Var_As_String (pp->place[p].var, st));
                break;
            default:
                fprintf (fp, "Place Type = ERROR\n");
        }
        
        // Preset:
        fprintf (fp, "\nPreset:\n");
        for (j = 0; j < pp->place[p].c_Preset; j++)
            fprintf (fp, "T[%2d];\t", pp->place[p].preset[j]);

        // Postset:
        fprintf (fp, "\n\nPostset:\n");
        for (j = 0; j < pp->place[p].c_Postset; j++)
            fprintf (fp, "T[%2d];\t", pp->place[p].postset[j]);
        fprintf (fp, "\n");
        fprintf (fp, "----------------------------------------------\n");
    }
    
    // Print transitions.
    fprintf (fp, "\n-------------\n");
    fprintf (fp, "Transitions:\n");
    fprintf (fp, "-------------");
    
    // For each Transition.
    for (i = 0; i < pp_obb[bI].c_TR; i++)
    {
        t = pp_obb[bI].tr[i];
        fprintf (fp, "\nTransition = %d\n", t);
        switch (pp->transition[t].t_Type)
        {
            case t_EXPR:
                fprintf (fp, "Transition type = EXPRESSION\n");
                fprintf (fp, "Associated expression = %s\n", 
                      get_Expr_As_String (pp->transition[t].expr, st));
                break;
            case t_ID:
                fprintf (fp, "Transition type = IDENTITY\n");
                break;
            case t_DUMMY:
                fprintf (fp, "Transition type = DUMMY\n");
                break;
            default:
                fprintf (fp, "Transition type = ERROR\n");
        }

        // Print priority of the transition.
        switch (pp->transition[i].prio)
        {
            case pr_NORM:
                fprintf (fp, "Priority: NORM\n");
                break;
            case pr_HIGH:
                fprintf (fp, "Priority: HIGH\n");
                break;
            default:
                printf ("print_trans_py ():\n");
                printf ("Invalid priority of transition T%d.\n", i);
                printf ("Exception, Ignoring and continuing.\n");
                fprintf (fp, "Priority: ERROR\n");
        }
        
        // Preset:
        fprintf (fp, "\nPreset:\n");
        for (j = 0; j < pp->transition[t].c_Preset; j++)
            fprintf (fp, "P[%2d];\t", pp->transition[t].preset[j]);

        // Postset:
        fprintf (fp, "\n\nPostset:\n");
        for (j = 0; j < pp->transition[t].c_Postset; j++)
            fprintf (fp, "P[%2d];\t", pp->transition[t].postset[j]);
        fprintf (fp, "\n");
        fprintf (fp, "----------------------------------------------\n");
    }
    fprintf (fp, "\n");
}




/** Input:
 *      pp: The complete PRES+ net.
 *      st: The symbol table.
 *  Output:
 *      None:
 *  Functionality:
 *      Prints the whole PRES+ net to the file specified in 
 *    PRINT_PRES_PLUS.
 * */
void print_PP_Subnet_Wise (Pres_Plus *pp, PP_One_BB *pp_obb, BB_Array *bba,
                Symbol_Table *st)
{
    /******************* FUNCTION DECLARATIONS *********************/

    // Prints PRES+ coressponding to specified BB to file PRINT_PP_BB.
    void print_PP_BB_Wise (Pres_Plus *pp, PP_One_BB *pp_obb, int bI, 
                            Symbol_Table *st, char *output_file);
    /***************************************************************/

    int i, j;

    FILE *fp = fopen (PRINT_PP_SUBNET, "a+");
    if (fp == NULL)
    {
        printf ("print_PP_Subnet_Wise ():\n");
        perror (PRINT_PP_SUBNET);
        printf ("Returning without printing.\n\n");
        return;
    }

    fprintf (fp, "\nTotal Places = %d\n", pp->pl_count);
    fprintf (fp, "Total Transitions = %d\n\n", pp->tr_count);
    
    
    // Print all the input places:
    fprintf (fp, "Input places: \n");
    for (i = 0, j = 1; i < pp->pl_count; i++)
    {
        if (pp->place[i].c_Preset == 0)
        {
            fprintf (fp, "%d, ", i);
            if (j++ % 8 == 0)
                fprintf (fp, "\n");
        }
    }

    fclose (fp);
    

    for (i = 0; i < bba->total_BBs; i++)
        print_PP_BB_Wise (pp, pp_obb, i, st, PRINT_PRES_PLUS);

    
}



/** Input:
 *      The PRES+ model and The Symbol Table.
 *      file, file to which PRES+ shall be printed.
 *  Outputs:
 *      Returns Nothing.
 *  Functionality:
 *      Prints the PRES+ to the file specified int file.
 * */
void print_Pres_Plus (const Pres_Plus *pp, const Symbol_Table *st,
                        char *file)
{
    /******************* FUNCTION DECLARATIONS *********************/
    // Returns string for the variable at index "index" in symbol table.
    char *get_Var_As_String (int index, Symbol_Table *st);

    // Returns the expression represented by expr tree "root" as string.
    char *get_Expr_As_String (Expr *root, Symbol_Table *st);
    /***************************************************************/
    int i, j;
    int p, t; // Indices of a place and transition resp.
    
    FILE *fp = fopen (file, "w+");
    if (fp == NULL)
    {
        printf ("print_Pres_Plus:\n");
        perror (file);
        printf ("Returning without printing.\n\n");
        return;
    }

    fprintf (fp, "\nTotal Places = %d\n", pp->pl_count);
    fprintf (fp, "Total Transitions = %d\n\n", pp->tr_count);
    
    
    // Print all the input places:
    fprintf (fp, "Input places: \n");
    for (i = 0, j = 1; i < pp->pl_count; i++)
    {
        if (pp->place[i].c_Preset == 0)
        {
            fprintf (fp, "p%d, ", i);
            if (j++ % 8 == 0)
                fprintf (fp, "\n");
        }
    }
    
    // Print places:
    fprintf (fp, "\n----------\n");
    fprintf (fp, "Places:\n");
    fprintf (fp, "----------");
    
    // For each place corressponding to this BB.
    for (i = 0; i < pp->pl_count; i++)
    {
        p = i; // Just renaming i for clarity.
        fprintf (fp, "\nPlace = %d\n", p);
        switch (pp->place[p].p_Type)
        {
            case p_VAR:
                fprintf (fp, "Place Type = VARIABLE\n");
                fprintf (fp, "Associated variable = %s\n", 
                              get_Var_As_String (pp->place[p].var, st));
                break;
            case p_DUMMY:
                fprintf (fp, "Place Type = DUMMY\n");
                break;
            case p_SCANF:
                fprintf (fp, "Place Type = SCANF\n");
                fprintf (fp, "Associated variable = %s\n", 
                              get_Var_As_String (pp->place[p].var, st));
                break;
            case p_PRINTF:
                fprintf (fp, "Place Type = PRINTF\n");
                fprintf (fp, "Associated variable = %s\n", 
                              get_Var_As_String (pp->place[p].var, st));
                break;
            default:
                fprintf (fp, "Place Type = ERROR\n");
        }
        
        // Preset:
        fprintf (fp, "\nPreset:\n");
        for (j = 0; j < pp->place[p].c_Preset; j++)
            fprintf (fp, "T[%2d];\t", pp->place[p].preset[j]);

        // Postset:
        fprintf (fp, "\n\nPostset:\n");
        for (j = 0; j < pp->place[p].c_Postset; j++)
            fprintf (fp, "T[%2d];\t", pp->place[p].postset[j]);
        fprintf (fp, "\n");
        fprintf (fp, "----------------------------------------------\n");
    }
    
    // Print transitions.
    fprintf (fp, "\n-------------\n");
    fprintf (fp, "Transitions:\n");
    fprintf (fp, "-------------");
    
    // For each Transition.
    for (i = 0; i < pp->tr_count; i++)
    {
        t = i; // Renaming for clarity.
        fprintf (fp, "\nTransition = %d\n", t);
        switch (pp->transition[t].t_Type)
        {
            case t_EXPR:
                fprintf (fp, "Transition type = EXPRESSION\n");
                fprintf (fp, "Associated expression: %s\n", 
                      get_Expr_As_String (pp->transition[t].expr, st));
                break;
            case t_ID:
                fprintf (fp, "Transition type = IDENTITY\n");
                if (pp->transition[t].guard != NULL)
                    fprintf (fp, "Guard: %s\n", 
                      get_Expr_As_String (pp->transition[t].guard, st));
                break;
            case t_DUMMY:
                fprintf (fp, "Transition type = DUMMY\n");
                break;
            default:
                fprintf (fp, "Transition type = ERROR\n");
        }

        // Print priority of the transition.
        switch (pp->transition[i].prio)
        {
            case pr_NORM:
                fprintf (fp, "Priority: NORM\n");
                break;
            case pr_HIGH:
                fprintf (fp, "Priority: HIGH\n");
                break;
            default:
                printf ("print_trans_py ():\n");
                printf ("Invalid priority of transition T%d.\n", i);
                printf ("Exception, Ignoring and continuing.\n");
                fprintf (fp, "Priority: ERROR\n");
        }
        
        // Preset:
        fprintf (fp, "\nPreset:\n");
        for (j = 0; j < pp->transition[t].c_Preset; j++)
            fprintf (fp, "P[%2d];\t", pp->transition[t].preset[j]);

        // Postset:
        fprintf (fp, "\n\nPostset:\n");
        for (j = 0; j < pp->transition[t].c_Postset; j++)
            fprintf (fp, "P[%2d];\t", pp->transition[t].postset[j]);
        fprintf (fp, "\n");
        fprintf (fp, "----------------------------------------------\n");
    }
    fprintf (fp, "\n");
    
    fclose (fp);
}


/***************** For the python script *****************************/
/** Inputs:
 *      pp: The PRES+ net.
 *      st: The symbol table.
 *  Output:
 *      Returns Nothing.
 *      Prints the PRES+ net to a file specified in OUT_PY in the format
 *    described blow.
 * 
 *  Places:
 *  <Index of the place in PP.place[]> <Associated variable> <new line char>
 *  <Pre trans index 1> <Pre trans index 2>  <new line char>      
 *                          Indices of pre-transitions
 *                          in PP.transition[] separated by spaces.
 *  <Post trans index 1> <Post trans index 2>  <new line char>    
 *                          Indices of post-transitions
 *                          in PP.transition[] separated by spaces.
 *  <new line char>
 * 
 *  Transitions:
 *  <Index of the transition in PP.transition[]> <new line char> 
 *  E <Expression associated to the transition as a string> <new line char>
 *  G <Guard expression as a string.> <new line char>
 *  <Indicies of Pre-places separated by spaces.> <new line char>
 *  <Indicies of Post-places separated by spaces.> <new line char>
 *  <new line char>
 * 
 *  First all places are printed then all transitions.
 *  After the places and the transitions, for each variable have been
 *  printed, then the variables and their defining transitions are 
 *  printed from the pp_obb array.
 * */
 void print_PP_for_Python_Script (Pres_Plus *pp, Symbol_Table *st,
                                  PP_One_BB *pp_obb, BB_Array *bba)
 {
    /******************* FUNCTION DECLARATIONS *********************/
    // Prints all the places for the python script.
    void print_places_py (Pres_Plus *pp, Symbol_Table *st, FILE *fp);
    
    // Prints all the transitions for the python script.
    void print_trans_py (Pres_Plus *pp, Symbol_Table *st, FILE *fp);
    /***************************************************************/
    int i, j; // For iteration.
    int pI; // Index of a place.
    int tI; // Index of a transition.
    
    FILE *fp = fopen (OUT_PY, "w+");
    if (fp == NULL)
    {
        printf ("print_PP_for_Python_Script:\n");
        perror (OUT_PY);
        printf ("Returning without printing.\n\n");
        return;
    }
    
    // Print places.
    print_places_py (pp, st, fp);
    
    // Print transitions.
    print_trans_py (pp, st, fp);
    
    // Print all the symbols.
    fprintf (fp, "Symbols:\n");
    for (i = 0; i < st->total_Symbols; i++)
    {
        fprintf (fp, "%s\n", st->symbol[i].vName);
    }
    fprintf (fp, "Symbols-end:\n");
    
    /*/ Print transitions and defined variables.
    fprintf (fp, "Definitions:\n");
    for (i = 0; i < bba->total_BBs; i++)
    {
        // For each variable:
        for (j = 0; j < st->total_Symbols; j++)
        {
            if (pp_obb[i].ldt[j] != -1)
                fprintf (fp, "T%d %s\n", pp_obb[i].ldt[j], 
                                         st->symbol[j].vName); 
        }
    }
    fprintf (fp, "Definitions-end\n\n"); */
    fclose (fp);
}


/** Input: 
 *      pp: The PRES+ net.
 *      st: Symbol Table.
 *      fp: Pointer to file to which we want to write.
 *  Output:
 *      returns nothing.
 *  Prints all the transitions of the PRES+ pp to the file fp in the
 *  format specified in the function print_PP_for_Python_Script()
 * */
void print_trans_py (Pres_Plus *pp, Symbol_Table *st, FILE *fp)
{
    /******************* FUNCTION DECLARATIONS *********************/
    // Returns the expression represented by expr tree "root" as string.
    char *get_Expr_As_String (Expr *root, Symbol_Table *st);
    /***************************************************************/
    int i, j; // For iteration.
    
    /* Print all the transitions. 
     * A switch statement required because need to print based on type.
     ******************************************************************/
    fprintf (fp, "Transitions:\n");
    for (i = 0; i < pp->tr_count; i++)
    {
        // Index of the transition.
        fprintf (fp, "%d\n", i);
        
        // Based on type of the transition:
        switch (pp->transition[i].t_Type)
        {
            case t_EXPR:
                // Expression associated to the transition as a string.
                fprintf (fp, "Type: EXPR\n");
                fprintf (fp, "Expr: %s\n", get_Expr_As_String (
                                    pp->transition[i].expr, st));
                fprintf (fp, "Guard: \n");
                break;
            case t_ID:
                // Only ID transitions can have a guard.
                fprintf (fp, "Type: ID\n");
                fprintf (fp, "Expr: \n");
                if (pp->transition[i].guard != NULL)
                    fprintf (fp, "Guard: %s\n", get_Expr_As_String (
                                    pp->transition[i].guard, st));
                else
                    fprintf (fp, "Guard: \n");
                break;
            case t_DUMMY:
                fprintf (fp, "Type: DUMMY\n");
                fprintf (fp, "Expr: \n");
                fprintf (fp, "Guard: \n");
                break;
            default:
                printf ("print_trans_py ():\n");
                printf ("Invalid transition type of transition T%d.\n", i);
                printf ("Exception, Ignoring and continuing.\n");
                continue;
        }
        
        // Print priority of the transition.
        switch (pp->transition[i].prio)
        {
            case pr_NORM:
                fprintf (fp, "Priority: NORM\n");
                break;
            case pr_HIGH:
                fprintf (fp, "Priority: HIGH\n");
                break;
            default:
                printf ("print_trans_py ():\n");
                printf ("Invalid priority of transition T%d.\n", i);
                printf ("Exception, Ignoring and continuing.\n");
                fprintf (fp, "Priority: ERROR\n");
        }

        /* Print PRE places. *****************************************/
        fprintf (fp, "PRE: ");
        for (j = 0; j < pp->transition[i].c_Preset; j++)
        {
            fprintf (fp, "%d ", pp->transition[i].preset[j]);
        }
        fprintf (fp, "\n");
        
        /* Print POST places. ****************************************/
        fprintf (fp, "POST: ");
        for (j = 0; j < pp->transition[i].c_Postset; j++)
        {
            fprintf (fp, "%d ", pp->transition[i].postset[j]);
        }
        fprintf (fp, "\n\n");
    }
    fprintf (fp, "Transitions-end\n\n");
}


/** Input: 
 *      pp: The PRES+ net.
 *      st: Symbol Table.
 *      fp: Pointer to file to which we want to write.
 *  Output:
 *      returns nothing.
 *  Prints all the placess of the PRES+ pp to the file fp in the
 *  format specified in the function print_PP_for_Python_Script()
 * */
void print_places_py (Pres_Plus *pp, Symbol_Table *st, FILE *fp)
{
    /* Print all the places.      
     * A switch statement required because need to print based on type.  
     *****************************************************************/

    int i, j; // For iteration.
    for (i = 0; i < pp->pl_count; i++)
    {
        // Index of the place.
        fprintf (fp, "%d\n", i);
        switch (pp->place[i].p_Type)
        {
            case p_VAR:
            case p_SCANF:
            case p_PRINTF:
                // Print the variable associated to the place.
                fprintf (fp, "var: %s\n", st->symbol[pp->place[i].var].vName);
                break;
            case p_DUMMY:
                // Print the variable associated to the place.
                fprintf (fp, "var: DUMMY\n");
                break;
            default:
                printf ("print_places_py():\n");
                printf ("The type of the place P%d is invalid.\n", i);
                printf ("Exception, Ignoring and continuing.\n");
        }
    
        // PRE transitions of a place.
        fprintf (fp, "PRE: ");
        for (j = 0; j < pp->place[i].c_Preset; j++)
        {
            fprintf (fp, "%d ", pp->place[i].preset[j]);
        }
        fprintf (fp, "\n");
        
        // POST transitions of a place:
        fprintf (fp, "POST: ");
        for (j = 0; j < pp->place[i].c_Postset; j++)
        {
            fprintf (fp, "%d ", pp->place[i].postset[j]);
        }
        fprintf (fp, "\n\n");
    }
    fprintf (fp, "Places-end\n\n");
}




/** It doesn't have any role right now. 
 *  
 *  It is a table which stores for each variable, the transition a variable
 *    is defined in.
 * 
 *    The role of this table was to reduce time complexity, not to 
 *       provide new functionality, but complexity factor has been ignored
 *       for the time being.
 * * /
void print_Var_Def_Transition_Table (Pres_Plus *pp, Symbol_Table *st)
{
    FILE *fp = fopen (VAR_TRANS_DEF, "w+");
    if (fp == NULL)
    {
        perror ("print_Var_Def_Transition_Table: ");
        printf ("Returning without printing. \n");
        return;
    }
    
    int i, j;
    // For each variable:
    for (i = 0; i < st->total_Symbols; i++)
    {
        fprintf (fp, "Var = %s\n", st->symbol[i].vName);
        fprintf (fp, "Defined in transitions: ");
        // For each transition the variable is defined in:
        for (j = 0; j < pp->vds[i].tr_count; j++)
        {
            fprintf (fp, "%4d,", pp->vds[i].vt_map[j]);
        }
        fprintf (fp, "\n\n");
    }
    fclose (fp);
}*/
























