#include "Types.h"

/** Input:
 *  pp: The overall PRES+ net. The PRES+ subnet for the basic block bI
 *      is stored in it.
 *  subs: (subnets) Pointer to an array s.t. bI element contains indices of the
 *          places and transitions in pp.places and pp.transitions array
 *          corresponding to the subnet of the basic block bI.
 *  bI: Index of the basic block whose PRES+ which contains the statement
 *      sI.
 *  sI: Index of the statement whose PRES+ shall be constructed.
 *  bba: Pointer to the array of basic blocks.
 *  st: Pointer to the Symbol Table.
 *  Output:
 *     Constructs the PRES+ subnet for the statement at index sI of the
 *   basic block bI.
 *     The PRES+ subnet is stored in the overall PRES+ pointed by pp.
 *     The indices of the places and the transitions constructed for
 *   the subnet are stored in the subs array at the bI-th index.
 *     The subnet for the statement sI is attached to the subnet
 *   corresponding to the basic block bI.
 *
 *  ALGORITHM:
 *     Based on type of the statement (sI) a function is invoked to
 *   construct the PRES+ subnet for the statement.
 * */
void construct_PP_One_Stm(Pres_Plus *pp, PP_One_BB *pp_obb, BB_Array *bba,
                          int bI, int sI, Symbol_Table *st) {
  /******************* FUNCTION DECLARATIONS *********************/
  // Constructs PRES+ for a binary assignment statement.
  void construct_PP_Binary_Assignment(Pres_Plus * pp, PP_One_BB * pp_obb,
                                      BB_Array * bba, int bI, int sI,
                                      Symbol_Table *st);

  // Constructs PRES+ for a unary assignment statement.
  void construct_PP_Unary_Assignment(Pres_Plus * pp, PP_One_BB * pp_obb,
                                     BB_Array * bba, int bI, int sI,
                                     Symbol_Table *st);

  // Constructs PRES+ for an identity assignment statement.
  void construct_PP_Identity_Assignment(Pres_Plus * pp, PP_One_BB * pp_obb,
                                        BB_Array * bba, int bI, int sI,
                                        Symbol_Table *st);

  // Constructs PRES+ for a scanf statement.
  void construct_PP_Scanf(Pres_Plus * pp, PP_One_BB * pp_obb, BB_Array * bba,
                          int bI, int sI, Symbol_Table *st);

  // Constructs PRES+ for a printf statement.
  void construct_PP_Printf(Pres_Plus * pp, PP_One_BB * pp_obb, BB_Array * bba,
                           int bI, int sI, Symbol_Table *st);

  // Constructs PRES+ for a return statement.
  void construct_PP_Return(Pres_Plus * pp, PP_One_BB * pp_obb, BB_Array * bba,
                           int bI, int sI, Symbol_Table *st);

  // Prints the PRES+ to the file defined in PRINT_PP_ST.
  void print_PP_Statement_Wise(Pres_Plus * pp, PP_One_BB * pp_obb, int bI,
                               int sI, Symbol_Table *st);
  /***************************************************************/

  switch (bba->BB[bI].stmnt[sI].sType) {
  case s_ASGNMT_BI:
    construct_PP_Binary_Assignment(pp, pp_obb, bba, bI, sI, st);
    break;
  case s_ASGNMT_UN:
    construct_PP_Unary_Assignment(pp, pp_obb, bba, bI, sI, st);
    break;
  case s_ASGNMT_ID:
    construct_PP_Identity_Assignment(pp, pp_obb, bba, bI, sI, st);
    break;
  case s_SCANF:
    construct_PP_Scanf(pp, pp_obb, bba, bI, sI, st);
    break;
  case s_PRINTF:
    construct_PP_Printf(pp, pp_obb, bba, bI, sI, st);
    break;
  case s_RETURN:
    construct_PP_Return(pp, pp_obb, bba, bI, sI, st);
    break;
  default:
    // print error.
    printf("function: construct_PP_One_Statement:\n");
    printf("BB = %d, St = %d, Statement type is not correct.\n", bI + 2,
           sI + 1);
    printf("Exception; Continuing by ignoring the statement.\n\n");
  }

#ifdef PRINT_PP_ST
  print_PP_Statement_Wise(pp, pp_obb, bI, sI, st);
#endif
}

// Constructs PRES+ for a binary assignment statement.
void construct_PP_Binary_Assignment(Pres_Plus *pp, PP_One_BB *pp_obb,
                                    BB_Array *bba, int bI, int sI,
                                    Symbol_Table *st) {
  /******************* FUNCTION DECLARATIONS *********************/
  // Returns index of the first operand, -1 if doesn't exist.
  int get_First_Operand(BB_Array * bba, int bI, int sI);

  // Returns the index of the second operand, -1 if doesn't exist.
  int get_Second_Operand(BB_Array * bba, int bI, int sI);

  // Constructs a Dummy place, and adds it to the input array.
  int new_Dummy_Place(Pres_Plus * pp, PP_One_BB * pp_obb, int bI);

  // Constructs a place for the given variable.
  int construct_Place_For_Variable(Pres_Plus * pp, PP_One_BB * pp_obb, int bI,
                                   int vI);

  // Add a new transition to the PRES+ of bI, returns its index.
  int new_Transition(Pres_Plus * pp, PP_One_BB * pp_obb, int bI);

  // Adds given place to the preset of given transition.
  void add_To_Preset_Of_Transition(Pres_Plus * pp, int tI, int pI);

  // Adds given transtion to the postset of given place.
  void add_To_Postset_Of_Place(Pres_Plus * pp, int pI, int tI);
  /***************************************************************/
  // Defined variable.
  int vI = bba->BB[bI].stmnt[sI].var;

  // Indices of the two operands.
  int i1 = get_First_Operand(bba, bI, sI);
  int i2 = get_Second_Operand(bba, bI, sI);

  // Constructing places for the operands.
  int p1, p2, pD;
  p1 = p2 = pD = -1; // Initialize.

  if (i1 != -1) {
    p1 = construct_Place_For_Variable(
        pp, pp_obb, bI, i1); /*
                              * Constructs a place for variable i1, as well as
                              * links it to rest of the PRES+. */
  }

  if (i2 != -1) {
    p2 = construct_Place_For_Variable(pp, pp_obb, bI, i2);
  }

  // If both operands are literals.
  if (i1 == -1 && i2 == -1) {
    pD = new_Dummy_Place(
        pp, pp_obb,
        bI); /* Constructs a dummy
              * place and adds it to the input places array of bI. */
  }

  // Construct a transition for the expression.
  int t = new_Transition(pp, pp_obb, bI);
  pp->transition[t].t_Type = t_EXPR;
  pp->transition[t].expr = bba->BB[bI].stmnt[sI].expr;

  // Mark that t defines vI. No more used.
  // pp->vds[vI].vt_map[pp->vds[vI].tr_count++] = t;

  // Add p1, p2 and pD to the preset of t and viceversa, if they exist.
  if (p1 != -1) {
    add_To_Preset_Of_Transition(pp, t, p1);
    add_To_Postset_Of_Place(pp, p1, t);
  }

  if (p2 != -1) {
    add_To_Preset_Of_Transition(pp, t, p2);
    add_To_Postset_Of_Place(pp, p2, t);
  }

  if (pD != -1) {
    add_To_Preset_Of_Transition(pp, t, pD);
    add_To_Postset_Of_Place(pp, pD, t);
  }

  // Mark t as ldt of the variable defined in this statement.
  pp_obb[bI].ldt[vI] = t;
}

// Constructs PRES+ for a unary assignment statement.
void construct_PP_Unary_Assignment(Pres_Plus *pp, PP_One_BB *pp_obb,
                                   BB_Array *bba, int bI, int sI,
                                   Symbol_Table *st) {
  /******************* FUNCTION DECLARATIONS *********************/
  // Returns index of the first operand.
  int get_First_Operand(BB_Array * bba, int bI, int sI);

  // Constructs a Dummy place, and adds it to the input array.
  int new_Dummy_Place(Pres_Plus * pp, PP_One_BB * pp_obb, int bI);

  // Constructs a place for the given variable.
  int construct_Place_For_Variable(Pres_Plus * pp, PP_One_BB * pp_obb, int bI,
                                   int vI);

  // Add a new transition to the PRES+ of bI, returns its index.
  int new_Transition(Pres_Plus * pp, PP_One_BB * pp_obb, int bI);

  // Adds given place to the preset of given transition.
  void add_To_Preset_Of_Transition(Pres_Plus * pp, int tI, int pI);

  // Adds given transtion to the postset of given place.
  void add_To_Postset_Of_Place(Pres_Plus * pp, int pI, int tI);
  /***************************************************************/
  int vI = bba->BB[bI].stmnt[sI].var; // variable defined by sI.

  // Index of the operand.
  int i1 = get_First_Operand(bba, bI, sI);

  // Constructing place for the operand.
  int p;
  p = -1; // Initailzing to default value.

  if (i1 != -1) {
    p = construct_Place_For_Variable(pp, pp_obb, bI, i1);
  } else {
    p = new_Dummy_Place(pp, pp_obb, bI);
  }

  // Construct a transition for the expression on RHS.
  int t = new_Transition(pp, pp_obb, bI);
  pp->transition[t].t_Type = t_EXPR;
  pp->transition[t].expr = bba->BB[bI].stmnt[sI].expr;

  // Mark that t defines vI. No more used.
  // pp->vds[vI].vt_map[pp->vds[vI].tr_count++] = t;

  // Add t to the postset of p and vice versa.
  add_To_Postset_Of_Place(pp, p, t);
  add_To_Preset_Of_Transition(pp, t, p);

  // Set t as the latest defining transition for the variable on LHS.
  pp_obb[bI].ldt[vI] = t;
}

// Constructs PRES+ for an identity assignment statement.
void construct_PP_Identity_Assignment(Pres_Plus *pp, PP_One_BB *pp_obb,
                                      BB_Array *bba, int bI, int sI,
                                      Symbol_Table *st) {
  /******************* FUNCTION DECLARATIONS *********************/
  // Returns index of the first operand.
  int get_First_Operand(BB_Array * bba, int bI, int sI);

  // Constructs a place for the given variable.
  int construct_Place_For_Variable(Pres_Plus * pp, PP_One_BB * pp_obb, int bI,
                                   int vI);

  // Adds a new place to the PRES+ of bI, returns its index.
  int new_Place(Pres_Plus * pp, PP_One_BB * pp_obb, int bI);

  // Add a new transition to the PRES+ of bI, returns its index.
  int new_Transition(Pres_Plus * pp, PP_One_BB * pp_obb, int bI);

  // Adds given place to the preset of given transition.
  void add_To_Preset_Of_Transition(Pres_Plus * pp, int tI, int pI);

  // Adds given transtion to the postset of given place.
  void add_To_Postset_Of_Place(Pres_Plus * pp, int pI, int tI);

  // Returns the case about how to join vI to PP.
  Join_Case get_Link_Case(Pres_Plus * pp, PP_One_BB * pp_obb, int bI, int vI);

  /* Constructs a new input place and identity transition.
   * Joins the two input place as postset of this transition.
   * Retruns index of the new transition.*/
  int handle_New_Transition_Case(Pres_Plus * pp, PP_One_BB * pp_obb, int bI,
                                 int pI_O, int pI_N);

  // Add the given place to the input set of PRES+ for give BB.
  void handle_New_Variable_Case(Pres_Plus * pp, PP_One_BB * pp_obb, int bI,
                                int pI);

  // Returns the input place for variable vI, -1 if doesn't exits.
  int get_Input_Place(Pres_Plus * pp, PP_One_BB * pp_obb, int bI, int vI);
  /***************************************************************/
  int vI; // Defined variable Index.
  vI = bba->BB[bI].stmnt[sI].var;

  int i1; // Index of variable on RHS.
  i1 = get_First_Operand(bba, bI, sI);

  Join_Case jc = get_Link_Case(pp, pp_obb, bI, i1);

  int p_Old; // Used in new transition case.

  int ldt, p, t;
  switch (jc) {
  case c_JOIN:
    // Set LDT of i1 as LDT of vI.
    ldt = pp_obb[bI].ldt[i1];
    pp_obb[bI].ldt[vI] = ldt;

    // Mark that ldt of i1 also defines vI.
    // pp->vds[vI].vt_map[pp->vds[vI].tr_count++] = ldt;

    break;
  case c_NEW_TR:
    /* Prohibit construction of this new place.
     * It is an output place of a transition which is not used.
    p = new_Place (pp, pp_obb, bI);
    pp->place[p].p_Type = p_VAR;
    pp->place[p].var    = i1;
    */

    p_Old = get_Input_Place(pp, pp_obb, bI, i1);

    t = handle_New_Transition_Case(pp, pp_obb, bI, p_Old,
                                   -1); /*
                                         * It was p instead of -1. */

    /*t = new_Transition (pp, pp_obb, bI);
    pp->transition[t].t_Type = t_ID;

    add_To_Postset_Of_Place (pp, p, t);
    add_To_Preset_Of_Transition (pp, t, p);
    * Skipping creation of new identity transition. */

    // Mark t as ldt of both i1, and vI.
    pp_obb[bI].ldt[i1] = t;
    pp_obb[bI].ldt[vI] = t;

    // Mark that t defines both i1 and vI.
    // pp->vds[i1].vt_map[pp->vds[i1].tr_count++] = t;
    // pp->vds[vI].vt_map[pp->vds[vI].tr_count++] = t;
    break;
  case c_NEW_VAR:
    p = new_Place(pp, pp_obb, bI);
    pp->place[p].p_Type = p_VAR;
    pp->place[p].var = i1;

    handle_New_Variable_Case(pp, pp_obb, bI, p);

    t = new_Transition(pp, pp_obb, bI);
    pp->transition[t].t_Type = t_ID;

    add_To_Postset_Of_Place(pp, p, t);
    add_To_Preset_Of_Transition(pp, t, p);

    // Mark t as ldt of both i1, and vI.
    pp_obb[bI].ldt[i1] = t;
    pp_obb[bI].ldt[vI] = t;

    // Mark that t defines both i1 and vI.
    // pp->vds[i1].vt_map[pp->vds[i1].tr_count++] = t;
    // pp->vds[vI].vt_map[pp->vds[vI].tr_count++] = t;

    break;
  default:
    printf("construct_PP_Identity_Assignment:\n");
    printf("BB = %d, Join operaion case invalid.\n", bI + 2);
    printf("Exception, Continuing.\n");
  }

  /* Get LDT of i1.
  int lI;
  lI = pp_obb[bI].ldt[i1];

  if (lI != -1)
  {
      // update latest defining transition for vI.
      pp_obb[bI].ldt[vI] = lI;
  }
  else
  {
      // LDT doesn't exist, construct a place and resolve link case.
      int p = construct_Place_For_Variable (pp, pp_obb, bI, i1);

      // LDT for i1 may be constructed while joining it to rest of PP.
      lI = pp_obb[bI].ldt[i1];
      if (lI != -1)
      {
          pp_obb[bI].ldt[vI] = lI;
      }
      else
      {
          // Construct an Identity transition.
          int t = new_Transition (pp, pp_obb, bI);
          pp->transition[t].t_Type = t_ID;

          // Add t to the preset of p and vice versa.
          add_To_Preset_Of_Transition (pp, t, p);
          add_To_Postset_Of_Place (pp, p, t);

          // Mark t as LDT for both i1, and vI.
          / pp_obb[bI].ldt[i1] = t; /* Remove the comment to set
              * this transition as LDT of the variable on RHS. /
          pp_obb[bI].ldt[vI] = t;
      }
  }*/
}

// Constructs PRES+ for a scanf statement.
void construct_PP_Scanf(Pres_Plus *pp, PP_One_BB *pp_obb, BB_Array *bba, int bI,
                        int sI, Symbol_Table *st) {
  /******************* FUNCTION DECLARATIONS *********************/
  // Adds a new place to the PRES+ of bI, returns its index.
  int new_Place(Pres_Plus * pp, PP_One_BB * pp_obb, int bI);

  // Add a new transition to the PRES+ of bI, returns its index.
  int new_Transition(Pres_Plus * pp, PP_One_BB * pp_obb, int bI);

  // Adds given place to the preset of given transition.
  void add_To_Preset_Of_Transition(Pres_Plus * pp, int tI, int pI);

  // Adds given transtion to the postset of given place.
  void add_To_Postset_Of_Place(Pres_Plus * pp, int pI, int tI);
  /***************************************************************/
  // variable defiened in the statement.
  int vI = bba->BB[bI].stmnt[sI].var;

  // New SCANF place.
  int p = new_Place(pp, pp_obb, bI);

  pp->place[p].p_Type = p_SCANF;
  pp->place[p].var = vI;

  // New Identity Transition.
  int t = new_Transition(pp, pp_obb, bI);

  pp->transition[t].t_Type = t_ID;

  // Mark that t defines vI.
  // pp->vds[vI].vt_map[pp->vds[vI].tr_count++] = t;

  // Add p to the preset of t and viceversa.
  add_To_Preset_Of_Transition(pp, t, p);
  add_To_Postset_Of_Place(pp, p, t);

  // Mark t as ldt for the variable associated to p.
  pp_obb[bI].ldt[vI] = t;
}

// Constructs PRES+ for a printf statement.
void construct_PP_Printf(Pres_Plus *pp, PP_One_BB *pp_obb, BB_Array *bba,
                         int bI, int sI, Symbol_Table *st) {
  /******************* FUNCTION DECLARATIONS *********************/
  // Adds a new place to the PRES+ coressponding to bI.
  int new_Place(Pres_Plus * pp, PP_One_BB * pp_obb, int bI);

  // Joins given place to the rest of the PRES+.
  void resolve_Link_Case(Pres_Plus * pp, PP_One_BB * pp_obb, int bI, int pI);
  /***************************************************************/
  // The variable printed in this statement.
  int vI = bba->BB[bI].stmnt[sI].var;

  // Construct a printf place for this variable.
  int p = new_Place(pp, pp_obb, bI);
  pp->place[p].p_Type = p_PRINTF;
  pp->place[p].var = vI;

  resolve_Link_Case(pp, pp_obb, bI, p);
}

// Constructs PRES+ for a return statement.
void construct_PP_Return(Pres_Plus *pp, PP_One_BB *pp_obb, BB_Array *bba,
                         int bI, int sI, Symbol_Table *st) {
  // does nothing.
}
