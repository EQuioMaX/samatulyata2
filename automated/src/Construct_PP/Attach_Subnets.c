/* Important points:
 * 1. The fact that all the predecessors of a loop header which belong
 *       to the loop body are NORMAL basic blocks is used "intrinsically".
 *       If the fact is not true then the dfs itself needs to be modified.
 * 2. For identity transitions -- they have at-most one pre-place which
 *       is of type VAR and all other must be DUMMY.
 *       The transition forwards the token associated to the VAR place
 *           if such place exists.
 *       A DUMMY token if all the pre-places are DUMMY.
 * 3. Maximum transitions are of type DUMMY.
 *      DUMMY type means that it can have any number of pre-place with
 *         any type i.e. VAR/DUMMY.
 *      When all the pre-places of a DUMMY trans get a token, it produces
 *         a DUMMY token for all its post-places.
 *      All the post-places of a DUMMY trans must be of type DUMMY.
 * */

#include "Types.h"

/** Input:
 *  pp: The overall PRES+ net consisting of unattached subnets of all
 *     the basic blocks in the control flow graph.
 *  pp_obb: (subnets) Pointer to an array s.t. an element contains
 *          indices of the places and transitions in pp.places and
 *          pp.transitions array corresponding to the subnet of the
 *          basic block at the same index in the basic blocks array.
 *  bba: Pointer to the array of basic blocks.
 *  st: Pointer to the Symbol Table.
 *  Output:
 *      The function attaches the subnets to obtain a single PRES+ net.
 *      The original PRES+ net pp is modified.
 *
 *  ALGORITHM:
 *  1.  Invoke a dfs_attach ()
 *  2.  Ignore back edges
 *  3.  Join successor(s) while back tracking.
 *  4.  Back edges are also joined while back tracking by considering
 *      them as predecessor current bb edges explicitly.
 * */
void attach_subnets(Pres_Plus *pp, PP_One_BB *pp_obb, BB_Array *bba,
                    Symbol_Table *st) {
  /******************* FUNCTION DECLARATIONS *********************/
  // Performs a dfs traversal on the cfg and attaches while back-tracking.
  void dfs_attach(Pres_Plus * pp, PP_One_BB * pp_obb, BB_Array * bba,
                  const int bI, Symbol_Table *st);

  // Adds the dummy input places of the very first subnet, subnet 0,
  // to the post-set of an identity transition. Provides one dummy
  // Input place to the identity transition.
  void const_single_dummy_input_place(Pres_Plus * pp, PP_One_BB * pp_obb);
  /***************************************************************/

  int i;
  for (i = 0; i < bba->total_BBs; i++) {
    bba->BB[i].visited = False;
  }

  dfs_attach(pp, pp_obb, bba, 0, st); // 0 being the first basic block.

  const_single_dummy_input_place(
      pp, pp_obb); /* Constructs a single
                    * dummy input place for all dummy places. */
}

/** Input:
 *  pp: The overall PRES+ net consisting of unattached subnets of all
 *     the basic blocks in the control flow graph.
 *  pp_obb: (subnets) Pointer to an array s.t. an element contains
 *          indices of the places and transitions in pp.places and
 *          pp.transitions array corresponding to the subnet of the
 *          basic block at the same index in the basic blocks array.
 *  bba: Pointer to the array of basic blocks.
 *  st: Pointer to the Symbol Table.
 *  bI:  Index of the current basic block.
 *  Output:
 *      None
 *
 *  DESCRIPTION:
 *  Invokes the recursion for the successor(s) of bI and attaches
 *  them to the bI while back tracking.
 * */
void dfs_attach(Pres_Plus *pp, PP_One_BB *pp_obb, BB_Array *bba, const int bI,
                Symbol_Table *st) {
  /******************* FUNCTION DECLARATIONS *********************/

  // Returns True if bs-->be forms a back edge, False otherwise.
  Bool is_back_edge(BB_Array * bba, const int bs, const int be);

  /* Constructs defining transitions for all those variables which are
   *   live at the end of the subnet bI.
   * Also synchronizes the transitions with the maximum transition
   *   of the subnet.*/
  void const_def_trans_each_live_var_at_end(Pres_Plus * pp, PP_One_BB * pp_obb,
                                            BB_Array * bba, const int bI);

  // Attaches a subnet to its successors.
  void attach_successors(Pres_Plus * pp, PP_One_BB * pp_obb, BB_Array * bba,
                         const int bI, Symbol_Table *st);

  // Prints the attach sequence to file specified in OP_ATTACH_SEQ.
  void print_attach_bbs(const int indent, const int pr, const int s1,
                        const int s2);

  // Prints the bI to file specified in OP_ATTACH_SEQ as currently
  // traversed subnet.
  void print_attach_dfs(const int indent, const int bI);

  // Constructs a maximum transition for the subnte bI.
  void const_maximum_trans(Pres_Plus * pp, PP_One_BB * pp_obb, BB_Array * bba,
                           const int bI);

  // Constructs loop synch places for the  subnet bI.
  void create_loop_synch_places(Pres_Plus * pp, PP_One_BB * pp_obb,
                                const int bI);

  /* Adds the input places of the false successor of the loop header
   *   subnet bI to the input places array of the subnet bI. */
  void add_dummy_in_places_to_loop_header_input(
      Pres_Plus * pp, PP_One_BB * pp_obb, BB_Array * bba, const int bI);

  /* Adds those input places of the false succ of bI to its own input
   *   places array which are not defined in the loop body. */
  void add_live_independent_in_pl_to_loop_header_in(
      Pres_Plus * pp, PP_One_BB * pp_obb, BB_Array * bba, const int bI);
  /***************************************************************/

  if (bba->BB[bI].trv_state != NOT_VISITED)
    return;

  bba->BB[bI].trv_state = VISITING;

  // printf ("Dfs for BB=%d.\n", bI+2);

  static int indent = 0; // Only used to beautify the output.
  indent++;
  print_attach_dfs(indent, bI);

  int i;
  int pred;     // Index of a predecessor of bI.
  int ts;       // true successor of a BB.
  int c_min_tr; // Count of the minimal transitions for loop headers.

  switch (bba->BB[bI].bb_Type) {
  case bb_COND:
    /****** Invoke recursion for the two successors. ********/
    dfs_attach(pp, pp_obb, bba, bba->BB[bI].trueNext, st);
    dfs_attach(pp, pp_obb, bba, bba->BB[bI].falseNext, st);

    /***** Attach the successors ****************************/
    attach_successors(pp, pp_obb, bba, bI, st);

    /*printf ("attach BB-%d and BB-%d to BB-%d.\n",
                    bba->BB[bI].trueNext+2,
                    bba->BB[bI].falseNext+2, bI+2);*/
    print_attach_bbs(indent, bI, bba->BB[bI].trueNext, bba->BB[bI].falseNext);

    if (bba->BB[bI].trueNext < bI && bba->BB[bI].bb_Type == bb_COND) {
      /*** The subnet is a loop header *****************/
      // Attach to the two predecessors which fall in the
      // loop body.

      /* Definition:
       * MINIMAL TRANSITION: A transition in the post-set
       *   of an input place of the subnet bI is a minimal
       *   trans of bI.
       * */

      // For each predecessor of bI.
      for (i = 0; i < bba->BB[bI].parent_Count; i++) {
        pred = bba->BB[bI].parent[i];
        if (bba->BB[pred].trv_state == VISITED) {
          // printf ("Attach special BB = %d to BB %d\n", bI+2, pred+2);
          print_attach_bbs(indent, pred, bI, -1);

          attach_successors(pp, pp_obb, bba, pred,
                            st); /*
                                  * Attaches the loop header to its predecessors
                                  *   in the loop body.
                                  * Property that such a predecessor is a
                                  *   NORMAL basic block used intrinsically.
                                  * */
        }
      }

      /* Add the DUMMY input places of the false successor to
       *   the input places array of the subnet. */
      add_dummy_in_places_to_loop_header_input(pp, pp_obb, bba, bI);

      /* Add the variables not defined in the loop body and
       *   input to the false successor to the input places
       *   array of the loop header.
       * */
      add_live_independent_in_pl_to_loop_header_in(pp, pp_obb, bba, bI);
    }
    break;

  case bb_NORM:
    /* The property that the predecessors of a loop header inside
     *   the loop body are always NORMAL subnets is "intrinsically"
     *   used here.
     * If you find that the property doesn't hold then the code
     *   will NOT WORK.
     * */

    // if (is_back_edge (bba, bI, bba->BB[bI].trueNext) == False)
    ts = bba->BB[bI].trueNext;
    if (bba->BB[ts].trv_state != VISITING) {
      // bI --> bI.true_successor is not a back edge.
      /****** Invoke recursion for the successor. ********/
      dfs_attach(pp, pp_obb, bba, bba->BB[bI].trueNext, st);

      /***** Attach the successor ************************/
      attach_successors(pp, pp_obb, bba, bI, st);
      // printf ("attach BB = %d to %d\n", ts+2, bI+2);
      print_attach_bbs(indent, bI, bba->BB[bI].trueNext, -1);
    } else {
      print_attach_bbs(indent, ts, bI, -1);
      // printf ("Maximum transition to be constructed in BB = %d\n", bI+2);
      // bI --> bI.true_successor is a back edge.

      /* Construct defining transitions and the respective
       * pre-places for the variables which are live at end
       * of the basic blcok bI, but have no defining transition
       * which belongs to the subnet[bI]. */

      const_maximum_trans(pp, pp_obb, bba, bI);
      /* Construct MAXIMUM transition at the end of the BB.
       *
       * MAXIMUM transitions should not be constructed here because
       *   the INTERFACE (guarded minimal identity) transitions
       *   of the subnet bI has not been constructed at this
       *   point.
       *
       * SOLUTION:
       *   Maximum transitions are constructed at this point.
       *      and given a DUMMY type.
       *   The pre-places of this transition are all the vars
       *      live at the end of the subnet bI.
       * */
      /* Constructing the minimal transitions in the
       * subnet pred. */

      const_def_trans_each_live_var_at_end(pp, pp_obb, bba, bI);
      /* Construct defining transition for each var live at the
       *   end of bI.
       *
       * Need:
       *   1. The input places of the loop header are attached
       *         to this transition.
       *   2. These transitions are synchronized with the
       *         execution of the max transition of the subnet.
       *      As a result it prevents overlapping of loop
       *         iterations.
       * */
    }
    break;

  case bb_LAST:
    break;

  default:
    // print error.
    printf("dfs_attach: BB=%d.\n", bI + 2);
    printf("The basic block has an invalid type.\n");
    printf("Fatal error, Exiting.\n\n");
    exit(1);
  }

  indent--;
  bba->BB[bI].trv_state = VISITED;
}

/** Inputs:
 *    pp, pp_obb, bba: Usual arguments.
 *    bI: index of a loop header.
 *  Outputs:
 *    returns nothing.
 *  Functionality:
 *      Adds places associated to variables which were not defined in
 *         the loop body to the input places array of the loop header bI.
 * */
void add_live_independent_in_pl_to_loop_header_in(Pres_Plus *pp,
                                                  PP_One_BB *pp_obb,
                                                  BB_Array *bba, const int bI) {
  /******************* FUNCTION DECLARATIONS *********************/
  /* Add input places of the false succ of bI with vI as associated var
   *   to the input places array of the subnet bI itself. */
  void add_var_in_pl_to_loop_header_input(Pres_Plus * pp, PP_One_BB * pp_obb,
                                          BB_Array * bba, const int bI,
                                          const int vI);

  /* Returns True, if there exists a reaching definition at the
   *    beginning of the false successor of the loop header bI
   *    which defines vI.
   * False, otherwise*/
  Bool is_var_defined_in_loop_body(BB_Array * bba, const int bI, const int vI);

  /* Returns the number of entries made to arr.
   * arr will contain the indices corresponding to set bits in bv. */
  int get_Set_Bit_Indices(Bit_Vector bv, int *arr);
  /***************************************************************/

  if (bba->BB[bI].trueNext >= bI || bba->BB[bI].bb_Type != bb_COND) {
    printf("add_live_independent_in_pl_to_loop_header_in ():\n");
    printf("Invoked for a basic block = %d, which is not a loop header.\n",
           bI + 2);
    printf("Fatal Error. Exiting.\n\n");
    exit(1);
  }

  const int ts = bba->BB[bI].trueNext;  // True successor of bI.
  const int fs = bba->BB[bI].falseNext; // False succ.

  int vI; // index of a variable.
  int pI; // index of a place.

  // Get all variables live at the beginning of fs.
  int arr[MAX_VARIABLES]; // Array to store live variables at beginning of bI.
  int len;                // size of the array arr.
  len = get_Set_Bit_Indices(bba->BB[fs].live_in, arr);

  // For each variable live at the beginning of the false successor of
  // the subnet bI.
  int i, j;
  for (i = 0; i < len; i++) {
    vI = arr[i];
    if (is_var_defined_in_loop_body(bba, bI, vI) == False) {
      /* Variable vI is not defined within the loop body of the
       *   loop of the subnet bI.
       * Add the input places of the subnet fs to the input
       *   places list of the loop header. */
      add_var_in_pl_to_loop_header_input(pp, pp_obb, bba, bI, vI);
    }
  }
}

/** Inputs:
 *    pp, pp_obb, bba: Usual arguments.
 *    bI: index of a loop header.
 *    vI: index of a variable.
 *  Outputs:
 *    returns nothing.
 *  Functionality:
 *      Adds the input places of the false successor of the loop header
 *        bI with vI as the associated variable to the input places
 *        array of the subnet bI.
 *
 *  This function must be invoked from
 *    add_live_independent_in_pl_to_loop_header_in () only.
 * */
void add_var_in_pl_to_loop_header_input(Pres_Plus *pp, PP_One_BB *pp_obb,
                                        BB_Array *bba, const int bI,
                                        const int vI) {
  /******************* FUNCTION DECLARATIONS *********************/
  // Adds place pI to input places array of subnet bI.
  void add_To_Input_Places_Array(Pres_Plus * pp, PP_One_BB * pp_obb, int bI,
                                 int pI);
  /***************************************************************/

  const int ts = bba->BB[bI].trueNext;  // True successor of bI.
  const int fs = bba->BB[bI].falseNext; // False succ.

  int pI; // Index of a place.
  int i;

  // For each input place of the subnet fs.
  for (i = 0; i < pp_obb[fs].c_in; i++) {
    pI = pp_obb[fs].in[i];

    if (pp->place[pI].var == vI) {
      // Add pI to the input places array of the subnet bI.
      add_To_Input_Places_Array(pp, pp_obb, bI, pI);
    }
  }
}

/** Inputs:
 *    pp, pp_obb, bba: Usual arguments.
 *    bI: index of a loop header.
 *  Outputs:
 *    returns nothing.
 *  Functionality:
 *      Adds the DUMMY input places of the false successor of the
 *        subnet bI to its own input places array.
 * */
void add_dummy_in_places_to_loop_header_input(Pres_Plus *pp, PP_One_BB *pp_obb,
                                              BB_Array *bba, const int bI) {
  /******************* FUNCTION DECLARATIONS *********************/
  // Adds place pI to input places array of subnet bI.
  void add_To_Input_Places_Array(Pres_Plus * pp, PP_One_BB * pp_obb, int bI,
                                 int pI);
  /***************************************************************/

  if (bba->BB[bI].bb_Type != bb_COND || bba->BB[bI].trueNext >= bI) {
    printf("add_dummy_in_places_to_loop_header_input ():\n");
    printf("Invoked for non loop header subnet. \n");
    printf("Fatal Error. Exiting.\n\n");
    exit(1);
  }

  int fs; // False successor.
  fs = bba->BB[bI].falseNext;
  int pI; // Index of a place.
  int i;

  for (i = 0; i < pp_obb[fs].c_in; i++) {
    pI = pp_obb[fs].in[i];
    if (pp->place[pI].p_Type == p_DUMMY) {
      add_To_Input_Places_Array(pp, pp_obb, bI, pI);
    }
  }
}

/** Inputs:
 *    pp, pp_obb: Usual arguments.
 *  Outputs:
 *    returns nothing.
 *  Functionality:
 *  1. If number of DUMMY input places of the first subnet i.e. 0 is
 *       more than 1:
 *         Constructs an identity transitions with a dummy input place.
 *         Attaches all the dummy input places of the subnet-0 as post-
 *           places to this transition.
 * */
void const_single_dummy_input_place(Pres_Plus *pp, PP_One_BB *pp_obb) {
  /******************* FUNCTION DECLARATIONS *********************/
  // Adds a new place to the PRES+ of bI, returns its index.
  int new_Dummy_Place(Pres_Plus * pp, PP_One_BB * pp_obb, int bI);

  // Add a new transition to the PRES+ of bI, returns its index.
  int new_Transition(Pres_Plus * pp, PP_One_BB * pp_obb, int bI);

  // Adds given place to the postset of given transition.
  void add_To_Postset_Of_Transition(Pres_Plus * pp, int tI, int pI);

  // Adds given transition to the preset of given place.
  void add_To_Preset_Of_Place(Pres_Plus * pp, int pI, int tI);

  // Adds given place to the preset of given transition.
  void add_To_Preset_Of_Transition(Pres_Plus * pp, int tI, int pI);

  // Adds given transtion to the postset of given place.
  void add_To_Postset_Of_Place(Pres_Plus * pp, int pI, int tI);
  /***************************************************************/
  const int bI = 0; // It always works for the very first subnet.
  int i;
  int pI, pI1, pI2, tI; // Indices of places and a transition.
  int flag = False;     // Changed to true when first DUMMY input place is
                        // encountered.

  tI = -1; // Index of the transition which provides tokens to dummy
           // input places.

  pI = pI2 = -1;
  // For each input place of the subnet bI:
  for (i = 0; i < pp_obb[bI].c_in; i++) {
    pI = pp_obb[bI].in[i];

    if (pp->place[pI].p_Type == p_DUMMY) {
      // The place is a DUMMY place.
      if (flag == False) {
        // Change the flag to true.
        flag = True;
        pI1 = pI; // Storing the pI for future use.
      } else if (pI != pI2) {
        // It second or more DUMMY input place.
        if (tI == -1) {
          // Construct an IDENTITY transition.
          tI = new_Transition(pp, pp_obb, bI);

          pp->transition[tI].t_Type = t_ID; // Assign type.

          pI2 = new_Dummy_Place(
              pp, pp_obb, bI); /* Constructs
                                * a new dummy place and also adds it to the
                                * input places list of the subnet bI. */

          // Attach pI2 to tI as pre-place.
          add_To_Preset_Of_Transition(pp, tI, pI2);
          add_To_Postset_Of_Place(pp, pI2, tI);

          // Attach pI1 to tI as post-place.
          add_To_Postset_Of_Transition(pp, tI, pI1);
          add_To_Preset_Of_Place(pp, pI1, tI);
        }

        // Attach pI to tI as a post-place.
        add_To_Postset_Of_Transition(pp, tI, pI);
        add_To_Preset_Of_Place(pp, pI, tI);
      }
    }
  }
}

/** Inputs:
 *    pp, pp_obb: Usual meaning.
 *    bI: Index of the subnet in which maximum transition is to be
 *        constructed.
 *  Outputs:
 *    returns nothing.
 *  Functionality:
 * 1. Constructs a new transition. It is the maximum transition.
 * 2. Assigns DUMMY type to this transition, though it is debatable.
 * 3. For each variable v live at the end of the subnet bI:
 *        constructs a place p associated with the variable v.
 *        Attaches p to the max trans as pre-place.
 *        Attaches p to the LDT of v, if the LDT exists.
 *            else adds p to the input places array of the subnet bI.
 * 4. In pp_obb, sets the max_trans attribute to this trans.
 * */
void const_maximum_trans(Pres_Plus *pp, PP_One_BB *pp_obb, BB_Array *bba,
                         const int bI) {
  /******************* FUNCTION DECLARATIONS *********************/
  // Adds a new place to the PRES+ of bI, returns its index.
  int new_Place(Pres_Plus * pp, PP_One_BB * pp_obb, int bI);

  // Adds given place to the postset of given transition.
  void add_To_Postset_Of_Transition(Pres_Plus * pp, int tI, int pI);

  // Adds given transition to the preset of given place.
  void add_To_Preset_Of_Place(Pres_Plus * pp, int pI, int tI);

  // Adds given place to the preset of given transition.
  void add_To_Preset_Of_Transition(Pres_Plus * pp, int tI, int pI);

  // Adds given transtion to the postset of given place.
  void add_To_Postset_Of_Place(Pres_Plus * pp, int pI, int tI);

  // Add a new transition to the PRES+ of bI, returns its index.
  int new_Transition(Pres_Plus * pp, PP_One_BB * pp_obb, int bI);

  // Adds place pI to input places array of subnet bI.
  void add_To_Input_Places_Array(Pres_Plus * pp, PP_One_BB * pp_obb, int bI,
                                 int pI);

  /* Returns the number of entries made to arr.
   * arr will contain the indices corresponding to set bits in bv. */
  int get_Set_Bit_Indices(Bit_Vector bv, int *arr);
  /***************************************************************/
  /* IMPORTANT PROPERTY:
   * Variables live at the end of a subnet is a superset of the
   *   variables live at the beginning of all its successors.
   * OR
   *  Var-Live-end[bI] = UNION Var-Live-beginning[s]
   *                          where s is a successor of bI.
   * */

  // Get all variables live at the end of bI.
  int arr[MAX_VARIABLES]; // Array to store live variables at end of bI.
  int len;                // size of the array arr.
  len = get_Set_Bit_Indices(bba->BB[bI].live_out, arr);

  int vI; // Index of a variable.
  int i;  // For iteration.
  int pI; // Index of a place.
  int tI; // Index of a transition.

  int mtI; // Index of the maximum transition.
  mtI = new_Transition(pp, pp_obb, bI);

  pp->transition[mtI].t_Type = t_DUMMY; /* Assign DUMMY type. */

  pp_obb[bI].max_trans = mtI;

  // For each variable live at the end of the subnet bI.
  for (i = 0; i < len; i++) {
    vI = arr[i];

    // Construct a place for vI and attach it to mtI as pre-place.
    pI = new_Place(pp, pp_obb, bI);
    pp->place[pI].p_Type = p_VAR;
    pp->place[pI].var = vI;

    // Attaching pI to mtI.
    add_To_Preset_Of_Transition(pp, mtI, pI);
    add_To_Postset_Of_Place(pp, pI, mtI);

    /* Attach pI:
     *   To its ldt if the ldt exists.
     *   add to input places array of bI otherwise.
     * */
    if (pp_obb[bI].ldt[vI] == -1) {
      // No definition of vI in bI and vI is live at end of BB bI.
      add_To_Input_Places_Array(pp, pp_obb, bI, pI);
    } else {
      // vI has a definition in bI.
      tI = pp_obb[bI].ldt[vI];

      // Attach pI to tI as post-place.
      add_To_Postset_Of_Transition(pp, tI, pI);
      add_To_Preset_Of_Place(pp, pI, tI);
    }
  }
}

/** Inputs:
 *    pp, pp_obb, bI: Usual meaning.
 *  Outputs:
 *    returns nothing.
 *  Functionality:
 * 1. This function constructs an identity transition for each variable
 *      at the end of the subnet bI.
 * 2. Each identity transition has two pre-places:
 *    a. The variable defined by the transition.
 *    b. A DUMMY pre-place attached (for synchronization) to the maximum
 *         transition.
 * 3. Each such transition is marked as the LDT for the variable defined
 *      by the variable.
 * */
void const_def_trans_each_live_var_at_end(Pres_Plus *pp, PP_One_BB *pp_obb,
                                          BB_Array *bba, const int bI) {
  /******************* FUNCTION DECLARATIONS *********************/
  // Adds a new place to the PRES+ of bI, returns its index.
  int new_Place(Pres_Plus * pp, PP_One_BB * pp_obb, int bI);

  // Adds given place to the postset of given transition.
  void add_To_Postset_Of_Transition(Pres_Plus * pp, int tI, int pI);

  // Adds given transition to the preset of given place.
  void add_To_Preset_Of_Place(Pres_Plus * pp, int pI, int tI);

  // Adds given place to the preset of given transition.
  void add_To_Preset_Of_Transition(Pres_Plus * pp, int tI, int pI);

  // Adds given transtion to the postset of given place.
  void add_To_Postset_Of_Place(Pres_Plus * pp, int pI, int tI);

  // Add a new transition to the PRES+ of bI, returns its index.
  int new_Transition(Pres_Plus * pp, PP_One_BB * pp_obb, int bI);

  // Adds place pI to input places array of subnet bI.
  void add_To_Input_Places_Array(Pres_Plus * pp, PP_One_BB * pp_obb, int bI,
                                 int pI);

  /* Returns the number of entries made to arr.
   * arr will contain the indices corresponding to set bits in bv. */
  int get_Set_Bit_Indices(Bit_Vector bv, int *arr);
  /***************************************************************/
  /* IMPORTANT PROPERTY:
   * Variables live at the end of a subnet is a superset of the
   *   variables live at the beginning of all its successors.
   * OR
   *  Var-Live-end[bI] = UNION Var-Live-beginning[s]
   *                          where s is a successor of bI.
   * */

  // Get all variables live at the end of bI.
  int arr[MAX_VARIABLES]; // Array to store live variables at end of bI.
  int len;                // size of the array arr.
  len = get_Set_Bit_Indices(bba->BB[bI].live_out, arr);

  int vI;  // Index of a variable.
  int i;   // For iteration.
  int pI;  // Index of a place associated to a variable.
  int dpI; // Index of a dummy place.
  int tI;  // Index of a transition.
  int ntI; // Index of a new identity transition constructed for a var.

  int mtI; // Index of the maximum transition.
  mtI = pp_obb[bI].max_trans;

  if (mtI == -1) {
    // Fatal error.
    printf("const_def_trans_each_live_var_at_end()\n");
    printf("BB = %d, maximum transition is -1.\n", bI + 2);
    printf("Fatal Error.. Exiting... \n\n");
    exit(1);
  }

  // For each variable live at the end of the subnet bI.
  for (i = 0; i < len; i++) {
    vI = arr[i];

    // Construct a defining identity transition for the var.
    ntI = new_Transition(pp, pp_obb, bI);
    pp->transition[ntI].t_Type = t_ID;
    pp->transition[ntI].prio = pr_HIGH; /* Only high priority
                                         * transitions in the system. */

    // Construct a place for the variable vI.
    pI = new_Place(pp, pp_obb, bI);
    pp->place[pI].p_Type = p_VAR;
    pp->place[pI].var = vI;

    // Attach pI to tI as a pre-place.
    add_To_Preset_Of_Transition(pp, ntI, pI);
    add_To_Postset_Of_Place(pp, pI, ntI);

    if (pp_obb[bI].ldt[vI] == -1) {
      // No LDT of vI in bI, add to input places array.
      add_To_Input_Places_Array(pp, pp_obb, bI, pI);

      // Update the ldt.
      pp_obb[bI].ldt[vI] = ntI;
    } else {
      // vI has a definition in bI.
      tI = pp_obb[bI].ldt[vI];

      // Attach pI to tI as post-place.
      add_To_Postset_Of_Transition(pp, tI, pI);
      add_To_Preset_Of_Place(pp, pI, tI);

      // Update the ldt.
      pp_obb[bI].ldt[vI] = ntI;
    }

    // Construct the DUMMY pre-place of ntI for synchronization.
    dpI =
        new_Place(pp, pp_obb, bI); /* New DUMMY place function
                                    * must not be used because it also adds the
                                    * place to input places array of bI. */
    pp->place[dpI].p_Type = p_DUMMY;
#ifdef CHECK_EQV
    pp->place[dpI].var = DUM_VAR_INDEX;
#endif // CHECK_EQV

    // Add dpI to ntI as pre-place.
    add_To_Preset_Of_Transition(pp, ntI, dpI);
    add_To_Postset_Of_Place(pp, dpI, ntI);

    // Attach dpI to mtI as post-place.
    add_To_Postset_Of_Transition(pp, mtI, dpI);
    add_To_Preset_Of_Place(pp, dpI, mtI);
  }
}

/** Inputs:
 *  pp: The overall PRES+ net consisting of unattached subnets of all
 *     the basic blocks in the control flow graph.
 *  pp_obb: (subnets) Pointer to an array s.t. an element contains
 *          indices of the places and transitions in pp.places and
 *          pp.transitions array corresponding to the subnet of the
 *          basic block at the same index in the basic blocks array.
 *  bba: Pointer to the array of basic blocks.
 *  st: Pointer to the Symbol Table.
 *  bI: The subnet whose successor(s) should be attached to the subnet
 *      itself.
 *
 *  Outputs:
 *      None.
 *
 *  Functionality:
 *     Based on the type (Conditional or Normal) of the basic block bI
 *    invokes a routine for the attachment.
 * */
void attach_successors(Pres_Plus *pp, PP_One_BB *pp_obb, BB_Array *bba,
                       const int bI, Symbol_Table *st) {
  /******************* FUNCTION DECLARATIONS *********************/
  // Attaches the two successors of a conditional subnet.
  void attach_successors_cond_BB(Pres_Plus * pp, PP_One_BB * pp_obb,
                                 BB_Array * bba, const int bI,
                                 Symbol_Table *st);

  // Attaches the successor of a normal subnet.
  void attach_successors_norm_BB(Pres_Plus * pp, PP_One_BB * pp_obb,
                                 BB_Array * bba, const int bI,
                                 Symbol_Table *st);
  /***************************************************************/

  switch (bba->BB[bI].bb_Type) {
  case bb_COND:
    attach_successors_cond_BB(pp, pp_obb, bba, bI, st);
    break;
  case bb_NORM:
    attach_successors_norm_BB(pp, pp_obb, bba, bI, st);
    break;
  default:
    printf("attach_successors: invoked for BB = %d\n", bI);
    printf("Type of the basic block is invalid.\n");
    printf("Fatal Error: Exiting.\n\n");
    exit(1);
  }
}

/** Inputs:
 *  pp, pp_obb, bba, st: Usual arguments.
 *
 *  bI: A conditional subnet which shall be attached to its successors.
 *
 *  Outputs:
 *      None.
 *
 *  Functionality:
 *  1. Constructs minimal identity transitions for all the variables
 *     live at the beginning of the two successors.
 *  2. Attaches the transitions to the ldts in the subnet bI via
 *     required places.
 *  3. Attaches the minimal transitions to the input places of the
 *     respective subnets.
 * */
void attach_successors_cond_BB(Pres_Plus *pp, PP_One_BB *pp_obb, BB_Array *bba,
                               const int bI, Symbol_Table *st) {
  /******************* FUNCTION DECLARATIONS *********************/
  // Adds a new place to the PRES+ of bI, returns its index.
  int new_Place(Pres_Plus * pp, PP_One_BB * pp_obb, int bI);

  // Add a new transition to the PRES+ of bI, returns its index.
  int new_Transition(Pres_Plus * pp, PP_One_BB * pp_obb, int bI);

  // Adds given place to the postset of given transition.
  void add_To_Postset_Of_Transition(Pres_Plus * pp, int tI, int pI);

  // Adds given transition to the preset of given place.
  void add_To_Preset_Of_Place(Pres_Plus * pp, int pI, int tI);

  // Adds given place to the preset of given transition.
  void add_To_Preset_Of_Transition(Pres_Plus * pp, int tI, int pI);

  // Adds given transtion to the postset of given place.
  void add_To_Postset_Of_Place(Pres_Plus * pp, int pI, int tI);

  // Adds place pI to input places array of subnet bI.
  void add_To_Input_Places_Array(Pres_Plus * pp, PP_One_BB * pp_obb, int bI,
                                 int pI);

  /* Returns the number of entries made to arr.
   * arr will contain the indices corresponding to set bits in bv. */
  int get_Set_Bit_Indices(Bit_Vector bv, int *arr);

  // Returns a negation of the expression e.
  Expr *negate_Expression(Expr * e);

  /* Attaches the input places of the subnet bI to the transition tI
   * as post-places. */
  void att_in_places_to_trans(Pres_Plus * pp, PP_One_BB * pp_obb, const int bI,
                              const int tI, const int vI);

  /* Attaches the dummy input places of the two successors to
   * the subnet bI via neccessary gaurded minimal transitions. */
  void att_dummy_input_places_of_successors(Pres_Plus * pp, PP_One_BB * pp_obb,
                                            BB_Array * bba, const int bI);

  /* Constructs a place for each variable in the expression tree
   * pointed by guard. */
  void const_places_for_guard_vars(Pres_Plus * pp, PP_One_BB * pp_obb,
                                   const int bI, const int tI_ts,
                                   const int tI_fs, Expr *guard);

  /* Returns True, if there exists a reaching definition at the
   *    beginning of the false successor of the loop header bI
   *    which defines vI.
   * False, otherwise*/
  Bool is_var_defined_in_loop_body(BB_Array * bba, const int bI, const int vI);
  /***************************************************************/

  // Get all variables live at the end of bI.
  int arr[MAX_VARIABLES]; // Array to store live variables at end of bI.
  int len;                // size of the array arr.
  len = get_Set_Bit_Indices(bba->BB[bI].live_out, arr);

  int i, j;         // For iteration.
  int vI;           // Index of a variable.
  int tI_ts, tI_fs; // Index of a trans for true and false succ respectively.
  int pI;           // Index of a place.
  int ts, fs;       // True and false successors of the BB bI.

  ts = bba->BB[bI].trueNext;
  fs = bba->BB[bI].falseNext;

  Expr *gaurd, *neg_gaurd;
  gaurd = bba->BB[bI].cond;
  neg_gaurd = negate_Expression(bba->BB[bI].cond);

  // For each variable live at the end of subnet bI.
  for (i = 0; i < len; i++) {
    vI = arr[i];

    /* 1. Construct an identity minimal guarded transition for the
     *    variable at the beginning of each successor.
     * 2. Construct pre-places for the transitions. One pre-place
     *    for each variable shared by the two transitions.
     * 3. Attach this pre-place to the ldt in bI, if ldt exists.
     *    otherwise mark it as an input place for the subnet.
     * 4. In both of the successors, attach the input places of
     *    the variable vI to the respective minimal transitions as
     *    post-place.
     * */

    /***** For the true successor. *********************************/
    tI_ts = new_Transition(pp, pp_obb, ts);

    pp->transition[tI_ts].t_Type = t_ID; // Type of trans is IDENTITY.
    pp->transition[tI_ts].guard = gaurd; // Assign the guard.

    if (pp_obb[ts].ldt[vI] == -1)
      pp_obb[ts].ldt[vI] = tI_ts; // Set as ldt if no ldt for vI.

    // Attach tI_ts to all the input places of ts with vI as associated var.
    att_in_places_to_trans(pp, pp_obb, ts, tI_ts, vI);

    /***** For the false successor. *********************************/
    tI_fs = new_Transition(pp, pp_obb, fs);

    pp->transition[tI_fs].t_Type = t_ID;     // Type of trans is IDENTITY.
    pp->transition[tI_fs].guard = neg_gaurd; /* Assign the negation
                                              * of guard. */

    if (pp_obb[fs].ldt[vI] == -1)
      pp_obb[fs].ldt[vI] = tI_fs; /* Set tI_fs as ldt if no ldt for
                                   * vI in false succ. */

    if (bba->BB[bI].trueNext < bI && bba->BB[bI].bb_Type == bb_COND) {
      // bI is a loop header

      // Attach tI_fs to all the input places of the false successor
      // with vI as associated var to the transition tI_fs,
      // only if vI is defined in the loop body.
      if (is_var_defined_in_loop_body(bba, bI, vI) == True) {
        // Attach the places associated with vI to tI_fs.
        att_in_places_to_trans(pp, pp_obb, fs, tI_fs, vI);
      } else {
        ; /* Do nothing. Don't attach variable to the tI_fs.
           * The places corresponding to the variable will be
           *   added to the input places of the loop header
           *   after attaching the predecessors of the loop header
           *   belonging to the loop body.
           * */
      }
    } else {
      // bI is not a loop header
      // Attach tI_fs to all the input places of the false successor
      // with vI as associated var to the transition tI_fs.
      att_in_places_to_trans(pp, pp_obb, fs, tI_fs, vI);
    }

    /**** Set tI_ts and tI_fs as negations of each other. ********/
    pp->transition[tI_ts].itng = tI_fs;
    pp->transition[tI_fs].itng = tI_ts;

    /**** Attach the minimal transitions to the respective ldts in
     * bI or add a pre-place to them and make the pre-place as
     * an input place. *******************************************/

    pI = new_Place(pp, pp_obb, bI);

    pp->place[pI].p_Type = p_VAR;
    pp->place[pI].var = vI;

    // Attach pI to the two transitions as a pre-place.
    add_To_Preset_Of_Transition(pp, tI_ts, pI);
    add_To_Postset_Of_Place(pp, pI, tI_ts);

    add_To_Preset_Of_Transition(pp, tI_fs, pI);
    add_To_Postset_Of_Place(pp, pI, tI_fs);

    // Attach pI to its ldt in bI if the ldt exists.
    if (pp_obb[bI].ldt[vI] != -1) {
      add_To_Postset_Of_Transition(pp, pp_obb[bI].ldt[vI], pI);
      add_To_Preset_Of_Place(pp, pI, pp_obb[bI].ldt[vI]);
    } else {
      // Add pI to the list of input places of the subnet bI.
      add_To_Input_Places_Array(pp, pp_obb, bI, pI);
    }

    /* Construct pre-places for the guarded transitions and
     * attach them to the respective LDTs in bI or add
     * the to the input-places array of the subnet bI. */
    const_places_for_guard_vars(pp, pp_obb, bI, tI_ts, tI_fs, gaurd);
  }

  // Attach the dummy input places of the two successors.
  att_dummy_input_places_of_successors(pp, pp_obb, bba, bI);
}

/** Inputs:
 *      bba: Array of basic blocks.
 *      bI: A loop header.
 *      vI: A variable.
 *  Output:
 *      True, if the variable vI is defined in the loop body of the
 *              loop header bI.
 *      False, otherwise.
 *  Functionality:
 *    Scans through the reaching defitions-in of the false successor
 *       of bI for the variable vI.
 *    If any of the defition belongs to loop body returns True
 *    False, otherwise.
 *
 *    To check if a definition belongs to loop body, uses the rule that:
 *       All the basic blocks belonging to loop body have index less
 *          than the loop header but greater than or equal to that of the
 *          ture scuccessor of the loop header.
 *    A border line:
 *       If a variable is defined in loop header itself (it happens!),
 *         then the defition will be executed as many times as the loop
 *         is executed.
 *       Hence, the varaible is said to be defined in the loop body.
 * */
Bool is_var_defined_in_loop_body(BB_Array *bba, const int bI, const int vI) {
  /******************* FUNCTION DECLARATIONS *********************/
  // Returns the number of enteries made to arr.
  // Arr will contain the indices coressponding to 1 in bv.
  int get_Set_Bit_Indices(Bit_Vector bv, int *arr);

  // revaluates u to bI and sI.
  void unpair_Indices(int u, int *bI, int *sI);
  /***************************************************************/
  if (bba->BB[bI].trueNext >= bI || bba->BB[bI].bb_Type != bb_COND) {
    printf("is_var_defined_in_loop_body ():\n");
    printf("Invoked for a basic block = %d, which is not a loop header.\n",
           bI + 2);
    printf("Fatal Error. Exiting.\n\n");
    exit(1);
  }

  const int ts = bba->BB[bI].trueNext;  // True successor of bI.
  const int fs = bba->BB[bI].falseNext; // False succ.

  int arr[MAX_STMNTS]; // An array to get all the
                       // reaching definitions at the beginning of the false
                       // succ.
  int len = 0;         // Number elements in the array arr.

  len = get_Set_Bit_Indices(
      bba->BB[fs].Din,
      arr); /* Gets all the
             * reaching definitions at the beginning of the BB fs. */

  int i;
  int dI;       // Index of a definition, GLOBAL index of a statement.
  int dbI, dsI; // Definition basic block and statement index resp.
  // For each reaching definition at the beginning of the false succ.
  for (i = 0; i < len; i++) {
    dI = arr[i];
    if (bba->def_Var[dI] == vI) {
      // The statement dI defines vI
      unpair_Indices(dI, &dbI, &dsI); /* get the index of the
                                       * BB dI belongs. dsI is the relative
                                       * index in the BB dbI. */

      if (dbI >= ts && dbI <= bI) {
        // dbI belongs to the loop body.
        return True;
      }
      // else dI doesn't belong to loop body, continue.
    }
    // else dI doesn't define vI, continue.
  }

  return False;
}

/** Inputs:
 *    pp, pp_obb: Usual arguments.
 *    bI: Index of the subnet to which the new places (pre-places for
 *          the two transitions tI_ts, tI_fs) shall be added.
 *    tI_ts, tI_fs: Indices of guarded-identity transitions at the
 *              beginning of the two successors of the conditional
 *              subnet bI.
 *    dv: Variable defined by the transitions tI_ts and tI_fs.
 *        Both of them must define the same variable. Only their guards
 *        are negations of each other.
 *    guard: The guard associated to the transition tI_ts.
 *           Negation of the guard is associated to tI_fs, but the
 *           negation has same variables as guard.
 *  Outputs:
 *      Returns nothing.
 *  Functionality:
 *      Constructs pre-places for the variables involved in the
 *      expression guard.
 *      Attaches the pre-places to the transitions tI_ts and tI_fs as
 *      pre-places.
 *      Attaches the pre-places to the respective LDT in bI or adds them
 *      to the input places list of the subnet bI.
 *      One place is constructed for each variable associated to the
 *      guard.
 *      Atmost two variables can be associated to the guard.
 * */
void const_places_for_guard_vars(Pres_Plus *pp, PP_One_BB *pp_obb, const int bI,
                                 const int tI_ts, const int tI_fs,
                                 Expr *guard) {
  /******************* FUNCTION DECLARATIONS *********************/
  // Constructs a place for variable vI and takes necc actions
  // on the place to attach it to necc transitions.
  void const_place_for_a_guard_var(Pres_Plus * pp, PP_One_BB * pp_obb,
                                   const int bI, const int tI_ts,
                                   const int tI_fs, const int vI);

  // Returns the first operand in the expression tree root.
  // -1 if no first operand.
  int get_First_Operand_Expr(Expr * root);

  // Returns the second operand in the expression tree root.
  // -1 if no second operand.
  int get_Second_Operand_Expr(Expr * root);
  /***************************************************************/
  int v1, v2; // To store the indices of the atmost two variables the
              // expression guard.

  int in_var; /* The variable input to the transitions tI_ts and tI_fs.
               * 1. Both the transitions are identity transitions having guards
               *    as negation of each other.
               * 2. An ID transition without a guard must have exactly one pre-
               *    place.
               * 3. They must have exactly one pre-place because pre-places for
               *    the guards are yet to be constructed.
               * */

  int pI_temp = pp->transition[tI_ts].preset[0];

  if (pI_temp != -1)
    in_var = pp->place[pI_temp].var;
  else {
    printf("const_places_for_guard_vars():\n");
    printf("Transition = %d has invalid preset.\n", tI_ts);
    printf("Exception. Continuing without constructing ");
    printf("pre-places of the guard for the guarded minimal transition.\n\n");
    return;
  }

  if ((v1 = get_First_Operand_Expr(guard)) != -1) {
    // First operand exists
    if (v1 != in_var)
      // The variable is not same as the read variable.
      const_place_for_a_guard_var(pp, pp_obb, bI, tI_ts, tI_fs, v1);
  }

  if ((v2 = get_Second_Operand_Expr(guard)) != -1) {
    // Second operand exists:
    if (v2 != in_var)
      // The variable is not same as the read variable.
      const_place_for_a_guard_var(pp, pp_obb, bI, tI_ts, tI_fs, v2);
  }
}

/** Inputs:
 *  pp, pp_obb: Usual inputs.
 *  bI: Index of the subnet in which the new place should be constructed.
 *  vI: Index of the variable in symbol table for which we want to
 *      create a place.
 *  tI_ts, tI_fs: Guarded transitions in the true and false successors
 *          of the bI.
 * Outputs:
 *  returns nothing.
 *  Functionality:
 *      Constructs a place for the variable vI.
 *      Attaches vI to tI_ts and tI_fs as a pre-place.
 *      Attaches the place to the ldt in bI as a post-place or adds
 *          the place to the input-places list of the subnet bI.
 * */
void const_place_for_a_guard_var(Pres_Plus *pp, PP_One_BB *pp_obb, const int bI,
                                 const int tI_ts, const int tI_fs,
                                 const int vI) {
  /******************* FUNCTION DECLARATIONS *********************/
  // Adds a new place to the PRES+ of bI, returns its index.
  int new_Place(Pres_Plus * pp, PP_One_BB * pp_obb, int bI);

  // Adds given place to the postset of given transition.
  void add_To_Postset_Of_Transition(Pres_Plus * pp, int tI, int pI);

  // Adds given transition to the preset of given place.
  void add_To_Preset_Of_Place(Pres_Plus * pp, int pI, int tI);

  // Adds given place to the preset of given transition.
  void add_To_Preset_Of_Transition(Pres_Plus * pp, int tI, int pI);

  // Adds given transtion to the postset of given place.
  void add_To_Postset_Of_Place(Pres_Plus * pp, int pI, int tI);

  // Adds place pI to input places array of subnet bI.
  void add_To_Input_Places_Array(Pres_Plus * pp, PP_One_BB * pp_obb, int bI,
                                 int pI);
  /***************************************************************/
  int pI; // Index of the new place.
  pI = new_Place(pp, pp_obb, bI);

  pp->place[pI].p_Type = p_VAR;
  pp->place[pI].var = vI;

  // Add pI to the pre-set of tI_ts and vice-versa.
  add_To_Preset_Of_Transition(pp, tI_ts, pI);
  add_To_Postset_Of_Place(pp, pI, tI_ts);

  // Add pI to the pre-set of tI_fs and vice-versa.
  add_To_Preset_Of_Transition(pp, tI_fs, pI);
  add_To_Postset_Of_Place(pp, pI, tI_fs);

  int ldt = -1; // Stores index of the ldt of vI in bI.
  if (pp_obb[bI].ldt[vI] != -1) {
    // LDT exists for vI. Add vI to the postset of the ldt.
    ldt = pp_obb[bI].ldt[vI];

    // Add pI to the postset of ldt and vice-versa.
    add_To_Postset_Of_Transition(pp, ldt, pI);
    add_To_Preset_Of_Place(pp, pI, ldt);
  } else {
    // Add pI to the input places list of the subnet bI.
    add_To_Input_Places_Array(pp, pp_obb, bI, pI);
  }
}

/** Inputs:
 *  pp, pp_obb, bba, st: Usual arguments.
 *
 *  bI: A normal subnet which shall be attached to its successor.
 *
 *  Outputs:
 *      None.
 *
 *  Functionality:
 *  Attaches the input places of the only successor of the subnet bI to
 *  the respective ldts in bI.
 *  For those variables which don't have an ldt the input places of the
 *  successor are marked as input places of the subnet bI itself.
 * */
void attach_successors_norm_BB(Pres_Plus *pp, PP_One_BB *pp_obb, BB_Array *bba,
                               const int bI, Symbol_Table *st) {
  /******************* FUNCTION DECLARATIONS *********************/

  // Adds given place to the postset of given transition.
  void add_To_Postset_Of_Transition(Pres_Plus * pp, int tI, int pI);

  // Adds given transition to the preset of given place.
  void add_To_Preset_Of_Place(Pres_Plus * pp, int pI, int tI);

  // Adds place pI to input places array of subnet bI.
  void add_To_Input_Places_Array(Pres_Plus * pp, PP_One_BB * pp_obb, int bI,
                                 int pI);
  /***************************************************************/

  int i;     // For iteration.
  int in_pI; // Index of an input place.
  int ts;    // True and only successor of the basic block bI.
  int vI;    // Index of a variable.
  int tI;    // Index of a latest defining transition for some variable.

  ts = bba->BB[bI].trueNext;

  // For each input place of the subnet ts:
  for (i = 0; i < pp_obb[ts].c_in; i++) {
    in_pI = pp_obb[ts].in[i];

    switch (pp->place[in_pI].p_Type) {
    case p_VAR:
    case p_PRINTF:

      vI = pp->place[in_pI].var; // Variable associated to the place.

      if (pp_obb[bI].ldt[vI] != -1) {
        // ldt exists for the variable vI in the subnet bI.

        tI = pp_obb[bI].ldt[vI]; // index of the ldt for bI.

        // Add in_pI to the post-set of the transition lI and vice versa.
        add_To_Postset_Of_Transition(pp, tI, in_pI);
        add_To_Preset_Of_Place(pp, in_pI, tI);
      } else {
        // ldt doesn't exist for the variable vI in the subnet bI.

        // Add in_pI to the input places list of the subnet bI.
        add_To_Input_Places_Array(pp, pp_obb, bI, in_pI);
      }
      break;
    case p_DUMMY:
      if (pp_obb[bI].max_trans != -1) {
        // attach pI to the max trans as post-place.
        tI = pp_obb[bI].max_trans;

        add_To_Postset_Of_Transition(pp, tI, in_pI);
        add_To_Preset_Of_Place(pp, in_pI, tI);
      } else {
        // Add in_pI to the input places list of the subnet bI.
        add_To_Input_Places_Array(pp, pp_obb, bI, in_pI);
      }
      break;
    }
  }
}

/** Input:
 *  pp, pp_obb, bba, st: Usual arguments.
 *
 *  bI: A conditional subnet.
 *
 *  Outputs:
 *      None.
 *
 *  Functionality:
 *      If there exists a DUMMY input place in either of the successors,
 *    then
 *        1. construct a DUMMY place dI in bI.
 *        2. construct tI_ts and tI_fs.
 *
 *      Now loop the input places of the two successors and attach
 *    each DUMMY input place to the respective tI_ts or tI_fs.
 *
 *  Very important. This function is to be called for conditional
 *    subnets only.
 *
 *  WRONG:     The DUMMY input places of each successor are joined to the
 *   subnet via a gaurded minimal transition.
 * */
void att_dummy_input_places_of_successors(Pres_Plus *pp, PP_One_BB *pp_obb,
                                          BB_Array *bba, const int bI) {
  /******************* FUNCTION DECLARATIONS *********************/
  // Adds a new place to the PRES+ of bI, returns its index.
  int new_Dummy_Place(Pres_Plus * pp, PP_One_BB * pp_obb, int bI);

  // Add a new transition to the PRES+ of bI, returns its index.
  int new_Transition(Pres_Plus * pp, PP_One_BB * pp_obb, int bI);

  // Adds given place to the postset of given transition.
  void add_To_Postset_Of_Transition(Pres_Plus * pp, int tI, int pI);

  // Adds given transition to the preset of given place.
  void add_To_Preset_Of_Place(Pres_Plus * pp, int pI, int tI);

  // Adds given place to the preset of given transition.
  void add_To_Preset_Of_Transition(Pres_Plus * pp, int tI, int pI);

  // Adds given transtion to the postset of given place.
  void add_To_Postset_Of_Place(Pres_Plus * pp, int pI, int tI);

  // Returns a negation of the expression e.
  Expr *negate_Expression(Expr * e);

  /* Constructs a place for each variable in the expression tree
   * pointed by guard. */
  void const_places_for_guard_vars(Pres_Plus * pp, PP_One_BB * pp_obb,
                                   const int bI, const int tI_ts,
                                   const int tI_fs, Expr *guard);
  /***************************************************************/
  if (bba->BB[bI].bb_Type != bb_COND) {
    printf("att_dummy_input_places_of_successors ():\n");
    printf("Invoked for a non conditional subnet.\n");
    printf("Fatal Error. Exiting. \n\n");
    exit(1);
  }

  int i, j;         // For iteration.
  int vI;           // Index of a variable.
  int tI_ts, tI_fs; // Index of a transition.
  int pI;           // Index of a place.
  int ts, fs;       // True and false successors of the BB bI.

  ts = bba->BB[bI].trueNext;
  fs = bba->BB[bI].falseNext;

  Expr *gaurd, *neg_gaurd;
  gaurd = bba->BB[bI].cond;
  neg_gaurd = negate_Expression(bba->BB[bI].cond);

  tI_ts = tI_fs = -1; // Initialize the transitions to -1.

  /****** If there exists a DUMMY input place in the true successor.*/
  // For all the input places of the true successor.
  for (i = 0; i < pp_obb[ts].c_in; i++) {
    pI = pp_obb[ts].in[i];
    if (pp->place[pI].p_Type == p_DUMMY) {
      /* There exists DUMMY input place. */
      // Construct the guarded minimal identity transitions.

      // TRUE SUCCESSOR:
      tI_ts = new_Transition(pp, pp_obb, ts);
      // FALSE SUCCESSOR:
      tI_fs = new_Transition(pp, pp_obb, fs);
      break;
    }
  }

  // If tI_ts and tI_fs == -1
  if (tI_ts == -1) {
    // For each input place of the false successor:
    for (i = 0; i < pp_obb[fs].c_in; i++) {
      pI = pp_obb[fs].in[i];
      if (pp->place[pI].p_Type == p_DUMMY) {
        // Construct the guarded minimal identity transitions.
        // TRUE SUCCESSOR:
        tI_ts = new_Transition(pp, pp_obb, ts);
        // FALSE SUCCESSOR:
        tI_fs = new_Transition(pp, pp_obb, fs);
        break;
      }
    }
  }

  // If there exists a DUMMY input place in either of the successors:
  if (tI_ts == -1) {
    // None of the successors have a DUMMY input place.
    return;
  }

  // Assing type and gaurd to the transitions.
  pp->transition[tI_ts].t_Type = t_ID;
  pp->transition[tI_ts].guard = gaurd;

  pp->transition[tI_fs].t_Type = t_ID;
  pp->transition[tI_fs].guard = neg_gaurd;

  // Construct pre-place for the two transitions.
  pI = new_Dummy_Place(pp, pp_obb,
                       bI); /* Also adds pI to the
                             * input places array of the subnet bI. */

  // Attach tI_ts to pI:
  add_To_Postset_Of_Place(pp, pI, tI_ts);
  add_To_Preset_Of_Transition(pp, tI_ts, pI);

  // Attach tI_fs to pI:
  add_To_Postset_Of_Place(pp, pI, tI_fs);
  add_To_Preset_Of_Transition(pp, tI_fs, pI);

  /****** Attach DUMMY input places to tI_ts and tI_fs **************/
  // For all the input places of the true successor.
  for (i = 0; i < pp_obb[ts].c_in; i++) {
    pI = pp_obb[ts].in[i];
    if (pp->place[pI].p_Type == p_DUMMY) {
      // Add pI to the post-set of tI_ts.
      add_To_Postset_Of_Transition(pp, tI_ts, pI);
      add_To_Preset_Of_Place(pp, pI, tI_ts);
    }
  }

  /* For all the input places of the false successor, only if
   *   basic block bI is not a loop header. */
  if (bba->BB[bI].trueNext < bI && bba->BB[bI].bb_Type == bb_COND) {
    /* bI is a loop header.
     * Don't join its input dummy places to the transition tI_fs,
     *   but add to the input places array of the subnet.
     * IMPORTANT:
     *   The DUMMY places are added to the input places list of the
     *     loop-header subnet bI only once the subnet bI has been
     *     attached to its predecessors inside the loop.
     *   If the places are added to the input places array right
     *     now then they will un-necessary tokens from the max
     *     transition in the loop body.
     *
     * tI_fs will play the role of burning extra dummy tokens
     *   constructed by the execution of the loop.
     *
     * It may be necc. to converge all these dummy input places to
     *   one place for optimization purposes only.
     * */
  } else {
    /* bI is not a loop header, a normal conditional BB.
     * Attach the DUMMY input places to the tI_fs as postset. */
    for (i = 0; i < pp_obb[fs].c_in; i++) {
      pI = pp_obb[fs].in[i];
      if (pp->place[pI].p_Type == p_DUMMY) {
        // Add pI to the post-set of tI_fs.
        add_To_Postset_Of_Transition(pp, tI_fs, pI);
        add_To_Preset_Of_Place(pp, pI, tI_fs);
      }
    }
  }

  // Construct pre-places for tI_ts, tI_fs for the variables involved
  // in guard of the transitions.
  const_places_for_guard_vars(pp, pp_obb, bI, tI_ts, tI_fs, gaurd);
}

/** Inputs:
 *   pp: The overall PRES+ net.
 *   pp_obb: An array to identify the subnets corresponding to basic
 *           blocks.
 *   bI: Index of subnet whoose input places shall be attached as
 *       post places to the transition tI.
 *   vI: Index of the variable which is defined by tI and hence only the
 *       input places of bI corresponding to vI shall be attached to
 *       tI as post places.
 *  Outputs:
 *      None.
 *  Functionality:
 *      Attaches the input places of the subnet bI having vI as the
 *    associated variable to the transition tI as post-places.
 * */
void att_in_places_to_trans(Pres_Plus *pp, PP_One_BB *pp_obb, const int bI,
                            const int tI, const int vI) {
  /******************* FUNCTION DECLARATIONS *********************/
  // Adds given place to the postset of given transition.
  void add_To_Postset_Of_Transition(Pres_Plus * pp, int tI, int pI);

  // Adds given transition to the preset of given place.
  void add_To_Preset_Of_Place(Pres_Plus * pp, int pI, int tI);
  /***************************************************************/

  int j;  // For iteration.
  int pI; // Holds index on an input place.

  for (j = 0; j < pp_obb[bI].c_in; j++) {
    pI = pp_obb[bI].in[j];

    switch (pp->place[pI].p_Type) {
    case p_VAR:
    case p_PRINTF:
      if (pp->place[pI].var == vI) {
        // Add pI to the post-set of tI_ts and vice-versa.
        add_To_Postset_Of_Transition(pp, tI, pI);
        add_To_Preset_Of_Place(pp, pI, tI);
      }
      break;
    case p_DUMMY:
      // Do nothing.
      break;
    default:
      printf("att_in_places_to_trans:\n");
      printf("Type of place P-%d is invalid.\n", pI);
      printf(
          "While attaching input places of subnet = %d to the transition T-%d",
          bI + 2, tI);
      printf("Ignoring the exception and continuing. \n\n");
    }
  }
}

/***** OUTPUT FUNCTIONS ************************************************/

/**  Prints the attach order of subnets to the file defined in
 *  OP_ATTACH_SEQ.
 * */
void print_attach_bbs(const int indent, const int pr, const int s1,
                      const int s2) {
  int j; // Used for beautification of output.

  FILE *fp = fopen(OP_ATTACH_SEQ, "a");
  if (fp == NULL) {
    perror("print_attach_bbs: Could not open output file");
    return;
  }

  for (j = 0; j < indent; j++)
    fprintf(fp, "\t");
  if (s2 == -1) {
    fprintf(fp, "Attaching BB-%d to BB-%d.\n", s1 + 2, pr + 2);
  } else {
    fprintf(fp, "Attaching BB-%d and BB-%d to BB-%d.\n", s1 + 2, s2 + 2,
            pr + 2);
  }
  fclose(fp);
}

/** INPUTS:
 *      bI, index of basic block/subnet currently being traversed.
 *      indent: for beautification of output.
 *  OUTPUTS:
 *      NONE.
 *  Prints the bI to the file specified in OP_ATTACH_SEQ as bI is being
 *  visited for dfs.
 * */
void print_attach_dfs(const int indent, const int bI) {
  int j; // Used for beautification of output.

  FILE *fp = fopen(OP_ATTACH_SEQ, "a");
  if (fp == NULL) {
    perror("print_attach_dfs: Could not open output file");
    return;
  }

  for (j = 0; j < indent; j++)
    fprintf(fp, "\t");

  fprintf(fp, "DFT:- visiting BB-%d.\n", bI + 2);

  fclose(fp);
}
