#include "Types.h"

/* This file contains the various functions used by the Bison grammar
 * in Parse_One_Line.y */

/** Input:
 *      bba: pointer to the basic block array.
 *      bI:  index of the basic block which is begenning.
 *  Output:
 *      returns nothing.
 *
 *  Description*******************************************************
 *  if the true successor of basic block (bI - 1) is "-1", then
 *    This function sets the true successor of basic block (bI - 1) to bI.
 *    And the type of basic block (bI - 1) to NORMAL.
 *
 *  REASON / LOGIC ***************************************************
 *      This function is invoked when a new basic block begins.
 *  e.g. is <bb 3>: thus bI = 3.
 *
 *      The file is read sequentially from top to bottom, thus
 *  the basic blocks appear in increasing order of the basic block
 *  indices (bI).
 *
 *      Thus the previous basic block was bI - 1.
 *      If the basic block (bI - 1) didn't have any conditional or
 *  unconditional jump (goto) statement, it implies that only true
 *  successor exists for this basic block, and that is bI.
 *
 *      Since basic block bI-1 has only one successor, type is
 *  NORMAL.
 *  */
void switch_To_Next_BB(BB_Array *bba, int bI) {
  if (bI != 0) {
    if (bba->BB[bI - 1].trueNext == -1) {
      // bI - 1 didn't have any goto statement.
      bba->BB[bI - 1].trueNext = bI;
      bba->BB[bI - 1].bb_Type = bb_NORM;
    }
  }

  /*   Since one basic block has ended and next is beginning,
   * increment the count of basic blocks (total_BBs).
   *   Another function is invoked for the last basic block. *****/
  if (bI != 0) {
    bba->total_BBs++;

    if (bba->total_BBs >= MAX_BBs) {
      printf("switch_To_Next_BB:\n");
      printf("Number of Basic Blocks, %d exceeded MAX_BBs.\n", bI + 2);
      printf("Fatal error, Exiting.\n");
      exit(1);
    }
  }
}

/** Input:
 *      name: name of the variable as a string.
 *      st:   pointer to the symbol table.
 *  Output:
 *      returns nothing.
 *      Adds the variable with name, "name" to the symbol table st.
 * */
void add_Var_To_Symbol_Table(char *name, Symbol_Table *st) {
  /* Assuming No need to check if variable if already there in the
   * Symbol Table. */

  int j = st->total_Symbols++;

  if (j >= MAX_VARIABLES) {
    printf("add_Var_To_Symbol_Table:\n");
    printf("While adding var = %s, total", name);
    printf(" number of variables used excceded MAX_VARIABLES.\n");
    printf("Fatal error. Exiting.\n");
    exit(1);
  }

  st->symbol[j].vName = name; /* The variable name stored in name
                               * must have been dynamically allocated.
                               *   In bison, then it is dynamically allocated.*/
}

/** Input:
 *      bba: pointer to the basic block array.
 *      s:   type of the current statement, either SCANF or PRINTF.
 *  Output:
 *      returns nothing.
 *      Assigns type of the current statement in current BB.
 * */
void set_Statement_Type(BB_Array *bba, Stmnt_Type s) {
  int bI = bba->total_BBs; // Current Basic block.

  int sI = bba->BB[bI].total_Stmnts; // Current Statement.

  // Set the current Statement type as s.
  bba->BB[bI].stmnt[sI].sType = s;
}

/** Input:
 *      bba: pointer to the basic block array.
 *      s:   type of the current statement, either SCANF or PRINTF.
 *  Output:
 *      returns nothing.
 *      Increments the count of statements in the current basic block.
 * */
void add_Statement(BB_Array *bba) {
  int bI = bba->total_BBs; // Current Basic block.

  // Add a new empty statement to this basic block.
  bba->BB[bI].total_Stmnts++;

  if (bba->BB[bI].total_Stmnts >= MAX_BB_STMNTS) {
    printf("add_Statement\n");
    printf("Number of statements in basic block %d ", bI + 2);
    printf("exceeded allowed limit (MAX_BB_STATEMENTS). \n");
    printf("Fatal error. Exiting.\n");
    exit(1);
  }
}

/** Input:
 *      bba:  pointer to the basic block array.
 *      dVar: pointer to the name of the defined variable in the
 *            associated assignment statement.
 *      root: pointer to the expression tree on the RHS of the
 *            associated assignment statement.
 *      st:   pointer to the symbol table.
 *  Output:
 *      returns nothing.
 *      Stores the two attributes of the associated statement in the
 *    current empty statement in the current BB.
 * */
void add_Assignment_Statement(BB_Array *bba, char *dVar, Expr *root,
                              Symbol_Table *st) {
  /******************* FUNCTION DECLARATIONS *********************/

  // Return index of var in symbol table if found, -1 otherwise.
  int get_Var_Index_In_Symbol_Table(char *var, Symbol_Table *st);

  /***************************************************************/
  int bI = bba->total_BBs;           // Current Basic block.
  int sI = bba->BB[bI].total_Stmnts; // Current Statement.

  int vI = get_Var_Index_In_Symbol_Table(dVar, st); /* Index of dVar
                                                     * in Symbol Table. */
  if (vI == -1) {
    // Error
    printf("add_Assignment_Statement\n");
    printf("Variable %s not found in Symbol Table. ", dVar);
    printf("Basic block being processed = %d.\n", bI + 2);
    printf("Fatal error. Exiting.\n\n");
    exit(1);
  }

  bba->BB[bI].stmnt[sI].var = vI;
  bba->BB[bI].stmnt[sI].expr = root;
}

/** Input:
 *      bba:  pointer to the basic block array.
 *      root: pointer to the expression tree of the condition.
 *  Output:
 *      returns nothing.
 *      Stores the expression tree of the condition in the cond field
 *    of the current BB.
 *      Sets the Type of this basic block as conditional.
 * */
void add_Condition(BB_Array *bba, Expr *root) {
  int bI = bba->total_BBs; // Current Basic block.

  bba->BB[bI].cond = root;       // set condition.
  bba->BB[bI].bb_Type = bb_COND; // set bb type
}

/** Input:
 *      bba:  pointer to the basic block array.
 *      succ: index of an successor.
 *  Output:
 *      returns nothing.
 *      Sets the successor.
 *      if true successor == -1:
 *          sets true successor.
 *      else if false successor == -1:
 *          sets false successor.
 *      else
 *          print error.
 * */
void add_Successor(BB_Array *bba, int succ) {
  int bI = bba->total_BBs; // Current Basic block.

  if (bba->BB[bI].trueNext == -1)
    bba->BB[bI].trueNext = succ;
  else if (bba->BB[bI].falseNext == -1)
    bba->BB[bI].falseNext = succ;
  else {
    // error
    printf("add_Successor ():\n");
    printf("Basic block %d, trying to add successor while", bI + 2);
    printf(" both true and false successor exist.\n");
    printf("Fatal error. Exiting.\n\n");
    exit(1);
  }

  if (bba->BB[bI].bb_Type != bb_COND) {
    /* Had this bb been a conditional basic block its type would
     * have been set as CONDITIONAL when the if statement was
     * encoutered.
     * Since the bb type is not conditional, and a successor
     * exists, it has to be a NORMAL basic block.
     * */
    bba->BB[bI].bb_Type = bb_NORM;
  }
}

/** Input:
 *      var: name of the variable to be searched in the symbol table
 *      st:  pointer to the symbol table.
 *  Output:
 *      A positive integer, if the variable was found in symbol table.
 *      -1, if the variable was not found in the symbol table.
 *    This function perfoms a linear search on the symbol table, st,
 *  to find the variable var.
 * */
int get_Var_Index_In_Symbol_Table(char *var, Symbol_Table *st) {
  int i;
  for (i = 0; i < st->total_Symbols; i++) {
    if (strcmp(var, st->symbol[i].vName) == 0) {
      // variable found in symbol table.
      return i;
    }
  }
  // control reaches heare means variable var is not is symbol table.
  return -1;
}

/** Input:
 *      bba:  pointer to the basic block array.
 *      var:  name of the variable to be added as an attribute of the
 *          current statement.
 *      st:   pointer to the symbol table.
 *  Output:
 *      returns nothing.
 *      Stores the index of "var" in the var_Index field of the current
 *   statement.
 *      This function must be called only for PRINTF and SCANF
 *   statements.
 * */
void add_Var(BB_Array *bba, char *var, Symbol_Table *st) {
  /******************* FUNCTION DECLARATIONS *********************/

  // Return index of var in symbol table if found, -1 otherwise.
  int get_Var_Index_In_Symbol_Table(char *var, Symbol_Table *st);

  // Constructs a node in the Expression tree for the given variable
  // and returns the pointer to the node.
  Expr *construct_Var_Node(char *var, Symbol_Table *st);
  /***************************************************************/
  int bI = bba->total_BBs; // Current Basic block.

  int vI = get_Var_Index_In_Symbol_Table(var, st); /* Index of dVar
                                                    * in Symbol Table. */
  if (vI == -1) {
    // Error
    printf("add_Var\n");
    printf("Variable %s not found in Symbol Table. ", var);
    printf("Basic block being processed = %d.\n", bI + 2);
    printf("Fatal error. Exiting.\n");
    exit(1);
  }

  int sI = bba->BB[bI].total_Stmnts; // Current Statement.

  bba->BB[bI].stmnt[sI].var = vI;
  bba->BB[bI].stmnt[sI].expr = construct_Var_Node(var, st);
}

/** Input:
 *      var: name of the variable for which the expression node is
 *           to be constructed as a string.
 *      st:  pointer to the symbol table.
 *  Output:
 *      returns pointer to the node constructed for the variable "var".
 * */
Expr *construct_Var_Node(char *var, Symbol_Table *st) {
  /******************* FUNCTION DECLARATIONS *********************/

  // Return index of var in symbol table if found, -1 otherwise.
  int get_Var_Index_In_Symbol_Table(char *var, Symbol_Table *st);

  /***************************************************************/
  /*Bool negation; // controls the negation value of the var.
  // Is the var negated or not.
  if (*var == '-')
  {

      negation = True;
      var++; // Increment var to ingore -.
  }
  else if (*var == '+')
  {
      negation = False;
      var++; // Increment var to ignore +.
  }
  else
      negation = False;*/

  if (*var == '-' || *var == '+') {
    printf("construct_Var_Node (): Variable |%s| begins with + or -\n", var);
    printf("Such variables are not allowed. \n");
    printf("The error could be because the gcc is allowing statements");
    printf("like a = -c + b;\n");
    printf("I have removed support for such statements.\n");
    printf("Fatal Error. Exiting. \n\n");
    exit(1);
  }

  int vI = get_Var_Index_In_Symbol_Table(var, st); /* Index of dVar
                                                    * in Symbol Table. */
  if (vI == -1) {
    // Error
    printf("construct_Var_Node\n");
    printf("Variable %s not found in Symbol Table.\n", var);
    printf("Fatal error. Exiting.\n");
    exit(1);
  }

  Expr *node = malloc(sizeof(Expr));
  if (node == NULL) {
    // Error
    perror("construct_Var_Node, allocating memory for a node.\n");
    printf("Fatal error. Exiting.\n");
    exit(1);
  }

  node->eType = e_VAR; // Set type of this node.
  node->val_var = vI;
  // node->negation  = negation; the notion of negation has been removed.

  node->left = NULL; // Intialize children to NULL.
  node->right = NULL;

  return node;
}

/** Input:
 *      val: value of the integer literal.
 *  Output:
 *      Pointer to an instance of type Expr, representing the value, val.
 * */
Expr *construct_Int_Node(int val) {
  Expr *node = malloc(sizeof(Expr));
  if (node == NULL) {
    // Error
    perror("construct_Int_Node, allocating memory for node.\n");
    printf("Fatal error. Exiting.\n");
    exit(1);
  }

  node->eType = e_LITERAL; // Set node type.
  node->val_var = val;

  node->left = NULL;
  node->right = NULL;

  return node;
}

/** Input:
 *      oprtr: operator as a string.
 *  Output:
 *      pointer to a node of type Expr representing the oprtr.
 * */
Expr *construct_Oprtr_Node(char *oprtr) {
  Oprtr op;
  if (oprtr == NULL)
    // The node represents a Unary expression.
    op = op_IR_RELEVANT;

  else if (strcmp(oprtr, "+") == 0)
    op = op_PLUS;
  else if (strcmp(oprtr, "-") == 0)
    op = op_BINARY_MINUS;
  else if (strcmp(oprtr, "*") == 0)
    op = op_MULTIPLY;
  else if (strcmp(oprtr, "/") == 0)
    op = op_DIVIDE;
  else if (strcmp(oprtr, "%") == 0)
    op = op_MODULUS;

  else if (strcmp(oprtr, ">=") == 0)
    op = op_GRT_THN_OR_EQUAL;
  else if (strcmp(oprtr, ">") == 0)
    op = op_GRT_THN;
  else if (strcmp(oprtr, "<=") == 0)
    op = op_LESS_THN_OR_EQUAL;
  else if (strcmp(oprtr, "<") == 0)
    op = op_LESS_THN;
  else if (strcmp(oprtr, "=") == 0)
    op = op_SINGLE_EQUAL;
  else if (strcmp(oprtr, "==") == 0)
    op = op_DOUBLE_EQUAL;
  else if (strcmp(oprtr, "!=") == 0)
    op = op_NOT_EQUAL;

  else if (strcmp(oprtr, "|") == 0)
    op = op_BITWISE_OR;
  else if (strcmp(oprtr, "&") == 0)
    op = op_BITWISE_AND;
  else if (strcmp(oprtr, "~") == 0)
    op = op_NOT;
  else if (strcmp(oprtr, "<<") == 0)
    op = op_LEFT_SHIFT;
  else if (strcmp(oprtr, ">>") == 0)
    op = op_RIGHT_SHIFT;

  else if (strcmp(oprtr, "||") == 0)
    op = op_LOGICAL_OR;
  else if (strcmp(oprtr, "&&") == 0)
    op = op_LOGICAL_AND;

  else {
    // Error.
    printf("construct_Oprtr_Node:\n");
    printf("Operator = %s, not of a known type.\n", oprtr);
    printf("Fatal error. Exiting.\n");
    exit(1);
  }

  Expr *node = malloc(sizeof(Expr));
  if (node == NULL) {
    // Error
    perror("construct_Oprtr_Node, allocating memory for node.\n");
    printf("Fatal error. Exiting.\n");
    exit(1);
  }

  /* Based operator node can't be set. Not it is set in the grammar
   *   itself. * /
  // Set node type
  if (op == op_IR_RELEVANT || op == op_NOT)
      node->eType = e_UNARY;
  else
      node->eType = e_BINARY;
  */

  node->oprtr = op;
  node->left = NULL;
  node->right = NULL;

  return node;
}

/** Input:
 *      bba: pointer to the basic block array.
 *  Output:
 *      returns nothing.
 *      sets current basic block as the last basic block.
 *      Increments the count of basic blocks.
 * */
void set_Last_BB(BB_Array *bba) {
  int bI = bba->total_BBs; // Current Basic block.

  // Add a new empty statement to this basic block.
  bba->BB[bI].bb_Type = bb_LAST;

  // printf ("set_Last_BB: total_BBs = %d.\n", bba->total_BBs );

  bba->total_BBs++; /* Increment the count of basic blocks.
                     * Only at end of a basic block, count of BBs is
                     * incremented.*/
  if (bba->total_BBs >= MAX_BBs) {
    printf("set_Last_BB:\n");
    printf("Number of Basic Blocks, %d exceeded MAX_BBs.\n", bI + 2);
    printf("Fatal error, Exiting.\n");
    exit(1);
  }
}

/** Input:
 *      bba: pointer to the basic block array.
 *      var: name of the defined variable as a string.
 *      st:  pointer to the symbol table.
 *  Output:
 *      Returns nothing.
 *      Sets the bit corresponding to the current statement in the
 *    bit vector of the variable var to 1.
 *      In the definitions array sets that the current statement defines
 *    the variable var.
 * */
void set_As_Defining_Statement(BB_Array *bba, char *var, Symbol_Table *st) {
  /******************* FUNCTION DECLARATIONS *********************/

  // Pairs the indices u and v to one integer.
  int pair_Indices(int u, int v);

  // Return index of var in symbol table if found, -1 otherwise.
  int get_Var_Index_In_Symbol_Table(char *var, Symbol_Table *st);

  // Sets given bit in given bit vector.
  void set_Bit(Bit_Vector vector, int i);
  /***************************************************************/
  int vI = get_Var_Index_In_Symbol_Table(var, st);
  if (vI == -1) {
    // Error
    printf("set_As_Defining_Statement\n");
    printf("Variable %s not found in Symbol Table.\n", var);
    printf("Fatal error. Exiting.\n");
    exit(1);
  }

  int bI = bba->total_BBs;           // Current Basic block.
  int sI = bba->BB[bI].total_Stmnts; // Current Statement.

  int uI = pair_Indices(bI, sI); /* Map the BB and statement index
                                  * to one unique integer. */

  set_Bit(st->symbol[vI].def, uI); /* Set the bit corresponding
                                    * to the current statement in the bit vector
                                    * of the variable var. */

  // Set in the variable definitions array that statement uI defines vI.
  bba->def_Var[uI] = vI;
}
