#include "Types.h"
// include/Construct_PP
/** Input:
 *      pp:  pointer to PRES+.
 *      pp_obb: pointer to the array where i-th element contains indices
 *          of places and transitions corressponding to i-th BB.
 *      bI:  index of the current basic block.
 *      vI:  variable of this operand
 *  Output:
 *      Returns index of the place constrcuted for the associated
 *   variable.
 * */
int construct_Place_For_Variable(Pres_Plus *pp, PP_One_BB *pp_obb, int bI,
                                 int vI) {
  /******************* FUNCTION DECLARATIONS *********************/
  // Adds a new place to the PRES+ of bI, returns its index.
  int new_Place(Pres_Plus * pp, PP_One_BB * pp_obb, int bI);

  // Joins given place to the rest of the PRES+.
  void resolve_Link_Case(Pres_Plus * pp, PP_One_BB * pp_obb, int bI, int pI);
  /***************************************************************/
  int p = new_Place(pp, pp_obb, bI);

  pp->place[p].p_Type = p_VAR;
  pp->place[p].var = vI;

  // Join p to the rest of the PRES+.
  resolve_Link_Case(pp, pp_obb, bI, p);
  return p;
}

/** Input:
 *      pp:  pointer to PRES+.
 *      pp_obb: pointer to the array where i-th element contains indices
 *          of places and transitions corressponding to i-th BB.
 *      bI:  index of the current basic block.
 *  Output:
 *      Index of the newly added place.
 *  Adds a place to the PRES+ for basic blcok bI.
 * */
int new_Place(Pres_Plus *pp, PP_One_BB *pp_obb, int bI) {
  int p = pp->pl_count++;
  if (p >= MAX_PLACES) {
    printf("new_Place:\n");
    printf("Number of places exceeded MAX_PLACES, processing BB ");
    printf("= %d.\nFatal, Exiting.\n", bI);
    exit(1);
  }

  // Add place to the place list of PRES+ for Basic Block bI.
  int p2;
  p2 = pp_obb[bI].c_PL++; // Safe because of check above.
  pp_obb[bI].pl[p2] = p;

  // Initialization of the place.
  pp->place[p].c_Preset = 0;
  pp->place[p].c_Postset = 0;
  pp->place[p].var = -1;
  pp->place[p].p_Type = p_ERROR;

  return p;
}

/** Input:
 *      pp:  pointer to PRES+.
 *      pp_obb: pointer to the array where i-th element contains indices
 *          of places and transitions corressponding to i-th BB.
 *      bI:  index of the current basic block.
 *  Output:
 *      Index of the newly added transition.
 *  Adds a transition to the PRES+ for basic blcok bI.
 * */
int new_Transition(Pres_Plus *pp, PP_One_BB *pp_obb, int bI) {
  int t = pp->tr_count++;
  if (t >= MAX_TRANSITIONS) {
    printf("new_Transition:\n");
    printf("Number of transitions exceeded MAX_TRANSITIONS, ");
    printf("processing BB = %d.\nFatal, Exiting.\n", bI);
    exit(1);
  }

  // Add transition to the transition list of PRES+ for bI.
  int t2;
  t2 = pp_obb[bI].c_TR++; // Safe because of check above.
  pp_obb[bI].tr[t2] = t;

  // Initailize this transition.
  pp->transition[t].c_Preset = 0;
  pp->transition[t].c_Postset = 0;
  pp->transition[t].t_Type = t_ERROR;

  pp->transition[t].expr = NULL;
  pp->transition[t].guard = NULL;
  pp->transition[t].itng = -1;

  pp->transition[t].prio = pr_NORM;

  return t;
}

/** Input:
 *      pp: pointer to PRES+ intermediate.
 *      tI:  Index of a transition.
 *      pI:  Index of a place.
 *  Output:
 *      returns nothing.
 *  Adds pI to the postset of the transition tI.
 * */
void add_To_Postset_Of_Transition(Pres_Plus *pp, int tI, int pI) {
  int i = pp->transition[tI].c_Postset++;
  if (i >= MAX_POSTSET) {
    printf("add_To_Postset_Of_Transition:\n");
    printf("Number of transitions exceeded MAX_POSTSET, ");
    printf("processing transition = %d.\nFatal, Exiting.\n", tI);
    exit(1);
  }

  // Set postset of tI.
  pp->transition[tI].postset[i] = pI;
}

/** Input:
 *      pp: pointer to PRES+ intermediate.
 *      tI:  Index of a transition.
 *      pI:  Index of a place.
 *  Output:
 *      returns nothing.
 *  Adds pI to the preset of the transition tI.
 * */
void add_To_Preset_Of_Transition(Pres_Plus *pp, int tI, int pI) {
  int i = pp->transition[tI].c_Preset++;
  if (i >= MAX_PRESET) {
    printf("add_To_Preset_Of_Transition:\n");
    printf("Number of transitions exceeded MAX_PRESET, ");
    printf("processing transition = %d.\nFatal, Exiting.\n", tI);
    exit(1);
  }

  // Set preset of tI.
  pp->transition[tI].preset[i] = pI;
}

/** Input:
 *      pp: pointer to PRES+ intermediate.
 *      pI:  Index of a place.
 *      tI:  Index of a transition.
 *  Output:
 *      returns nothing.
 *  Adds tI to the postset of the place pI.
 * */
void add_To_Postset_Of_Place(Pres_Plus *pp, int pI, int tI) {
  int i = pp->place[pI].c_Postset++;
  if (i >= MAX_POSTSET) {
    printf("add_To_Postset_Of_Place:\n");
    printf("Number of places exceeded MAX_POSTSET, ");
    printf("processing place = %d.\nFatal, Exiting.\n", pI);
    exit(1);
  }

  // Set postset of pI.
  pp->place[pI].postset[i] = tI;
}

/** Input:
 *      pp: pointer to PRES+ intermediate.
 *      pI:  Index of a place.
 *      tI:  Index of a transition.
 *  Output:
 *      returns nothing.
 *  Adds tI to the preset of the place pI.
 * */
void add_To_Preset_Of_Place(Pres_Plus *pp, int pI, int tI) {
  int i = pp->place[pI].c_Preset++;
  if (i >= MAX_PRESET) {
    printf("add_To_Preset_Of_Place:\n");
    printf("Number of places exceeded MAX_PRESET, ");
    printf("processing places = %d.\nFatal, Exiting.\n", pI);
    exit(1);
  }

  // Set preset of pI.
  pp->place[pI].preset[i] = tI;
}

/** Input:
 *      pp:  pointer to PRES+.
 *      pp_obb: pointer to the array where i-th element contains indices
 *          of places and transitions corressponding to i-th BB.
 *      bI:  index of the current basic block.
 *      pI:  index of the place which shall be linked to rest of the
 *          PRES+.
 *  Output:
 *      Returns nothing.
 *      Joins the place pI to rest of the PRES+ of basic block bI.
 *  DESCRIPTION:
 *  1.  Invokes a routine to get the method how to join the given place
 *     to rest of the PRES+ corresponding to the current BB.
 *  2.  Invokes a routine based on the type of the join operaion.
 * */
void resolve_Link_Case(Pres_Plus *pp, PP_One_BB *pp_obb, int bI, int pI) {
  /******************* FUNCTION DECLARATIONS *********************/
  // Returns the Link case.
  Join_Case get_Link_Case(Pres_Plus * pp, PP_One_BB * pp_obb, int bI, int vI);

  // Joins a place to its latest defining transition.
  void handle_Join_Case(Pres_Plus * pp, PP_One_BB * pp_obb, int bI, int pI);

  /* Constructs a new input place and identity transition.
   * Joins the two input place as postset of this transition.
   * Returns Index of the new transition. */
  int handle_New_Transition_Case(Pres_Plus * pp, PP_One_BB * pp_obb, int bI,
                                 int pI_O, int pI_N);

  // Add the given place to the input set of PRES+ for give BB.
  void handle_New_Variable_Case(Pres_Plus * pp, PP_One_BB * pp_obb, int bI,
                                int pI);

  // Adds pI to the input places array.
  void add_To_Input_Places_Array(Pres_Plus * pp, PP_One_BB * pp_obb, int bI,
                                 int pI);

  // Returns the input place for variable vI, -1 if doesn't exits.
  int get_Input_Place(Pres_Plus * pp, PP_One_BB * pp_obb, int bI, int vI);
  /***************************************************************/
  // Variable associated to vI.
  int vI = pp->place[pI].var;

  Join_Case jc = get_Link_Case(pp, pp_obb, bI, vI);

  switch (jc) {
  case c_JOIN:
    handle_Join_Case(pp, pp_obb, bI, pI);
    break;
  case c_NEW_TR:
    /* Uncomment this part to add PI to input places array and
     * doing nothing else.
    add_To_Input_Places_Array (pp, pp_obb, bI, pI);
    break;*/

    /* Comment this part to handle this place as
     * NEW TRANSITION case.
     **/
    vI = pp->place[pI].var;
    int p_Old = get_Input_Place(pp, pp_obb, bI, vI);
    handle_New_Transition_Case(pp, pp_obb, bI, p_Old, pI);
    break;
  case c_NEW_VAR:
    handle_New_Variable_Case(pp, pp_obb, bI, pI);
    break;
  default:
    // error.
    printf("resolve_Link_Case:\n");
    printf("BB = %d, Place = %d, Join operaion case invalid.\n", bI, pI);
    printf("Exception, Continuing.\n");
  }
}

/** Input:
 *      pp:  pointer to PRES+.
 *      pp_obb: pointer to the array where i-th element contains indices
 *          of places and transitions corressponding to i-th BB.
 *      bI:  index of the current basic block.
 *      vI:  Index of a variable.
 *  Output:
 *      if there exists a latest defining for the variable:
 *          return case_JOIN.
 *      else if there exists an input place for the variable:
 *          return case_NEW_TRANSITION.
 *      else:
 *          return case_NEW_VARIABLE.
 * */
Join_Case get_Link_Case(Pres_Plus *pp, PP_One_BB *pp_obb, int bI, int vI) {
  /******************* FUNCTION DECLARATIONS *********************/
  // Returns the input place for variable vI, -1 if doesn't exits.
  int get_Input_Place(Pres_Plus * pp, PP_One_BB * pp_obb, int bI, int vI);
  /***************************************************************/

  // latest defining transition of the associated variable.
  int lI;
  lI = pp_obb[bI].ldt[vI];

  // Input place for vI.
  int iP;
  iP = get_Input_Place(pp, pp_obb, bI, vI);

  if (lI != -1) {
    return c_JOIN;
  } else if (iP != -1) {
    // Input place for variable vI.
    return c_NEW_TR;
  } else {
    return c_NEW_VAR;
  }
  return c_ERROR;
}

/** Input:
 *      pp:  pointer to PRES+.
 *      pp_obb: pointer to the array where i-th element contains indices
 *          of places and transitions corressponding to i-th BB.
 *      bI:  index of the current basic block.
 *      pI:  Index of the place that needs to be merged to the PRES+.
 *  Output:
 *      t <= The latest defining transition of the variable associated
 *          to pI.
 *      Add pI to the postset of t and vice versa.
 * */
void handle_Join_Case(Pres_Plus *pp, PP_One_BB *pp_obb, int bI, int pI) {
  /******************* FUNCTION DECLARATIONS *********************/
  // Adds given place to the postset of given transition.
  void add_To_Postset_Of_Transition(Pres_Plus * pp, int tI, int pI);

  // Adds given transition to the preset of given place.
  void add_To_Preset_Of_Place(Pres_Plus * pp, int pI, int tI);
  /***************************************************************/
  // Variable associated to pI.
  int vI;
  vI = pp->place[pI].var;

  // latest defining transition of the associated variable.
  int t;
  t = pp_obb[bI].ldt[vI];

  // Add pI to the postset of t and vice versa.
  add_To_Postset_Of_Transition(pp, t, pI);
  add_To_Preset_Of_Place(pp, pI, t);
}

/** Input:
 *      pp:  pointer to PRES+.
 *      pp_obb: pointer to the array where i-th element contains indices
 *          of places and transitions corressponding to i-th BB.
 *      bI:  index of the current basic block.
 *      pI:  Index of the place that needs to be merged to the PRES+.
 *  Output:
 *      returns nothing.
 *  Description:
 *      p1 <= Construct a new place with same variable as associated
 *           to pI.
 *      t  <= A new Identity transition.
 *      Mark t as the latest defining transition of the associated
 *    variable.
 *      Add p1 to the preset of t and vice versa.

 *      Add pI_O to the postset of t and vice versa.
 *      Add pI_N to the postset of t and vice versa.
 *
 *      In input places array, remove pI_O with p1.
 * */
int handle_New_Transition_Case(Pres_Plus *pp, PP_One_BB *pp_obb, int bI,
                               int pI_O, int pI_N) {
  /******************* FUNCTION DECLARATIONS *********************/
  // Adds a new place to the PRES+ of bI, returns its index.
  int new_Place(Pres_Plus * pp, PP_One_BB * pp_obb, int bI);

  // Add a new transition to the PRES+ of bI, returns its index.
  int new_Transition(Pres_Plus * pp, PP_One_BB * pp_obb, int bI);

  // Adds given place to the postset of given transition.
  void add_To_Postset_Of_Transition(Pres_Plus * pp, int tI, int pI);

  // Adds given transition to the preset of given place.
  void add_To_Preset_Of_Place(Pres_Plus * pp, int pI, int tI);

  // Adds given place to the preset of given transition.
  void add_To_Preset_Of_Transition(Pres_Plus * pp, int tI, int pI);

  // Adds given transtion to the postset of given place.
  void add_To_Postset_Of_Place(Pres_Plus * pp, int pI, int tI);
  /***************************************************************/
  int vI; // Index of the associated variable.
  vI = pp->place[pI_O].var;

  // New place representing same variable as pI.
  int nP = new_Place(pp, pp_obb, bI);

  pp->place[nP].p_Type = p_VAR;
  pp->place[nP].var = vI;

  // New Identity transition.
  int nT = new_Transition(pp, pp_obb, bI);
  pp->transition[nT].t_Type = t_ID;

  // Mark nT as LDT of associated var.
  pp_obb[bI].ldt[vI] = nT; /* This is necessary because
                            * if it is not done, next place for vI will be
                            * treated as NEW TRANSITION case also. */

  // Add nP to the preset of nT and vice versa.
  add_To_Preset_Of_Transition(pp, nT, nP);
  add_To_Postset_Of_Place(pp, nP, nT);

  // Add pI_O to the postset of nT and vice versa.
  add_To_Postset_Of_Transition(pp, nT, pI_O);
  add_To_Preset_Of_Place(pp, pI_O, nT);

  if (pI_N != -1) {
    // pI_N is a place which will be used by some other transition.
    // Add pI_N to the postset of nT and vice versa.
    add_To_Postset_Of_Transition(pp, nT, pI_N);
    add_To_Preset_Of_Place(pp, pI_N, nT);
  }

  // In the input places array of bI, remove pI_O by nP.
  int i;
  Bool found = False; // if pI_O is not found in input array.

  // For each input place associated to bI.
  for (i = 0; i < pp_obb[bI].c_in; i++) {
    if (pp_obb[bI].in[i] == pI_O) {
      pp_obb[bI].in[i] = nP;
      found = True;
      break;
    }
  }

  if (found == False) {
    printf("handle_New_Transition_Case:\n");
    printf("Place = %d, not found in input array of PRES+ ", pI_O);
    printf("corressponding to BB = %d, Var = %d.\n", bI + 2,
           pp->place[pI_O].var);
    printf("Fatal, Exiting.\n");
    exit(1);
  }
  return nT;
}

/** Input:
 *      pp:  pointer to PRES+.
 *      pp_obb: pointer to the array where i-th element contains indices
 *          of places and transitions corressponding to i-th BB.
 *      bI:  index of the current basic block.
 *      pI:  Index of the place that needs to be merged to the PRES+.
 *  Output:
 *      Add pI to the input places array.
 * */
void handle_New_Variable_Case(Pres_Plus *pp, PP_One_BB *pp_obb, int bI,
                              int pI) {
  int i = pp_obb[bI].c_in++;
  if (i >= MAX_PLACES) {
    printf("handle_New_Variable_Case:\n");
    printf("Number of input places exceeded MAX_PLACES, ");
    printf("processing BB = %d, place = %d.\nFatal, Exiting.\n", bI, pI);
    exit(1);
  }

  pp_obb[bI].in[i] = pI;
}

/** Input:
 *      pp:  pointer to PRES+.
 *      pp_obb: pointer to the array where i-th element contains indices
 *          of places and transitions corressponding to i-th BB.
 *      bI:  index of the current basic block.
 *      pI:  Index of the place that needs to be merged to the PRES+.
 *  Output:
 *      Add pI to the input places array.
 * */
void add_To_Input_Places_Array(Pres_Plus *pp, PP_One_BB *pp_obb, int bI,
                               int pI) {
  // pp is not necessary. Remove it.

  int i = pp_obb[bI].c_in++;
  if (i >= MAX_PLACES) {
    printf("add_To_Input_Places_Array ():\n");
    printf("Number of input places exceeded MAX_PLACES, ");
    printf("processing BB = %d, place = %d.\nFatal, Exiting.\n", bI, pI);
    printf("Fatal Error. Exiting. \n\n");
    exit(1);
  }

  pp_obb[bI].in[i] = pI;
}

/** Input:
 *      pp:  pointer to PRES+.
 *      pp_obb: pointer to the array where i-th element contains indices
 *          of places and transitions corressponding to i-th BB.
 *      bI:  index of the current basic block.
 *      vI:  index of the variable corresponding to whom we need to find
 *          the input place.
 *  Output:
 *      Returns the index of the place if there exists an input place
 *    associated to the variable vI.
 *      -1 other wise.
 * */
int get_Input_Place(Pres_Plus *pp, PP_One_BB *pp_obb, int bI, int vI) {
  // Input place for vI.
  int i, p;
  for (i = 0; i < pp_obb[bI].c_in; i++) {
    p = pp_obb[bI].in[i];
    if (pp->place[p].var == vI) {
      return p;
    }
  }
  return -1;
}

/** Input:
 *      pp:  pointer to PRES+.
 *      pp_obb: pointer to the array where i-th element contains indices
 *          of places and transitions corressponding to i-th BB.
 *      bI:  index of the current basic block.
 *  Output:
 *      Returns index of the DUMMY place constrcuted.
 * */
int new_Dummy_Place(Pres_Plus *pp, PP_One_BB *pp_obb, int bI) {
  /******************* FUNCTION DECLARATIONS *********************/
  // Adds a new place to the PRES+ of bI, returns its index.
  int new_Place(Pres_Plus * pp, PP_One_BB * pp_obb, int bI);

  /***************************************************************/
  int p = new_Place(pp, pp_obb, bI);

  pp->place[p].p_Type = p_DUMMY;
#ifdef CHECK_EQV
  pp->place[p].var = DUM_VAR_INDEX;
#endif // CHECK_EQV

  // Add p to the input array of bI.
  int i = pp_obb[bI].c_in++;
  if (i >= MAX_PLACES) {
    printf("new_Dummy_Place:\n");
    printf("Number of input places exceeded MAX_LIST_SIZE, ");
    printf("processing BB = %d, place = %d.\nFatal, Exiting.\n", bI, p);
    exit(1);
  }

  pp_obb[bI].in[i] = p;
  return p;
}

/** Input:
 *    pp: Pointer to the PRES+.
 *    tI: Index of a transition.
 *    arr: Pointer to an array to store the vars defined by transition
 *          tI. size of the array should be MAX_POSTSET.
 *  Output:
 *    Returns number of vars added to array arr.
 *  Functionality:
 *
 * */
int get_Vars_In_Postset(const Pres_Plus *pp, const int tI, int *arr) {
  int i, pI;

  // For each place in the post-set of transition tI.
  for (i = 0; i < pp->transition[tI].c_Postset; i++) {
    pI = pp->transition[tI].postset[i];
    switch (pp->place[pI].p_Type) {
    case p_VAR:
    case p_SCANF:
    case p_PRINTF:
      arr[i] = pp->place[pI].var;
      break;
    case p_DUMMY:
#ifdef CONVERT_PP // Only if equivalence check is to be done.
      /* Soumyadip's code requires each place to have a variable
       *   associated to it. For that purpose only the a
       *   dummy variable is added to the model construction.
       * */
      arr[i] = DUM_VAR_INDEX;
#endif
      break;
    default:
      printf("get_Vars_In_Postset (): Place = %d has ", pI);
      printf("invalid type.\n");
      printf("Fatal Error. Exiting.\n\n");
      exit(1);
    }
  }
  return i;
}

/** Inputs:
 *       an integer array and length of the array.
 *  Output:
 *      Returns the number of integers in the array after removing
 *      duplicates.
 *  Functionality:
 *      Removes duplicates from the integer array arr containing variables.
 * */
int remove_Duplicates_Var_Arr(int *arr, int len) {
  int l, r; // Left and right end of the array.

  Bool farr[MAX_VARIABLES];

  int i;
  for (i = 0; i < MAX_VARIABLES; i++)
    farr[i] = False; // Initializing the flag array.

  int vI;
  l = 0;
  r = len - 1;
  // For each element in the array.
  while (l <= r) {
    vI = arr[l];
    // printf ("len = %d, l = %d, r = %d, vI = %d.\n", len, l, r, vI);
    if (farr[vI] == True) {
      // Duplicate occurrence of vI.
      arr[l] = arr[r];
      r--;
    } else {
      // First occurrence of vI.
      farr[vI] = True;
      l++;
    }
  }
  return r + 1; // +1 because number of elements is returned.
}
