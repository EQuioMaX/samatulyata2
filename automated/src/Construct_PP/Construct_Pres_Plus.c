
#include "Types.h" // Kulwant's header file.
#include "types.h" // Soumaydip's header file.

int main(int argc, char *argv[]) {
  /******************* FUNCTION DECLARATIONS *********************/
  // Initializes various features for debbuging.
  void initialize_Debbuging();

  /* Intiailizes the instances of the basic block array and symbol
   * table. */
  extern int construct_BBArray_and_ST(char *inFile, BB_Array *bba,
                                      Symbol_Table *st);

  // Prints the basic blocks array to file specified in file.
  extern void print_BB_Array_To_File(BB_Array * bba, Symbol_Table * st,
                                     char *file);

  /* Constructs and returns a pointer to PRES+. */
  Pres_Plus *construct_Pres_Plus(BB_Array * bba, Symbol_Table * st);

  // Print BB Array with live variable analysis and statements.
  void print_BB_Array_To_File_Without_Reaching_Defs(
      BB_Array * bba, Symbol_Table * st, char *file);
  // Prints a PRES+ to the file specified in file.
  void print_Pres_Plus(const Pres_Plus *pp, const Symbol_Table *st, char *file);

#ifdef CONVERT_PP
  // Converts Kulwant's PRES+ to Soumyadip's PRES+.
  PRESPLUS *convert_Kul_to_Som_PP(Pres_Plus * pp, Symbol_Table * st);
#endif

#ifdef CHECK_EQV
  void run_Soumyadip_Prog(char *, char *);
#endif
  /***************************************************************/

  /**** To extend my module to Soumyadip's module. ***************/
#ifdef CHECK_EQV
  if (argc == 3) {
    run_Soumyadip_Prog(argv[1], argv[2]);
  }
  return 0;
#endif

  /***** My (Kulwant's) orginal code. ******************************/
  if (argc != 2) {
    printf("main: Number of inputs not correct.\n");
    printf("The command line input should be: <executable> ");
    printf("<input .cfg file>\n");
    printf("Fatal Error, exiting.\n");
    exit(1);
  }

  initialize_Debbuging();

  /* Inovke the routine to construct the Basic block array and
   * the symbol table.
   * */
  BB_Array *bba = malloc(sizeof(BB_Array));        // The basic block array.
  Symbol_Table *st = malloc(sizeof(Symbol_Table)); // The symbol table.

  if (bba == NULL || st == NULL) {
    perror("main (): bba or st, memory allocation failed: ");
    exit(1);
  }

  construct_BBArray_and_ST(argv[1], bba, st);

// Print Basic block array to file.
#ifdef PRINT_BB_ARRAY
  print_BB_Array_To_File_Without_Reaching_Defs(bba, st, PRINT_BB_ARRAY);
#endif
#ifdef PRINT_BB_ARRAY_LOG
  print_BB_Array_To_File(bba, st, PRINT_BB_ARRAY_LOG);
#endif

  /*printf ("Total bbs = %d, sizeof (BB_Array)=%d, sizeof (Symbol_Table) =
     %d\n", bba->total_BBs, sizeof (BB_Array), sizeof (Symbol_Table)); */

  Pres_Plus *pp = construct_Pres_Plus(bba, st);

#ifdef PRINT_PRES_PLUS
  print_Pres_Plus(pp, st, PRINT_PRES_PLUS);
#endif

#ifdef CONVERT_PP
  printf("Construction of Kulwant's PRES+ complete.\n");
  printf("Converting Kulwant's PRES+ to Soumyadip's PRES+.\n\n");

  PRESPLUS *spp;                       // A pointer to Soumyadip's PRES+.
  spp = convert_Kul_to_Som_PP(pp, st); /* Convert's Kulwant's PRES+
                                        * to Soumyadip's PRES+. */

  printf("Converstion complete.\n\n");
#endif

  /** Free the memory allocated on the heap. *********************/
  free(bba);
  free(st);

  return 0;
}

/** Input:
 *      bba: pointer to basic block array.
 *      st:  pointer to the symbol table.
 *  Output:
 *      Returns a pointer to an instance of Pres Plus for given Basic
 *    blocks array.
 *  Description:
 *  1.   Allocates memory for the PRES+ on HEAP.
 *  2.   For each basic block invokes a routine to construct PRES+.
 *  3.   For each basic block invokes a routine to link its PRES+ to
 *     rest of the PRES+.
 * */
Pres_Plus *construct_Pres_Plus(BB_Array *bba, Symbol_Table *st) {
  /******************* FUNCTION DECLARATIONS *********************/
  // Constructs PRES+ for a basic block.
  void construct_PP_One_BB(Pres_Plus * pp, PP_One_BB * pp_obb, BB_Array * bba,
                           int bI, Symbol_Table *st);

  // Initailizes PRES+ to default values.
  void initialize_Pres_Plus(Pres_Plus * pp);

  // Plots the PRES+ as an image.
  void plot_Pres_Plus(Pres_Plus * pp, PP_One_BB * pp_obb, BB_Array * bba,
                      Symbol_Table * st);

  // Plots the PRES+ with basic block indices of each place and tran.
  void plot_Pres_Plus_With_BB_Indices(Pres_Plus * pp, PP_One_BB * pp_obb,
                                      BB_Array * bba, Symbol_Table * st);

  /*/ Joins the PRES+ for given BB to rest of the PRES+.
  void join_To_Rest_PP (Pres_Plus *pp, PP_One_BB *pp_obb,
                          BB_Array *bba, int bI,
                          Symbol_Table *st, int *ldt_PP);*/

  // Constructs Output places from Output transitions in each BB.
  void construct_Output_Places(Pres_Plus * pp, PP_One_BB * pp_obb, int bI);

  // Attaches the subnets to get the complete PRES+ net.
  void attach_subnets(Pres_Plus * pp, PP_One_BB * pp_obb, BB_Array * bba,
                      Symbol_Table * st);

  // Sets the Input and Output places of the PRES+.
  void set_Input_Output_Places(Pres_Plus * pp);

  // Joins PRES+ of individual BBs to construct the overal PRES+ net.
  void join_PP_of_individual_BBs(Pres_Plus * pp, PP_One_BB * pp_obb,
                                 BB_Array * bba, Symbol_Table * st);

  // Prints the var and transition defitions table to a file.
  // void print_Var_Def_Transition_Table (Pres_Plus *pp, Symbol_Table *st);

  // Prints PRES+ coressponding to specified BB to file PRINT_PP_BB.
  // void print_PP_BB_Wise (Pres_Plus *pp, PP_One_BB *pp_obb, int bI,
  //                        Symbol_Table *st, char *output_file);

  // Prints the PRES+ to file specified in PRINT_PRES_PLUS.
  void print_PP_Subnet_Wise(Pres_Plus * pp, PP_One_BB * pp_obb, BB_Array * bba,
                            Symbol_Table * st);

  // Print for the python script.
  void print_PP_for_Python_Script(Pres_Plus * pp, Symbol_Table * st,
                                  PP_One_BB * pp_obb, BB_Array * bba);
  /***************************************************************/
  // Allocate memory for PRES+.
  Pres_Plus *pp = malloc(sizeof(Pres_Plus));

  // Allocate an array for PRES+ for each BB.
  // PP_One_BB pp_obb[MAX_BBs]; // Going for allocation on HEAP. March 23, 2016.
  PP_One_BB *pp_obb = malloc(MAX_BBs * sizeof(PP_One_BB));

  if (pp == NULL || pp_obb == NULL) // || pp_obb... added on Mar 23, 2016.
  {
    perror("construct_Pres_Plus ():");
    printf(
        "Memory couldn't be allocated for PRES+ or Subnets (pp_obb) array.\n");
    printf("Fatal error. Exiting.\n\n");
    exit(1);
  }

  /* printf ("sizeof (Pres_Plus)=%d, sizeof (PP_One_BB) = %d\n",
                              sizeof (Pres_Plus), sizeof (PP_One_BB)); */

  initialize_Pres_Plus(pp);

  int i;
  // Construct PRES+ for each BB.
  for (i = 0; i < bba->total_BBs; i++) {
    construct_PP_One_BB(pp, pp_obb, bba, i, st);
  }

  /********* attach subnets  **************************************/
  attach_subnets(pp, pp_obb, bba, st);

  // set_Reaching_Transitions (pp_obb, bba);

  /* This code is redundant.
  int ldt_PP[MAX_VARIABLES+1]; // used while merging Input places.
  for (i = 0; i < MAX_VARIABLES+1; i++)
      ldt_PP[i] = -1; // Initialization.

  // Link PRES+ for each BB to rest of the PRES+.
  for (i = 0; i < bba->total_BBs; i++)
  {
      join_To_Rest_PP (pp, pp_obb, bba, i, st, ldt_PP);
  }

  // Join PRES+
  // join_PP_of_individual_BBs (pp, pp_obb, bba, st);
  */

#ifdef CONSTRUCT_OUTPUT_PLACES
  /* Construct Output places. Ideally printf statment should produce
   *   the output places only.
   * Only for Soumyadip's code it is to be done.
   * */
  for (i = 0; i < bba->total_BBs; i++) {
    construct_Output_Places(pp, pp_obb, i);
  }
#endif

// print_Var_Def_Transition_Table (pp, st); No more relevant.

// Set Input and Output places.
// set_Input_Output_Places (pp);

/* #ifdef PLOT_PP
    plot_Pres_Plus (pp, pp_obb, bba, st);
#endif */
#ifdef PLOT_PP_WITH_BB_INDX
  plot_Pres_Plus_With_BB_Indices(pp, pp_obb, bba, st);
#endif

// Print the PRES+ subnet wise.
#ifdef PRINT_PP_SUBNET
  print_PP_Subnet_Wise(pp, pp_obb, bba, st);
#endif

#ifdef OUT_PY
  print_PP_for_Python_Script(pp, st, pp_obb, bba);
#endif

  // Proper PRES+ is printed in the main only.

  /** Free the memory allocated on the heap. *********************/
  free(pp_obb);

  return pp;
}

/** Input:
 *      pp: pointer to a PRES+ net.
 *  Output:
 *      initializes pp to default values.
 * */
void initialize_Pres_Plus(Pres_Plus *pp) {
  pp->c_in = 0;
  pp->c_out = 0;

  pp->pl_count = 0;
  pp->tr_count = 0;

  /* Not used any more. The data structure has been removed.
  int i;
  for (i = 0; i < MAX_VARIABLES; i++)
  {
      pp->vds[i].tr_count = 0;
  }*/
}

/** Input:
 *      None:
 *  Output:
 *      returns nothing.
 *      Initalizes various files to blank if the DEBUT flag is set.
 * */
void initialize_Debbuging() {
#ifdef PRINT_SYMBOL_TABLE
  FILE *f1 = fopen(PRINT_SYMBOL_TABLE, "w+");
  if (f1 != NULL)
    fclose(f1);
#endif

#ifdef PRINT_BB_ARRAY
  FILE *f2 = fopen(PRINT_BB_ARRAY, "w+");
  if (f2 != NULL)
    fclose(f2);
#endif

#ifdef PRINT_PRES_PLUS
  FILE *f3 = fopen(PRINT_PRES_PLUS, "w+");
  if (f3 != NULL)
    fclose(f3);
#endif

#ifdef DEBUG
  FILE *f;
  f = fopen(O_REACHING_DEFs, "w+");
  if (f != NULL)
    fclose(f);

  f = fopen(O_GEN_KILL_ALL_BB, "w+");
  if (f != NULL)
    fclose(f);

  f = fopen(O_GEN_KILL_ALL_STMNT, "w+");
  if (f != NULL)
    fclose(f);

  f = fopen(PRINT_PP_ST, "w+");
  if (f != NULL)
    fclose(f);

  f = fopen(PRINT_PP_BB, "w+");
  if (f != NULL)
    fclose(f);

  f = fopen(PLOT_PP, "w+");
  if (f != NULL)
    fclose(f);

  f = fopen(PLOT_PP_WITH_BB_INDX, "w+");
  if (f != NULL)
    fclose(f);

  f = fopen(PRINT_JOINING_DEBUG, "w+");
  if (f != NULL)
    fclose(f);

  f = fopen(OUT_MERGE_DECISION, "w+");
  if (f != NULL)
    fclose(f);

  f = fopen(OP_ATTACH_SEQ, "w+");
  if (f != NULL)
    fclose(f);

  f = fopen(OUT_PY, "w+");
  if (f != NULL)
    fclose(f);

  f = fopen(PRINT_BB_ARRAY_LOG, "w+");
  if (f != NULL)
    fclose(f);

  f = fopen(OUT_DEF_USE_SETs, "w+");
  if (f != NULL)
    fclose(f);

  f = fopen(VAR_TRANS_DEF, "w+");
  if (f != NULL)
    fclose(f);
#endif
}

/** Input:
 *      bba: Pointer to the basic blocks array.
 *      bI:  Index of the basic block currently being considered.
 *      vI:  Index of the variable corresponding to which the live
 *           definitions shall be stored in rd.
 *      rd:  An empty of size MAX_BBs, to which output shall be stored.
 *  Output:
 *      Number enteries made to the rd array.
 *    This function stores the indices of the statements which define
 *  variable vI to the array rd and returns their count.
 * */
int get_Reaching_Definitions(BB_Array *bba, int bI, int vI, int *rd) {
  /******************* FUNCTION DECLARATIONS *********************/
  // Returns the number of enteries made to arr.
  // Arr will contain the indices coressponding to 1 in bv.
  int get_Set_Bit_Indices(Bit_Vector bv, int *arr);
  /***************************************************************/
  int set_bits[MAX_STMNTS] = {
      -1};          /* Reaching definitions. Indices of
                     * reaching definitions of a variable. Size is MAX_STMNTS because
                     * atmost all the bits in the given bit vector can be one.
                     * */
  int sb_count = 0; // Number of enteries in set_bits.

  // Get indices of the set bits.
  sb_count = get_Set_Bit_Indices(bba->BB[bI].Din, set_bits);

  int i, k;
  // For each set bit.
  for (i = 0, k = 0; i < sb_count; i++) {
    if (bba->def_Var[set_bits[i]] == vI) {
      // Collect those definitions from set_bits which define vI.
      rd[k++] = set_bits[i];
    }
  }
  return k;
}

/** Input:
 *      bba: Pointer to the basic blocks array.
 *      bI:  Index of the basic block currently being considered.
 *      vI:  Index of the variable corresponding to which the live
 *           definitions shall be stored in rd.
 *      defs:  An empty of size MAX_BBs, to shich output shall be stored.
 *  Output:
 *      Number enteries made to the defs array.
 *    This function stores the indices of the statements which define
 *  variable vI and are present in Dout of bI.
 * */
int get_Definitions_In_Dout(BB_Array *bba, int bI, int vI, int *defs) {
  /******************* FUNCTION DECLARATIONS *********************/
  // Returns the number of enteries made to arr.
  // Arr will contain the indices coressponding to 1 in bv.
  int get_Set_Bit_Indices(Bit_Vector bv, int *arr);
  /***************************************************************/
  int set_bits[MAX_STMNTS] = {
      -1};          /* Reaching definitions. Indices of
                     * reaching definitions of a variable. Size is MAX_STMNTS because
                     * atmost all the bits in the given bit vector can be one.
                     * */
  int sb_count = 0; // Number of enteries in set_bits.

  // Get indices of the set bits.
  sb_count = get_Set_Bit_Indices(bba->BB[bI].Dout, set_bits);

  int i, k;
  // For each set bit.
  for (i = 0, k = 0; i < sb_count; i++) {
    if (bba->def_Var[set_bits[i]] == vI) {
      // Collect those definitions from set_bits which define vI.
      defs[k++] = set_bits[i];
    }
  }
  return k;
}

/** Input:
 *      pp_obb: pointer to the array where i-th element contains indices
 *          of places and transitions corressponding to i-th BB.
 *      bb:  pointer to the basic blocks array.
 *      bI:  current Basic Block Index.
 *      vI:  Index of a variable.
 *      ndp: pointer to array in which the non defining parents shall
 *          be stored. size of this array is MAX_BBs.
 *  Output:
 *      Number of enteries made to the ndp array.
 *  DESCRITPION:
 *    Adds those parents of bI to ndp which doesn't define varaible
 *  vI.
 * */
int get_Non_Defining_Parents(PP_One_BB *pp_obb, BB_Array *bba, int bI, int vI,
                             int *ndp) {
  // Evaluate from bit vector.

  int i, j, k;
  int p;     // A parent of b.
  int count; // To keep count of enteries in ndp.
  Bool def;

  // For each parent of bI:
  count = 0;
  for (i = 0; i < bba->BB[bI].parent_Count; i++) {
    p = bba->BB[bI].parent[i];

    // For each statement of parent p:
    def = False;
    j = MAX_BB_STMNTS * p;
    for (k = 0; k < bba->BB[bI].total_Stmnts; j++, k++) {
      if (bba->def_Var[j] == vI) {
        // Parent p defines vI;
        def = True;
        break;
      }
    }
    if (def == False) {
      // Immediate parent p doesn't define vI.
      ndp[count++] = p;
    }
  }
  return count;
}

/** Input:
 *      pp: pointer to the PRES+.
 *      vI: index of the variable to be searched.
 *  Output:
 *      index of the place with associated variable as vI, if found
 *    in input places array of the PRES+.
 *      -1, other wise.
 * */
int find_Place_In_Input_Array(Pres_Plus *pp, int vI) {
  int i, p;
  for (i = 0; i < pp->c_in; i++) {
    p = pp->in[i];
    if (pp->place[p].var == vI)
      return p;
  }
  return -1;
}

/** Input:
 *      pp:  Pointer to PRES+.
 *      pp_obb: pointer to the array where i-th element contains indices
 *          of places and transitions corressponding to i-th BB.
 *      bI:   Basic Block Index.
 *  Output:
 *      Retunrs nothing.
 *  DESCRITPION:
 *    Constructs places for the output transition with associated
 *  variables being the variables definied by that transition.
 * */
void construct_Output_Places(Pres_Plus *pp, PP_One_BB *pp_obb, int bI) {
  /******************* FUNCTION DECLARATIONS *********************/
  // Constructs a place for the given variable.
  int construct_Place_For_Variable(Pres_Plus * pp, PP_One_BB * pp_obb, int bI,
                                   int vI);

  // Adds given place to the postset of given transition.
  void add_To_Postset_Of_Transition(Pres_Plus * pp, int tI, int pI);

  // Adds given transition to the preset of given place.
  void add_To_Preset_Of_Place(Pres_Plus * pp, int pI, int tI);

  // Returns true if variable vI is found in a place in postset of tI.
  Bool is_Var_In_Postset_Of_Transition(Pres_Plus * pp, int tI, int vI);
  /***************************************************************/
  // For ldt of each variable:
  int t, i;
  Bool b;
  int nP; // New place.
  for (i = 0; i < MAX_VARIABLES; i++) {
    t = pp_obb[bI].ldt[i];
    if (t != -1) {
      b = is_Var_In_Postset_Of_Transition(pp, t, i);
      if (b == False) {
        // t doesn't have a post place with i as associated var.
        nP = construct_Place_For_Variable(pp, pp_obb, bI, i);
      }
    }
  }
}

/** Input:
 *      pp:  pointer to PRES+.
 *      tI:  Index of a transition.
 *      vI:  Index of a variable.
 *  Output:
 *      True, if transition tI has a post place whoose associated
 *          variable is vI.
 *      False, other wise.
 * */
Bool is_Var_In_Postset_Of_Transition(Pres_Plus *pp, int tI, int vI) {
  int i, p;
  for (i = 0; i < pp->transition[tI].c_Postset; i++) {
    p = pp->transition[tI].postset[i];
    if (pp->place[p].var == vI)
      return True;
  }
  return False;
}

/** INPUT:
 *      pp: pointer to PRES+.
 *  Output:
 *      Returns nothing.
 *    This function sets the input and output places of the PRES+.
 * */
void set_Input_Output_Places(Pres_Plus *pp) {
  int i, cI, cO;
  // For each place.
  for (i = 0; i < pp->pl_count; i++) {
    if (pp->place[i].c_Preset == 0) {
      cI = pp->c_in++; // Safe because loop bounded by MAX_PLACES.
      pp->in[cI] = i;
    }

    if (pp->place[i].c_Postset == 0) {
      cO = pp->c_out++; // Safe because loop bounded by MAX_PLACES.
      pp->out[cO] = i;
    }
  }
}
