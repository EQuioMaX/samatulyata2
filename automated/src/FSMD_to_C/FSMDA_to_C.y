/* FSMD Parser */

%{

 #include <stdio.h>
 #include <stdlib.h>
 #include <string.h>

 // #include "FsmdaHeader.h"
 // #include "parser.h"
 
 
 /* Kulwant's code begins. **************************************/
 // void print_statement_list (STMT_LIST *);
 // #define OUT_C_CODE "Outputs/C_Code.c"
 FILE *g_fp; // A global variable to print c code to a file.
 int   g_total_conds, g_cur_cond; /* Global variables: Total conditions
       * in a statement in FSMDA and number of the condition being read by the parser. */ 
 
 
 
 #define MAX_SYMBOLS 100 // Total number of symbols allowed in the program.
 #define MAX_VAR_LEN 50  // Max number of characters in a variable.
 typedef struct {
    int num_Symbols;    // Number of symbols present in the symbol array.
    char *symbol[MAX_SYMBOLS][MAX_VAR_LEN]; /* Each element points to a character string
            * dynamically allocated (using malloc()) while parsing. */
 } Symbol_Table;
 
 Symbol_Table *g_ST; /* A global pointer to a Symbol_Table so that the symbol table can be used
            * in the rules of the grammar. */
 
 
 long int g_symb_offset; // Offset in the file from beginning of the file for printing symbols.
 long int g_origin; 
 
 extern int yydebug;

 
 /* A global declaration for using the function in the grammar. 
  * This functions adds a variable to the symbol table, only if the variable is
  * not already present in the symbol table. 
  * Returns 1 if the variable var is added to the symbol table, 0 otherwise.*/
 int add_var_to_Symbol_Table (Symbol_Table *st, char *var);
 /* Kulwant's code ends. ****************************************/

%}

%union{
	char* string;
	struct normalized_cell* norm;
	struct Assign* data_trans;
	struct Assign_list* dt_list;
	struct Substatement* substmt;
	struct Substatement_list* substmt_list;
	struct Statement* stmt;
	struct Statement_list* stmt_list;
	}

%token <string> IDENTIFIER CONSTANT STRING_LITERAL
%token <string> LE_OP GE_OP EQ_OP NE_OP
%token <string> AND OR NOT

%token <string> MULT DIV MOD PLUS MINUS /* Added by Kulwant Singh. */

%token <string> READ OUT

%token <string> EQUAL COMMA SEMIC SEPARATOR
%token <string> LT_OP GT_OP LEFT_BR RIGHT_BR LEFT_SQBR RIGHT_SQBR

%type <string> variable relational_operator state_name
%type <string> fsmd_name
%type <string> transitions

%type <string> primary_expression array_expression 
%type <string> unary_expression multiplicative_expression expression
%type <string> primary_conditional_expression secondary_conditional_expression conditional_expression
%type <string> argument_list uninterpreted_function
%type <string> function_name

%type <string> assignment_expression
%type <string> operation_list operations
%type <string> substatement
%type <string> substatement_list
%type <string> statement
%type <string> statement_list


%left MULT DIV MOD
%left PLUS MINUS

%start translation_fsmd
%%

variable
	: IDENTIFIER	{ if (add_var_to_Symbol_Table (g_ST, $1))
	                  {
	                     // Point file pointer to the position of printing symbols.
	                     // printf ("g_symb_offset = %ld, seek set = %d\n", g_symb_offset, SEEK_SET);
	                     if (fseek (g_fp, g_symb_offset, SEEK_SET))
	                     {
	                        perror ("fseek failed.");
	                        printf ("Variable = %s\n", $1);
	                        perror ("Fatal Error, Exiting..\n\n");
	                        exit (1);
	                     }
	                     
	                     // Print the symbol.
	                     fprintf (g_fp, "\tint %s;\n", $1);
	                     // printf ("int %s;\n", $1);
	                     
	                     // Getting the new current offset.
	                     if ((g_symb_offset = ftell (g_fp)) == -1)
	                     {
	                        perror ("ftell () failed in grammar, Fatal Error, Exiting.");
	                        exit (1);
	                     }
	                     
	                     // Pointer file pointer to the end of the file.
	                     if (fseek (g_fp, 0, SEEK_END))
	                     {
	                        perror ("fseek failed.");
	                        printf ("Variable = %s\n", $1);
	                        perror ("Fatal Error, Exiting..\n\n");
	                        exit (1);
	                     }
	                  } 
	                  
	                  $$ = $1;
	                  
	                  /* Kulwant: strcpy($$,$1); */}
	;

variable_list
	: variable                  { ; /* Kulwant: The vairable_list rule is not at all used by me. */}
	| variable_list variable
	;

primary_expression
	: variable						{ $$ = $1; /* Kulwant: $$ = createVariable($1);*/ }
	| CONSTANT						{ $$ = $1; /* Kulwant: $$ = createConstant($1);*/ }
	| LEFT_BR expression RIGHT_BR	{ strcat ($1, $2); strcat ($1, $3); $$ = $1; 
	                                  free ($2); free ($3);/* Kulwant: $$ = $2; */}
	| uninterpreted_function		{ $$ = $1; }
	| array_expression				{ $$ = $1; }
	;

array_expression
	: variable LEFT_SQBR expression RIGHT_SQBR			{ strcat ($1, $2); strcat ($1, $3); strcat ($1, $4); 
	                                                      $$ = $1; free ($2); free ($3); free ($4); 
	                                                      /* Kulwant: $$ = createArray($1, $3);*/}
	| array_expression LEFT_SQBR expression RIGHT_SQBR  { strcat ($1, $2); strcat ($1, $3); strcat ($1, $4);
	                                                      $$ = $1; free ($2); free ($3); free ($4); 
	                                                      /* Kulwant: $$ = addArrayDimension($1, $3);*/ }
	;

unary_expression
	: primary_expression			{ $$ = $1; }
	| MINUS primary_expression		{ strcat ($1, $2); $$ = $1; free ($2);
	                                  /* Kulwant: $$ = negateExpression($2);*/}
	;

multiplicative_expression
	: unary_expression									{ $$ = $1; }
	| multiplicative_expression MULT unary_expression	{ strcat ($1, $2); strcat ($1, $3); 
	                                                      $$ = $1; free ($2); free ($3);  
	                                                      /* Kulwant: $$ = Mult_Sum_With_Sum($1, $3, $$);*/}
	| multiplicative_expression DIV unary_expression	{ strcat ($1, $2); strcat ($1, $3); 
	                                                      $$ = $1; free ($2); free ($3);  
	                                                      /* Kulwant: $$ = createExpression_Mod_Div($1, $3, '/');*/}
	| multiplicative_expression MOD unary_expression	{ strcat ($1, $2); strcat ($1, $3); 
	                                                      $$ = $1; free ($2); free ($3);  
	                                                      /* Kulwant: $$ = createExpression_Mod_Div($1, $3, '%');*/}
	;

expression
	: multiplicative_expression							{ $$ = $1; }
	| expression PLUS multiplicative_expression			{ strcat ($1, $2); strcat ($1, $3); 
	                                                      $$ = $1; free ($2); free ($3);  
	                                                      /* Kulwant: $$ = Add_Sums($1, $3, $$);*/ }
	| expression MINUS multiplicative_expression		{ strcat ($1, $2); strcat ($1, $3); 
	                                                      $$ = $1; free ($2); free ($3);  
	                                                      /* Kulwant: $$ = Add_Sums($1, negateExpression($3), $$);*/ }
	;

assignment_expression
	: variable EQUAL expression							{ strcat ($1, $2); strcat ($1, $3); 
	                                                      fprintf (g_fp, "\t\t%s;\n", $1);
	                                                      free ($1); free ($2); free ($3);  
	                                                      /* Kulwant: $$ = createDataTransVar($1, $3);*/ }
	| array_expression EQUAL expression 				{ strcat ($1, $2); strcat ($1, $3); 
	                                                      fprintf (g_fp, "\t\t%s;\n", $1);
	                                                      free ($1); free ($2); free ($3);  
	                                                      /* Kulwant: $$ = createDataTransArray($1, $3);*/ }
	| READ LEFT_BR variable COMMA variable RIGHT_BR		{ strcpy ($1, "scanf (\"%d\", &");
	                                                      // $2 is (.
	                                                      // strcat ($1, $3); strcat ($1, $4); // Some places.
	                                                      strcat ($1, $5); strcat ($1, $6);
	                                                      fprintf (g_fp, "\t\t%s;\n", $1);
	                                                      free ($1); free ($2); free ($3); free ($4);
	                                                      free ($5); free ($6); 
	                                                      /* Kulwant: $$ = createDataTransVarRead($5, $3); 
	                                                      /* port,  var: var  = port */ }
	| OUT LEFT_BR variable COMMA expression RIGHT_BR	{ strcat ($1, "printf (\"%d\", "); 
	                                                      // strcat ($1, $3); strcat ($1, $4);
	                                                      strcat ($1, $5); strcat ($1, $6);
	                                                      fprintf (g_fp, "\t\t%s;\n", $1);
	                                                      free ($1); free ($2); free ($3); free ($4);
	                                                      free ($5); free ($6); 
	                                                      /* Kulwant: $$ = createDataTransVarOut($3, $5);  
	                                                      /* port, expr: port = expr */ }
	;

relational_operator
	: LT_OP		{ $$ = $1; /* Kulwant: strcpy($$,$1); */}
	| GT_OP		{ $$ = $1; /* Kulwant: strcpy($$,$1); */}
	| LE_OP		{ $$ = $1; /* Kulwant: strcpy($$,$1); */}
	| GE_OP		{ $$ = $1; /* Kulwant: strcpy($$,$1); */}
	| EQ_OP		{ $$ = $1; /* Kulwant: strcpy($$,$1); */}
	| NE_OP		{ $$ = $1; /* Kulwant: strcpy($$,$1); */}
	;

primary_conditional_expression
	: expression relational_operator expression		{   strcat ($1, $2); strcat ($1, $3); 
	                                                    $$ = $1; free ($2); free ($3);  
	                                                    /* Kulwant: $$ = createConditionalExpression($1, $2, $3); */ }
	| NOT expression relational_operator expression	{   strcat ($1, " ("); /* chagnes !var to ! (var.*/
	                                                    strcat ($1, $2); strcat ($1, $3); strcat ($1, $4);
	                                                    strcat ($1, ") "); 
	                                                    $$ = $1; free ($2); free ($3); free ($4);
	                                                    /* Kulwant: $$ = negateConditionalExpression( 
	                                                                createConditionalExpression($2, $3, $4) );*/}
	;

secondary_conditional_expression
	: primary_conditional_expression										{ $$ = $1; }
	| secondary_conditional_expression AND primary_conditional_expression	{ 
	                                                    strcat ($1, $2); strcat ($1, $3); 
	                                                    $$ = $1; free ($2); free ($3);  
	                                                    /* Kulwant: $$ = createAndExpression($1, $3); */}
	| secondary_conditional_expression OR primary_conditional_expression	{ 
	                                                    strcat ($1, $2); strcat ($1, $3); 
	                                                    $$ = $1; free ($2); free ($3);  
	                                                    /* Kulwant: $$ = createOrExpression($1, $3);*/}
	| LEFT_BR secondary_conditional_expression RIGHT_BR						{ 
	                                                    strcat ($1, $2); strcat ($1, $3); 
	                                                    $$ = $1; free ($2); free ($3);  
	                                                    /* Kulwant: $$ = $2;*/}
	| NOT LEFT_BR secondary_conditional_expression RIGHT_BR					{ 
	                                                    strcat ($1, $2); strcat ($1, $3); strcat ($1, $4);
	                                                    $$ = $1; free ($2); free ($3); free ($4);
	                                                    /* Kulwant: $$ = negateConditionalExpression($3);*/ }
	;

conditional_expression
	: MINUS									{ $$ = "";
	                                          /*$$ = (char *)malloc (sizeof (char)*2);
	                                          $$[1] = '1';
	                                          $$[2] = '\0'; / *g_cur_cond++;*/}
	| secondary_conditional_expression	    { $$ = $1;   
	                                          if (g_cur_cond == 1)
	                                            fprintf (g_fp, "\n\tif (%s)\n \t{\n", $1);

	                                          else if (g_cur_cond == g_total_conds)
	                                            fprintf (g_fp, "\n\telse\n \t{\n");

	                                          else if (g_cur_cond > 1 && g_cur_cond < g_total_conds)
	                                            fprintf (g_fp, "\n\telse if (%s)\n \t{\n", $1);

	                                          else
	                                            printf ("g_cur_cond = %d, is violating its domain. g_total_conds = %d\n",
	                                                        g_cur_cond, g_total_conds);
	                                          g_cur_cond++;
	                                        }
	;

state_name
	: IDENTIFIER	{ $$ = $1; /* Kulwant: strcpy($$,$1); */}
	;

transitions
	: CONSTANT		{ $$ = $1; /* Kulwant: strcpy($$,$1); */}
	;

operation_list
	: assignment_expression { 
	        ; /* Kulwant * /
	        $$ = (DT_LIST*)malloc(sizeof(DT_LIST)); 
	        $$->action[0].lhs = $1->lhs; 
	        $$->action[0].rhs = $1->rhs; 
	        $$->numOperations = 1; */ 
	    }
	| operation_list COMMA assignment_expression	{ 
	        ; /* Kulwant * /
	        $$->action[$$->numOperations].lhs = $3->lhs; 
	        $$->action[$$->numOperations].rhs = $3->rhs;  
	        ($$->numOperations)++; */ 
	    }
	;

operations
	: MINUS				{ 
	        ; /* Kulwant * /
	        $$ = (DT_LIST*)malloc(sizeof(DT_LIST)); 
	        $$->numOperations = 0; */ 
	    }
	| operation_list	{ 
	        ; /* Kulwant * /
	        $$ = $1; */ 
	    }
	;

substatement
	: conditional_expression SEPARATOR operations state_name	{ 
	        fprintf (g_fp, "\t\tgoto %s;\n", $4);
	        if (g_total_conds > 1)
    	        fprintf (g_fp, "\t}\n");
	        /* Kulwant * /
	        $$ = (SUBSTMT*)malloc(sizeof(SUBSTMT));
	        $$->condition = $1;
	        $$->assignments = $3;
	        strcpy($$->stateName, $4); */
	    }
	;

substatement_list
	: substatement						{
	        ; /* Kulwant * / 
	        $$ = (SUBSTMT_LIST*)malloc(sizeof(SUBSTMT_LIST));
	        $$->substmt[0] = $1;
	        $$->numSubstmts = 1; */
	    }
	| substatement_list substatement	{ 
	        ; /* Kulwant * /
	        $$->substmt[$$->numSubstmts] = $2; 
	        ($$->numSubstmts)++; */ 
	    }
	;


statement_2
    : substatement_list SEMIC	{;}
    | SEMIC
    ;

statement
	: state_name transitions { fprintf (g_fp, "  %s:\n", $1);
	                           g_total_conds = atoi ($2);
	                           // printf (g_fp, "State: %s Total conditions: %d, %s\n", $1, g_total_conds, $2);
	                           g_cur_cond = 1; } 
	  statement_2{ /* This non terminal was added by Kulwant Singh. */
	    ;/* Kulwant * / $$ = (STMT*)malloc(sizeof(STMT)); 
	        strcpy($$->stateName, $1);
	        if(constval($2) != $3->numSubstmts)
	        {
	            printf("\n\nMismatch in number of transitions for State %s\nExiting System\n", $1); 
	            exit(0);
	        }
	        strcpy($$->numTransitions, $2);
	        $$->substmts = $3; */
	    }
	/* | state_name transitions SEMIC	{ 
	        ; /* Kulwant * /
	        $$ = (STMT*)malloc(sizeof(STMT)); 
	        strcpy($$->stateName, $1);
	        if(constval($2) != 0)
	        {
	            printf("\n\nMismatch in number of transitions for State %s\nExiting System\n", $1);
	            exit(0);
	        }
	        strcpy($$->numTransitions, "0");
	        $$->substmts = NULL; * /
	   }*/
	;

statement_list
	: statement							{ ;/* Kulwant * /$$ = (STMT_LIST*)malloc(sizeof(STMT_LIST)); 
	                                        $$->statements[0] = $1; $$->numStatements = 1; */}
	| statement_list statement			{ ;/* Kulwant * /$$->statements[$$->numStatements] = $2; 
	                                         ($$->numStatements)++;*/                        }
	;

translation_fsmd
	: fsmd_name additional_declaration statement_list	{ 
	        ; // Kulwant * / createFSMD($1, $3); /* additional_declaration is needed for SAST tool */ 
            /* Kulwant's code begins. **************************************/
            // print_statement_list ($3);
            /* Kulwant's code ends. ****************************************/
	    }
	| fsmd_name statement_list							{ ;/* Kulwant * / createFSMD($1, $2);*/ }
	;

fsmd_name
	: STRING_LITERAL	{ strcpy($$, $1); }
	;

additional_declaration
	: CONSTANT CONSTANT variable_list SEMIC
	| CONSTANT CONSTANT SEMIC
	;

argument_list
	: expression										{ ;/* Kulwant * / $$ = $1; */}
	| argument_list COMMA expression					{ ;/* Kulwant * / $$ = addArgument($1, $3); */}
	;

uninterpreted_function
	: function_name LEFT_BR argument_list RIGHT_BR		{;;/* Kulwant * /  $$ = createFunction($1, $3); */ }
	;

function_name
	: IDENTIFIER		{ ;/* Kulwant * / $$ = createVariable($1); / * 'v' will later be replaced by 'f' */ }
	;

%%


extern char yytext[];
extern int column;

extern FILE *yyin;



int yyerror(char *s)
{
	fflush(stdout);
	printf("\n%*s\n%*s\n", column, "^", column, s);
}



/** Inputs:
 *    fileName: Input file containing the FSDM.
 *    outfile: Name of the file in which the corresponding C code shall be written.
 *  Outputs:
 *    None.
 *  Functionality:
 *    Invokes the parser and performs various initializations.
 */
void callParser_FSMDA_to_C (char *fileName, char *outfile)
{

 	// printf("\nCalling Parser....");

 	// printf("\nInput file: %s\n", fileName);

 	yyin = fopen(fileName, "r");

 	if(yyin == NULL)
	{
 		printf("\n Cannot open input file.\n");
 		perror (fileName);
		exit(1);
	}

	/*indexof_symtab("-");
	//The next statement adds the variable to corresponding var_list
	if(!(flagVar_List))
		indexof_varlist("-", &V0);
	else
		indexof_varlist("-", &V1);*/

    // Construct an empty Symbol_Table and store the pointer in the global variable.
    g_ST = (Symbol_Table *) malloc (sizeof (Symbol_Table)); 
    if (g_ST == NULL)
    {
        perror ("callParser(): Couldn't allocate memory for the Symbol Table.");
        perror ("Fatal error. Exiting...\n\n");
    }
    g_ST->num_Symbols = 0; // Initialization.

    // Open the output .c file.
    g_fp = fopen (outfile, "w+");
    if (g_fp == NULL)
    {
        perror ("callParser(): cannot open the output C file.");
        perror ("Fatal Error, Exiting...\n\n");
        exit (1);
    }
    
    // Print the main function and all.
    fprintf (g_fp, "void main ()\n{\n");

    
    // Set the global variable for position for printing symbols.
    if ((g_symb_offset = ftell (g_fp)) == -1)
    {
        perror ("ftell () failed, Fatal Error, Exiting\n");
        exit (1);
    }
    
    int i;
    for (i = 0; i < MAX_SYMBOLS; i++)
        fprintf (g_fp, "                                  "); /* Empty 
            * spaces which will be replaced by variable declarations later. */
    fprintf (g_fp, "\n"); // Terminating the massive line. 
    
    
    //  yydebug = 1; // Uncomment to print huge amount of info on std output.


 	yyparse();  // Invoke the parser.


    // Print the Symbol table.    
    
    fprintf (g_fp, "\t;\n}\n");
    
    // Close the output .c file.
    if (fclose (g_fp))
    {
        perror ("callParser(): cannot close the output C file.");
        perror ("Ignoring and continuing.\n\n");
    }
}


/** Inputs:
 *   The symbol table st and the variable var.
 *  Output:
 *   1 if the variable var is a new variable and is added to the symbol table.
 *   0 Otherwise.
 *  Functionality:
 *    Adds the variable var to the Symbol Table st only if the variable is not
 *       present in the symbol table already.
 *    Performs a linear search on the variables already present in the Symbol Table.
 */
int add_var_to_Symbol_Table (Symbol_Table *st, char *var)
{
    int i;

    char *v; // Pointer to a variable in the symbol table.
    // For each variable v in the symbol table.
    for (i = 0; i < st->num_Symbols; i++)
    {
        v = st->symbol[i];
        // printf ("Comparing |%s| and |%s|, i = %d\n", v, var, i);
        if (strcmp (v, var) == 0)
        {
            // printf ("Strings Matched, returning.\n");
            return 0; // variable var is already present in the symbol table.
        }
    }
    
    if (st->num_Symbols < MAX_SYMBOLS)
    {
        // printf ("Adding variable |%s| to the symbol table at index = %d\n", var, st->num_Symbols);
        
        i = st->num_Symbols++;
        
        st->symbol[i][0] = '\0'; // initialization.
        
        if (strlen (var) >= MAX_VAR_LEN)
        {
            printf ("Length of variable |%s| is more then the maximum allowed variable length (MAX_VAR_LEN))", var);
            printf ("Fatal Error. Exiting..\n\n");
            exit (1);
        }
        
        strcpy (st->symbol[i], var);
    }
    else
    {
        perror ("add_var_to_Symbol_Table(): Total number of symbols exceeded the");
        perror ("maximum number of symbols (MAX_SYMBOLS) allowed in the program.");
        perror ("Fatal Error. Exiting..\n");
        exit (1);
    }
    return 1;
}




