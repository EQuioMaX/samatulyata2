/* A Bison parser, made by GNU Bison 3.0.2.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2013 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

#ifndef YY_YY_FSMDA_TO_C_TAB_H_INCLUDED
# define YY_YY_FSMDA_TO_C_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 1
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    IDENTIFIER = 258,
    CONSTANT = 259,
    STRING_LITERAL = 260,
    LE_OP = 261,
    GE_OP = 262,
    EQ_OP = 263,
    NE_OP = 264,
    AND = 265,
    OR = 266,
    NOT = 267,
    MULT = 268,
    DIV = 269,
    MOD = 270,
    PLUS = 271,
    MINUS = 272,
    READ = 273,
    OUT = 274,
    EQUAL = 275,
    COMMA = 276,
    SEMIC = 277,
    SEPARATOR = 278,
    LT_OP = 279,
    GT_OP = 280,
    LEFT_BR = 281,
    RIGHT_BR = 282,
    LEFT_SQBR = 283,
    RIGHT_SQBR = 284
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE YYSTYPE;
union YYSTYPE
{
#line 49 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:1909  */

	char* string;
	struct normalized_cell* norm;
	struct Assign* data_trans;
	struct Assign_list* dt_list;
	struct Substatement* substmt;
	struct Substatement_list* substmt_list;
	struct Statement* stmt;
	struct Statement_list* stmt_list;
	

#line 96 "FSMDA_to_C.tab.h" /* yacc.c:1909  */
};
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_FSMDA_TO_C_TAB_H_INCLUDED  */
