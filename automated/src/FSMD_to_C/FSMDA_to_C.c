#include <stdio.h>

int main (int argc, char *argv[])
{
    /******************* FUNCTION DECLARATIONS *********************/
    /* Invokes the parser for FSMDA to C conversion. */
    void callParser_FSMDA_to_C (char *fileName, char *outfile);
    /***************************************************************/
    
    if (argc != 3)
    {
		printf("\nInadequate number of parameters provided\n");
        printf ("<executable> <FSMD> <.c file for output>\n");
        printf ("Exiting \n\n");
		return 1;
    }
    
    callParser_FSMDA_to_C (argv[1], argv[2]);
    
    return 0;
}