/* A Bison parser, made by GNU Bison 3.0.2.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2013 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.0.2"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* Copy the first part of user declarations.  */
#line 3 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:339  */


 #include <stdio.h>
 #include <stdlib.h>
 #include <string.h>

 // #include "FsmdaHeader.h"
 // #include "parser.h"
 
 
 /* Kulwant's code begins. **************************************/
 // void print_statement_list (STMT_LIST *);
 // #define OUT_C_CODE "Outputs/C_Code.c"
 FILE *g_fp; // A global variable to print c code to a file.
 int   g_total_conds, g_cur_cond; /* Global variables: Total conditions
       * in a statement in FSMDA and number of the condition being read by the parser. */ 
 
 
 
 #define MAX_SYMBOLS 100 // Total number of symbols allowed in the program.
 #define MAX_VAR_LEN 50  // Max number of characters in a variable.
 typedef struct {
    int num_Symbols;    // Number of symbols present in the symbol array.
    char *symbol[MAX_SYMBOLS][MAX_VAR_LEN]; /* Each element points to a character string
            * dynamically allocated (using malloc()) while parsing. */
 } Symbol_Table;
 
 Symbol_Table *g_ST; /* A global pointer to a Symbol_Table so that the symbol table can be used
            * in the rules of the grammar. */
 
 
 long int g_symb_offset; // Offset in the file from beginning of the file for printing symbols.
 long int g_origin; 
 
 extern int yydebug;

 
 /* A global declaration for using the function in the grammar. 
  * This functions adds a variable to the symbol table, only if the variable is
  * not already present in the symbol table. 
  * Returns 1 if the variable var is added to the symbol table, 0 otherwise.*/
 int add_var_to_Symbol_Table (Symbol_Table *st, char *var);
 /* Kulwant's code ends. ****************************************/


#line 112 "FSMDA_to_C.tab.c" /* yacc.c:339  */

# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* In a future release of Bison, this section will be replaced
   by #include "FSMDA_to_C.tab.h".  */
#ifndef YY_YY_FSMDA_TO_C_TAB_H_INCLUDED
# define YY_YY_FSMDA_TO_C_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 1
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    IDENTIFIER = 258,
    CONSTANT = 259,
    STRING_LITERAL = 260,
    LE_OP = 261,
    GE_OP = 262,
    EQ_OP = 263,
    NE_OP = 264,
    AND = 265,
    OR = 266,
    NOT = 267,
    MULT = 268,
    DIV = 269,
    MOD = 270,
    PLUS = 271,
    MINUS = 272,
    READ = 273,
    OUT = 274,
    EQUAL = 275,
    COMMA = 276,
    SEMIC = 277,
    SEPARATOR = 278,
    LT_OP = 279,
    GT_OP = 280,
    LEFT_BR = 281,
    RIGHT_BR = 282,
    LEFT_SQBR = 283,
    RIGHT_SQBR = 284
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE YYSTYPE;
union YYSTYPE
{
#line 49 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:355  */

	char* string;
	struct normalized_cell* norm;
	struct Assign* data_trans;
	struct Assign_list* dt_list;
	struct Substatement* substmt;
	struct Substatement_list* substmt_list;
	struct Statement* stmt;
	struct Statement_list* stmt_list;
	

#line 194 "FSMDA_to_C.tab.c" /* yacc.c:355  */
};
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_FSMDA_TO_C_TAB_H_INCLUDED  */

/* Copy the second part of user declarations.  */

#line 209 "FSMDA_to_C.tab.c" /* yacc.c:358  */

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# define YY_ATTRIBUTE_PURE   YY_ATTRIBUTE ((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE ((__unused__))
#endif

#if !defined _Noreturn \
     && (!defined __STDC_VERSION__ || __STDC_VERSION__ < 201112)
# if defined _MSC_VER && 1200 <= _MSC_VER
#  define _Noreturn __declspec (noreturn)
# else
#  define _Noreturn YY_ATTRIBUTE ((__noreturn__))
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif


#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYSIZE_T yynewbytes;                                            \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / sizeof (*yyptr);                          \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  4
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   151

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  30
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  29
/* YYNRULES -- Number of rules.  */
#define YYNRULES  63
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  120

/* YYTRANSLATE[YYX] -- Symbol number corresponding to YYX as returned
   by yylex, with out-of-bounds checking.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   284

#define YYTRANSLATE(YYX)                                                \
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, without out-of-bounds checking.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,    96,    96,   135,   136,   140,   141,   142,   144,   145,
     149,   152,   158,   159,   164,   165,   168,   171,   177,   178,
     181,   187,   191,   195,   204,   215,   216,   217,   218,   219,
     220,   224,   227,   236,   237,   241,   245,   249,   256,   260,
     278,   282,   286,   293,   302,   307,   314,   327,   333,   342,
     343,   347,   347,   377,   379,   384,   390,   394,   398,   399,
     403,   404,   408,   412
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 0
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "IDENTIFIER", "CONSTANT",
  "STRING_LITERAL", "LE_OP", "GE_OP", "EQ_OP", "NE_OP", "AND", "OR", "NOT",
  "MULT", "DIV", "MOD", "PLUS", "MINUS", "READ", "OUT", "EQUAL", "COMMA",
  "SEMIC", "SEPARATOR", "LT_OP", "GT_OP", "LEFT_BR", "RIGHT_BR",
  "LEFT_SQBR", "RIGHT_SQBR", "$accept", "variable", "variable_list",
  "primary_expression", "array_expression", "unary_expression",
  "multiplicative_expression", "expression", "assignment_expression",
  "relational_operator", "primary_conditional_expression",
  "secondary_conditional_expression", "conditional_expression",
  "state_name", "transitions", "operation_list", "operations",
  "substatement", "substatement_list", "statement_2", "statement", "$@1",
  "statement_list", "translation_fsmd", "fsmd_name",
  "additional_declaration", "argument_list", "uninterpreted_function",
  "function_name", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284
};
# endif

#define YYPACT_NINF -65

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-65)))

#define YYTABLE_NINF -64

#define yytable_value_is_error(Yytable_value) \
  0

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
      -2,   -65,     7,   129,   -65,   -65,    13,    59,   -65,    39,
      39,    34,   -65,   -65,   -65,    39,   -65,   -65,   -65,    38,
       2,   -65,   -65,    49,   -65,     8,    50,   -65,     6,    64,
     -65,    74,   -65,   112,    92,   -65,   124,    61,   -65,    26,
     -65,   -65,    51,    50,     6,    92,    47,   -65,    88,    56,
      47,    47,    47,    47,    47,   -65,   -65,   -65,   -65,    47,
      47,   -65,   -65,    47,    32,    32,    69,   -65,   -65,    47,
      58,    47,    54,   -65,   -65,    90,    94,   -65,   -65,   -65,
     112,   112,   120,    47,   -65,   -65,   -65,    96,   114,    -7,
      37,   -65,    93,    39,   120,    53,   -65,   120,   -65,   -65,
     140,   140,    47,    47,    43,   -65,    47,   -65,   123,   125,
     120,   120,   -65,   120,   140,    47,   118,   104,   -65,   -65
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       0,    57,     0,     0,     1,    40,     0,     0,    53,    56,
       0,     0,    41,    51,    54,    55,     2,    59,     3,     0,
       0,    58,     4,     2,     6,     0,    38,    50,     0,     5,
      12,     9,    14,    18,     0,    33,    39,     0,    47,     0,
      52,     8,     0,     0,     0,     0,     0,    13,     0,     0,
       0,     0,     0,     0,     0,    27,    28,    29,    30,     0,
       0,    25,    26,     0,     0,     0,     0,    49,    48,     0,
       0,     0,     0,     7,    36,     0,     0,    15,    16,    17,
      19,    20,    31,     0,    34,    35,    44,     0,     0,     0,
       0,    42,    45,     0,    60,     0,    37,    32,    10,    11,
       0,     0,     0,     0,     0,    46,     0,    62,     0,     0,
      21,    22,    43,    61,     0,     0,     0,     0,    23,    24
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
     -65,   -11,   -65,   -10,   -64,    76,    79,   -24,    44,   102,
      77,   -13,   -65,    57,   -65,   -65,   -65,   110,   -65,   -65,
     109,   -65,   141,   -65,   -65,   -65,   -65,   -65,   -65
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int8 yydefgoto[] =
{
      -1,    29,    19,    30,    31,    32,    33,    34,    91,    63,
      35,    36,    37,     7,    13,    92,    93,    38,    39,    40,
       8,    20,     9,     2,     3,    10,    95,    41,    42
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int8 yytable[] =
{
      18,    45,    90,     1,    48,    23,    24,     4,    22,    23,
      24,    23,    24,   102,    25,    49,    47,    11,    25,    26,
      48,    50,    72,    43,    27,    43,    75,    76,    28,    23,
      24,    70,    28,    47,    44,    23,    24,    16,    25,    82,
      90,    16,     5,    26,    83,    94,    16,    97,    67,    43,
      23,    24,    28,    23,    24,    89,    17,   103,    46,    45,
      21,    87,    88,    12,    43,    51,    64,    65,    64,    65,
      59,    60,    16,    46,   106,   -63,    46,    69,   110,   111,
     107,    73,   113,    74,    66,    96,    86,    87,    88,   108,
     109,   117,    50,    89,    55,    56,    57,    58,    55,    56,
      57,    58,    51,   116,    59,    60,    59,    60,    59,    60,
      59,    60,    61,    62,   104,    73,    61,    62,    14,    98,
      59,    60,   100,    99,    14,    52,    53,    54,    77,    78,
      79,   119,     5,     6,    64,    65,    59,    60,    80,    81,
     101,    84,    85,    16,   114,   118,   115,    71,   112,    68,
     105,    15
};

static const yytype_uint8 yycheck[] =
{
      11,    25,    66,     5,    28,     3,     4,     0,    19,     3,
       4,     3,     4,    20,    12,    28,    26,     4,    12,    17,
      44,    28,    46,    17,    22,    17,    50,    51,    26,     3,
       4,    44,    26,    43,    26,     3,     4,     3,    12,    63,
     104,     3,     3,    17,    12,    69,     3,    71,    22,    17,
       3,     4,    26,     3,     4,    66,    22,    20,    26,    83,
      22,    18,    19,     4,    17,    28,    10,    11,    10,    11,
      16,    17,     3,    26,    21,    26,    26,    26,   102,   103,
      27,    27,   106,    27,    23,    27,    17,    18,    19,   100,
     101,   115,    28,   104,     6,     7,     8,     9,     6,     7,
       8,     9,    28,   114,    16,    17,    16,    17,    16,    17,
      16,    17,    24,    25,    21,    27,    24,    25,     9,    29,
      16,    17,    26,    29,    15,    13,    14,    15,    52,    53,
      54,    27,     3,     4,    10,    11,    16,    17,    59,    60,
      26,    64,    65,     3,    21,    27,    21,    45,   104,    39,
      93,    10
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,     5,    53,    54,     0,     3,     4,    43,    50,    52,
      55,     4,     4,    44,    50,    52,     3,    22,    31,    32,
      51,    22,    31,     3,     4,    12,    17,    22,    26,    31,
      33,    34,    35,    36,    37,    40,    41,    42,    47,    48,
      49,    57,    58,    17,    26,    37,    26,    33,    37,    41,
      28,    28,    13,    14,    15,     6,     7,     8,     9,    16,
      17,    24,    25,    39,    10,    11,    23,    22,    47,    26,
      41,    39,    37,    27,    27,    37,    37,    35,    35,    35,
      36,    36,    37,    12,    40,    40,    17,    18,    19,    31,
      34,    38,    45,    46,    37,    56,    27,    37,    29,    29,
      26,    26,    20,    20,    21,    43,    21,    27,    31,    31,
      37,    37,    38,    37,    21,    21,    31,    37,    27,    27
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    30,    31,    32,    32,    33,    33,    33,    33,    33,
      34,    34,    35,    35,    36,    36,    36,    36,    37,    37,
      37,    38,    38,    38,    38,    39,    39,    39,    39,    39,
      39,    40,    40,    41,    41,    41,    41,    41,    42,    42,
      43,    44,    45,    45,    46,    46,    47,    48,    48,    49,
      49,    51,    50,    52,    52,    53,    53,    54,    55,    55,
      56,    56,    57,    58
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     1,     1,     2,     1,     1,     3,     1,     1,
       4,     4,     1,     2,     1,     3,     3,     3,     1,     3,
       3,     3,     3,     6,     6,     1,     1,     1,     1,     1,
       1,     3,     4,     1,     3,     3,     3,     4,     1,     1,
       1,     1,     1,     3,     1,     1,     4,     1,     2,     2,
       1,     0,     4,     1,     2,     3,     2,     1,     4,     3,
       1,     3,     4,     1
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                  \
do                                                              \
  if (yychar == YYEMPTY)                                        \
    {                                                           \
      yychar = (Token);                                         \
      yylval = (Value);                                         \
      YYPOPSTACK (yylen);                                       \
      yystate = *yyssp;                                         \
      goto yybackup;                                            \
    }                                                           \
  else                                                          \
    {                                                           \
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;                                                  \
    }                                                           \
while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256



/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)

/* This macro is provided for backward compatibility. */
#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*----------------------------------------.
| Print this symbol's value on YYOUTPUT.  |
`----------------------------------------*/

static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  FILE *yyo = yyoutput;
  YYUSE (yyo);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
  YYUSE (yytype);
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyoutput, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yytype_int16 *yyssp, YYSTYPE *yyvsp, int yyrule)
{
  unsigned long int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[yyssp[yyi + 1 - yynrhs]],
                       &(yyvsp[(yyi + 1) - (yynrhs)])
                                              );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
yystrlen (const char *yystr)
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            /* Fall through.  */
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (! (yysize <= yysize1
                         && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                    return 2;
                  yysize = yysize1;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
      return 2;
    yysize = yysize1;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
{
  YYUSE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        YYSTYPE *yyvs1 = yyvs;
        yytype_int16 *yyss1 = yyss;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * sizeof (*yyssp),
                    &yyvs1, yysize * sizeof (*yyvsp),
                    &yystacksize);

        yyss = yyss1;
        yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yytype_int16 *yyss1 = yyss;
        union yyalloc *yyptr =
          (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
                  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
#line 96 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:1646  */
    { if (add_var_to_Symbol_Table (g_ST, (yyvsp[0].string)))
	                  {
	                     // Point file pointer to the position of printing symbols.
	                     // printf ("g_symb_offset = %ld, seek set = %d\n", g_symb_offset, SEEK_SET);
	                     if (fseek (g_fp, g_symb_offset, SEEK_SET))
	                     {
	                        perror ("fseek failed.");
	                        printf ("Variable = %s\n", (yyvsp[0].string));
	                        perror ("Fatal Error, Exiting..\n\n");
	                        exit (1);
	                     }
	                     
	                     // Print the symbol.
	                     fprintf (g_fp, "\tint %s;\n", (yyvsp[0].string));
	                     // printf ("int %s;\n", $1);
	                     
	                     // Getting the new current offset.
	                     if ((g_symb_offset = ftell (g_fp)) == -1)
	                     {
	                        perror ("ftell () failed in grammar, Fatal Error, Exiting.");
	                        exit (1);
	                     }
	                     
	                     // Pointer file pointer to the end of the file.
	                     if (fseek (g_fp, 0, SEEK_END))
	                     {
	                        perror ("fseek failed.");
	                        printf ("Variable = %s\n", (yyvsp[0].string));
	                        perror ("Fatal Error, Exiting..\n\n");
	                        exit (1);
	                     }
	                  } 
	                  
	                  (yyval.string) = (yyvsp[0].string);
	                  
	                  /* Kulwant: strcpy($$,$1); */}
#line 1411 "FSMDA_to_C.tab.c" /* yacc.c:1646  */
    break;

  case 3:
#line 135 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:1646  */
    { ; /* Kulwant: The vairable_list rule is not at all used by me. */}
#line 1417 "FSMDA_to_C.tab.c" /* yacc.c:1646  */
    break;

  case 5:
#line 140 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:1646  */
    { (yyval.string) = (yyvsp[0].string); /* Kulwant: $$ = createVariable($1);*/ }
#line 1423 "FSMDA_to_C.tab.c" /* yacc.c:1646  */
    break;

  case 6:
#line 141 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:1646  */
    { (yyval.string) = (yyvsp[0].string); /* Kulwant: $$ = createConstant($1);*/ }
#line 1429 "FSMDA_to_C.tab.c" /* yacc.c:1646  */
    break;

  case 7:
#line 142 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:1646  */
    { strcat ((yyvsp[-2].string), (yyvsp[-1].string)); strcat ((yyvsp[-2].string), (yyvsp[0].string)); (yyval.string) = (yyvsp[-2].string); 
	                                  free ((yyvsp[-1].string)); free ((yyvsp[0].string));/* Kulwant: $$ = $2; */}
#line 1436 "FSMDA_to_C.tab.c" /* yacc.c:1646  */
    break;

  case 8:
#line 144 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:1646  */
    { (yyval.string) = (yyvsp[0].string); }
#line 1442 "FSMDA_to_C.tab.c" /* yacc.c:1646  */
    break;

  case 9:
#line 145 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:1646  */
    { (yyval.string) = (yyvsp[0].string); }
#line 1448 "FSMDA_to_C.tab.c" /* yacc.c:1646  */
    break;

  case 10:
#line 149 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:1646  */
    { strcat ((yyvsp[-3].string), (yyvsp[-2].string)); strcat ((yyvsp[-3].string), (yyvsp[-1].string)); strcat ((yyvsp[-3].string), (yyvsp[0].string)); 
	                                                      (yyval.string) = (yyvsp[-3].string); free ((yyvsp[-2].string)); free ((yyvsp[-1].string)); free ((yyvsp[0].string)); 
	                                                      /* Kulwant: $$ = createArray($1, $3);*/}
#line 1456 "FSMDA_to_C.tab.c" /* yacc.c:1646  */
    break;

  case 11:
#line 152 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:1646  */
    { strcat ((yyvsp[-3].string), (yyvsp[-2].string)); strcat ((yyvsp[-3].string), (yyvsp[-1].string)); strcat ((yyvsp[-3].string), (yyvsp[0].string));
	                                                      (yyval.string) = (yyvsp[-3].string); free ((yyvsp[-2].string)); free ((yyvsp[-1].string)); free ((yyvsp[0].string)); 
	                                                      /* Kulwant: $$ = addArrayDimension($1, $3);*/ }
#line 1464 "FSMDA_to_C.tab.c" /* yacc.c:1646  */
    break;

  case 12:
#line 158 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:1646  */
    { (yyval.string) = (yyvsp[0].string); }
#line 1470 "FSMDA_to_C.tab.c" /* yacc.c:1646  */
    break;

  case 13:
#line 159 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:1646  */
    { strcat ((yyvsp[-1].string), (yyvsp[0].string)); (yyval.string) = (yyvsp[-1].string); free ((yyvsp[0].string));
	                                  /* Kulwant: $$ = negateExpression($2);*/}
#line 1477 "FSMDA_to_C.tab.c" /* yacc.c:1646  */
    break;

  case 14:
#line 164 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:1646  */
    { (yyval.string) = (yyvsp[0].string); }
#line 1483 "FSMDA_to_C.tab.c" /* yacc.c:1646  */
    break;

  case 15:
#line 165 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:1646  */
    { strcat ((yyvsp[-2].string), (yyvsp[-1].string)); strcat ((yyvsp[-2].string), (yyvsp[0].string)); 
	                                                      (yyval.string) = (yyvsp[-2].string); free ((yyvsp[-1].string)); free ((yyvsp[0].string));  
	                                                      /* Kulwant: $$ = Mult_Sum_With_Sum($1, $3, $$);*/}
#line 1491 "FSMDA_to_C.tab.c" /* yacc.c:1646  */
    break;

  case 16:
#line 168 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:1646  */
    { strcat ((yyvsp[-2].string), (yyvsp[-1].string)); strcat ((yyvsp[-2].string), (yyvsp[0].string)); 
	                                                      (yyval.string) = (yyvsp[-2].string); free ((yyvsp[-1].string)); free ((yyvsp[0].string));  
	                                                      /* Kulwant: $$ = createExpression_Mod_Div($1, $3, '/');*/}
#line 1499 "FSMDA_to_C.tab.c" /* yacc.c:1646  */
    break;

  case 17:
#line 171 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:1646  */
    { strcat ((yyvsp[-2].string), (yyvsp[-1].string)); strcat ((yyvsp[-2].string), (yyvsp[0].string)); 
	                                                      (yyval.string) = (yyvsp[-2].string); free ((yyvsp[-1].string)); free ((yyvsp[0].string));  
	                                                      /* Kulwant: $$ = createExpression_Mod_Div($1, $3, '%');*/}
#line 1507 "FSMDA_to_C.tab.c" /* yacc.c:1646  */
    break;

  case 18:
#line 177 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:1646  */
    { (yyval.string) = (yyvsp[0].string); }
#line 1513 "FSMDA_to_C.tab.c" /* yacc.c:1646  */
    break;

  case 19:
#line 178 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:1646  */
    { strcat ((yyvsp[-2].string), (yyvsp[-1].string)); strcat ((yyvsp[-2].string), (yyvsp[0].string)); 
	                                                      (yyval.string) = (yyvsp[-2].string); free ((yyvsp[-1].string)); free ((yyvsp[0].string));  
	                                                      /* Kulwant: $$ = Add_Sums($1, $3, $$);*/ }
#line 1521 "FSMDA_to_C.tab.c" /* yacc.c:1646  */
    break;

  case 20:
#line 181 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:1646  */
    { strcat ((yyvsp[-2].string), (yyvsp[-1].string)); strcat ((yyvsp[-2].string), (yyvsp[0].string)); 
	                                                      (yyval.string) = (yyvsp[-2].string); free ((yyvsp[-1].string)); free ((yyvsp[0].string));  
	                                                      /* Kulwant: $$ = Add_Sums($1, negateExpression($3), $$);*/ }
#line 1529 "FSMDA_to_C.tab.c" /* yacc.c:1646  */
    break;

  case 21:
#line 187 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:1646  */
    { strcat ((yyvsp[-2].string), (yyvsp[-1].string)); strcat ((yyvsp[-2].string), (yyvsp[0].string)); 
	                                                      fprintf (g_fp, "\t\t%s;\n", (yyvsp[-2].string));
	                                                      free ((yyvsp[-2].string)); free ((yyvsp[-1].string)); free ((yyvsp[0].string));  
	                                                      /* Kulwant: $$ = createDataTransVar($1, $3);*/ }
#line 1538 "FSMDA_to_C.tab.c" /* yacc.c:1646  */
    break;

  case 22:
#line 191 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:1646  */
    { strcat ((yyvsp[-2].string), (yyvsp[-1].string)); strcat ((yyvsp[-2].string), (yyvsp[0].string)); 
	                                                      fprintf (g_fp, "\t\t%s;\n", (yyvsp[-2].string));
	                                                      free ((yyvsp[-2].string)); free ((yyvsp[-1].string)); free ((yyvsp[0].string));  
	                                                      /* Kulwant: $$ = createDataTransArray($1, $3);*/ }
#line 1547 "FSMDA_to_C.tab.c" /* yacc.c:1646  */
    break;

  case 23:
#line 195 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:1646  */
    { strcpy ((yyvsp[-5].string), "scanf (\"%d\", &");
	                                                      // $2 is (.
	                                                      // strcat ($1, $3); strcat ($1, $4); // Some places.
	                                                      strcat ((yyvsp[-5].string), (yyvsp[-1].string)); strcat ((yyvsp[-5].string), (yyvsp[0].string));
	                                                      fprintf (g_fp, "\t\t%s;\n", (yyvsp[-5].string));
	                                                      free ((yyvsp[-5].string)); free ((yyvsp[-4].string)); free ((yyvsp[-3].string)); free ((yyvsp[-2].string));
	                                                      free ((yyvsp[-1].string)); free ((yyvsp[0].string)); 
	                                                      /* Kulwant: $$ = createDataTransVarRead($5, $3); 
	                                                      /* port,  var: var  = port */ }
#line 1561 "FSMDA_to_C.tab.c" /* yacc.c:1646  */
    break;

  case 24:
#line 204 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:1646  */
    { strcat ((yyvsp[-5].string), "printf (\"%d\", "); 
	                                                      // strcat ($1, $3); strcat ($1, $4);
	                                                      strcat ((yyvsp[-5].string), (yyvsp[-1].string)); strcat ((yyvsp[-5].string), (yyvsp[0].string));
	                                                      fprintf (g_fp, "\t\t%s;\n", (yyvsp[-5].string));
	                                                      free ((yyvsp[-5].string)); free ((yyvsp[-4].string)); free ((yyvsp[-3].string)); free ((yyvsp[-2].string));
	                                                      free ((yyvsp[-1].string)); free ((yyvsp[0].string)); 
	                                                      /* Kulwant: $$ = createDataTransVarOut($3, $5);  
	                                                      /* port, expr: port = expr */ }
#line 1574 "FSMDA_to_C.tab.c" /* yacc.c:1646  */
    break;

  case 25:
#line 215 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:1646  */
    { (yyval.string) = (yyvsp[0].string); /* Kulwant: strcpy($$,$1); */}
#line 1580 "FSMDA_to_C.tab.c" /* yacc.c:1646  */
    break;

  case 26:
#line 216 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:1646  */
    { (yyval.string) = (yyvsp[0].string); /* Kulwant: strcpy($$,$1); */}
#line 1586 "FSMDA_to_C.tab.c" /* yacc.c:1646  */
    break;

  case 27:
#line 217 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:1646  */
    { (yyval.string) = (yyvsp[0].string); /* Kulwant: strcpy($$,$1); */}
#line 1592 "FSMDA_to_C.tab.c" /* yacc.c:1646  */
    break;

  case 28:
#line 218 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:1646  */
    { (yyval.string) = (yyvsp[0].string); /* Kulwant: strcpy($$,$1); */}
#line 1598 "FSMDA_to_C.tab.c" /* yacc.c:1646  */
    break;

  case 29:
#line 219 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:1646  */
    { (yyval.string) = (yyvsp[0].string); /* Kulwant: strcpy($$,$1); */}
#line 1604 "FSMDA_to_C.tab.c" /* yacc.c:1646  */
    break;

  case 30:
#line 220 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:1646  */
    { (yyval.string) = (yyvsp[0].string); /* Kulwant: strcpy($$,$1); */}
#line 1610 "FSMDA_to_C.tab.c" /* yacc.c:1646  */
    break;

  case 31:
#line 224 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:1646  */
    {   strcat ((yyvsp[-2].string), (yyvsp[-1].string)); strcat ((yyvsp[-2].string), (yyvsp[0].string)); 
	                                                    (yyval.string) = (yyvsp[-2].string); free ((yyvsp[-1].string)); free ((yyvsp[0].string));  
	                                                    /* Kulwant: $$ = createConditionalExpression($1, $2, $3); */ }
#line 1618 "FSMDA_to_C.tab.c" /* yacc.c:1646  */
    break;

  case 32:
#line 227 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:1646  */
    {   strcat ((yyvsp[-3].string), " ("); /* chagnes !var to ! (var.*/
	                                                    strcat ((yyvsp[-3].string), (yyvsp[-2].string)); strcat ((yyvsp[-3].string), (yyvsp[-1].string)); strcat ((yyvsp[-3].string), (yyvsp[0].string));
	                                                    strcat ((yyvsp[-3].string), ") "); 
	                                                    (yyval.string) = (yyvsp[-3].string); free ((yyvsp[-2].string)); free ((yyvsp[-1].string)); free ((yyvsp[0].string));
	                                                    /* Kulwant: $$ = negateConditionalExpression( 
	                                                                createConditionalExpression($2, $3, $4) );*/}
#line 1629 "FSMDA_to_C.tab.c" /* yacc.c:1646  */
    break;

  case 33:
#line 236 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:1646  */
    { (yyval.string) = (yyvsp[0].string); }
#line 1635 "FSMDA_to_C.tab.c" /* yacc.c:1646  */
    break;

  case 34:
#line 237 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:1646  */
    { 
	                                                    strcat ((yyvsp[-2].string), (yyvsp[-1].string)); strcat ((yyvsp[-2].string), (yyvsp[0].string)); 
	                                                    (yyval.string) = (yyvsp[-2].string); free ((yyvsp[-1].string)); free ((yyvsp[0].string));  
	                                                    /* Kulwant: $$ = createAndExpression($1, $3); */}
#line 1644 "FSMDA_to_C.tab.c" /* yacc.c:1646  */
    break;

  case 35:
#line 241 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:1646  */
    { 
	                                                    strcat ((yyvsp[-2].string), (yyvsp[-1].string)); strcat ((yyvsp[-2].string), (yyvsp[0].string)); 
	                                                    (yyval.string) = (yyvsp[-2].string); free ((yyvsp[-1].string)); free ((yyvsp[0].string));  
	                                                    /* Kulwant: $$ = createOrExpression($1, $3);*/}
#line 1653 "FSMDA_to_C.tab.c" /* yacc.c:1646  */
    break;

  case 36:
#line 245 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:1646  */
    { 
	                                                    strcat ((yyvsp[-2].string), (yyvsp[-1].string)); strcat ((yyvsp[-2].string), (yyvsp[0].string)); 
	                                                    (yyval.string) = (yyvsp[-2].string); free ((yyvsp[-1].string)); free ((yyvsp[0].string));  
	                                                    /* Kulwant: $$ = $2;*/}
#line 1662 "FSMDA_to_C.tab.c" /* yacc.c:1646  */
    break;

  case 37:
#line 249 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:1646  */
    { 
	                                                    strcat ((yyvsp[-3].string), (yyvsp[-2].string)); strcat ((yyvsp[-3].string), (yyvsp[-1].string)); strcat ((yyvsp[-3].string), (yyvsp[0].string));
	                                                    (yyval.string) = (yyvsp[-3].string); free ((yyvsp[-2].string)); free ((yyvsp[-1].string)); free ((yyvsp[0].string));
	                                                    /* Kulwant: $$ = negateConditionalExpression($3);*/ }
#line 1671 "FSMDA_to_C.tab.c" /* yacc.c:1646  */
    break;

  case 38:
#line 256 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:1646  */
    { (yyval.string) = "";
	                                          /*$$ = (char *)malloc (sizeof (char)*2);
	                                          $$[1] = '1';
	                                          $$[2] = '\0'; / *g_cur_cond++;*/}
#line 1680 "FSMDA_to_C.tab.c" /* yacc.c:1646  */
    break;

  case 39:
#line 260 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:1646  */
    { (yyval.string) = (yyvsp[0].string);   
	                                          if (g_cur_cond == 1)
	                                            fprintf (g_fp, "\n\tif (%s)\n \t{\n", (yyvsp[0].string));

	                                          else if (g_cur_cond == g_total_conds)
	                                            fprintf (g_fp, "\n\telse\n \t{\n");

	                                          else if (g_cur_cond > 1 && g_cur_cond < g_total_conds)
	                                            fprintf (g_fp, "\n\telse if (%s)\n \t{\n", (yyvsp[0].string));

	                                          else
	                                            printf ("g_cur_cond = %d, is violating its domain. g_total_conds = %d\n",
	                                                        g_cur_cond, g_total_conds);
	                                          g_cur_cond++;
	                                        }
#line 1700 "FSMDA_to_C.tab.c" /* yacc.c:1646  */
    break;

  case 40:
#line 278 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:1646  */
    { (yyval.string) = (yyvsp[0].string); /* Kulwant: strcpy($$,$1); */}
#line 1706 "FSMDA_to_C.tab.c" /* yacc.c:1646  */
    break;

  case 41:
#line 282 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:1646  */
    { (yyval.string) = (yyvsp[0].string); /* Kulwant: strcpy($$,$1); */}
#line 1712 "FSMDA_to_C.tab.c" /* yacc.c:1646  */
    break;

  case 42:
#line 286 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:1646  */
    { 
	        ; /* Kulwant * /
	        $$ = (DT_LIST*)malloc(sizeof(DT_LIST)); 
	        $$->action[0].lhs = $1->lhs; 
	        $$->action[0].rhs = $1->rhs; 
	        $$->numOperations = 1; */ 
	    }
#line 1724 "FSMDA_to_C.tab.c" /* yacc.c:1646  */
    break;

  case 43:
#line 293 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:1646  */
    { 
	        ; /* Kulwant * /
	        $$->action[$$->numOperations].lhs = $3->lhs; 
	        $$->action[$$->numOperations].rhs = $3->rhs;  
	        ($$->numOperations)++; */ 
	    }
#line 1735 "FSMDA_to_C.tab.c" /* yacc.c:1646  */
    break;

  case 44:
#line 302 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:1646  */
    { 
	        ; /* Kulwant * /
	        $$ = (DT_LIST*)malloc(sizeof(DT_LIST)); 
	        $$->numOperations = 0; */ 
	    }
#line 1745 "FSMDA_to_C.tab.c" /* yacc.c:1646  */
    break;

  case 45:
#line 307 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:1646  */
    { 
	        ; /* Kulwant * /
	        $$ = $1; */ 
	    }
#line 1754 "FSMDA_to_C.tab.c" /* yacc.c:1646  */
    break;

  case 46:
#line 314 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:1646  */
    { 
	        fprintf (g_fp, "\t\tgoto %s;\n", (yyvsp[0].string));
	        if (g_total_conds > 1)
    	        fprintf (g_fp, "\t}\n");
	        /* Kulwant * /
	        $$ = (SUBSTMT*)malloc(sizeof(SUBSTMT));
	        $$->condition = $1;
	        $$->assignments = $3;
	        strcpy($$->stateName, $4); */
	    }
#line 1769 "FSMDA_to_C.tab.c" /* yacc.c:1646  */
    break;

  case 47:
#line 327 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:1646  */
    {
	        ; /* Kulwant * / 
	        $$ = (SUBSTMT_LIST*)malloc(sizeof(SUBSTMT_LIST));
	        $$->substmt[0] = $1;
	        $$->numSubstmts = 1; */
	    }
#line 1780 "FSMDA_to_C.tab.c" /* yacc.c:1646  */
    break;

  case 48:
#line 333 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:1646  */
    { 
	        ; /* Kulwant * /
	        $$->substmt[$$->numSubstmts] = $2; 
	        ($$->numSubstmts)++; */ 
	    }
#line 1790 "FSMDA_to_C.tab.c" /* yacc.c:1646  */
    break;

  case 49:
#line 342 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:1646  */
    {;}
#line 1796 "FSMDA_to_C.tab.c" /* yacc.c:1646  */
    break;

  case 51:
#line 347 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:1646  */
    { fprintf (g_fp, "  %s:\n", (yyvsp[-1].string));
	                           g_total_conds = atoi ((yyvsp[0].string));
	                           // printf (g_fp, "State: %s Total conditions: %d, %s\n", $1, g_total_conds, $2);
	                           g_cur_cond = 1; }
#line 1805 "FSMDA_to_C.tab.c" /* yacc.c:1646  */
    break;

  case 52:
#line 351 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:1646  */
    { /* This non terminal was added by Kulwant Singh. */
	    ;/* Kulwant * / $$ = (STMT*)malloc(sizeof(STMT)); 
	        strcpy($$->stateName, $1);
	        if(constval($2) != $3->numSubstmts)
	        {
	            printf("\n\nMismatch in number of transitions for State %s\nExiting System\n", $1); 
	            exit(0);
	        }
	        strcpy($$->numTransitions, $2);
	        $$->substmts = $3; */
	    }
#line 1821 "FSMDA_to_C.tab.c" /* yacc.c:1646  */
    break;

  case 53:
#line 377 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:1646  */
    { ;/* Kulwant * /$$ = (STMT_LIST*)malloc(sizeof(STMT_LIST)); 
	                                        $$->statements[0] = $1; $$->numStatements = 1; */}
#line 1828 "FSMDA_to_C.tab.c" /* yacc.c:1646  */
    break;

  case 54:
#line 379 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:1646  */
    { ;/* Kulwant * /$$->statements[$$->numStatements] = $2; 
	                                         ($$->numStatements)++;*/                        }
#line 1835 "FSMDA_to_C.tab.c" /* yacc.c:1646  */
    break;

  case 55:
#line 384 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:1646  */
    { 
	        ; // Kulwant * / createFSMD($1, $3); /* additional_declaration is needed for SAST tool */ 
            /* Kulwant's code begins. **************************************/
            // print_statement_list ($3);
            /* Kulwant's code ends. ****************************************/
	    }
#line 1846 "FSMDA_to_C.tab.c" /* yacc.c:1646  */
    break;

  case 56:
#line 390 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:1646  */
    { ;/* Kulwant * / createFSMD($1, $2);*/ }
#line 1852 "FSMDA_to_C.tab.c" /* yacc.c:1646  */
    break;

  case 57:
#line 394 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:1646  */
    { strcpy((yyval.string), (yyvsp[0].string)); }
#line 1858 "FSMDA_to_C.tab.c" /* yacc.c:1646  */
    break;

  case 60:
#line 403 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:1646  */
    { ;/* Kulwant * / $$ = $1; */}
#line 1864 "FSMDA_to_C.tab.c" /* yacc.c:1646  */
    break;

  case 61:
#line 404 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:1646  */
    { ;/* Kulwant * / $$ = addArgument($1, $3); */}
#line 1870 "FSMDA_to_C.tab.c" /* yacc.c:1646  */
    break;

  case 62:
#line 408 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:1646  */
    {;;/* Kulwant * /  $$ = createFunction($1, $3); */ }
#line 1876 "FSMDA_to_C.tab.c" /* yacc.c:1646  */
    break;

  case 63:
#line 412 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:1646  */
    { ;/* Kulwant * / $$ = createVariable($1); / * 'v' will later be replaced by 'f' */ }
#line 1882 "FSMDA_to_C.tab.c" /* yacc.c:1646  */
    break;


#line 1886 "FSMDA_to_C.tab.c" /* yacc.c:1646  */
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
#line 415 "./src/FSMD_to_C/FSMDA_to_C.y" /* yacc.c:1906  */



extern char yytext[];
extern int column;

extern FILE *yyin;



int yyerror(char *s)
{
	fflush(stdout);
	printf("\n%*s\n%*s\n", column, "^", column, s);
}



/** Inputs:
 *    fileName: Input file containing the FSDM.
 *    outfile: Name of the file in which the corresponding C code shall be written.
 *  Outputs:
 *    None.
 *  Functionality:
 *    Invokes the parser and performs various initializations.
 */
void callParser_FSMDA_to_C (char *fileName, char *outfile)
{

 	// printf("\nCalling Parser....");

 	// printf("\nInput file: %s\n", fileName);

 	yyin = fopen(fileName, "r");

 	if(yyin == NULL)
	{
 		printf("\n Cannot open input file.\n");
 		perror (fileName);
		exit(1);
	}

	/*indexof_symtab("-");
	//The next statement adds the variable to corresponding var_list
	if(!(flagVar_List))
		indexof_varlist("-", &V0);
	else
		indexof_varlist("-", &V1);*/

    // Construct an empty Symbol_Table and store the pointer in the global variable.
    g_ST = (Symbol_Table *) malloc (sizeof (Symbol_Table)); 
    if (g_ST == NULL)
    {
        perror ("callParser(): Couldn't allocate memory for the Symbol Table.");
        perror ("Fatal error. Exiting...\n\n");
    }
    g_ST->num_Symbols = 0; // Initialization.

    // Open the output .c file.
    g_fp = fopen (outfile, "w+");
    if (g_fp == NULL)
    {
        perror ("callParser(): cannot open the output C file.");
        perror ("Fatal Error, Exiting...\n\n");
        exit (1);
    }
    
    // Print the main function and all.
    fprintf (g_fp, "void main ()\n{\n");

    
    // Set the global variable for position for printing symbols.
    if ((g_symb_offset = ftell (g_fp)) == -1)
    {
        perror ("ftell () failed, Fatal Error, Exiting\n");
        exit (1);
    }
    
    int i;
    for (i = 0; i < MAX_SYMBOLS; i++)
        fprintf (g_fp, "                                  "); /* Empty 
            * spaces which will be replaced by variable declarations later. */
    fprintf (g_fp, "\n"); // Terminating the massive line. 
    
    
    //  yydebug = 1; // Uncomment to print huge amount of info on std output.


 	yyparse();  // Invoke the parser.


    // Print the Symbol table.    
    
    fprintf (g_fp, "\t;\n}\n");
    
    // Close the output .c file.
    if (fclose (g_fp))
    {
        perror ("callParser(): cannot close the output C file.");
        perror ("Ignoring and continuing.\n\n");
    }
}


/** Inputs:
 *   The symbol table st and the variable var.
 *  Output:
 *   1 if the variable var is a new variable and is added to the symbol table.
 *   0 Otherwise.
 *  Functionality:
 *    Adds the variable var to the Symbol Table st only if the variable is not
 *       present in the symbol table already.
 *    Performs a linear search on the variables already present in the Symbol Table.
 */
int add_var_to_Symbol_Table (Symbol_Table *st, char *var)
{
    int i;

    char *v; // Pointer to a variable in the symbol table.
    // For each variable v in the symbol table.
    for (i = 0; i < st->num_Symbols; i++)
    {
        v = st->symbol[i];
        // printf ("Comparing |%s| and |%s|, i = %d\n", v, var, i);
        if (strcmp (v, var) == 0)
        {
            // printf ("Strings Matched, returning.\n");
            return 0; // variable var is already present in the symbol table.
        }
    }
    
    if (st->num_Symbols < MAX_SYMBOLS)
    {
        // printf ("Adding variable |%s| to the symbol table at index = %d\n", var, st->num_Symbols);
        
        i = st->num_Symbols++;
        
        st->symbol[i][0] = '\0'; // initialization.
        
        if (strlen (var) >= MAX_VAR_LEN)
        {
            printf ("Length of variable |%s| is more then the maximum allowed variable length (MAX_VAR_LEN))", var);
            printf ("Fatal Error. Exiting..\n\n");
            exit (1);
        }
        
        strcpy (st->symbol[i], var);
    }
    else
    {
        perror ("add_var_to_Symbol_Table(): Total number of symbols exceeded the");
        perror ("maximum number of symbols (MAX_SYMBOLS) allowed in the program.");
        perror ("Fatal Error. Exiting..\n");
        exit (1);
    }
    return 1;
}




