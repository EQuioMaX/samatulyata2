#include "types.h" // Soumaydip's header file.
#include "Types.h" // Kulwant's header file.

extern PRESPLUS *g_SPP; // Defined in printmodel.c; by Kulwant to print variables in expressions.

/** Inputs:
 *    m1 and m2: Paths of two cfg file from which PRES+ should be 
 *          constructed and equivalence or programs should be checked.
 *  Outputs:
 *    Returns nothing.
 *  Functionality:
 *    Constructs PRES+ models for both files using Kulwant's method.
 *    Invokes conversion routine to Soumyadip's PRES+ for both.
 *    Invokes Soumyadip's Dynamic cut point equivalence checker on both.
 *  The function will not produce any debugging output.
 *    For debugging output construct PRES+ for each file individually.
 * */
void run_Soumyadip_Prog (char *m1, char *m2)
{
  #ifdef CHECK_EQV
    /******************* FUNCTION DECLARATIONS *********************/
    /* Intiailizes the instances of the basic block array and symbol
     * table. */
    extern int construct_BBArray_and_ST (char *inFile, BB_Array *bba, 
                                Symbol_Table *st);
    
    /* Constructs and returns a pointer to PRES+. */
    Pres_Plus *construct_Pres_Plus (BB_Array *bba, Symbol_Table *st);

    // Converts Kulwant's PRES+ to Soumyadip's PRES+.
    PRESPLUS *convert_Kul_to_Som_PP (Pres_Plus *pp, Symbol_Table *st);
    
    // Checks equivalence of given two PRES+ models.
    void check_Equivalence_Wrapper (PRESPLUS *spp1, PRESPLUS *spp2);
    
    // Prints a Kulant's PRES+ to file specified in file.
    void print_Pres_Plus (const Pres_Plus *pp, const Symbol_Table *st,
                            char *file);    
    /***************************************************************/
    #undef DEBUG // No debugging when two input files.
    
    /* For the first cfg file. ***************************************/
    
    // Construct basic blocks array and the symbol table.
    BB_Array *bba    = malloc (sizeof (BB_Array));    // The basic block array.
    Symbol_Table *st = malloc (sizeof (Symbol_Table)); // The symbol table.
    
    if (bba == NULL || st == NULL)
    {
        perror ("run_Soumyadip_Prog (): For First cfg file bba or st.");
        exit (1);
    }
    
    printf ("\nConstructing the PRES+ for the first input program.\n");
    construct_BBArray_and_ST (m1, bba, st);
    
    Pres_Plus *pp = construct_Pres_Plus (bba, st);
    
    print_Pres_Plus (pp, st, KUL_PRESPLUS_1);
    
    printf ("PRES+ successfully constructed, converting it to Soumyadip's PRES+.\n");    
    PRESPLUS *spp1; // A pointer to Soumyadip's PRES+.
    spp1 = convert_Kul_to_Som_PP (pp, st); /* Convert's Kulwant's PRES+
            * to Soumyadip's PRES+. */

    printf ("PRES+ successfully converted.\n\n");    

    free (pp); // Free the memory used by Kulwant's PRES+.
    


    /* For the second cfg file. **************************************/
    printf ("Constructing the PRES+ for the second input program.\n");
    construct_BBArray_and_ST (m2, bba, st);
    
    pp = construct_Pres_Plus (bba, st);
    
    print_Pres_Plus (pp, st, KUL_PRESPLUS_2);
    
    printf ("PRES+ successfully constructed, converting it to Soumyadip's PRES+.\n");    
    PRESPLUS *spp2; // A pointer to Soumyadip's PRES+.
    spp2 = convert_Kul_to_Som_PP (pp, st);  // Convert PRES+.

    printf ("PRES+ successfully converted.\n\n");    

    free (pp); // Free the memory used by Kulwant's PRES+.
    free (bba);
    free (st);

    /* Compare the two PRES+ nets. ***********************************/
    printf ("Invoking the equivalence checker.\n");

    check_Equivalence_Wrapper (spp1, spp2);
    printf ("Equivalence checker finished.\n\n");
  #endif // CHECK_EQV
}


PRESPLUS *convert_Kul_to_Som_PP (Pres_Plus *pp, Symbol_Table *st)
{
    /******************* FUNCTION DECLARATIONS *********************/
    // Convert Kulwant's Symbol table to Soumyadip's symbol table.
    void convert_Symbol_Table (PRESPLUS *spp, const Symbol_Table *st);


    // Convert Kulwant's Places to Soumyadip's Places.
    void convert_Places (PRESPLUS *spp, const Pres_Plus *pp);
    
    // Convert Kulwant's Transitions to Soumyadip's Transitions.
    void convert_Transitions (PRESPLUS *spp, const Pres_Plus *pp, const Symbol_Table *st);

    // Prints a PRES+ model.
    int printpresplus(PRESPLUS model);

    // Redirects the stdout to the given file.
    int redirect_stdout_to_file (char *file);
    
    // Restores the stdout to the given file.
    void restore_stdout (int fd);
    
    // Adds initial -- input places.
    void add_places_with_initial_markings (PRESPLUS *spp, 
                                            const Pres_Plus *pp);
    /***************************************************************/
    PRESPLUS *spp = malloc (sizeof (PRESPLUS)); /* allocate memory for 
        * Soumyadip's PRES+. */
    if (spp == NULL)
    {
        perror ("convert_Kul_to_Som_PP(): Couldn't allocate memory for Soumyadip's PRES+.");
        printf ("Fatal Error. Exiting.\n\n");
        exit (1);
    }
    
    convert_Symbol_Table (spp, st); /* Converts the variables (Symbol Table)
        * in Kulwant's PRES+ to that in Soumyadip's PRES+. 
        * Note that in Soumyadip's data structure the symbol table is 
        * a part of the PRES+ itself.*/

    // printf ("convert Symbol Table done.\n");

    convert_Places (spp, pp); /* Converts the places in Kulwant's
        * PRES+ to those in Soumyadip's PRES+. */
    
    // printf ("convert Places done.\n");
    
    convert_Transitions (spp, pp, st); /* Converts the transitions in Kulwant's
        * PRES+ to those in Soumyadip's PRES+. */
    
    // Add places with empty preset to initial markings list.
    add_places_with_initial_markings (spp, pp);
    
    // printf ("convert Transitions done.\n");
    
  #ifdef OUT_CONVERSTION
    int fd = redirect_stdout_to_file (OUT_CONVERSTION);
    
    printpresplus (*spp);
    
    restore_stdout (fd);
  #endif
    return spp; /* Return soumyadip's PRES+. */
}


/** Inputs:
 *      spp: Soumyadip's PRES+.
 *      pp:  Kulwant's PRES+.
 *  Output:
 *      Returns Nothing.
 *  Functionality:
 *      Adds all the places with empty pre-sets to the initial markings 
 *  list.
 * */
void add_places_with_initial_markings (PRESPLUS *spp, const Pres_Plus *pp)
{
    int i, j;
    
    // For each place.
    for (i = 0, j = 0; i < pp->pl_count; i++)
    {
        if (pp->place[i].c_Preset == 0)
        {
            // Add the place to the initial markings list.
            spp->initial_marking[j++] = i; /* Mark the i-th place as 
                * an input place.
                * i-th place in Kulwant's PRES+ is mapped to i-th
                * place in Soumyadip's PRES+. */
            
            if (j >= MAXINITIALMARKING)
            {
                printf ("add_places_with_initial_markings ():\n");
                printf ("Number of places with empty preset increased ");
                printf ("MAXINITIALMARKING.\n");
                printf ("Fatal Error. Exiting.\n\n");
                exit (1);
            }
        }
    }
    spp->no_of_places_initially_marked = j; /* Set number of initially
        * marked places. */
}



/** Inputs:
 *    spp: Pointer to an instance of Soumyadip's PRES+. It is a value-
 *          return argument.
 *    st:  Pointer to the Symbol Table as pre Kulwant's data structures.
 *  Outputs:
 *    returns nothing.
 *  Functionality:
 *    Soumyadip's PRES+, undoubtedly, needs a symbol table. His data
 *      structures maintain the symbol table as a part of the PRES+ i.e.
 *      spp itself.
 *    The function reads the symbol table st and stores it in the 
 *      symbol table inside spp.
 * 
 *    Note that:
 *      i-th variable in Kulwant's Symbol table is mapped to i-th var
 *          in the Soumyadip's PRES+.
 * */
void convert_Symbol_Table (PRESPLUS *spp, const Symbol_Table *st)
{
    /******************* FUNCTION DECLARATIONS *********************/
    // Returns index of symbol if present in global symbol table.
    // Adds the symbol to the symbol table, otherwise.
    int indexof_symtab (char *symbol);
    /***************************************************************/
    int i;
    
    // For each variable in st.
    for (i = 0; i < st->total_Symbols; i++)
    {
        /* Adding variables to the global symbol table.
         * Using the function from Soumyadip. */
        indexof_symtab (st->symbol[i].vName);
        
        
        /* This part construct the Soumyadip's Symbol Table inside 
         *   the PRES+ spp. Since that Symbol Table is not used.
         *   and a global one is used.
         * Adding the variables to the global Symbol Table. *
        spp->var_table[i].name[0] = '\0'; // Initialize to empty string.
        
        strcpy (spp->var_table[i].name, st->symbol[i].vName); /* Copy
                                * name of the variable. * /
        
        spp->var_table[i].type = 1; /* For integer type. 
            * Hard coded because he doesn't has an enum for data types.
            * The value 1 found out by reading his code.. 
            * Not mentioned explicitly any where. * /
        
        // Soumaydip's expr ?????????????????????????????????????????

        if (i >= MAXNOVAR)
        {
            printf ("convert_Symbol_Table (): Number of variables ");
            printf ("exceeded MAXNOVAR.\n");
            printf ("Fatal Error. Exiting.\n\n");
            exit (1);
        }*/
    }
    spp->no_of_variables = st->total_Symbols;
}



/** Inputs:
 *    spp: Soumyadip's PRES+.
 *    pp: Kulwant's PRES+.
 *  Outputs:
 *    Returns Nothing.
 *  Functionality:
 *    For each place in Kulwant's PRES+, constructs an equivalent place
 *      in Soumyadip's PRES+.
 *    Note that:
 *       i-th place in Kulwant's PRES+ is mapped to i-th place in 
 *          Soumyadip's PRES+.
 * */
void convert_Places (PRESPLUS *spp, const Pres_Plus *pp)
{
    int i, j;
    
    // For each place in the PRES+ pp:
    for (i = 0; i < pp->pl_count; i++)
    {
        // att. 1, name of a place.
        spp->places[i].name[0] = '\0';
        sprintf (spp->places[i].name, "p%d", i);
        
        spp->places[i].var_index = pp->place[i].var; // associated var.
        
        // Copying the pre-set.
        for (j = 0; j < pp->place[i].c_Preset; j++)
        {
            spp->places[i].preset[j] = pp->place[i].preset[j];
            if (j >= MAXNOPRESET)
            {
                printf ("convert_Places (): Number of pre trans of place ");
                printf ("%d exceeded MAXNOPRESET, Preset = %d.\n", i, pp->place[i].c_Preset);
                printf ("Fatal Error. Exiting.\n\n");
                exit (1);
            }
        }
        spp->places[i].no_of_preset = pp->place[i].c_Preset;
        

        // Copying the post-set.
        for (j = 0; j < pp->place[i].c_Postset; j++)
        {
            spp->places[i].postset[j] = pp->place[i].postset[j];
            if (j >= MAXNOPOSTSET)
            {
                printf ("convert_Places (): Number of post trans of place ");
                printf ("%d exceeded MAXNOPOSTSET, Postset = %d.\n", i, pp->place[i].c_Postset);
                printf ("Fatal Error. Exiting.\n\n");
                exit (1);
            }
        }
        spp->places[i].no_of_postset = pp->place[i].c_Postset;
        
        // Initializing token-present att to false or 0.
        spp->places[i].token_present = 0;
        
        /* Assuming attributes label, lcut, copy, cut, mark and trans.
         * in Soumyadip's PRES+ are not to be initialized here only. */
        
        if (i >= MAXNOPLACES)
        {
            printf ("convert_Places (): Number of places ");
            printf ("exceeded MAXNOPLACES.\n");
            printf ("Fatal Error. Exiting.\n\n");
            exit (1);
        }
    }
    spp->no_of_places = pp->pl_count;
}


/** Inputs:
 *      spp: Pointer to Souymadip's PRES+. It is a value-return argument.
 *      pp, st: Pointer to Kulwant's PRES+ and symbol table.
 *  Outputs:
 *      returns nothing.
 *  Functionality:
 *      For each transition in pp, constructs a transition in spp.
 *    Note that i-th transition in pp is mapped to i-th transition in spp.
 *      Just like in the case of places and variables.
 * 
 *   Kulwant's expression tree is converted to a string. From the 
 *      string normalized cell representation is constructed using 
 *      Kunal's parser.
 * */
void convert_Transitions (PRESPLUS *spp, const Pres_Plus *pp, const Symbol_Table *st)
{
    /******************* FUNCTION DECLARATIONS *********************/
    /* Copies expression tree in Kulwant's form to Soumyadip's form. */
    EXPRNODE *copy_Expr_tree_Kul_to_Soum (const Expr *e);

    // Converts the expression associated to trans tI to normalized cell.
    void convert_to_Normalized_expr (PRESPLUS *spp, const Pres_Plus *pp, 
                                  const int tI, const Symbol_Table *st);
    
    // Returns the expression represented by expr tree "root" as string.
    char *get_Expr_As_String (Expr *root, Symbol_Table *st);

    // Converts given condition in form of a string to an NC representation.
    NC* parsecondition(char *condition,NC *root);
    
    // Parses the given expression and returns the equivalent expression tree.
    // EXPR ParseExpression (char *);
    /***************************************************************/
    
    int i, j;
    
    // For each transition in pp.
    for (i = 0; i < pp->tr_count; i++)
    {
        
        //printf ("tI=%d\n", i);

        spp->transitions[i].id = i;
        
        spp->transitions[i].name[0] = '\0';
        sprintf (spp->transitions[i].name, "t%d", i);
        
        
        /* spp->transitions[i].expr  = ParseExpression (
                        get_Expr_As_String (pp->transition[i].expr, st));
        
        spp->transitions[i].guard  = ParseExpression (
                        get_Expr_As_String (pp->transition[i].gaurd, st));
        
        spp->transitions[i].expr  = copy_Expr_tree_Kul_to_Soum (
                                        pp->transition[i].expr);
        
        
        spp->transitions[i].guard = copy_Expr_tree_Kul_to_Soum (
                                        pp->transition[i].gaurd);*/
        
        
        //printf ("invoking convert expr tree to normalized cell representation.\n");
        
        convert_to_Normalized_expr (spp, pp, i, st);
        
        //printf ("expr tree converted to NC, invoking NC for condition. \n");
        
        char *g;
        if (pp->transition[i].gaurd != NULL)
        {
            g = get_Expr_As_String (pp->transition[i].gaurd, st);

    
          #ifdef LOG_PP_CONVERSTION
            FILE *fp = fopen (LOG_PP_CONVERSTION, "a+");
            if (fp == NULL)
            {
                perror ("convert_Transitions: ().");
                printf ("%s -- conldn't open log file.\n", LOG_PP_CONVERSTION);
                printf ("Fatal. Exiting..\n\n");
                exit (1);
            }
            
            fprintf (fp, "Guard of transition t%d = %s.\n\n", i, g);
            
            if (fclose (fp))
            {
                perror (LOG_PP_CONVERSTION);
            }
          #endif
    
            spp->transitions[i].condition = parsecondition (g, 
                                            spp->transitions[i].condition);
            //printf ("condition to NC done.\n");
        }
        
        // Copying the pre-set.
        for (j = 0; j < pp->transition[i].c_Preset; j++)
        {
            spp->transitions[i].preset[j] = pp->transition[i].preset[j];
            if (j >= MAXNOPRESET)
            {
                printf ("convert_Transitions (): Number of pre places of ");
                printf ("transition %d exceeded MAXNOPRESET.\n", i);
                printf ("Fatal Error. Exiting.\n\n");
                exit (1);
            }
        }
        spp->transitions[i].no_of_preset = pp->transition[i].c_Preset;
        

        // Copying the post-set.
        for (j = 0; j < pp->transition[i].c_Postset; j++)
        {
            spp->transitions[i].postset[j] = pp->transition[i].postset[j];
            if (j >= MAXNOPOSTSET)
            {
                printf ("convert_Transitions (): Number of post places of ");
                printf ("transition %d exceeded MAXNOPOSTSET.\n", i);
                printf ("Fatal Error. Exiting.\n\n");
                exit (1);
            }
        }
        spp->transitions[i].no_of_postset = pp->transition[i].c_Postset;

        if (j >= MAXNOTRANS)
        {
            printf ("convert_Transitions (): Number of transitions ");
            printf ("exceeded MAXNOTRANS.\n");
            printf ("Fatal Error. Exiting.\n\n");
            exit (1);
        }

        /* Ignoring label, mark and cmplt attributes of a transition.
         *   as don't know what they mean. */
    }
    spp->no_of_transitions = pp->tr_count;
}


/** Inputs:
 *      e: Expression tree which shall be converted to equivalent 
 *          expression tree in Soumyadip's code.
 *  Outputs:
 *      void.
 *  Functionality:
 *      Convert Expression tree Kulwant to Soumyadip's.
 * */
EXPRNODE *copy_Expr_tree_Kul_to_Soum (const Expr *e)
{
    // Exit if root of expression tree of tI is NULL.
    if (e == NULL)
    {
        return NULL;
        /*printf ("const_Soumyadip_expr_tree_root (): Given transition ");
        printf ("%d has a NULL expression tree.\n", tI);
        printf ("Fatal Error. Exiting.\n\n");
        exit (1);*/
    }
    
    EXPRNODE *root = malloc (sizeof (EXPRNODE));
    if (root == NULL)
    {
        perror ("copy_Expr_tree_Kul_to_Soum (): malloc failed ");
        printf ("Fatal Error. Exiting..\n\n");
        exit (1);
    }
    
    // Copy expression tree e to root.
    switch (e->eType)
    {
        case e_BINARY:
            root->type  = 0; // Hard coded by Soumyadip only.
            root->value = get_Operator_Soumyadip (e->oprtr);
            root->left  = copy_Expr_tree_Kul_to_Soum (e->left);
            root->right = copy_Expr_tree_Kul_to_Soum (e->right);
            break;
        case e_UNARY:
            root->type  = 0;
            root->value = get_Operator_Soumyadip (e->oprtr);
            root->left  = copy_Expr_tree_Kul_to_Soum (e->left);
            root->right = NULL;
            break;
        case e_LITERAL:
            root->type  = 2; 
            root->value = e->val_var;
            root->left  = NULL;
            root->right = NULL;
            break;
        case e_VAR:
            root->type  = 1; 
            root->value = e->val_var; /* i-th var is mapped to i-th 
                        * index only. */
            root->left  = NULL;
            root->right = NULL;
            break;
        default:
            printf ("copy_Expr_tree_Kul_to_Soum (): ");
            printf ("invalid type of expression tree.\n");
            printf ("Fatal Error. Exiting.\n\n");
            exit (1);
    }
    return root;
}


/** Inputs:
 *      o: operator as per enum in Kulwant's header file.
 *  Outputs:
 *      Returns equivalent Soumyadip's operator.
 *  
 * */
int get_Operator_Soumyadip (const Oprtr o)
{
    int any = 11;
    switch (o)
    {
        case op_PLUS:          // binary "+"
            return 9;
        case op_BINARY_MINUS:      // binary "-"
            return 10;
        case op_MULTIPLY:           // "*"
        case op_DIVIDE:             // "/"
        case op_MODULUS:            // %
            return any;
    
        case op_GRT_THN_OR_EQUAL:    // >=
            return 4;
        case op_GRT_THN:             // >
            return 3;
        case op_LESS_THN_OR_EQUAL:   // <=
            return 6;
        case op_LESS_THN:            // <
            return 5;
        case op_DOUBLE_EQUAL:       // ==
            return 7;
        case op_NOT_EQUAL:          // !=
            return 8;
    
        case op_SINGLE_EQUAL:       // =
            return any;

        case op_BITWISE_OR:         // |
            return any;
        case op_BITWISE_AND:        // &
            return any;

        case op_NOT:                // ~
            return 2;
        case op_LEFT_SHIFT:         // <<
        case op_RIGHT_SHIFT:        // >>
            return any;
    
        case op_LOGICAL_OR:         // ||
            return 1;
        case op_LOGICAL_AND:         // &&
            return 0;
        
        case op_IR_RELEVANT:
        case op_UNKNOWN_OPERATOR:
            return any;
        
        default:
            printf ("get_Operator_Soumyadip: ");
            printf ("Operand is of no recognized type, error.\n");
            printf ("Fatal Error. Exiting.\n\n");
            exit (1);
    }
}


/** Inputs:
 *      spp: Pointer to Souymadip's PRES+. It is a value-return argument.
 *      tI: index of the transition whose expression should be converted
 *            to normalized expression.
 *      pp, st: Pointer to Kulwant's PRES+ and symbol table.
 *  Outputs:
 *      returns nothing.
 *  Functionality:
 *      For the transition, first convert's the variable on RHS to 
 *        normalized cell representation.
 *      Then for each variable defined by the transition do the same 
 *        process.
 * */
void convert_to_Normalized_expr (PRESPLUS *spp, const Pres_Plus *pp, 
                                  const int tI, const Symbol_Table *st)
{
    /******************* FUNCTION DECLARATIONS *********************/
    // fills the array arr with all the variables in post-places of trans tI.
    // Returns the count of the places.
    extern int get_Vars_In_Postset (const Pres_Plus *pp, const int tI, int *arr);

    // removes duplicates from the array arr containing variables and
    // Returns the count in the resultant array.
    int remove_Duplicates_Var_Arr (int *arr, int len);

    /* Returns the expression associated to a transition in the form
     * of a statically allocated string. */
    char *get_Expr_As_String_for_Normalized_Cell (const Pres_Plus *pp, 
                                  const int tI, const Symbol_Table *st);

    // Invokes the parser for converting given string to expression tree.
    // NC *callParser (char *);
    
    // Returns string for the variable at index "index" in symbol table.
    char *get_Var_As_String (int index, Symbol_Table *st);

    // Converts an assignment statement to respective data trans.
    // It involves conversion of expression on RHS to Normalized cell.
    extern void parseassignments(char *s, DATA_TRANS *actions);
    /***************************************************************/


    /* Get all the variables defined by transition tI.
     * The variables defined by transition tI is the set of variables
     *   associated to the post-places of the transition tI. */
    int arr[MAX_POSTSET]; // Array to hold vars defined by transition tI.
    int len = 0; // Number of elements in arr.
    
    // printf ("Getting Vars in POST-set. \n");
    len = get_Vars_In_Postset (pp, tI, arr);
    
    // printf ("Got Vars in POST-set, removing duplicates.\n");
    
    len = remove_Duplicates_Var_Arr (arr, len);
    
    // printf ("Removed duplicates, getting Expr as a string for NC.\n");
    
    char *e_str = get_Expr_As_String_for_Normalized_Cell (pp, tI, st);
    // printf ("Got Expr as a string for NC.\n");

    
    // NC *nc = callParser (e_str);
    
    
    char e_str_2[MAX_EXPR_SIZE] = {'\0'};

    if (len > 0)
    {
        // A var for lhs exists.
        sprintf (e_str_2, "%s = %s", get_Var_As_String (arr[0], st),
                                     e_str);
    }
    else
    {
        // A var for lhs doesn't exist. Use DUMMY variable.
        sprintf (e_str_2, "%s = %s", DUM_VAR_STR, e_str);
    }
    
  #ifdef LOG_PP_CONVERSTION
    FILE *fp = fopen (LOG_PP_CONVERSTION, "a+");
    if (fp == NULL)
    {
        perror ("convert_to_Normalized_expr: ().");
        printf ("%s -- conldn't open log file.\n", LOG_PP_CONVERSTION);
        printf ("Fatal. Exiting..\n\n");
        exit (1);
    }
    
    fprintf (fp, "Expression for transition t%d is |%s|.\n", tI, e_str_2); 
    
    if (fclose (fp))
    {
        perror (LOG_PP_CONVERSTION);
    }
  #endif
    
    parseassignments (e_str_2, spp->transitions[tI].action + 0); /*
        * Store the expression in actions[0]. */
    
    /*if (len > 0)
    {
        // For the first variable defined by the transition:
        spp->transitions[tI].action[0].lhs = arr[0];
        spp->transitions[tI].action[0].rhs = nc;
        
        
    }
    
    int i;
    
    if (len > 1)
    {
        // If the transition tI defines more than one variable.
        e_str = get_Var_As_String (arr[0], st);
        nc = callParser (e_str);
        
        // For each other variable defined by the transition tI.
        for (i = 1; i < len; i++)
        {
            spp->transitions[tI].action[i].lhs = arr[i];
            spp->transitions[tI].action[i].rhs = nc;
        }
    }*/
}


/** Inputs:
 *      pp, st: Pointer to PRES+ and Symbol Table.
 *      tI: Index of transition whose expression should be represented
 *            as a string.
 *  Output:
 *      Returns pointer to a statically allocated string containing
 *        the expression of the transition as a string.
 *  Functionality:
 *     Convert the expression of a transition to string  
 *       even if the transition is an ID or a DUMMY transition.
 * */
char *get_Expr_As_String_for_Normalized_Cell (const Pres_Plus *pp, 
                                  const int tI, const Symbol_Table *st)
{
    /******************* FUNCTION DECLARATIONS *********************/
    // Returns string for the variable at index "index" in symbol table.
    char *get_Var_As_String (int index, Symbol_Table *st);

    // Returns the expression represented by expr tree "root" as string.
    char *get_Expr_As_String (Expr *root, Symbol_Table *st);
    /***************************************************************/

    char *e_str = NULL; // Stores the expression tree as a string.
    int pI, vI; // Indices of place and variable respectively.
    
    // Based on the transition type invoke conversion to normalized cell.
    switch (pp->transition[tI].t_Type)
    {
        case t_ID:
        case t_DUMMY:
            /* 0-th pre-place of an ID transition is the one it should
             *    project as output.
             * A dummy trans must have at least one pre-place.
             * Based on this factor using the variable associated to
             *   the 0-th pre-place as the variable on RHS.
             * */
            if (pp->transition[tI].c_Preset <= 0)
            {
                printf ("get_Expr_As_String_Normalized_Cell (): Transition = %d", tI);
                printf ("has empty preset and is either and ID or DUMMY trans.\n");
                printf ("Fatal Error. Exiting.\n\n");
                exit (1);
            }
            pI = pp->transition[tI].preset[0];
            
            switch (pp->place[pI].p_Type)
            {
                case p_SCANF:
                case p_PRINTF:
                case p_VAR:
                    vI = pp->place[pI].var;
                    break;
                case p_DUMMY:
                    vI = DUM_VAR_INDEX;
                    break;
                default:
                    printf ("get_Expr_As_String_Normalized_Cell (): Place = %d", pI);
                    printf ("has invalid type.\n");
                    printf ("Fatal Error. Exiting.\n\n");
                    exit (1);
            }
            e_str = get_Var_As_String (vI, st);
            break;
            
        case t_EXPR:
            // get expression as a string.
            e_str = get_Expr_As_String (pp->transition[tI].expr, st);
            break;
            
        default:
            printf ("get_Expr_As_String_Normalized_Cell (): Transition = %d has ", tI);
            printf ("invalid type.\n");
            printf ("Fatal Error. Exiting.\n\n");
            exit (1);
    }
    return e_str;
}



/** Inputs:
 *      Two PRES+ (Soumyadip's) models.
 *  Output:
 *      Returns Nothing.
 *  Functionality:
 *      Invokes Soumyadip's Routine after Output redirection.
 * */
void check_Equivalence_Wrapper (PRESPLUS *spp1, PRESPLUS *spp2)
{
  #ifdef CHECK_EQV
    /******************* FUNCTION DECLARATIONS *********************/
    // Check equivalence of given two PRES+ nets.
    void findEquivalent(PRESPLUS N0,PRESPLUS N1);
    
    // Prints a PRES+ model.
    int printpresplus(PRESPLUS model);

    // Redirects the stdout to the given file.
    int redirect_stdout_to_file (char *file);
    
    // Restores the stdout to the given file.
    void restore_stdout (int fd);
    /***************************************************************/
    int fd = redirect_stdout_to_file (OUT_EQUIVALENCE);
    
    printf ("\n********************************************************\n");
    printf ("********************************************************\n");
    printf ("********************************************************\n");
    printf ("PRES+ 1\n");
    printf ("********************************************************\n\n");
    
    g_SPP = spp1;
    printpresplus (*spp1);
    
    // Added when program encountered a segmentation fault.
    // if (fdatasync (fd))
    //    perror ("fsync () first call failed");
    
    printf ("\n********************************************************\n");
    printf ("********************************************************\n");
    printf ("********************************************************\n");
    printf ("PRES+ 2\n");
    printf ("********************************************************\n\n");

    g_SPP = spp2;
    printpresplus (*spp2);
    
    // Added when program encountered a segmentation fault.
    // if (fdatasync (fd))
    //    perror ("fsync () second call failed");
    
    
    printf ("\n********************************************************\n");
    printf ("********************************************************\n");
    printf ("********************************************************\n");
    printf ("Equivalence check Report.\n");
    printf ("********************************************************\n\n");
    
    findEquivalent (*spp1, *spp2); // Check equivalence of two models.
    
    restore_stdout (fd);
  #endif    
}


/** Inputs:
 *      file: Path to a file.
 *  Outputs:
 *      Returns the file descriptor which contains a copy of stdout
 *         to be used for restoring stdout.
 *  Functionality:
 *      Redirects the stdout to the given file.
 * */
int redirect_stdout_to_file (char *file)
{
    /* Directing stdout to a file. **********************************/
    int my_out;
    /* Creates a new file to print. */
    if ((my_out=open (file, O_RDWR|O_CREAT, 0777))==-1)
    {
        perror ("Cannot open output file for output redirection.");
        perror ("redirect_stdout_to_file (), Fatal Error, Exiting.\n");
        exit (1);
    } 

    int t_stdout; // Temp variable to store stdout.
    /* Creates a new file descriptor for std out. To restore stdout.*/
    if ((t_stdout = dup (STDOUT_FILENO))==-1)
    {
        perror ("Cannot backup standard output.");
        perror ("redirect_stdout_to_file (), Fatal Error, Exiting.\n");
        exit (1);
    } 
    
    /* Overwrite stdout to point to the file pointed by my_out. */
    if (dup2 (my_out, STDOUT_FILENO) == -1)
    {
        perror ("Cannot redirect std output.");
        perror ("redirect_stdout_to_file (), Fatal Error, Exiting.\n");
        exit (1);
    }

    // Close my_out, no more required.
    if (close (my_out) == -1)
    {
        perror ("Cannot close redundant output file pointer.");
        perror ("redirect_stdout_to_file (), Fatal Error, Exiting.\n");
        exit (1);
    }
    /*****************************************************************/

    return t_stdout;
}


/** Inputs:
 *      fd: A file descriptor which contains the backed up copy of the
 *            stdout.
 *  Outputs:
 *      Returns nothing.
 *  Functionality:
 *      Restores the stdout to the file descriptor fd.
 * */
void restore_stdout (int fd)
{

    /* Redirecting output to stdout. *********************************/
    /* Flush the stdout. ***/
    if (fflush(stdout))
    {
        perror ("Cannot flush the stdout.");
        perror ("restore_stdout (), Ignoring the error.\n");
        exit (0);
    }
    
    /* Restore std output. */
    if (dup2  (fd, STDOUT_FILENO)==-1)
    {
        perror ("Cannot restore std output.");
        perror ("restore_stdout (), Fatal Error, Exiting.\n");
        exit (1);
    }
    
    // Close no more required t_stdout.
    if (close (fd) == -1)
    {
        perror ("Cannot close temporary pointer to std out.");
        perror ("restore_stdout (), Fatal Error, Exiting.\n");
        exit (1);
    }
    /*****************************************************************/
}
