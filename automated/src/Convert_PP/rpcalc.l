%option noyywrap

%{
#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#include "types.h"
#include "rpcalc.tab.h"
%}

%%
"_"    {smdplval.operetor = strdup("_"); return UNDER;}
"="    {smdplval.operetor = strdup("="); return EQ; }
">="   {smdplval.operetor = strdup(">=");return GRETEREQ;}
"<="   {smdplval.operetor = strdup("<="); return LESSEQ;}
">"    {smdplval.operetor = strdup(">"); return GRETER;}
"<"    {smdplval.operetor = strdup("<"); return LESS;}
"=="   {smdplval.operetor = strdup("=="); return EQEQ;}
"&"    {smdplval.operetor = strdup("&"); return AND;}
"|"    {smdplval.operetor = strdup("|"); return OR;}
"+"    {smdplval.operetor = strdup("+"); return PLUS;}
"-"    {smdplval.operetor = strdup("-"); return MINUS;}
"*"    {smdplval.operetor = strdup("*"); return MULT;}
"/"    {smdplval.operetor = strdup("/"); return DIV; }   
"!="   {smdplval.operetor = strdup("!="); return NOTEQ; }
[0-9]+ { smdplval.constant = atoi(smdptext) ; return NUM ; }
[a-zA-Z]+ { smdplval.literal = smdptext ; return CHAR ; /* + added by Kulwant.*/ } 
.       {; /* Ignore, added by Kulwant. */}
%%

void string_scan(char* buffer)
{
	smdp_scan_string(buffer) ;
}
