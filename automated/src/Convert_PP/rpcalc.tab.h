/* A Bison parser, made by GNU Bison 3.0.2.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2013 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

#ifndef YY_SMDP_RPCALC_TAB_H_INCLUDED
# define YY_SMDP_RPCALC_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef SMDPDEBUG
# if defined YYDEBUG
#if YYDEBUG
#   define SMDPDEBUG 1
#  else
#   define SMDPDEBUG 0
#  endif
# else /* ! defined YYDEBUG */
#  define SMDPDEBUG 0
# endif /* ! defined YYDEBUG */
#endif  /* ! defined SMDPDEBUG */
#if SMDPDEBUG
extern int smdpdebug;
#endif

/* Token type.  */
#ifndef SMDPTOKENTYPE
# define SMDPTOKENTYPE
  enum smdptokentype
  {
    CHAR = 258,
    NUM = 259,
    EQ = 260,
    GRETEREQ = 261,
    LESSEQ = 262,
    GRETER = 263,
    LESS = 264,
    EQEQ = 265,
    AND = 266,
    OR = 267,
    PLUS = 268,
    MINUS = 269,
    MULT = 270,
    DIV = 271,
    NOTEQ = 272,
    UNDER = 273
  };
#endif

/* Value type.  */
#if ! defined SMDPSTYPE && ! defined SMDPSTYPE_IS_DECLARED
typedef union SMDPSTYPE SMDPSTYPE;
union SMDPSTYPE
{
#line 28 "./src/Convert_PP/rpcalc.y" /* yacc.c:1909  */

    char     literal  ;
    int      constant ;
    char*    operetor ;
    char*    string ; // Added by Kulwant.
    EXPR  PresPlusExpr;

#line 89 "rpcalc.tab.h" /* yacc.c:1909  */
};
# define SMDPSTYPE_IS_TRIVIAL 1
# define SMDPSTYPE_IS_DECLARED 1
#endif


extern SMDPSTYPE smdplval;

int smdpparse (void);

#endif /* !YY_SMDP_RPCALC_TAB_H_INCLUDED  */
