#include "Utility.h"

PRESPLUS readKBDF()
{
  
  PRESPLUS model;

  model = readplace (model); 
  // reads places and the variables associated with them
  model = readtransition (model);
  model = readInitialMarking (model) ;
  
  model = connectSetPlaces (model) ;

  return model;
} 
