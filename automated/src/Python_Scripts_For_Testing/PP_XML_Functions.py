import xml.etree.ElementTree as ET
from xml.dom import minidom

"""
tree = ET.parse('Test1.xml')

root = tree.getroot()

for child in root.iter("place"):
    for k in child.iter("fillattr"):
        print (k)
"""

g_id = 1412360222       # Used for places and transitions.
g_id_var = 1412366763   # Used for variables.

def get_ID ():
    """
        Based on a global variable ID, generates a string id
        and returns it.
        The returned id is a string.
    """
    global g_id
    g_id += 1
    return "ID"+str (g_id)
############# FUNCTION ENDS ##########################################


def get_ID_var ():
    """
        Based on a global variable var_ID, generates a string id
        and returns it.
        The returned id identifies a variable.
    """
    global g_id_var
    s = 'ID' + str (g_id_var)
    g_id_var += 2
    return s


def objatts (elem, xCord="NA", yCord="NA", objText="", clr="White"):
    """
        ET:   It is a global object.
        elem: The element to which the attributes are to be added.
        xCord, yCord: X and Y co-ordinates of the object.
        objText: Text to be printed inside the object.
    """
    #### position attributes.
    d = {"x": xCord, "y": yCord}    # Can not be added here only.
    ET.SubElement (elem, "posattr", d)

    #### fill attributes.           # Fixed value.
    d = {"colour":clr, "pattern":"", "filled":"false"}
    ET.SubElement (elem, "fillattr", d)

    #### line attributes.           # Fixed
    d = {"colour":"Black", "thick":"1", "type":"Solid"}
    ET.SubElement (elem, "lineattr", d)

    #### Font attributes of the text. # Fixed.
    d = {"colour":"Black", "bold":"false"}
    ET.SubElement (elem, "textattr", d)

    #### Text inside the object.
    d = {"tool":"CPN Tools", "version":"4.0.1"}
    t = ET.SubElement (elem, "text", d)
    t.text = objText                 # Input to function.
############# FUNCTION ENDS ##########################################

def place_xml (prnt, pName="Unamed", pVar="NoVar", color='White', inX=1, inY=2):
    """
        pName: Name of the place.
        pVar:  Variable associated to the place.
        inX and inY: X and Y cords of the place specified as integers.
        Adds the place element in the xml file in the element tree ET.
    """
    pID = get_ID()
    d = {"id": pID}                       # Calculated value.
    place = ET.SubElement (prnt, "place", d)

    ############# INTERNALS OF A PLACE ################################
    #### Attributes of the place.
    objatts (place, xCord=str (inX), yCord=str (inY), objText=pName, clr=color)
                                # x and y cords: arguments to this function.

    #### Attributes of the ellipse enclosing the place.
    d = {"w":"60.000000", "h":"40.000000"}  # Constant value.
    ET.SubElement (place, "ellipse", d)

    #### Position of the box containing the token.
    d = {"x":"-10.000000", "y":"0.000000"}  # Constant Value (relative value)
    ET.SubElement (place, "token", d)

    #### Markings on the place.
    d = {"x":"0.000000", "y":"0.000000", "hidden":"false"} # Constant Values.
    m = ET.SubElement (place, "marking", d)

    #### Some snap on the markings: :(
    d = {"snap_id": "0", "anchor.horizontal":"0", "anchor.vertical":"0"}
                                                 # all are constants.
    ET.SubElement (m, "snap", d)

    #### Data type associate to the place. #########################
    d = {"id": get_ID()}               # Computed Value
    type = ET.SubElement (place, "type", d)

    #### #### Internals of the type.
    objatts (type, xCord=str (inX+40), yCord=str (inY-25), objText="INT")
                                # x and y cord relative to those of the place.

    #### Initial markings associated to a place. #########################
    d = {"id": get_ID()}               # Computed Value
    imark = ET.SubElement (place, "initmarkings", d)

    #### #### Internals of the marking container.
    objatts (imark, xCord=str (inX+50), yCord=str (inY+30), objText="")
                                # X and Y cord relative to those of the place.

    return pID
    ####################### Function Ends ######################################
############# FUNCTION ENDS ##########################################


def trans_xml (prnt, tName="Unnamed", tExpr="NA + NA", tGaurd="Na + NA", priority="NORM",
                color="White", inX=1, inY=2):
    """
       tName: Name of the transition.
       tExpr: Expression associated to the expression as a string.
       tGaurd: Gaurd associated to the transition.
       inX and inY: X and Y cords of the place specified as integers.
       Adds a transition element under the expression tree ET.
    """
    tID = get_ID ()
    d = {"id": tID, "explicit":"false"}       # id needs to be calculated.
    trans = ET.SubElement (prnt, "trans", d)

    ############# INTERNALS OF A TRANSITION ############################
    objatts (trans, xCord=str(inX), yCord=str(inY), objText=tName, clr=color)
                                        # X and Y cords as specified in arguments.

    #### Box of the transition
    d = {"w": "60.000000", "h": "40.000000"}    # Constant value
    t = ET.SubElement (trans, "box", d)

    #### Binding gaurd of the transition.
    d = {"x": "1", "y": "2"}              # X and Y cords need to be calcuated.
    t = ET.SubElement (trans, "binding", d)

    #### The gaurd condition. ##########################
    d = {"id": get_ID()}                           # Computed value
    grd = ET.SubElement (trans, "cond", d)

    #### #### ATTRIBUTES OF THE GAURD CONDITION.
    objatts (grd, xCord=str (inX-40), yCord=str (inY+30), objText=tGaurd)
                                # X and Y cords relative to the cords of the trans.
                                # Also modify the expression.
    ####################################################

    #### TIME associated to a transition. ##############
    d = {"id": get_ID()}                           # Computed value
    time = ET.SubElement (trans, "time", d)

    #### #### Various Attributes associated to the time.
    objatts (time, xCord=str (inX+40), yCord=str (inY+30))
                                # x and y cords relative to the transition.
    ####################################################

    #### code associated to a transition. ##############
    d = {"id": get_ID()}                           # Computed value
    code = ET.SubElement (trans, "code", d)

    #### #### Various Attributes associated to the code.
    objatts (code, xCord=str (inX+70), yCord=str (inY-40), objText=tExpr)
                                # x and y cords relative to the transition.
                                # Also specify the code properly.
    ####################################################

    #### prority associated to a transition. ###########
    d = {"id": get_ID()}                          # Computed value
    prio = ET.SubElement (trans, "priority", d)

    #### #### Various Attributes associated to the priority.
    if priority == 'HIGH':
        objatts (prio, xCord=str (inX-40), yCord=str (inY-30), objText='P_HIGH')
    else:
        objatts (prio, xCord=str (inX-40), yCord=str (inY-30))
                # x and y cords relative to the transition.
    ####################################################

    return tID
    ############### Function Ends ##########################################
############# FUNCTION ENDS ##########################################


def arc_xml (prnt, orntnt="NA", tID="NA", pID="NA", astdVar="NA", inX=100, inY=200):
    """
        prnt: Parent element in the xml.
        orntnt: orientation of the arc, PtoT or TtoP.
        tID: ID of the transition associated to the arc.
        pID: ID of the place associated to the arc.
        astdVar: Variable associated to the arc. Same as that of the
                 Variable defined by a transition if the arc begins with a trans.
                 Variable associated to the place, otherwise, if the arc begins
                 with a place.
        inX, inY: X and Y co-ordinates of the lable on the arc.
                  inX = X and inY = Y-50; Where X and Y are the co-ordinates of
                      the object from which the arc starts.
    """
    d = {"id": get_ID(), "orientation":orntnt, "order":"1"} # Only order is constant.
    arc = ET.SubElement (prnt, "arc", d)

    #### Attributes of the arc.
    objatts (arc, xCord="0.000000", yCord="0.000000", objText="")
                                                            # All are constants.

    #### Arrow pointer.
    d = {"headsize":"1.200000", "currentcyckle":"2"} # All constants.
    ET.SubElement (arc, "arrowattr", d)

    #### Transition end.
    d = {"idref": tID}       # tID is the id of the transition of this arc.
    ET.SubElement (arc, "transend", d)

    #### Place end.
    d = {"idref": pID}       # pID is the id of the place of this arc.
    ET.SubElement (arc, "placeend", d)
    ######################################

    #### Annotations on the arc.
    d = {"id": get_ID()}       # Not constant.
    annot = ET.SubElement (arc, "annot", d)

    objatts (annot, xCord=str (inX), yCord=str (inY), objText=astdVar)
                    # X and Y cords are needed.
                    # Check the representation of the associated var. Necc.
                    # specify the variable as objText.
    ##################### Function Ends ####################################
############# FUNCTION ENDS ##########################################


def create_cpn_symbols (root, symbols):
    """
    INPUTS:
        root: Root element to which all the elements should be added.
        symbols: A list containing all the variables.
    OUTPUTS:
        Returns Nothing.
    Functionality:
        Writes the variables in the cpn format under the element root.
    """

    # For each variable in the symbols list.
    for k in symbols:
        d = {'id':get_ID_var ()}
        var = ET.SubElement (root, 'var', d)

        t = ET.SubElement (var, 'type')

        dT = ET.SubElement (t, 'id')
        dT.text = 'INT'

        vN = ET.SubElement (var, 'id')
        vN.text = k

        ly = ET.SubElement (var, 'layout')
        ly.text = 'var ' + k + ': INT;'

############# FUNCTION ENDS ##########################################


#tree = ET.ElementTree (place)
#tree.write ("h.xml")

######################################
# tree = ET.Element ("root")
# place_xml (tree, "p2")
# tr = trans_xml (tree, "t2")
# arc = arc_xml (tree, astdVar="Assocaite Variable")

# xmlstr = minidom.parseString(ET.tostring(tree)).toprettyxml(indent="   ")
# with open("j.xml", "w+") as f:
#     f.write(xmlstr)
######################################



#xmlstr = minidom.parseString(ET.tostring(tr)).toprettyxml(indent="   ")
#with open("j.xml", "a") as f:
#    f.write(xmlstr)







#ET.dump (place)
