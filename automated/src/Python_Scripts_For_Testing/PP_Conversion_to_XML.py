import sys
import PP_XML_Functions as cfun
import queue as Q
# import xml.etree.ElementTree as ET
# from xml.dom import minidom


py_dir = "./src/Python_Scripts_For_Testing/" # Python directory containing source code.




x_diff = y_diff = 100
a_diff = 50

t_out    = 'tOut'   # Output variable of transitions.
p_din    = 'pDin'   # Dummy, extra input for transitions
spcl_var = 'spcl'
dum_var  = 'dum'
d_count  = 1        # Count of arcs with dummy variables

class Place:
    def __init__ (self, pNum=-1, var='dummy', preset=[], postset=[]):
        self.pNum    = pNum
        self.var     = var
        self.preset  = preset
        self.postset = postset

        self.id_cpn  = ''
        self.x_cord  = ''
        self.y_cord  = ''
        self.visited = False

    def print_obj (self):
        print ('pNum = ', self.pNum, '; var = ', self.var)
        print ('preset  = ', self.preset)
        print ('postset = ', self.postset)
        print ('cpn-id = ', self.id_cpn, ', cpn-X = ', self.x_cord,
               ', cpn-Y = ', self.y_cord)
        print ('')
    ############# FUNCTION ENDS ##########################################
    
    def print_elem_cpn (self, tree, pl_dict, t_dict, x_cord, y_cord):
        """
            Inputs:
                tree: The xml to which pritning should be done.
                pl_dict, t_dict: The places and the transitions dictionaries.
                x_cord, y_cord: x and y cords of the element to be printed.
            Outputs:
                Returns nothing.
            Functionality:
                Prints the place to the xml obj tree.
        """
        id_cpn = '0'
        if len (self.preset) == 0:
            id_cpn = cfun.place_xml (tree, pName='P'+ str(self.pNum),
                        color = "Yellow", pVar=self.var, inX=x_cord, inY=y_cord)
        elif len (self.postset) == 0:
            id_cpn = cfun.place_xml (tree, pName='P'+ str(self.pNum),
                        color = "Aqua", pVar=self.var, inX=x_cord, inY=y_cord)
        else:
            id_cpn = cfun.place_xml (tree, pName='P'+ str(self.pNum),
                        pVar=self.var, inX=x_cord, inY=y_cord) # White is default
                                # color.
        pl_dict[self.pNum].id_cpn = id_cpn
        pl_dict[self.pNum].x_cord = x_cord
        pl_dict[self.pNum].y_cord = y_cord
        ############# FUNCTION ENDS ##########################################
        
    def print_arc_cpn (self, tree, pl_dict, t_dict, prnt):
        """
            Inputs:
                tree: The xml to which pritning should be done.
                pl_dict, t_dict: The places and the transitions dictionaries.
                x_cord, y_cord: x and y cords of the element to be printed.
            Outputs:
                Returns nothing.
            Functionality:
                Prints the arc from prnt (a Transition) to self (a Place).
        """
        global t_out, a_diff
        if prnt == -1:
            # Input place no parent: No arc to be printed.
            return
            
        # Draw an arc from a transition -- prnt -- to the place -- self--.
        cfun.arc_xml (tree, orntnt="TtoP", tID=prnt.id_cpn, pID=self.id_cpn,
                     astdVar=t_out, inX=self.x_cord, inY=self.y_cord+a_diff)
                     
    ############# FUNCTION ENDS ##########################################
    
    def type_obj (self):
        """
            Inptus:
                None:
            Output
                Returns a constant 'P', representing place.
        """
        return 'P'
    ############# FUNCTION ENDS ##########################################
############# CLASS DEF ENDS #########################################


class Trans:
    def __init__ (self, tNum, tType, expr,
                    guard, priority, preset=[], postset=[]):
        self.tNum  = tNum
        self.tType = tType
        self.expr  = expr
        self.guard = guard
        self.priority = priority

        self.preset  = preset
        self.postset = postset

        self.dum_in  = True
        self.def_vars = []

        self.guard_cpn = ''
        self.expr_cpn  = ''
        self.id_cpn    = ''
        self.x_cord  = ''
        self.y_cord  = ''

        self.visited  = False
        
    def print_obj (self):
        print ("tNum = ", self.tNum, "; tType = ", self.tType)
        print ('Expr = ', self.expr, '; guard = ', self.guard)
        print ('Priority = ', self.priority)
        print ('expr_cpn = ', self.expr_cpn, '; guard_cpn = ', self.guard_cpn)
        print ('preset  = ', self.preset)
        print ('postset = ', self.postset)
        print ('cpn-id = ', self.id_cpn, ', cpn-X = ', self.x_cord,
               ', cpn-Y = ', self.y_cord)
        print ('')
    ############# FUNCTION ENDS ##########################################
    
    def print_elem_cpn (self, tree, pl_dict, t_dict, x_cord, y_cord):
        """
            Inputs:
                tree: The xml to which pritning should be done.
                x_cord, y_cord: x and y cords of the element to be printed.
            Outputs:
                Returns nothing.
            Functionality:
                Prints the transtion to the xml obj tree.
        """
        id_cpn = '0'
        if self.priority == 'HIGH':
            id_cpn = cfun.trans_xml (tree, tName='T'+ str(self.tNum),
                    tExpr=self.expr_cpn, tGaurd=self.guard_cpn, priority=self.priority,
                    color='Fucia', inX=x_cord, inY=y_cord)
        elif self.guard != '':
            id_cpn = cfun.trans_xml (tree, tName='T'+ str(self.tNum), tExpr=self.expr_cpn,
                    tGaurd=self.guard_cpn, priority=self.priority, color='Lime',
                    inX=x_cord, inY=y_cord)
        else:
            id_cpn = cfun.trans_xml (tree, tName='T'+ str(self.tNum), tExpr=self.expr_cpn,
                    tGaurd=self.guard_cpn, priority=self.priority,
                    inX=x_cord, inY=y_cord)
        t_dict[self.tNum].id_cpn = id_cpn
        t_dict[self.tNum].x_cord = x_cord
        t_dict[self.tNum].y_cord = y_cord
    ############# FUNCTION ENDS ##########################################
    
    def print_arc_cpn (self, tree, pl_dict, t_dict, prnt):
        """
            Inputs:
                tree: The xml to which pritning should be done.
                pl_dict, t_dict: The places and the transitions dictionaries.
                x_cord, y_cord: x and y cords of the element to be printed.
            Outputs:
                Returns nothing.
            Functionality:
                Prints the arc from prnt (a Place) to self (a Transition).
        """
        global t_out, a_diff
        
        # Draw an arc from a place -- prnt -- to the transition -- self--.
        cfun.arc_xml (tree, orntnt="PtoT", tID=self.id_cpn, pID=prnt.id_cpn,
             astdVar=prnt.var, inX=self.x_cord, inY=self.y_cord+a_diff)
    ############# FUNCTION ENDS ##########################################
    
    def type_obj (self):
        """
            Inptus:
                None:
            Output
                Returns a constant 'T', representing transition.
        """
        return 'T'
    ############# FUNCTION ENDS ##########################################
############# CLASS DEF ENDS #########################################




def read_places (fr):
    """
        Inputs:
            fr: Object of the file we want to read for places.
        Outputs:
            A list and a dictionary.
            First is a list containing keys (Place numbers) of all the
                input places of the PRES+.
            Second one is a dictionary containing all the places of
                 the PRES+ and various attributes as objects of the
                 class Place.

    """
    global dum_var

    # fr is a pointer to the file which is to be read for places.
    l = fr.readline().strip()
    # print (l)
    pl_dict = {}
    in_pl   = []
    while (l != 'Places-end'):
        # print ("|" + l + "|")
        pNum = int (l)
        vl    = fr.readline().strip().split(":")
        # print (vl)
        # print ("|")
        var   = vl[1].replace ('.', '_')

        if var.strip() == 'DUMMY':
            var = dum_var

        prl  = fr.readline().strip().split(":")
        prst = []
        if len (prl[1]) != 0:
            prst = [int (k.strip()) for k in prl[1].strip().split(' ')]

        psl  = fr.readline().strip().split(":")
        post = []
        if len (psl[1]) != 0:
            #print (psl[1].split(' '))
            post = [int (k.strip()) for k in psl[1].strip().split(' ')]

        o = Place (pNum, var, prst, post)

        pl_dict[pNum] = o

        if len (prst) == 0:
            in_pl.append (pNum)

        l = fr.readline().strip()
        l = fr.readline().strip()

    return in_pl, pl_dict
############# FUNCTION ENDS ##########################################


def read_trans (fr):
    """
        Inputs:
            fr: Object of the file we want to read for transitions.
        Outputs:
            Returns a dictionary containing all the transitions read from
            the file fr.
        Functionality:
            Reads all the transitions in file represented by fr.
    """
    l = fr.readline().strip()
    while (l != "Transitions:"):
        l = fr.readline().strip()

    l = fr.readline().strip()
    tdic={}
    while (l != "Transitions-end"):
        # print (l)
        tNum = int (l)

        l = fr.readline().strip().split(":")
        # print (l)
        typ = l[1].strip()

        l    = fr.readline().strip().split(":")
        # print (l)
        expr = l[1].strip().replace('.', '_')

        l     = fr.readline().strip().split(":")
        # print (l)
        guard = l[1].strip().replace('.', '_')

        l  = fr.readline().strip().split(':')
        prio = l[1].strip()

        l    = fr.readline().strip().split(":")
        prst = []
        if len (l[1]) != 0:
            prst = [int (k.strip()) for k in (l[1].strip().split(' '))]


        l      = fr.readline().strip().split(":")
        postst = []
        if len (l[1]) != 0:
            postst = [int (k.strip()) for k in (l[1].strip().split(' '))]

        fr.readline().strip()
        l = fr.readline().strip()

        o = Trans (tNum, typ, expr, guard, prio, prst, postst)
        tdic[tNum] = o

    return tdic
############# FUNCTION ENDS ##########################################


def read_symbols (fr):
    """
        Inputs:
            fr: Object of the file we want to read for transitions.
        Outputs:
            Returns a list containing names of all the variables read from the
            file.
        Functionality:
            Reads all the symbols in file represented by fr.
    """
    global spcl_var, dum_var, t_out

    l = fr.readline().strip()
    while (l != "Symbols:"):
        l = fr.readline().strip()

    l = fr.readline().strip()
    symbols = [dum_var, spcl_var, t_out]
    while (l != "Symbols-end:"):
        symb = l.replace ('.', '_')
        symbols.append (symb);

        l = fr.readline().strip()

    return symbols
############# FUNCTION ENDS ##########################################



def read_definitions (trans, fr):
    l = fr.readline().strip()
    while (l != "Definitions:"):
        l = fr.readline().strip()

    l = fr.readline().strip()
    while (l != "Definitions-end"):
        # add the read element to trans
        l = l.split (' ')
        print (l)
        trans[l[0]].def_vars.append (l[1])

        l = fr.readline().strip()
############# FUNCTION ENDS ##########################################


def const_id_trans (pl_dict, t_dict, tI, pI, var):
    """
        Inputs:
            pl_dict, t_dict: Dictionaries containing places and
                transitions respectively.
            tI, pI: Index of the transition and the place between whom
                    an identity transition should be constructed.
            var: The variable defined by tI which will be the only
                 defined by tI after processing.
        Outputs:
            Returns Nothing.
        Functionality:
        Algo:
        Construct a new place with f_var as the associated var.
        Construct a new identity transition.
        Attach the place p in the post-set of this new transition
            and remove it from the post-set of the transition k.
        Add the new place to the preset of the new transition.
        Add the new place to the postset of the transition k.
    """
    #~ print ("\nOld Transition: before modification: ")
    #~ t_dict[tI].print_obj ()

    # Add a place to the PRES+.
    cur_max_pl = max (pl_dict.keys())
    cur_max_pl += 1
    nP = cur_max_pl

    # Add a transition to the PRES+.
    cur_max_tr = max (t_dict.keys())
    cur_max_tr += 1
    nT = cur_max_tr

    # Construct a place with, var as the assocaited variable and tI as
    # pre-transition and nT as post transition.
    oP = Place (nP, var, [tI], [nT])
    pl_dict[nP] = oP

    # Add attributes to the transition nT. nP is the pre-place of the
    # transition and pI becomes the post place.
    oT = Trans (nT, 'ID', '', '', [nP], [pI])
    t_dict[nT] = oT

    # In the pre-set of pI add replace tI with nT.
    loc = pl_dict[pI].preset.index (tI)
    pl_dict[pI].preset[loc] = nT

    # In the post-set of tI, replace pI with nP.
    loc = t_dict[tI].postset.index (pI)
    t_dict[tI].postset[loc] = nP

    # print ("\nPrinting all the for entities: ")
    # print ("Old Transition:")
    # t_dict[tI].print_obj ()

    # print ("\nNew Place:")
    # pl_dict[nP].print_obj()

    # print ("\nNew Transition:")
    # t_dict[nT].print_obj ()

    # print ("\nOld Place")
    # pl_dict[pI].print_obj ()
############# FUNCTION ENDS ##########################################



def enforce_one_def_per_trans (pl_dict, t_dict):
    """
        Inputs:
            pl_dict, t_dict: Dictionaries containing places and
                transitions respectively.
        Outputs:
            Returns Nothing.
        Functionality:
        For each transition:
            For all variables in post places construct and id transition
            except the first one.
    """
    cur_max_tr = max (t_dict.keys())
    # print ("Max Trans = ", cur_max_tr)

    cur_max_pl = max (pl_dict.keys())
    # print ("Max Place = ", cur_max_pl)

    # For each transition:
    # tk_list =
    for k in list (t_dict.keys ()):
        v = t_dict[k]
        f_var = ''      # first variable in postset places:

        # for each place in the postset of a transition:
        for p in v.postset:
            # print ("Place = ", p)
            if f_var == '':
                f_var = pl_dict[p].var
            else:
                const_id_trans (pl_dict, t_dict, k, p, f_var)
                #   print ("Construct ID trans for t = ", k, " and p = ", p)

        # print ("---------------------")
############# FUNCTION ENDS ##########################################


def const_cpn_expr_ID_2 (pl_dict, t_dict, tI):
    """
        Inputs:
            pl_dict, t_dict: Dictionaries containing places and
                transitions respectively.
            tI: Index of an ID transition for which expression is to
                be constructed in CPN format.
        Outputs:
            Returns Nothing.
        Functionality:
            Assigning the expression based on pre-places.
    """
    global t_out
    # pre_var = set () # An empty set containing the variables in the preset of tI.
    # # For each pre-place of the transition:
    # for k in t_dict[tI].preset:
    #     v = pl_dict[k].var # Variable associated to the pre-place.
    #     pre_var.add (v)

    # pre_var = set () # An empty set containing the variables in the preset of tI.
    # # For each pre-place of the transition:
    # for k in t_dict[tI].preset:
        # v = pl_dict[k].var # Variable associated to the pre-place.
        # pre_var.add (v)

    # if 'dum' in pre_var:
        # pre_var.remove ('dum')

    # if len (pre_var) == 1:
        # # print the variable in the set as input of the ID transition.
        # var_rhs = pre_var.pop()
    # else:
        # print ("ERROR: The preset of the ID trans ", tI, "has more then one variable in ")
        # print ("its preset: ", pre_var)
        # print ("Fatal Error, Exiting...")
        # sys.exit ()

    p_rhs   = t_dict[tI].preset[0]
    var_rhs = pl_dict[p_rhs].var

    #### Inputs of the expression in the transition.
    # t_dict[tI].expr_cpn  = 'input ('
    # if len (pre_var) > 0:
    #     e = pre_var.pop ()
    #     t_dict[tI].expr_cpn += e

    #     for k in pre_var:
    #         t_dict[tI].expr_cpn += ' ,' + k
    # t_dict[tI].expr_cpn += ');\n'

    #### Output of the transition:
    if len (t_dict[tI].postset) == 0:
        t_dict[tI].expr_cpn = 'input ();\n'
        t_dict[tI].expr_cpn += 'output ();\n'
        t_dict[tI].expr_cpn += 'action ();\n'
    else:
        t_dict[tI].expr_cpn  = 'input (' + var_rhs + ');\n'
        t_dict[tI].expr_cpn += 'output (' + t_out + ');\n'
        t_dict[tI].expr_cpn += 'action (' + var_rhs + ');\n '
############# FUNCTION ENDS ##########################################




def get_cpn_oprtr (oprtr):
    """
        Inputs:
            oprtr: Operator we want to convert to cpn format.
        Output:
            returns html fromat of the oprtr.
    """
    if oprtr == '==':
        return ' = '
    elif oprtr == '<':
        return ' < '
    elif oprtr == '<=':
        return ' <= '
    elif oprtr == '>':
        return ' > '
    elif oprtr == '>=':
        return ' >= '
    elif oprtr == '!=':
        return ' <> '
    else:
        print ('ERROR: Invlaid operator encountered |', oprtr, '|.\n')
        return ''
############# FUNCTION ENDS ##########################################


def cons_cpn_guard (pl_dict, t_dict, tI):
    """
        Inputs:
            pl_dict, t_dict: Dictionaries containing places and
                transitions respectively.
            tI: Index of a transition for which guard is to
                be constructed in CPN format.
        Outputs:
            Returns Nothing.
    """
    # print ('Construct CPN guard of the transition: ', tI)
    gl = t_dict[tI].guard.strip(' ').split(' ')

    t_dict[tI].guard_cpn = gl[0] + get_cpn_oprtr (gl[1]) + gl[2]
############# FUNCTION ENDS ##########################################


def const_cpn_expr_EXPR_2 (pl_dict, t_dict, tI):
    """
        Inputs:
            pl_dict, t_dict: Dictionaries containing places and
                transitions respectively.
            tI: Index of an EXPR transition for which expression is to
                be constructed in CPN format.
        Outputs:
            Returns Nothing.
        Functionality:
            A approach based on pre-places taken here against the approach
              of expression trees in the its counter part.
    """
    global t_out
    pre_var = set () # An empty set containing the variables in the preset of tI.
    # For each pre-place of the transition:
    for k in t_dict[tI].preset:
        v = pl_dict[k].var # Variable associated to the pre-place.
        pre_var.add (v)

    #### Inputs of the expression in the transition.
    t_dict[tI].expr_cpn  = 'input ('
    if len (pre_var) > 0:
        e = pre_var.pop ()
        t_dict[tI].expr_cpn += e

        for k in pre_var:
            t_dict[tI].expr_cpn += ', ' + k
    t_dict[tI].expr_cpn += ');\n'
    
    t_dict[tI].expr = t_dict[tI].expr.replace ('%', 'mod')
    #### Output of the transition:
    if len (t_dict[tI].postset) == 0:
        t_dict[tI].expr_cpn += 'output ();\n'
        t_dict[tI].expr_cpn += 'action ();\n'
    else:
        t_dict[tI].expr_cpn += 'output (' + t_out + ');\n'
        t_dict[tI].expr_cpn += 'action (' + t_dict[tI].expr + ');\n '

    # print ('Transition ', tI, ' has assocated expr:\n', t_dict[tI].expr_cpn, '\n')
############# FUNCTION ENDS ##########################################
############# FUNCTION ENDS ##########################################


def const_cpn_expr_DUMMY (pl_dict, t_dict, k):
    if len (t_dict[k].preset) == 0:
        print ("Fatal Error:")
        print ("Maximum transition with empty preset.")
        print ("The transition is:")
        t_dict[k].print_obj()
        print ("Exiting... \n\n")
        sys.exit()
    pI = t_dict[k].preset[0]
    v = pl_dict[pI].var
    t_dict[k].expr_cpn  = 'input (' + v + ');\n'
    t_dict[k].expr_cpn += 'output (' + t_out + ');\n'
    t_dict[k].expr_cpn += 'action (' + v + ');'
############# FUNCTION ENDS ##########################################



def construct_exprs_in_xml_format (pl_dict, t_dict):
    """
        Inputs:
            pl_dict, t_dict: Dictionaries containing places and
                transitions respectively.
        Outputs:
            Returns Nothing.
        Functionality:
        For each transition:
            If it an ID transition. Constructs the associated expression
              in cpn format using pre and post-places.
            If it an EXPR transitions, constructs the associated
              expression from the expression.
            If it is a guarded transition, constructs an cpn format
              guard for the transition.
    """
    # For each transition:
    for k, v in t_dict.items():

        if t_dict[k].tType == 'ID':
            const_cpn_expr_ID_2 (pl_dict, t_dict, k)
            # Construct expression for ID trans.
            if t_dict[k].guard != '' and t_dict[k].guard != 'error':
                # A guarded trans. Handle it.
                cons_cpn_guard (pl_dict, t_dict, k)

        elif t_dict[k].tType == 'EXPR':
            if t_dict[k].expr != 'error':
                # construct xml expression.
                const_cpn_expr_EXPR_2 (pl_dict, t_dict, k)
            else:
                print ('ERROR: Transition ', k, 'has invalid expression.\n')

        elif t_dict[k].tType == 'DUMMY':
            # Maximum transitions are the only DUMMY transitions.
            const_cpn_expr_DUMMY (pl_dict, t_dict, k)
        else:
            print ('ERROR: ', 'Transition ', k, 'has an invalid type.\n')
############# FUNCTION ENDS ##########################################


def print_PP_to_File (pl_dict, t_dict):
    """
        Prints the PRES+ represented by pl_dict, t_dict to a file.
    """
    fp = open ("PP_Out.txt", "w+")

    s = sys.stdout
    sys.stdout = fp

    for p, v in pl_dict.items():
        v.print_obj ()

    for k, v in t_dict.items():
        v.print_obj ()

    sys.stdout = s
    fp.close ()
############# FUNCTION ENDS ##########################################


def dfs_to_create_cpn_file (tree, pl_dict, t_dict, p_obj, c_obj, type_obj, x_cord, y_cord, level):
    """
    INPUTS:
        tree: The xml tree which we want elements.
        pl_dict, t_dict: Dictionaries containing places and transitions
            respectively.
        p_obj: Predecessor of current object which invoked the dfs for c_obj.
        c_obj: Current object, either a place or a transition.
        type_obj: Type of the current object. It belongs to {P, T}. P for place
            and T for transition.
        x_cord, y_cord: X and Y co-ordinates of the object c_obj.
            x_cord is a list where i-th element contains the x-cord of for
            c_obj reagarding c-obj at the i-th level.
        level: level of the c_obj.
    Outputs:
        returns Nothing.
    Functionality:
        Constructs the cpn element for the object c_obj and continues the
        traversal of the PRES+ in a dfs manner.
    """
    global x_diff
    global y_diff
    global a_diff
    global t_out
    global d_count

    #### Termination of recursion.
    if c_obj.visited == True:
        if p_obj == 'none':
            print ('ERROR: Object: ', c_obj.print_obj (),
                    'but has been visited but has no predecessor.\n')
        else:
            if type_obj == 'P':
                cfun.arc_xml (tree, orntnt="TtoP", tID=p_obj.id_cpn, pID=c_obj.id_cpn,
                     astdVar=t_out, inX=c_obj.x_cord, inY=c_obj.y_cord+a_diff)

                # print ('Draw back edge from T-', p_obj.tNum, ' to P-', c_obj.pNum)
            elif type_obj == 'T':
                if p_obj.var != 'dum' or t_dict[c_obj.tNum].dum_in:
                    t_dict[c_obj.tNum].dum_in = False
                    cfun.arc_xml (tree, orntnt="PtoT", tID=c_obj.id_cpn, pID=p_obj.id_cpn,
                         astdVar=p_obj.var, inX=p_obj.x_cord, inY=p_obj.y_cord-a_diff)
                else:
                    cfun.arc_xml (tree, orntnt="PtoT", tID=c_obj.id_cpn, pID=p_obj.id_cpn,
                         astdVar=p_obj.var+str(d_count), inX=p_obj.x_cord, inY=p_obj.y_cord-a_diff)
                    d_count+=1
                # print ('Draw back edge from P-', p_obj.pNum, ' to T-', c_obj.tNum)
            else:
                print ('ERROR: Object ', c_obj.print_obj(), ' has invalid type_obj = ',
                        type_obj, '\n')
        return

    c_obj.visited = True

    #### Construct place/transition for c_obj.
    if type_obj == 'P':
        if len (c_obj.preset) == 0:
            id_cpn = cfun.place_xml (tree, pName='P'+ str(c_obj.pNum), color = "Yellow",
                                  pVar=c_obj.var, inX=x_cord[level], inY=y_cord)
        elif len (c_obj.postset) == 0:
            id_cpn = cfun.place_xml (tree, pName='P'+ str(c_obj.pNum), color = "Aqua",
                                  pVar=c_obj.var, inX=x_cord[level], inY=y_cord)
        else:
            id_cpn = cfun.place_xml (tree, pName='P'+ str(c_obj.pNum),
                                  pVar=c_obj.var, inX=x_cord[level], inY=y_cord)
        pl_dict[c_obj.pNum].id_cpn = id_cpn
        pl_dict[c_obj.pNum].x_cord = x_cord[level]
        pl_dict[c_obj.pNum].y_cord = y_cord
        x_cord[level] += x_diff
        # print ('id_cpn = ', c_obj.id_cpn)
    else:
        if c_obj.priority == 'HIGH':
            id_cpn = cfun.trans_xml (tree, tName='T'+ str(c_obj.tNum), tExpr=c_obj.expr_cpn,
                                    tGaurd=c_obj.guard_cpn, priority=c_obj.priority, color='Fucia',
                                    inX=x_cord[level], inY=y_cord)
        elif c_obj.guard != '':
            id_cpn = cfun.trans_xml (tree, tName='T'+ str(c_obj.tNum), tExpr=c_obj.expr_cpn,
                                    tGaurd=c_obj.guard_cpn, priority=c_obj.priority, color='Lime',
                                    inX=x_cord[level], inY=y_cord)
        else:
            id_cpn = cfun.trans_xml (tree, tName='T'+ str(c_obj.tNum), tExpr=c_obj.expr_cpn,
                                    tGaurd=c_obj.guard_cpn, priority=c_obj.priority,
                                    inX=x_cord[level], inY=y_cord)
        t_dict[c_obj.tNum].id_cpn = id_cpn
        t_dict[c_obj.tNum].x_cord = x_cord[level]
        t_dict[c_obj.tNum].y_cord = y_cord
        x_cord[level] += x_diff

    #### Draw an edge from p_obj to c_obj.
    if p_obj != 'none':
        if type_obj == 'P':
            # print ('Draw forward edge from T-', p_obj.tNum, ' to P-', c_obj.pNum)
            cfun.arc_xml (tree, orntnt="TtoP", tID=p_obj.id_cpn, pID=c_obj.id_cpn,
                     astdVar=t_out, inX=c_obj.x_cord, inY=c_obj.y_cord+a_diff)
        else:
            if p_obj.var != 'dum' or t_dict[c_obj.tNum].dum_in:
                t_dict[c_obj.tNum].dum_in = False
                cfun.arc_xml (tree, orntnt="PtoT", tID=c_obj.id_cpn, pID=p_obj.id_cpn,
                         astdVar=p_obj.var, inX=p_obj.x_cord, inY=p_obj.y_cord-a_diff)
            else:
                cfun.arc_xml (tree, orntnt="PtoT", tID=c_obj.id_cpn, pID=p_obj.id_cpn,
                         astdVar=p_obj.var+str(d_count), inX=p_obj.x_cord, inY=p_obj.y_cord-a_diff)
                d_count+=1
            # print ('Draw forward edge from P-', p_obj.pNum, ' to T-', c_obj.tNum)

    #### Based on type of the c_obj, invoke the dfs for all the successors of c_obj.
    if type_obj == 'P':
        # dist = -50 * level # x cord of successor with respect to c_obj.
        # xval = x_cord[level]
        for succ in c_obj.postset:
            dfs_to_create_cpn_file (tree, pl_dict, t_dict, c_obj, t_dict[succ], 'T',
                    x_cord=x_cord, y_cord=y_cord-y_diff, level=level+1)
                                    # successor at a lower position.
            # x_cord[level] += x_diff
        #    dist += 100
    elif type_obj == 'T':
        # dist = -50 * level # x cord of successor with respect to c_obj.
        # xval = x_cord[level]
        for succ in c_obj.postset:
            dfs_to_create_cpn_file (tree, pl_dict, t_dict, c_obj, pl_dict[succ], 'P',
                    x_cord=x_cord, y_cord=y_cord-y_diff, level=level+1)
            # x_cord[level] += x_diff
        #    dist += 100
    else:
        print ('ERROR: Object ', c_obj.print_obj(), ' has invalid type_obj = ',
                type_obj, '\n')
############# FUNCTION ENDS ##########################################





def bfs_to_create_cpn_file (tree, pl_dict, in_places, t_dict):
    """
    INPUTS:
        tree: The xml tree which we want elements.
        pl_dict, t_dict: Dictionaries containing places and transitions
            respectively.
        in_places: Input places of the PRES+.
    Outputs:
        returns Nothing.
    Functionality:
        Constructs the cpn element for the object c_obj and continues the
        traversal of the PRES+ in a bfs manner.
    """
    global x_diff, y_diff, a_diff, t_out, d_count # Global varaibles.
    x_cord = x_cord_org = y_cord = 100 # X, Y cords, inital.
    level  = 0         # The level of input places is the level zero.
    qu     = Q.Queue() # Insializes an empty queue.

    ###### The BFS algo begins: ##########
    ## Equeue all the input places:
    for p in in_places:
        qu.put ([-1, pl_dict[p]]) # Enqueue the place p.
    qu.put ("X") # Equeue a delimiter for the end of a level.
    
    while (not qu.empty()):
        e = qu.get() # Remove an element from the queue.
        if e == 'X':
            level  += 1
            x_cord =  x_cord_org
            y_cord -= 100
            qu.task_done()
            if not qu.empty():
                qu.put ('X')
            # else don't insert this element.
            continue
        # else process further -- e is a true element.
        
        #### Processing the element e.
        if e[1].visited == False:
            # Print the xml for the element.
            e[1].print_elem_cpn (tree, pl_dict, t_dict, x_cord, y_cord)
            x_cord += 100
            
        # Print an arc from e[0] to e[1].
        e[1].print_arc_cpn (tree, pl_dict, t_dict, e[0])
        
        # add successors of e[1] to the queue only if e[1] is not visited.
        if e[1].visited == False:
            if e[1].type_obj() == 'P':
                for k in e[1].postset:
                    qu.put ([e[1], t_dict[k]])
                pl_dict[e[1].pNum].visited = True
            else:
                for k in e[1].postset:
                    qu.put ([e[1], pl_dict[k]])
                t_dict[e[1].tNum].visited = True
        qu.task_done() # Mark end of procesing of the element.

############# FUNCTION ENDS ##########################################


def prepare_PP (pp_file="./PP_for_Python_Script.txt"):
    """
        Inputs:
            pp_file: Input file conatining the PRES+ produced by the C code.
            The format is specified in a separate file called PP_C_to_PY_format.txt.
        Outputs:
          1. A list conatining integers (ids/keys) of the input places.
          2. A dictionary conatining all the places of the PRES+. Key is an
             integer, id of the place, value is an object of the class Place
             conatining all the required attributes of a place.
          3. A dictionary conatining all the transitions of the PRES+.
             Key value pairs similar to that of Places dictionary.
             Key is an integer, and id for the transition, value is an object
             of the transition class.
          4. A list containing all the symbols read from the file.
        Functionality:
            Reads the PRES+ from the file specified in pp_file.
            Modifies the PRES+ such that it is suitable to use it in the CPN
            Tools.

            To look into what are these modifications read this fucntion. Each
            modfication on the PRES+ is well commented.
    """
    fr = open (pp_file, "r")

    in_places, pl_dict = read_places (fr)  # Read all places.
                                 # A dictionary containing all the places -
                                 # is returned.
                                 # in_places is a list containing input places.

    t_dict  = read_trans (fr)     # Read all the transitions.
                                 # A dictionary of transitions is returned.


    symbols = read_symbols (fr)

    # read_definitions (t_dict, fr) # Read the definitions and add to respective
                                 # transitions.

    fr.close()

    # for k, v in pl_dict.items():
    #    v.print_obj()

    #~ print ("-------------------------------------------------------------")
    #~ for k, v in t_dict.items():
        #~ v.print_obj()
    #~ print ("-------------------------------------------------------------")

    # enforce_one_def_per_trans (pl_dict, t_dict) # Enforce that a transition
        # defines atmost one variable.

    #~ Constructing the expression string for all the transitions.
    #~ For ID transitions:
    #~ An ID transition can have exactly one pre-place, excluding the one's used
      #~ in the guard. Because ID transition is a unary assignment operation.
    #~ Since the places for the variables used in the guard condition are
      #~ constructed after the places for the variables used in expressions,
      #~ the only pre-place to be used in expression of ID transition can be
      #~ the 0-th pre-place of an ID transition.
    #~ An ID transition can have multiple post-places but all of them must
      #~ have the same associated variable. Ensured by the function
      #~ enforce_one_def_per_trans().
    #~ For EXPR transitions:
      #~ Use the available expression string.

    construct_exprs_in_xml_format (pl_dict, t_dict) # This function modifies
        # both the expression and the guard expressions to the CPN format.


    # print_PP_to_File (pl_dict, t_dict) # Prints the PRES+ to a file.

    return in_places, pl_dict, t_dict, symbols
############# FUNCTION ENDS ##########################################


def print_cords_to_file (pl_dict, t_dict):
    """
        Prints the PRES+ represented by pl_dict, t_dict to a file.
    """
    fp = open ("PP_Cords.txt", "w+")

    s = sys.stdout
    sys.stdout = fp

    for p, v in pl_dict.items():
        print (v.x_cord, ' ', v.y_cord)
        # v.print_obj ()

    for k, v in t_dict.items():
        print (v.x_cord, ' ', v.y_cord)

    sys.stdout = s
    fp.close ()
############# FUNCTION ENDS ##########################################



def const_output_file (variables, pta, out_net="Output_Net.cpn"):
    """
        Inputs: 
            variables: XML tree containing all the variables.
            pta: XML tree containing all the places, transitions and arcs.
            out_net: Name of the output file.
        Outputs: Retuns Nothing.
        Functionality: 
            Writes these files and xml trees: t a.xml, variables, c.xml, pta and 
                e.xml to the file out_net in that order.
            Writes these files to the file specified in out_net. 
    """
    global py_dir
    
    fout = open (out_net, "w+")
    
    # a.xml read and write at once.
    a_file = py_dir + 'a.xml'
    fin = open (a_file, 'r')
    fout.write (fin.read())
    fout.write ("\n")
    fin.close()
    
    # Writing the Symbols 
    for v in variables.iter ('var'):
        s = cfun.minidom.parseString(cfun.ET.tostring(v)).toprettyxml(indent="   ")
        i = s.find ('\n')
        s = s[i+1:]
        fout.write (s)
    
    # Writing c.xml
    c_file = py_dir + 'c.xml'
    fin = open (c_file, 'r')
    fout.write (fin.read())
    fout.write ("\n")
    fin.close()
    
    # Writing the places.
    for v in pta.iter ('place'):
        s = cfun.minidom.parseString(cfun.ET.tostring(v)).toprettyxml(indent="   ")
        i = s.find ('\n')
        s = s[i+1:]
        fout.write (s)
    
    for z in range(0, 10):
        fout.write ("\n")
    
    # Writing the transtions
    for v in pta.iter ('trans'):
        s = cfun.minidom.parseString(cfun.ET.tostring(v)).toprettyxml(indent="   ")
        i = s.find ('\n')
        s = s[i+1:]
        fout.write (s)

    for x in range(0, 10):
        fout.write ("\n")
    
    # Writing the arcs
    for v in pta.iter ('arc'):
        s = cfun.minidom.parseString(cfun.ET.tostring(v)).toprettyxml(indent="   ")
        i = s.find ('\n')
        s = s[i+1:]
        fout.write (s)

    for x in range(0, 10):
        fout.write ("\n")

    # Writing e.xml
    e_file = py_dir + 'e.xml'
    fin = open (e_file, 'r')
    fout.write (fin.read())
    fout.write ("\n")
    fin.close()

    fout.close()
############# FUNCTION ENDS ##########################################


if len (sys.argv) != 1 and len (sys.argv) != 3:
    print ("Improper arguments:")
    print ("<Python_script> <Input File> <Output File>")
    print ("Exiting\n")
    sys.exit (1)

if len (sys.argv) == 3:
    pp_file = sys.argv[1]
    out_net = sys.argv[2]
else:
    pp_file = "./PP_for_Python_Script.txt"
    out_net = "Output_Net.cpn"

in_places, pl_dict, t_dict, symbols = prepare_PP (pp_file) # Obtain the PRES+ for which a CPN
    # represenation can be produced.

#### Root of the xml tree.
tree = cfun.ET.Element ("root")

# difference of 100 points between two objects in the cpn.

# x_cords = [100 for k in range (0,100)]
# for i in in_places:
#     dfs_to_create_cpn_file (tree, pl_dict, t_dict, p_obj='none', c_obj=pl_dict[i],
#                 type_obj='P', x_cord = x_cords, y_cord = 100, level=0) # Initially
#         # c_obj is an input place. Later on it can be either a transtion or a
#         # place. Because in python c_obj is not bound to any data type.

bfs_to_create_cpn_file (tree, pl_dict, in_places, t_dict)

print_PP_to_File (pl_dict, t_dict) # Prints the PRES+ to a file.

print_cords_to_file (pl_dict, t_dict)

# xmlstr = cfun.minidom.parseString(cfun.ET.tostring(tree)).toprettyxml(indent="   ")
# with open("PandT.xml", "w+") as f:
#     f.write(xmlstr)
# 

print ('\nColor coding:')
print ('Lime: A Guarded transition')
print ('Pink: A higher priority transition')
print ('Yellow: An input place.')
print ('Blue: An output place.\n\n\n')

bfs_to_create_cpn_file (tree, pl_dict, in_places, t_dict)

#### For the variables used in the program.
variables = cfun.ET.Element ('variables') # Create a new root element.

# Add all new dummy symbols to the cpn symbols list.
for k in range (0, d_count+1):
    symbols.append (dum_var + str (k))

cfun.create_cpn_symbols (variables, symbols)  # Write the variables to the new root.

# xmlstr = cfun.minidom.parseString(cfun.ET.tostring(variables)).toprettyxml(indent="   ")
# with open("Symbols.xml", "w+") as f:
#     f.write(xmlstr)
# 


const_output_file (variables, tree, out_net) # Constructs one final output file.















def const_cpn_expr_ID (pl_dict, t_dict, tI):
    """
        Inputs:
            pl_dict, t_dict: Dictionaries containing places and
                transitions respectively.
            tI: Index of an ID transition for which expression is to
                be constructed in CPN format.
        Outputs:
            Returns Nothing.
    """
    global spcl_var

    # print ('Construct expression for the ID transition: ', tI)
    p_rhs   = t_dict[tI].preset[0]
    var_rhs = pl_dict[p_rhs].var


    var_lhs = ''
    if len (t_dict[tI].postset) > 0:
        p_lhs   = t_dict[tI].postset[0]
        var_lhs = pl_dict[p_lhs].var


    # For each pre-place of the transition tI.
    dv = False
    for k in t_dict[tI].preset:
        if pl_dict[k].var == 'dum' and var_rhs != 'dum':
            dv = True;


    if dv and var_rhs != 'dum':
        # print ('var_rhs = ', var_rhs, ' and dv is True; tI = ', tI)
        t_dict[tI].expr_cpn  = 'input ('  + var_rhs + ", dum);\n"
    else:
        t_dict[tI].expr_cpn  = 'input ('  + var_rhs + ");\n"


    #### OUTPUT
    if dv:
        # It is possible that input contains dum as a variable.
        # Hecne if output equals dum replace it with spcl var.
        if var_lhs == 'dum':
            t_dict[tI].expr_cpn += 'output (' + spcl_var + ");\n"
        elif var_lhs == var_rhs : # If var_lhs is same as var_rhs print spcl var.
            t_dict[tI].expr_cpn += 'output (' + spcl_var + ");\n"
        else:
            t_dict[tI].expr_cpn += 'output (' + var_lhs + ");\n"
    else:
        # Input contains exactly one variable.
        if var_rhs == var_lhs:
            t_dict[tI].expr_cpn += 'output (' + spcl_var + ");\n"
        else:
            t_dict[tI].expr_cpn += 'output (' + var_lhs + ");\n"




    if len (t_dict[tI].postset) == 0:
        t_dict[tI].expr_cpn += 'action ();\n'
    else:
        t_dict[tI].expr_cpn += 'action (' + var_rhs + ");\n"

    # print ('Expression ID = ', t_dict[tI].expr_cpn)
############# FUNCTION ENDS ##########################################






def const_cpn_expr_EXPR (pl_dict, t_dict, tI):
    """
        Inputs:
            pl_dict, t_dict: Dictionaries containing places and
                transitions respectively.
            tI: Index of an EXPR transition for which expression is to
                be constructed in CPN format.
        Outputs:
            Returns Nothing.
    """
    # print ('Construct CPN-expression for the EXPR transition: ', tI)
    global spcl_var

    el = t_dict[tI].expr.strip().split(' ')
    # print (el)
    if len (el) == 3:
        # Binary expression.
        o1 = el[0].strip(' ')
        o2 = el[2].strip(' ')

        dvar = ''
        if len (t_dict[tI].postset) > 0:
            pI = t_dict[tI].postset[0]
            dvar = pl_dict[pI].var

        if o1.isdigit() and o2.isdigit():
            t_dict[tI].expr_cpn  = 'input ();\n '
        elif o1.isdigit() == True and o2.isdigit() == False:
            t_dict[tI].expr_cpn  = 'input (' + o2 + ');\n '
        elif o1.isdigit() == False and o2.isdigit() == True:
            t_dict[tI].expr_cpn  = 'input (' + o1 + ');\n '
        else:
            t_dict[tI].expr_cpn  = 'input ('  + o1 + ', ' + o2 + ');\n '



        # if o1.strip() == dvar.strip() or o2.strip() == dvar.strip():
        #     t_dict[tI].expr_cpn += 'output (' + spcl_var + ');\n '
        # else:
        #     t_dict[tI].expr_cpn += 'output (' + dvar + ');\n '



        if len (t_dict[tI].postset) == 0:
            t_dict[tI].expr_cpn += 'output ();\n '
            t_dict[tI].expr_cpn += 'action ();\n'
        else:
            t_dict[tI].expr_cpn += 'output (' + t_out + ');\n '
            t_dict[tI].expr_cpn += 'action (' + t_dict[tI].expr + ');\n '

        # print ('Expression 3 = ', t_dict[tI].expr_cpn)

    elif len (el) == 2:
        # Unary expression.
        o1 = el[1].strip(' ')

        dvar = ''
        if len (t_dict[tI].postset) > 0:
            pI   = t_dict[tI].postset[0]
            dvar = pl_dict[pI].var

        t_dict[tI].expr_cpn  = 'input ('  + o1 + ');\n '

        # if o1.strip() == dvar.strip():
        #     t_dict[tI].expr_cpn += 'output (' + spcl_var + ');\n '
        # else:
        #     t_dict[tI].expr_cpn += 'output (' + dvar + ');\n '

        if len (t_dict[tI].postset) == 0:
            t_dict[tI].expr_cpn += 'output ();\n '
            t_dict[tI].expr_cpn += 'action ();\n'
        else:
            t_dict[tI].expr_cpn += 'output (' + t_out + ');\n '
            t_dict[tI].expr_cpn += 'action (' + t_dict[tI].expr + ');\n'

        # print ('Expression = ', t_dict[tI].expr_cpn)

    elif len (el) == 1:
        # Constant function.

        dvar = ''
        if len (t_dict[tI].postset) > 0:
            pI   = t_dict[tI].postset[0]
            dvar = pl_dict[pI].var

        t_dict[tI].expr_cpn  = 'input ();\n '
        # t_dict[tI].expr_cpn += 'output (' + dvar + ');\n '

        if dvar == '':
            # no postplace for this transition.
            t_dict[tI].expr_cpn += 'output ();\n '
            t_dict[tI].expr_cpn += 'action ();\n '
        else:
            # post place exists.
            t_dict[tI].expr_cpn += 'output (' + t_out + ');\n '
            t_dict[tI].expr_cpn += 'action (' + t_dict[tI].expr.strip() + ');\n '

        # print ('Expression 1 = ', t_dict[tI].expr_cpn)
    else:
        print ('ERROR: Transition = ', tI, ' has invalid expression = ', t_dict[tI].expr, '\n')
############# FUNCTION ENDS ##########################################
