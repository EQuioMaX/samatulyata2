#include "Enums.h"

/*********************************************************************
 *          DATA STRUCTURES
 *********************************************************************/

/********* BIT VECTOR **********************************************/
/**** MACROS FOR BIT VECTOR ************/
#define MAX_STMNTS                                                             \
  (MAX_BB_STMNTS * MAX_BBs) /** Max number of                                  \
                             * statements that can occur in the                \
                             * whole program. One bit for each                 \
                             * statement. */

#define BV_ELEM_BITS                                                           \
  (sizeof(int) * 8) /** Number of bits in one                                  \
                     * element of the bit vector. */

#define BV_ELEMS                                                               \
  ((MAX_STMNTS > MAX_VARIABLES ? MAX_STMNTS : MAX_VARIABLES) / BV_ELEM_BITS +  \
   1) /** Num of                                                               \
       * elements (ints) in the Bit Vector. */

/** Bit Vector: Data type for the Bit Vectors.
 * */
typedef unsigned int Bit_Vector[BV_ELEMS];

/********* STATEMENT ***********************************************/
/** Expr_Node: Data St. for a Node of Expression Tree.
 * */
typedef struct Expr {
  Expr_Node_Type eType; /** The type of this expression node.
                         *  a binary, var, literal, or unary.
                         * *eType*     *Relevant Fields*
                         *  BINARY      oprtr, left, right.
                         *  UNARY       left.
                         *  VAR         negation, var_Index.
                         *  LITERAL     value.
                         *  */

  union {
    Oprtr oprtr; /** Operator stored in this node (+, - etc).
                  * It is IR_RELEVANT if expression is a
                  * UNARY.
                  */

    int val_var; /** Value of the literal, if expression is a literal.
                  * Index of the variable in symbol table if expression is
                  *   a variable.
                  * Don't care/ cannot be determined if the the expression is
                  *   neither a var nor a literal. */

    /*struct
    {
        /*Bool negation;  /** if operand is like -b, it is true,
         * if operand is like b, it is false.
         * it is relevant only if the operand is
         * a variable.
         *
         * Seems out of order -- NO..
         * cfg produced by gcc contains statements like:
         *  a = b + -c;
         *  How to store the minus operator on c. Hence it
         *    becomes necc. to store it in this fashion.
         *  Same is true for the ~ operator.
         * /

        int var_Index; /** This field is relevant only if the
                        *  eType is variable.
                        *  It stores the index of the variable
                        *  in the Symbol Table.
                        * /
    };

    int value; /** Value of the literal, if expression is a literal.
            * int, because only integer variables are allowed now.
            * if, expression Type is not literal, this field is
            * ir-relevant.
            * */
  };

  struct Expr *left, *right; /** expressions are stored
                              * as their parse trees, which in turn are binary
                              * trees. For the unary operators like ~ (neg) and
                              * - (unary) "right" is NULL.
                              * */
} Expr;

/** Statement: Data St. for a Statement.
 *    This data structure is used to extract and store the various
 *  information from a statement.
 * */
typedef struct {
  Stmnt_Type sType; /** For type of this statement,
                     *  i.e. either an assignment statemnet, scanf, printf or
                     *  return statement.
                     * */

  int var; /** Index of defined var in case of scanf or assignment
            * statement.
            *   Index of the output variable in case of printf statement.
            * */

  Expr *expr; /** 1. Expression on RHS in assignment statement.
               *  2. NULL for scanf, printf or return statement.
               * */
} Statement;

/********* BASIC BLOCKS *********************************************/
/** Basic_Block Data St. to represent a basic block
 * MAX_BBs -- maximum no. of basic blocks -- #defined
 * Note that the expression stored in bb is for the statement:
 * i.e. a = b + c;
 * while in pres+ it is for RHS only i.e. b + c only.
 * */
typedef struct {
  BBType bb_Type; /// type in {normal, condition, last}

  /*** Parents ****************************************************/
  int parent_Count;    /// Number of parents this basic block has.
  int parent[MAX_BBs]; /** Array containing indices of all parents of
                        * this basic block. */

  /*** Statements *************************************************/
  int total_Stmnts;               /// total no. of statemts. in this BB
  Statement stmnt[MAX_BB_STMNTS]; /** stmnt [i] contains all the
                                   * information about the i-th statement */

  /*** Condition **************************************************/
  Expr *cond; /** Expression tree, if bb_Type is CONDITIONAL.
               *  NULL, otherwise.*/

  /*** Flags (used in construction of PRES+) **********************/
  Trv_State trv_state; /** Traversal state: {VISITED, VISITING,
                        * NOT_VISITED} */
  Bool visited;        /// Used in dfs to mark if visited or not.
  Bool ppConstructed;  /** Ture, if PRES+ for this bb has been
                        * constructed.
                        * False, otherwise. */

  /*** Reaching definitions. **************************************/
  Bit_Vector Din, Dout; /** Reaching Definitions at beginning and
                         * end of this BB respectively.
                         * The reaching definition analysis is no more used but
                         *    DON'T DELETE Din AND Dout as the TESTED code for
                         *    reaching definitions is compiled with this module.
                         * Only thing is that the code is never invoked.
                         * */

  /*** Live Variables. ********************************************/
  Bit_Vector live_in, live_out; /** Live variables at beginning and
                                 * end of this BB respectively. */

  /*** Successors *************************************************/
  int trueNext;  /** index of its successor if type = "normal"
                  * index of true-successor, if type "condition"
                  * -1 if BBType is last. */
  int falseNext; /// -1 if "normal" -- false successor, if "condition"

} Basic_Block;

/** BB_Array: Data St. for Basic Blocks Array.
 * */
typedef struct {
  int total_BBs;           /// Total number of basic blocks;
  Basic_Block BB[MAX_BBs]; /// BB[i], contents of i-th Basic Block.

  int def_Var
      [MAX_STMNTS]; /** definitions array:
                     * def_Var[i] contains the index (in Symbol Table) of the
                     * variable defined in statement i.
                     * -1, if statement i doesn't define any variable.
                     *
                     * USE: If we want to know index of the variable defined by
                     *   a statement i (which is a reaching definition).
                     * This can be removed, but just to reduce time complexity,
                     *    Linear serach thousands of times for medium size nets,
                     *    it is used.
                     *
                     * Note: Segmentation fault is expected if you change the
                     *    size of this array.
                     * You may change the values specified in the macros but not
                     *    the expression itself.
                     * */
} BB_Array;

/** Symbol: Used to save the name of a variable.
 */
typedef struct {
  char *vName; /// Name of the varialbe, a string.

  Bit_Vector def; /** A bit vector listing all the definitions of the
                   * associated variable.
                   * i.e. def [i] = 1, if statement "i" defines the associated
                   * var. 0, otherwise. Used in live variable analysis.
                   * */

} Symbol;

/** Symbol_Table: Data Structure for all variables used in the program.
 * */
typedef struct {
  int total_Symbols;            // Total number of variables used.
  Symbol symbol[MAX_VARIABLES]; // symbol[i] <--> i-th variable info.
} Symbol_Table;

/********* PRES+ ****************************************************/
/** Place: Data St. for a place.
 * */
typedef struct {
  Place_Type p_Type; /** Type of this place.
                      * Place Type            Relevant fields
                      * REAL, SCANF, PRINTF   varI, value.
                      * For other types nither of var_Index or value.
                      * preset and postset are always relevant. */

  int var; /// Location of variable in symbol table.

  /* This field is not used.
   * int value;  /** Value of the variable in this place.
   *  Since only integer variables are allowed right now,
   *  this field is an int.
   *  Later it will be a union if multiple type of
   *  variables are to be allowed.
   */

  int c_Preset;  /// no of elements in the pre transition set.
  int c_Postset; /// no of elements in the post transition set.

  int preset[MAX_PRESET];   /// Stores indices of pre transitions.
  int postset[MAX_POSTSET]; /// indices of the post transitions.

} Place;

/** Data St. (Transition) for a transition.
 * */
typedef struct {
  Trnstn_Type t_Type; /** Type of the transition, see Trnstn_Type.
                       * Transition type         Relevant Fields.
                       * STMNT                    expr and guard.
                       * For other types these two are irrelevant.
                       * preset and postset are always relevant. */

  Expr *expr;  /// Expression represented by this transition.
  Expr *guard; /// Gaurd of for this transition.
               /// Both initialized to NULL.

  int itng; /** Index of Transition with Negation of the Guard asso-
             * ciated to a transition.
             * Since for a transition t having g as guard there exists a
             *   transition t' with ~g as its guard, itng of t is marked
             *   as t'. Similar is true for t' also.
             * Initialized to -1.
             * -1 if for unguarded transitions.
             * */

  int c_Preset;  /// no of elements in the pre place set.
  int c_Postset; /// no of elements in the post place set.

  int preset[MAX_PRESET];   /// Stores indices of the pre places.
  int postset[MAX_POSTSET]; /// indices of the post places.

  Priority prio; /** An enum with two types of priorities-- pr_HIGH and
                  * pr_NORM (Normal).
                  * Rule: A transition with normal (less) priority is never
                  * executed if a higher priority transition is enabled.
                  * */

  /** The initializations of various fields is done only when a
   *     transition is added to the PRES+.
   * */

} Transition;

/** PP_One_BB: Data St. for the PRES+ of one basic block.
 * */
typedef struct {
  int c_in;           /// Number of input places.
  int in[MAX_PLACES]; /// Indices of input places of this BB.

  int c_out;                /// Number of output places.
  int out[MAX_TRANSITIONS]; /** Indices of of output transitions of
                             * PRES+ of this BB. */

  int c_PL;           /// Number of places belonging to this BB.
  int pl[MAX_PLACES]; /** Indices of places belonging to PRES+ of
                       * this BB.*/

  int c_TR;                /// Number of transitions belonging to this BB.
  int tr[MAX_TRANSITIONS]; /** Indices of transitions belonging to
                            * PRES+ of this BB. */

  int ldt[MAX_VARIABLES]; /** Latest defintions, use of
                           * this data st. is local to construction of PRES+ for
                           * one basic block. */

  int max_trans; /** Index of the maximum transition of  a subnet.
                  * relevant only for subnet having a loop-header as
                  * a successor.*/

  /**** For reaching definition analysis. */
  int c_in_Reach;           /// Num of Reaching Transitions at start of a BB.
  int in_Reach[MAX_STMNTS]; /// Array of Reaching Trans at start of BB.

  int c_out_Reach;           /// Num of Reaching Transitions at end of a BB.
  int out_Reach[MAX_STMNTS]; /// Array for Reahing Trans at end of BB.
  /*******************************************************************/

  /* A data st. called loop_sych_pl has been removed as it is not used.
   * It is available in edit_18... */
} PP_One_BB;

/** Merged_Tran: mereged transitions list.
 * This data structure is no more used.
 * * /
typedef struct
{
    int count; /// Num of entries in the below array.
    int actual_trans[MAX_MAP_TRANS]; /// Transitions to be mapped.
    int map_trans;               /// Transition above trans are mapped to.
} Merged_Tran; */

/** Storing for each variable list of the transitions that define it.
 *
 * This data structure is no more used. * /
typedef struct
{
    int vt_map[MAX_TRANSITIONS];
    int tr_count; /** Number of transitions a var is
                                    * defined in. * /
} Var_Definitions; */

/** Pres_Plus: Data St. for a pres+ net.
 * */
typedef struct {
  int c_in;           /// Number of input places.
  int in[MAX_PLACES]; /// Indices of input places.

  int c_out;           /// Number of output places.
  int out[MAX_PLACES]; /// Indices of of output place of PRES+.

  int pl_count;            /// Number of places in the places array.
  Place place[MAX_PLACES]; /// An array of places.

  int tr_count; /// Number of transitions in the transitions array.
  Transition transition[MAX_TRANSITIONS]; /// Array of transitions.

  /* int c_Merged_Trans; /// Number of map entries.
  // Merged_Tran merged_Tran[MAX_MAP_ENTRIES]; /// Mapped transitions.
  These are no more used. */

  /// For each variable, list of transitions it is defined in.
  /* Var_Definitions vds[MAX_VARIABLES]; /* The code corresponding to
   * This data structure has been commented as it is not used. */
} Pres_Plus;

/********* QUEUE ***************************************************/
/** QElement: Data St. for one element of queue.
 * /
typedef struct
{
    int index;  /// Index of the basic block.
    int parent; /// Index of the parent basic block.
} QElement;


/** Queue: Data St. for Queue.
 *   Used in Breadth First traversal of the basic blocks array, a
 * control flow graph.
 * /
typedef struct
{
    int front;
    int rear;
    QElement queue[QUEUE_SIZE]; /// QUEUE_SIZE, max queue size, #defined.
} Queue; */

/********* REACHING DEFINITIONS *************************************/
/** Gen_Kill: A data structure to accumulate the gen and kill sets.
 * They may belong to a statement or a basic block.
 * Since Reaching definition analysis is not done it is not relevant.
 *   but it is kept just because a tested module for REACHING definition
 *   analysis is maintained.
 * */
typedef struct {
  Bit_Vector gen;  /// The gen set.
  Bit_Vector kill; /// The kill set.
} Gen_Kill;

/********* LIVE VAR ANALYSIS *************************************/
/** Def_Use: A data structure to accumulate the def and use sets.
 * They may belong to a statement or a basic block.
 * */
typedef struct {
  Bit_Vector def; /// The def set.
  Bit_Vector use; /// The use set.
} Def_Use;
