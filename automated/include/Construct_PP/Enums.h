

#include <ctype.h>
#include <fcntl.h>
#include <limits.h>
#include <regex.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#define DEBUG 1 // has to be defined always. Other wise litle
                // modification is needed in the code.

#ifdef CHECK_EQV
#define OUT_EQUIVALENCE "./PP_Outputs/Equivalence_Check_Result.txt"
#define CONVERT_PP                                                             \
  1 /* Undef if you don't want to convert Kulwant's                            \
     * PRES+ to Soumyadip's PRES+. */
#define KUL_PRESPLUS_1 "./PP_Outputs/Pres_Plus_1.txt"
#define KUL_PRESPLUS_2 "./PP_Outputs/Pres_Plus_2.txt"

#endif

#ifdef CONVERT_PP
/* #define CONSTRUCT_OUTPUT_PLACES 1  /* For constructing output places
 * for transitions whose output is never used. */
#define OUT_CONVERSTION "./PP_Outputs/PP_Converstion_Result.txt"
#define LOG_PP_CONVERSTION "./PP_Outputs/PP_Converstion_Log.txt"

/****** Dummy variables. *****/
#define DUM_VAR_INDEX                                                          \
  0 /** Index of the place associated to a dummy                               \
     * variable. By default it is kept as 0.                                   \
     * This index is enforced by adding a dummy variable to the                \
     * symbol table before adding any other variable.*/
#define DUM_VAR_STR                                                            \
  "dum_var" /* The name of the dummy variable. Hope                            \
             * that this var is not used in the Symbol table.                  \
             * No error handling is done if this var is used in the            \
             * program, and program may crash. */
#endif
/************* MACROS FOR FLOW GRAPH (BASIC BLOCKS ARRAY) ********/
#define MAX_BBs 30       /// Max basic blocks.
#define MAX_BB_STMNTS 25 /// Max num of statements in a basic block.
#define MAX_VARIABLES 40 /// Max number of variables used.
#define MAX_VAR_LEN 32   /// Max lenght of a variable.
#define MAX_EXPR_SIZE                                                          \
  (MAX_VAR_LEN * 3) /** Max num of characters                                  \
                     * in an expression. Since in .cfg file we have 3 address  \
                     * expressions only. Thus 3 variables atmost in an         \
                     * expression.*/

/************* MACROS FOR PRES+ **********************************/
#define MAX_PLACES 500      /// Max places in a pres+ net.
#define MAX_TRANSITIONS 300 /// Max transitions in a pres+ net.
#define MAX_PRESET 40       /// Max number of elements in a preset.
#define MAX_POSTSET 40      /// Max number of elements in a postset.
#define MAX_MAP_TRANS 10    /// Max transitions that can be merged.
#define MAX_MAP_ENTRIES 20  /// Max entries in Merged transitions in map.

/************* MACROS FOR OUTPUT FILES ***************************/
#ifdef DEBUG
#define CONSTRUCT_OUTPUT_PLACES                                                \
  1 /* For constructing output places                                          \
     * for transitions whose output is never used. */

#define O_REACHING_DEFs "PP_Outputs/Debug/Reaching_Definitions.txt"
#define O_GEN_KILL_ALL_BB "PP_Outputs/Debug/Gen_Kill_Each_BB.txt"
#define O_GEN_KILL_ALL_STMNT "PP_Outputs/Debug/Gen_Kill_Each_Statement.txt"
#define DEBUG_PARSER "PP_Outputs/Debug/Parsing_Output.txt"
#define PRINT_PP_ST "PP_Outputs/Debug/Pres_Plus_Statement_wise.txt"
#define PRINT_PP_BB "PP_Outputs/Debug/Pres_Plus_BB_wise.txt"
#define PLOT_PP "PP_Outputs/Debug/Pres_Plus_Image.gv" // print call comented.
#define PLOT_PP_WITH_BB_INDX "PP_Outputs/Debug/Pres_Plus_Image_With_BB_Index.gv"
#define PRINT_JOINING_DEBUG "PP_Outputs/Debug/Join_Operation_Debug.txt"
#define OUT_MERGE_DECISION "PP_Outputs/Debug/Merge_Decision.txt"
#define OP_ATTACH_SEQ "PP_Outputs/Debug/Subnet_attach_order.txt"
#define OUT_PY "PP_Outputs/Debug/PP_for_Python_Script.txt"
#define OUT_DEF_USE_SETs "PP_Outputs/Debug/Def_Use_Sets_Live_Var_Analysis.txt"
#define VAR_TRANS_DEF "PP_Outputs/Debug/Var_Transition_Definitions.txt"
#define PRINT_PP_SUBNET "PP_Outputs/Debug/Pres_Plus_With_Subnet_Boundaries.txt"
#define PLOT_CFG "PP_Outputs/Debug/GUI_CFG.gv"
#define STM_VAR_DEF_ARRAY "PP_Outputs/Debug/Statement_vs_Var_defined.txt"
#define PRINT_BB_ARRAY_LOG                                                     \
  "PP_Outputs/Debug/Detailed_Basic_Block_Array.txt" /*                         \
                                                     * This pointes to a very  \
                                                     * detailed basic blocks   \
                                                     * array. */
#endif

#define PRINT_SYMBOL_TABLE "PP_Outputs/Symbol_Table.txt"
#define PRINT_BB_ARRAY "PP_Outputs/BB_Array.txt"
#define PRINT_PRES_PLUS "PP_Outputs/Pres_Plus.txt"

/********* ALL ENUMs FIRST *************************/
/** BBType: enum for the type of a basic block.
 *  Definitions:
 *  NORMAL bb:       A basic block which has only 1 successor.
 *  CONDITIONAL bb:  A basic block which has 2 successors.
 *  LAST bb:         A basic block which has 0 successors.
 *  *A basic block is supposed to have either 0, 1 or 2 successors.
 */
typedef enum {
  bb_ERROR = 0, /// To catch use without initialization.
  bb_NORM,      /// for NORMAL basic blocks.
  bb_COND,      /// for CONDITIONAL basic blocks.
  bb_LAST       /// for LAST basic block.
} BBType;

/** Bool: enum for boolean data type.
 */
typedef enum {
  False = 0, /// The unitialized value is false.
  True       /// Initialized value is 1.
} Bool;

/** Trv_State: State of a basic block (BB) / subnet in a depth first
 *    traversal, performed to attach subnets corresponding to BBs.
 * */
typedef enum {
  NOT_VISITED, // A BB which has not been visited.
  VISITED,     // A BB whose successors are being traversed.
  VISITING     // A BB which has been visited (hence all the successors also).
} Trv_State;

/** Stmnt_Type: enum for various kinds of statements that can
 *  be encountered.
 *    Note: if, else, etc. are not stored as statements.
 *          The condition specified in the if class is stored as an
 *        attribute of the basic block it belongs to.
 * */
typedef enum {
  s_ERROR = 0, /// To catch use without initialization.

  s_ASGNMT_BI, /// Binary operation on RHS. eg. a = b + 10, b + c etc.
  s_ASGNMT_ID, /// Identity assignment. eg. a = b;
  s_ASGNMT_UN, /// Unary operation on RHS. eg. a = -b; a = 10.

  s_SCANF,  /// The scanf statement.
  s_PRINTF, /// The printf statement.

  s_RETURN, /// The return statment.
  s_OTHER   /// Statement not among above categories.
} Stmnt_Type;

/** Operand_Type: enum for type of operands.
 *    This Data St. is not for data type of operand, but about operand
 *  being a literal or variable.
 * */
typedef enum {
  o_ERROR = 0, /// To catch use without initialization.
  o_LITERAL,   /** literal, e.g. is 10, or "abc". Only integer
                * leterals are considered in code right now. */
  o_VARIABLE,  /// opreand is a variable.
  o_ABSENT     /** an operand is absent.
                *  In a = b; there is no second operand, so type
                *  of second operand will be o_Absent. */
} Operand_Type;

/** Oprtr: enum for various operators.
 * */
typedef enum {
  op_ERROR = 0,    /// To catch use without initialization.
  op_PLUS,         /// binary "+"
  op_BINARY_MINUS, /// binary "-"
  op_MULTIPLY,     /// "*"
  op_DIVIDE,       /// "/"
  op_MODULUS,      /// %

  op_GRT_THN_OR_EQUAL,  /// >=
  op_GRT_THN,           /// >
  op_LESS_THN_OR_EQUAL, /// <=
  op_LESS_THN,          /// <
  op_SINGLE_EQUAL,      /// =
  op_DOUBLE_EQUAL,      /// ==
  op_NOT_EQUAL,         /// !=

  op_BITWISE_OR,  /// |
  op_BITWISE_AND, /// &
  op_NOT,         /// ~
  op_LEFT_SHIFT,  /// <<
  op_RIGHT_SHIFT, /// >>

  op_LOGICAL_OR,  /// ||
  op_LOGICAL_AND, /// &&

  op_IR_RELEVANT,     /// For Unary operand expressions.
  op_UNKNOWN_OPERATOR /// Unknown is used when we are yet to figure
                      /// out the operator type.

} Oprtr;

/** Expr_Node_Type: enum for Type of a node of the expression tree.
 * */
typedef enum {
  e_ERROR = 0, /// To catch use without initialization.
  e_LITERAL,   /// "10", is an eg. of a string literal.
  e_BINARY,    /// a+b, is an eg. of a binary expression.
  e_UNARY,     /// a = -1 is an eg. of a unary expression.
  e_VAR        /// exression node contains a variable.
} Expr_Node_Type;

/********* ENUMS FOR PRES+ *************************************/
/** Place_Type: enum for Place type.
 *    A place can be representing a variable, synchronization, or can
 *  be there for uniformity of model (Dummy).
 * */
typedef enum {
  p_ERROR = 0,
  p_VAR,    /// The place represents a variable.
  p_SCANF,  /// place's value is read via scanf.
  p_PRINTF, /// place for variable printed using printf.

  p_DUMMY, /** Literal assignment statement, no actual input place,
            * so dummy place is added for uniformity. */

} Place_Type;

/** Trnstn_Type: enum for Transition Type.
 *    Whether the transition represents an expression,
 *  synchronization or is there for uniformity of model (Dummy).
 * */
typedef enum {
  t_ERROR = 0,
  t_ID,   /// Identity transition for assignments like a = b;
  t_EXPR, /// Transition contains an expression, e.g. b + c.

  t_DUMMY, /// Transition contains no expression, for uniformity.

} Trnstn_Type;

/** Join_Case: enum for possible cases that can occur while attaching
 * a place to its defining transition. */
typedef enum {
  c_ERROR = 0,
  c_JOIN,   /// Join to a defining transition.
  c_NEW_TR, /// Input-Input case.
  c_NEW_VAR /// The place represents a var used for first time.
} Join_Case;

/** Priority: Different types of priorities a transition can
 * have in the PRES+.
 * */
typedef enum { pr_ERROR = 0, pr_NORM, pr_HIGH } Priority;
