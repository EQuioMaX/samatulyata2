#ifndef UTILITY_H
#define UTILITY_H

#include "types.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <z3.h>

extern int drivemenu();
extern PRESPLUS readKBDF();
extern int printpresplus(PRESPLUS);
extern int writeFile(PRESPLUS);
extern PRESPLUS readFile();
extern PRESPLUS readplace(PRESPLUS);
extern PRESPLUS readtransition(PRESPLUS);
extern PRESPLUS readInitialMarking(PRESPLUS);
extern PRESPLUS readedge(PRESPLUS);
extern void CheckEquivalence(PRESPLUS m1, PRESPLUS m2);
void findEquivalent(PRESPLUS N0, PRESPLUS N1);
int extendedPaths(PATHSET P1, PATH temppath, PRESPLUS *model, int num);
int findnextPath(PATHSET P1, PRESPLUS *model, int p);
void unionEta(CPQ *eta, int s0, int s1);
int checkCondition(PATH P1, PATH tempPath);
int r_alpha_equal(r_alpha *r1, r_alpha *r2);
void CorrespondingPlaces(PRESPLUS M0, PRESPLUS M1, PATHSET P1, PATHSET P2);
void find_R_and_r_alpha(PRESPLUS model, PATH *path);
void Associate_R_and_r_alpha(PRESPLUS model, PATHSET paths);
void enQP(CPP_Q *P, int s0, int s1);

extern PRESPLUS connectSetPlaces(PRESPLUS);
extern PRESPLUS initialiseToZero(PRESPLUS);

// extern FSMD symbolicSimulate (PRESPLUS, FSMD, char*);
// extern interFSM createFSM (PRESPLUS, interFSM);
// extern int** retTrans(int*, int);
// extern FSMD createStaticEntities(PRESPLUS, char*, char* );
// extern int symbolicSimulate(PRESPLUS *, FSMD *);
// extern int writeFsmd (FSMD * );

extern int searchForVariable(PRESPLUS, char *);
extern int searchForPlace(PRESPLUS, char *);
extern int searchForTransition(PRESPLUS, char *);
extern void DecompilingEXPRNode(EXPR);
extern void DecompilingExpr(EXPR);
extern void Initial(PRESPLUS *model);
extern EXPR ParseExpression(char *);

extern CUTPOINT findcutpoints(PRESPLUS model, CUTPOINT cp);
extern PATHSET ConstructAllPaths(PRESPLUS *model);
extern void print_paths(PATHSET pathset);
extern void visualize(PRESPLUS model);

extern Z3_context mk_context();

extern Z3_ast EXPRNodetoZ3(Z3_context *ctxp, EXPR expr);

#endif
